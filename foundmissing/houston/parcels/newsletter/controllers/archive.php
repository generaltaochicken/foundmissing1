<?php
namespace Newsletter\Controllers;

class Archive extends \Manage\Controllers\Manage{

	private $newsletters;

	public $newsletters_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Deleted Newsletters";

		$this->newsletters = \Newsletter\Models\Newsletter::all(array("conditions" => "deleted = 1"));
		$this->archive_button = array("url" => \Newsletter\Models\Newsletter::link_all(), "title" => "Back");
		foreach($this->newsletters as $newsletter){
			$this->has_data = true;
			$this->newsletters_data[] = array(
				"id" => $newsletter->id,
				"entry_datetime" => $newsletter->entry_datetime,
				"body" => $newsletter->body,
				"link_restore" => $newsletter->link_restore(),
			);
		}

	}

	public function controller(){
		$this->set_view("Newsletter\Views\All");
	}

}

