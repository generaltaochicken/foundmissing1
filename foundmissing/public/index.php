<?php
/**
 * ---------------------------------------------------------------
 * Houston v1.1
 * ---------------------------------------------------------------
 * @author Tyler Savery | The Young Astronauts
 * @see http://www.houstonmvc.com
 * @license Open Source
 * ---------------------------------------------------------------
 */

/*if($_SERVER['SERVER_NAME'] == 'foundmissing.ca' || $_SERVER['SERVER_NAME'] == 'www.foundmissing.ca'){
	$splash = file_get_contents('splash.html');
	die($splash);
}*/
	session_start();
//$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];

/*if (isset($_GET['continue']) || strpos($url,'articles') == true || strpos($url,'reviews') == true || 
		strpos($url,'interviews') == true || strpos($url,'gallery') == true || strpos($url,'account') == true ||
		strpos($url,'manage') == true) {*/
/*if (isset($_POST['submit']) || strpos($url,'articles') == true || strpos($url,'reviews') == true || 
		strpos($url,'interviews') == true || strpos($url,'gallery') == true || strpos($url,'account') == true ||
		strpos($url,'manage') == true) {
	$_SESSION["continue"] = true;
}*/

//if ($_SESSION["continue"] == true) {
	ini_set('display_errors', 0);
	//ini_set('display_errors', 1);
	//ini_set('display_startup_errors', 1);
	//error_reporting(E_ALL);
	error_reporting(E_ALL & ~E_STRICT & ~E_NOTICE);

	define('DS', DIRECTORY_SEPARATOR);
	define('DOCUMENT_ROOT', dirname(dirname(__FILE__)).DS);

	require_once(DOCUMENT_ROOT.'houston'.DS.'parcels'.DS.'core'.DS.'construct.php');
	Core\Loader::init();
	Core\Construct::init();
	Core\Router::resolve();
//} else {
	//$splash = file_get_contents('splash.php');
	//die($splash);
//}


?>
