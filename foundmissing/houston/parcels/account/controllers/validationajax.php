<?php
namespace Account\Controllers;

class ValidationAjax extends \Frontend\Controllers\Frontend {
	public function __construct($uri, $data){
        parent::__construct($uri, $data);

        $this->data = $data;
		
		if (isset($_POST['type'])) {
			switch ($_POST['type']) {
				case "check_email":
					$this->check_if_email_exists();
					break;
				case "check_username":
					$this->check_username();
					break;
				default:
					echo 'none';
					break;
			}
		}
	}
	
	public function controller(){
		
    }
	
	protected function check_if_email_exists() {
		$user = \User\Models\User::first(array('conditions' => 'email = "' . $_POST['email'] . '"'));
		
		if (count($user) > 0) {
			echo 1;
		} else {
			echo 0;
		}
	}
	
	protected function check_username() {
		$user = \User\Models\User::first(array('conditions' => 'username = "' . $_POST['username'] . '"'));
		
		if (count($user) > 0) {
			echo 1;
		} else {
			echo 0;
		}
	}

}

?>