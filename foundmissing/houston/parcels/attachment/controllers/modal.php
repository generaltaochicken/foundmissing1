<?php
namespace Attachment\Controllers;

class Modal extends \Core\Controllers\Base_Controller{

	public $attachments = array();
	public $item_id = 0;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		//$attachments = \Attachment\Models\Attachment::all();
		if (isset($_GET['item_id'])) {
			$this->item_id = $_GET['item_id'];
			if ($_GET['type'] == 'footwear') {
				$footwear_attachments = \Footwear_attachment\Models\Footwear_attachment::select_by_sql('SELECT attachment_id FROM footwear_attachments WHERE footwear_id = ' . $this->item_id);
				
				foreach ($footwear_attachments as $fw) {
					$this->attachments[] = array(
						'preview' => '/uploads/images/' . \Attachment\Models\Attachment::select_by_sql('SELECT image_filename FROM attachments WHERE attachment_id = ' . $fw);
						'title'	=> $a->title,
						'id'	=> $a->id
					);
				}
			} else if ($_GET['type'] == 'album') {
			
			}
		}
		
		/*foreach($attachments as $a){
			$this->attachments[] = array(
				'preview' => $a->preview(),
				'title'	=> $a->title,
				'id'	=> $a->id
			);
		}*/

	}

	public function controller(){
		$this->set_view('Attachment\Views\Modal');
	}

}
