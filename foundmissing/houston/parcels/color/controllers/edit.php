<?php
namespace Color\Controllers;

class Edit extends \Manage\Controllers\Manage{

	private $color;

	public $page_title;
	public $link_back;
	public $form;
	public $existing;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		if(isset($_POST["submit"])){
			$this->submit_form();
		}

		if(isset($data["color_id"])){
			$this->color = \Color\Models\Color::find($data["color_id"]);
			$this->page_title = "Editing Color";
		} else {
			$this->color = new \Color\Models\Color;
			$this->page_title = "New Color";
		}

		$this->link_back = $this->color->link_all();

		$form = new \Form\Models\Form("edit", "POST", "", "well");

		$item = new \Form\Models\FormItemHidden(array("id" => "id", "title" => "Id", "value" => $this->color->id));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "title", "title" => "Title", "value" => $this->color->title));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "hex", "title" => "Hex", "class" => "colorpicker", "value" => $this->color->hex));
		$form->add_item($item);

		$item = new \Form\Models\FormItemSubmit(array("value" => "Save", "id" => "submit", "class" => "btn btn-primary", "cancel_url" => $this->color->link_all()));
		$form->add_item($item);

		$this->form = $form->render();

	}

	public function controller(){
		$this->set_view("Color\Views\Edit");
	}

	private function submit_form(){
		if($_POST["id"] > 0){
			$color = \Color\Models\Color::find($_POST["id"]);
			$flash_msg = "Color has been updated successfully.";
		} else {
			$color = new \Color\Models\Color;
			$flash_msg = "Color has been created successfully.";
			$color->ordering = \Color\Models\Color::get_next_ordering_number();
		}

		$color->title = $_POST["title"];
		$color->hex = $_POST["hex"];

		$color->save();
		$this->add_flash(array("message" => $flash_msg, "type" => "success", "Heading" => "Great!"));
		redirect_to($color->link_all());
	}

}
