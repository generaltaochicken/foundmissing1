<?php
namespace Comment_karma\Controllers;

class Restore extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$comment_karma = \Comment_karma\Models\Comment_karma::find($data["comment_karma_id"]);

		if($comment_karma){
			$comment_karma->deleted = 0;
			$comment_karma->save();
			$this->add_flash(array("message" => "Comment_karma has been restored!"));

			redirect_to($comment_karma->link_all());
		}
	}
}
