<?php
namespace Interview\Controllers;

class Archive extends \Manage\Controllers\Manage{

	private $interviews;

	public $interviews_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Deleted Interviews";

		$this->interviews = \Interview\Models\Interview::all(array("conditions" => "deleted = 1"));
		$this->archive_button = array("url" => \Interview\Models\Interview::link_all(), "title" => "Back");
		foreach($this->interviews as $interview){
			$this->has_data = true;
			$this->interviews_data[] = array(
				"id" => $interview->id,
				"entry_datetime" => $interview->entry_datetime,
				"author_name" => $interview->author_name,
				"title" => $interview->title,
				"slug" => $interview->slug,
				"excerpt" => $interview->excerpt,
				"body" => $interview->body,
				"main_image" => $interview->main_image,
				"link_restore" => $interview->link_restore(),
			);
		}

	}

	public function controller(){
		$this->set_view("Interview\Views\All");
	}

}

