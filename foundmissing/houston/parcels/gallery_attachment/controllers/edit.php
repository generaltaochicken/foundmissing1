<?php
namespace Gallery_attachment\Controllers;

class Edit extends \Manage\Controllers\Manage{

	private $gallery_attachment;

	public $page_title;
	public $link_back;
	public $form;
	public $existing;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		if(isset($_POST["submit"])){
			$this->submit_form();
		}

		if(isset($data["gallery_attachment_id"])){
			$this->gallery_attachment = \Gallery_attachment\Models\Gallery_attachment::find($data["gallery_attachment_id"]);
			$this->page_title = "Editing Gallery_attachment";
		} else {
			$this->gallery_attachment = new \Gallery_attachment\Models\Gallery_attachment;
			$this->page_title = "New Gallery_attachment";
		}

		$this->link_back = $this->gallery_attachment->link_all();

		$form = new \Form\Models\Form("edit", "POST", "", "well");

		$item = new \Form\Models\FormItemHidden(array("id" => "id", "title" => "Id", "value" => $this->gallery_attachment->id));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "gallery_id", "title" => "Gallery Id", "value" => $this->gallery_attachment->gallery_id));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "attachment_id", "title" => "Attachment Id", "value" => $this->gallery_attachment->attachment_id));
		$form->add_item($item);

		$item = new \Form\Models\FormItemSubmit(array("value" => "Save", "id" => "submit", "class" => "btn btn-primary", "cancel_url" => $this->gallery_attachment->link_all()));
		$form->add_item($item);

		$this->form = $form->render();

	}

	public function controller(){
		$this->set_view("Gallery_attachment\Views\Edit");
	}

	private function submit_form(){
		if($_POST["id"] > 0){
			$gallery_attachment = \Gallery_attachment\Models\Gallery_attachment::find($_POST["id"]);
			$flash_msg = "Gallery_attachment has been updated successfully.";
		} else {
			$gallery_attachment = new \Gallery_attachment\Models\Gallery_attachment;
			$flash_msg = "Gallery_attachment has been created successfully.";
			$gallery_attachment->entry_datetime = time();
			$gallery_attachment->ordering = \Gallery_attachment\Models\Gallery_attachment::get_next_ordering_number();
		}

		$gallery_attachment->gallery_id = $_POST["gallery_id"];
		$gallery_attachment->attachment_id = $_POST["attachment_id"];

		$gallery_attachment->save();
		$this->add_flash(array("message" => $flash_msg, "type" => "success", "Heading" => "Great!"));
		redirect_to($gallery_attachment->link_all());
	}

}
