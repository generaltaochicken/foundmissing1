<?php

namespace Account\Controllers;

class View extends \Account\Controllers\Account {

	public $user_data = array();
    public $favorites = array();
	public $profile_page = false;
	public $has_activities = false;
	
	public $view_page = true;

	public function __construct($uri, $data){
        parent::__construct($uri, $data);
		$this->title = "My Account | FM";
		
		if ($this->_user->karma > 0) {
			$karma = "+" . $this->_user->karma;
		} else if ($this->_user->karma < 0) {
			$karma = "-" . $this->_user->karma;
		} else {
			$karma = $this->_user->karma;
		}
		
        $this->user_data = array(
			'username' => $this->_user->username,
			'fullname' => $this->_user->fullname(),
			'city' => $this->_user->city . ', ',
			'city2' => $this->_user->city,
			'country' => $this->_user->country . '  /  ',
			'country2' => $this->_user->country,
			'age' => $this->_user->age,
			'title' => '/  ' . $this->_user->title . '  ',
			'title2' => $this->_user->title,
			'bio' => $this->_user->bio,
			'email' => $this->_user->email,
			'website' => $this->_user->website,
			'facebook' => $this->_user->facebook,
			'twitter' => $this->_user->twitter,
			'image' => '/uploads/images/' . $this->_user->image,
			'karma' => $karma,
			'date_joined' => time_to_friendly_date($this->_user->entry_datetime, 'Y.m.d'),
			'last_login' => \Frontend\Controllers\Frontend::time_elapsed_string($this->_user->update_datetime),
			'comments' => count(\Comment\Models\Comment::all(array('conditions' => 'deleted = 0 AND user_id = "' . $this->_user->id . '"'))),
			'reviews' => count(\Review\Models\Review::all(array('conditions' => 'deleted = 0 AND user_id = "' . $this->_user->id . '"')))
        );
		
		if ($this->_user->city == null) {
			$this->user_data['city'] = '';
		}
		
		if ($this->_user->country == null) {
			$this->user_data['country'] = '';
			if ($this->_user->city != null) {
				$this->user_data['city'] = $this->_user->city;
			}
		}
		
		if ($this->_user->age == null) {
			$this->user_data['age'] = '';
			if ($this->_user->country != null) {
				$this->user_data['country'] = $this->_user->country;
			}
		}
		
		if ($this->_user->title == null) {
			$this->user_data['title'] = '';
		}
		
		if ($this->_user->image == null) {
			$this->user_data['image'] = '/images/default_avatar.png';
		}
		
		if (isset($_POST["submit_picture"])) {
			$this->submit_profile_picture();
		}
		
		$this->get_user_activities();
    }

    public function controller(){
		$this->bookmarks = $this->get_bookmarks();
		
    	$this->set_view('Account\Views\View');
	}
	
	public function submit_profile_picture() {
		$allowedExts = array("gif", "jpeg", "jpg", "png");
		$temp = explode(".", $_FILES["file"]["name"]);
		$extension = end($temp);
		
		if ( (($_FILES["file"]["type"] == "image/gif") 		|| ($_FILES["file"]["type"] == "image/jpeg") 
			|| ($_FILES["file"]["type"] == "image/jpg") 		|| ($_FILES["file"]["type"] == "image/pjpeg")
			|| ($_FILES["file"]["type"] == "image/x-png") 	|| ($_FILES["file"]["type"] == "image/png"))
			&& in_array($extension, $allowedExts)) {
			
			if ($_FILES["file"]["error"] > 0) {
			
			} else {
				if (file_exists("uploads/images/" . $_FILES["file"]["name"])) {

					while($name < 14){
						$xx = rand(49,122) ;
						if( ($xx >= 49 && $xx <= 57) || ($xx >= 97 && $xx <= 122) ){
							$valword .= chr($xx) . "" ;
							$name++ ;
						}
					}

					$filename = $valword . '.' . $extension;
					
					move_uploaded_file($_FILES["file"]["tmp_name"], "uploads/images/" . $filename) ;
					$this->_user->image = $filename;
					$this->_user->save();
						
				} else {
					while($name < 14){
						$xx = rand(49,122) ;
						if( ($xx >= 49 && $xx <= 57) || ($xx >= 97 && $xx <= 122) ){
							$valword .= chr($xx) . "" ;
							$name++ ;
						}
					}

					$filename = $valword . '.' . $extension;
					
					move_uploaded_file($_FILES["file"]["tmp_name"], "uploads/images/" . $filename);
					$this->_user->image = $filename;
					$this->_user->save();
				}
			}
		} else {
		
		}
    	redirect_to('/account');
	}
	
	public function get_user_activities() {
		if (!empty($this->profile_data)) {
			$user_id = $this->profile_data['user_id'];
			$this->profile_page = true;
		} else {
			$user_id = $this->_user->id;
			$this->profile_page = false;
		}
		
		$activities = \Activity\Models\Activity::all(array('conditions' => 'deleted = 0 AND user_id = "' . $user_id  . '" OR parent_id = "' . $user_id  . '"', 'order' => 'entry_datetime DESC', 'limit' => 5));
		
		if (count(\Activity\Models\Activity::all(array('conditions' => 'deleted = 0 AND user_id = "' . $user_id  . '" OR parent_id = "' . $user_id  . '"'))) > 5) {
			$this->has_activities = true;
		}
		
		foreach ($activities as $act) {
			$date = \Comment\Models\Comment::first(array('select' => 'entry_datetime', 'conditions' => 'id = ' . $act->child_id));
			$json = json_decode($date->to_json(array('only' => 'entry_datetime')));
			$converted_date = \Frontend\Controllers\Frontend::time_elapsed_string($json->entry_datetime);
			
			switch ($act->activity_type) {
				case "article_comment":
					if ($act->parent_id != null && $act->parent_id == $user_id) {
						$replied_to_you = true;
					} else {
						$replied_to_you = false;
					}
					
					if ($act->parent_id == $act->user_id) {
						if ($act->user_id == $this->_user->id) {
							$this->identifier = 'your';
						} else {
							$this->identifier = $this->profile_data['fullname'] . '\'s';
							$user_name = \User\Models\User::find($act->user_id)->fullname();
						}
						
						if (empty($this->profile_data)) {
							$user_name = 'you';
						} else {
							$user_name = '';
						}
					} else if (!empty($this->profile_data)) {
						$this->identifier = $this->profile_data['fullname'] . '\'s';
						$user_name = \User\Models\User::find($act->user_id)->fullname();
					} else {
						$user_name = \User\Models\User::find($act->user_id)->fullname();
						$this->identifier  = 'your';
					}
			
					$user_activity[] = array(
						"article_comment" => $act->activity_type,
						"comment" => \Comment\Models\Comment::first(array('select' => 'comment_body', 'conditions' => 'id = ' . $act->child_id)),
						"user_fullname" => \User\Models\User::find($act->parent_id)->fullname(),
						"replied_to_user" => \User\Models\User::find($act->parent_id)->get_image_reply(),
						"user_username" => \User\Models\User::find($act->parent_id)->get_uusername(),
						"user_that_replied" => $user_name,
						"user_that_replied_username" => \User\Models\User::find($act->user_id)->get_uusername(),
						"user_that_replied_image" => \User\Models\User::find($act->user_id)->get_image_reply(),
						'article_prefix' => '/articles/',
						"article_slug" => \Article\Models\Article::first(array('select' => 'slug', 'conditions' => 'id = ' . $act->source_id)),
						"user_replied_to_you" => $replied_to_you,
						"object_url" => '#comment_' . $act->child_id,
						"date_time" => $converted_date
					);
					break;
				case "article_post":
					$article_image = \Article\Models\Article::first(array('select' => 'bg_image_header', 'conditions' => 'id = ' . $act->source_id));
					
					$user_activity[] = array(
						"article_post" => $act->activity_type,
						"comment" => \Comment\Models\Comment::first(array('select' => 'comment_body', 'conditions' => 'id = ' . $act->child_id)),
						'article_prefix' => '/articles/',
						"article_info" => \Article\Models\Article::first(array('select' => 'slug, title', 'conditions' => 'id = ' . $act->source_id)),
						"article_image" => ARTICLE_IMAGES . $article_image->bg_image_header,
						"date_time" => $converted_date
					);
					
					break;
				case "review_comment":
					if ($act->parent_id != null && $act->parent_id == $user_id) {
						$replied_to_you = true;
					} else {
						$replied_to_you = false;
					}
					
					if ($act->parent_id == $act->user_id) {
						if ($act->user_id == $this->_user->id) {
							$this->identifier = 'your';
						} else {
							$this->identifier = $this->profile_data['fullname'] . '\'s';
							$user_name = \User\Models\User::find($act->user_id)->fullname();
						}
						
						if (empty($this->profile_data)) {
							$user_name = 'you';
						} else {
							$user_name = '';
						}
					} else if (!empty($this->profile_data)) {
						$this->identifier = $this->profile_data['fullname'] . '\'s';
						$user_name = \User\Models\User::find($act->user_id)->fullname();
					} else {
						$user_name = \User\Models\User::find($act->user_id)->fullname();
						$this->identifier  = 'your';
					}
			
					$user_activity[] = array(
						"article_comment" => $act->activity_type,
						"comment" => \Comment\Models\Comment::first(array('select' => 'comment_body', 'conditions' => 'id = ' . $act->child_id)),
						"user_fullname" => \User\Models\User::find($act->parent_id)->fullname(),
						"replied_to_user" => \User\Models\User::find($act->parent_id)->get_image_reply(),
						"user_username" => \User\Models\User::find($act->parent_id)->get_uusername(),
						"user_that_replied" => $user_name,
						"user_that_replied_username" => \User\Models\User::find($act->user_id)->get_uusername(),
						"user_that_replied_image" => \User\Models\User::find($act->user_id)->get_image_reply(),
						'article_prefix' => '/reviews/',
						"article_slug" => \Review\Models\Review::first(array('select' => 'slug', 'conditions' => 'id = ' . $act->source_id)),
						"user_replied_to_you" => $replied_to_you,
						"object_url" => '#comment_' . $act->child_id,
						"date_time" => $converted_date
					);
					break;
				case "review_post":
					
					$user_activity[] = array(
						"article_post" => $act->activity_type,
						"comment" => \Comment\Models\Comment::first(array('select' => 'comment_body', 'conditions' => 'id = ' . $act->child_id)),
						'article_prefix' => '/reviews/',
						"article_info" => \Review\Models\Review::first(array('select' => 'slug, title', 'conditions' => 'id = ' . $act->source_id)),
						"article_image" => ARTICLE_IMAGES . $review->cover_image,
						"date_time" => $converted_date
					);
					break;
				case "interview_comment":
					if ($act->parent_id != null && $act->parent_id == $user_id) {
						$replied_to_you = true;
					} else {
						$replied_to_you = false;
					}
					
					if ($act->parent_id == $act->user_id) {
						if ($act->user_id == $this->_user->id) {
							$this->identifier = 'your';
						} else {
							$this->identifier = $this->profile_data['fullname'] . '\'s';
							$user_name = \User\Models\User::find($act->user_id)->fullname();
						}
						
						if (empty($this->profile_data)) {
							$user_name = 'you';
						} else {
							$user_name = '';
						}
					} else if (!empty($this->profile_data)) {
						$this->identifier = $this->profile_data['fullname'] . '\'s';
						$user_name = \User\Models\User::find($act->user_id)->fullname();
					} else {
						$user_name = \User\Models\User::find($act->user_id)->fullname();
						$this->identifier  = 'your';
					}
			
					$user_activity[] = array(
						"article_comment" => $act->activity_type,
						"comment" => \Comment\Models\Comment::first(array('select' => 'comment_body', 'conditions' => 'id = ' . $act->child_id)),
						"user_fullname" => \User\Models\User::find($act->parent_id)->fullname(),
						"replied_to_user" => \User\Models\User::find($act->parent_id)->get_image_reply(),
						"user_username" => \User\Models\User::find($act->parent_id)->get_uusername(),
						"user_that_replied" => $user_name,
						"user_that_replied_username" => \User\Models\User::find($act->user_id)->get_uusername(),
						"user_that_replied_image" => \User\Models\User::find($act->user_id)->get_image_reply(),
						'article_prefix' => '/interviews/',
						"article_slug" => \Interview\Models\Interview::first(array('select' => 'slug', 'conditions' => 'id = ' . $act->source_id)),
						"user_replied_to_you" => $replied_to_you,
						"object_url" => '#comment_' . $act->child_id,
						"date_time" => $converted_date
					);
					break;
				case "interview_post":
					$article_image = \Interview\Models\Interview::first(array('select' => 'main_image', 'conditions' => 'id = ' . $act->source_id));
					
					$user_activity[] = array(
						"article_post" => $act->activity_type,
						"comment" => \Comment\Models\Comment::first(array('select' => 'comment_body', 'conditions' => 'id = ' . $act->child_id)),
						'article_prefix' => '/interviews/',
						"article_info" => \Interview\Models\Interview::first(array('select' => 'slug, title', 'conditions' => 'id = ' . $act->source_id)),
						"article_image" => ARTICLE_IMAGES . $article_image->main_image,
						"date_time" => $converted_date
					);
					break;
				default:
					break;
			}
			
		}
		
		$this->user_activity = $user_activity;
	}
	
	protected function get_bookmarks() {
		$favs = \Favorites\Models\Favorites::all(array('conditions' => 'user_id = "' . $this->_user->id . '"', 'order' => 'entry_datetime DESC'));
		$users_favs = array();
		
		foreach ($favs as $fav) {
			$shoe = \Footwear\Models\Footwear::first(array('conditions' => 'id = "' . $fav->item_id . '" AND deleted = 0'));
			$shoe_attachment = \Footwear_attachment\Models\Footwear_attachment::first(array('conditions' => 'deleted = 0 AND footwear_id = ' . $shoe->id));
			$attachment = \Attachment\Models\Attachment::first(array('conditions' => 'deleted = 0 AND id = ' . $shoe_attachment->attachment_id));
			
			$users_favs[] = array(
				'id' => $shoe->id,
				'brand' => \Brand\Models\Brand::get_brand_name_by_id($shoe->brand_id),
				'name' => $shoe->model_name,
				'slug' => $shoe->slug,
				'image' => $attachment->image_filename,
				'added' => $this->time_elapsed_string($fav->entry_datetime)
			);
		}
		
		return $users_favs;
	}
}