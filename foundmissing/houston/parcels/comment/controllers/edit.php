<?php
namespace Comment\Controllers;

class Edit extends \Manage\Controllers\Manage{

	private $comment;

	public $page_title;
	public $link_back;
	public $form;
	public $existing;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		if(isset($_POST["submit"])){
			$this->submit_form();
		}

		if(isset($data["comment_id"])){
			$this->comment = \Comment\Models\Comment::find($data["comment_id"]);
			$this->page_title = "Editing Comment";
		} else {
			$this->comment = new \Comment\Models\Comment;
			$this->page_title = "New Comment";
		}

		$this->link_back = $this->comment->link_all();

		$form = new \Form\Models\Form("edit", "POST", "", "well");

		$item = new \Form\Models\FormItemHidden(array("id" => "id", "title" => "Id", "value" => $this->comment->id));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "parent_id", "title" => "Parent Id", "value" => $this->comment->parent_id));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "user_id", "title" => "User Id", "value" => $this->comment->user_id));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "comment_body", "title" => "Body", "value" => $this->comment->comment_body));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "replied_to", "title" => "Replied To", "value" => $this->comment->replied_to));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "post_type", "title" => "Post Type", "value" => $this->comment->post_type));
		$form->add_item($item);

		$item = new \Form\Models\FormItemSubmit(array("value" => "Save", "id" => "submit", "class" => "btn btn-primary", "cancel_url" => $this->comment->link_all()));
		$form->add_item($item);

		$this->form = $form->render();

	}

	public function controller(){
		$this->set_view("Comment\Views\Edit");
	}

	private function submit_form(){
		if($_POST["id"] > 0){
			$comment = \Comment\Models\Comment::find($_POST["id"]);
			$flash_msg = "Comment has been updated successfully.";
		} else {
			$comment = new \Comment\Models\Comment;
			$flash_msg = "Comment has been created successfully.";
			$comment->ordering = \Comment\Models\Comment::get_next_ordering_number();
		}

		$comment->parent_id = $_POST["parent_id"];
		$comment->user_id = $_POST["user_id"];
		$comment->comment_body = $_POST["comment_body"];
		$comment->replied_to = $_POST["replied_to"];
		$comment->post_type = $_POST["post_type"];

		$comment->save();
		$this->add_flash(array("message" => $flash_msg, "type" => "success", "Heading" => "Great!"));
		redirect_to($comment->link_all());
	}

}
