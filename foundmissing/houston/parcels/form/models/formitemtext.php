<?php

namespace Form\Models;

class FormItemText extends FormItem {
    
    public function __construct($attributes){
        $this->type = 'text';
        $this->template = 'text';
        parent::__construct($attributes);
    }
    
    
    
}