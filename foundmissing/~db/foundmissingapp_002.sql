-- phpMyAdmin SQL Dump
-- version 3.5.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 25, 2013 at 11:44 AM
-- Server version: 5.5.29
-- PHP Version: 5.3.15

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `foundmissingapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `attachments`
--

CREATE TABLE IF NOT EXISTS `attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL,
  `image_filename` varchar(255) NOT NULL,
  `image_width` int(11) NOT NULL,
  `image_height` int(11) NOT NULL,
  `image_mime` varchar(20) NOT NULL,
  `remote` int(1) NOT NULL DEFAULT '0',
  `extension` varchar(10) NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `entry_datetime` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `caption` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

--
-- Dumping data for table `attachments`
--

INSERT INTO `attachments` (`id`, `type`, `image_filename`, `image_width`, `image_height`, `image_mime`, `remote`, `extension`, `ordering`, `entry_datetime`, `deleted`, `title`, `caption`) VALUES
(32, '', 'b9i1fmafnfat.jpg', 690, 355, 'image/jpeg', 0, '', 0, 1366309639, 0, '', ''),
(33, '', 'f2zpj06cf3v3.jpg', 690, 355, 'image/jpeg', 0, '', 1, 1366309654, 0, '', ''),
(34, '', '1ydsrmeuer8q.jpg', 690, 355, 'image/jpeg', 0, '', 2, 1366309662, 0, '', ''),
(35, '', '142hi8c8l8ai.jpg', 690, 355, 'image/jpeg', 0, '', 3, 1366317999, 0, 'Nike/Jordan', 'Blah blah'),
(36, '', 'nin6n3cuw846.jpg', 2048, 968, 'image/jpeg', 0, '', 4, 1366663994, 0, '', ''),
(37, '', '9zdsahg4jver.jpg', 612, 612, 'image/jpeg', 0, '', 5, 1366755711, 0, 'Mirror', 'this is cool'),
(38, '', 'whrwqxhpz38b.png', 1455, 349, 'image/jpeg', 0, '', 6, 1366755732, 0, '', ''),
(39, '', 'dyjxy1fgs82m.jpg', 192, 192, 'image/jpeg', 0, '', 7, 1366765019, 0, '', ''),
(40, '', 'j0dnvf7s8c49.jpg', 612, 612, 'image/jpeg', 0, '', 8, 1366766895, 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE IF NOT EXISTS `brands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `title`, `description`, `ordering`, `deleted`) VALUES
(1, 'Air Jordan', '', 0, 0),
(2, 'Converse', 'These are the best shoes!', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `attachment_id` int(11) NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `entry_datetime` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

CREATE TABLE IF NOT EXISTS `colors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `hex` varchar(10) NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `colors`
--

INSERT INTO `colors` (`id`, `title`, `hex`, `ordering`, `deleted`) VALUES
(1, 'Black', '#000000', 0, 0),
(2, 'White', '#ffffff', 1, 0),
(3, 'Gray', '#C41313', 2, 0),
(4, 'Beige', '#F7FCC5', 3, 0),
(5, 'Brown', '#8C6246', 4, 0),
(6, 'Silver', '#E0E0E0', 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_type` varchar(20) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `body` text NOT NULL,
  `entry_datetime` int(11) NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `status` varchar(10) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `parent_type`, `parent_id`, `user_id`, `body`, `entry_datetime`, `ordering`, `status`, `deleted`) VALUES
(1, 'item', 9, 3, 'fsdgdfsg gdfsg dfdsf fgfsfdgfg', 1366835851, 1, '', 0),
(2, 'item', 9, 3, 'fsdgdfsg gdfsg dfdsf fgfsfdgfg', 1366836121, 1, 'a', 0),
(3, 'item', 9, 3, 'fsdgdfsg gdfsg dfdsf fgfsfdgfg', 1366836165, 1, 'a', 0),
(4, 'item', 9, 4, '', 1366836248, 1, 'p', 0),
(5, 'item', 9, 4, '', 1366836270, 1, 'p', 0),
(6, 'item', 9, 5, 'sgfdgfsdg', 1366836382, 1, 'p', 0),
(7, 'item', 9, 1, 'boobya', 1366836451, 1, 'p', 0);

-- --------------------------------------------------------

--
-- Table structure for table `designers`
--

CREATE TABLE IF NOT EXISTS `designers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `designers`
--

INSERT INTO `designers` (`id`, `title`, `description`, `ordering`, `deleted`) VALUES
(1, 'Cole Haan', '', 0, 0),
(2, 'Nike/Jordan', '', 1, 0),
(3, 'Converse', '', 2, 0),
(4, 'Adidas', '', 3, 0),
(5, 'New Balance', '', 4, 0),
(6, 'Red Wing', '', 5, 0),
(7, 'Clarks Original', '', 6, 0);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE IF NOT EXISTS `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `skew` varchar(255) DEFAULT NULL,
  `excerpt` text,
  `body` text,
  `price` decimal(11,2) DEFAULT NULL,
  `rating` int(5) DEFAULT NULL,
  `internal_notes` text,
  `category_id` int(5) DEFAULT NULL,
  `tags` text,
  `ordering` int(11) DEFAULT '1',
  `entry_datetime` int(11) DEFAULT NULL,
  `update_datetime` int(11) DEFAULT NULL,
  `publish_datetime` int(11) DEFAULT NULL,
  `unpublish_datetime` int(11) DEFAULT NULL,
  `deleted` int(1) DEFAULT '0',
  `type` varchar(20) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `brand_id` int(11) NOT NULL DEFAULT '0',
  `style_id` int(11) NOT NULL DEFAULT '0',
  `designer_id` int(11) NOT NULL DEFAULT '0',
  `colors` varchar(255) NOT NULL DEFAULT '0',
  `store_id` int(11) NOT NULL DEFAULT '0',
  `year` int(5) DEFAULT NULL,
  `purchase_url` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `user_id`, `title`, `slug`, `skew`, `excerpt`, `body`, `price`, `rating`, `internal_notes`, `category_id`, `tags`, `ordering`, `entry_datetime`, `update_datetime`, `publish_datetime`, `unpublish_datetime`, `deleted`, `type`, `status`, `brand_id`, `style_id`, `designer_id`, `colors`, `store_id`, `year`, `purchase_url`) VALUES
(8, 1, 'Home Slider', 'home-slider', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1366309595, NULL, 1366171200, NULL, 0, 'slider', NULL, 0, 0, 0, '0', 0, 0, NULL),
(9, 1, 'Nike Air Jordan IV - Retro 2012', 'nike-air-jordan-iv-retro-2012', '23213', 'So the word is out and the Jordan III white/cement will make another visit come 2011.', 'So the word is out and the Jordan III white/cement will make another visit come 2011. From pictures that have surfaced one has to be disappointed at the use of what looks like a glossy cheap leather. Outside of that and the fact that there is no Nike Air (wasnâ€™t on the 2003 release either) the shoe looks OK. Take a look at the side pictures below before you make your decision, and at the very least check out the different releases side by side. Enjoy!', '0.00', 0, '', 0, '', 1, 1366317636, NULL, 1295326800, NULL, 0, 'product', NULL, 1, 1, 2, '1,2,6', 4, 1999, 'http://www.test.com'),
(10, 1, 'Nice Boots', 'nice-boots', 'ffcc55', 'dasfdsgfdsgdfs gfdfdsfdas', 'dsfadsd sfadsf dsfdsf adsfdsfd', '55.00', NULL, '', NULL, 'adsf,sadsad,fdfd', 2, 1366663975, NULL, 1365480000, NULL, 0, 'product', NULL, 1, 2, 2, '1,4,6', 2, NULL, 'http://www.test.com');

-- --------------------------------------------------------

--
-- Table structure for table `item_attachments`
--

CREATE TABLE IF NOT EXISTS `item_attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `attachment_id` int(11) NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `entry_datetime` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=61 ;

--
-- Dumping data for table `item_attachments`
--

INSERT INTO `item_attachments` (`id`, `item_id`, `attachment_id`, `ordering`, `entry_datetime`, `deleted`) VALUES
(50, 8, 32, 0, 0, 0),
(51, 8, 33, 1, 0, 0),
(52, 8, 34, 2, 0, 0),
(53, 9, 35, 3, 0, 0),
(54, 9, 33, 4, 0, 0),
(55, 10, 36, 5, 0, 0),
(56, 9, 37, 6, 0, 1),
(57, 9, 38, 7, 0, 0),
(58, 9, 34, 8, 0, 0),
(59, 9, 39, 9, 0, 0),
(60, 9, 40, 10, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `stores`
--

CREATE TABLE IF NOT EXISTS `stores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `stores`
--

INSERT INTO `stores` (`id`, `title`, `description`, `ordering`, `deleted`) VALUES
(1, 'Chippewa', '', 0, 0),
(2, 'Sebago', '', 1, 0),
(3, 'Puma', '', 2, 0),
(4, 'Under Armour', '', 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `styles`
--

CREATE TABLE IF NOT EXISTS `styles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `styles`
--

INSERT INTO `styles` (`id`, `title`, `description`, `ordering`, `deleted`) VALUES
(1, 'Athletic', '', 0, 0),
(2, 'Boots', '', 1, 0),
(3, 'Loafers', '', 2, 0),
(4, 'Oxfords', '', 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(100) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `data` text NOT NULL,
  `admin` int(1) NOT NULL DEFAULT '0',
  `gender` varchar(10) NOT NULL,
  `shoe_size` varchar(25) NOT NULL,
  `city` varchar(100) NOT NULL,
  `twitter` varchar(50) NOT NULL,
  `website` varchar(255) NOT NULL,
  `entry_datetime` int(11) NOT NULL DEFAULT '0',
  `update_datetime` int(11) NOT NULL DEFAULT '0',
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `firstname`, `lastname`, `data`, `admin`, `gender`, `shoe_size`, `city`, `twitter`, `website`, `entry_datetime`, `update_datetime`, `deleted`) VALUES
(1, 'tylersavery', 'sdgdasfds', 'f5c38375cd3ffcbfe0957cb9ae6dce35', 'afds', '', '{"email":"fdfdsf@sdf","phone":"4169974264"}', 1, 'sgsdaf', 'sadgdsf', 'sdgsadf', 'asdfdsf', 'dsgdsaf', 0, 0, 0),
(2, 'dfad', 'dfsg', '1530d0e446ff74e728158fd5fe89e592', 'dfadsf', 'dsf', '', 0, 'dsfasd', 'fgdsfg', 'fdgdf', 'fdsg', 'fdsg', 0, 0, 0),
(3, 'adsfdasf', 'dsaf', '47df24e6aa353e1876e81388bfb24515', 'dasfadsf', 'das', '', 0, 'gfsdg', 'fdgf', 'fgs', 'fds', 'hdgfh', 0, 0, 0),
(4, 'asfdsf', 'retw', '0745da98f74d2927db1f7151bbdbdce1', 'gsdaf', 'das', '', 0, 'dsf', 'gfd', 'hgfdtr', 'bvcbc', 'sdgf', 0, 0, 0),
(5, 'adsfds', 'fdsg', 'eeab8d82f794c109fc842353426daee1', 'fgsdfg', 'fgds', '', 0, 'fdsg', 'fdsg', 'dfsg', 'dsfg', 'hdsfg', 0, 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
