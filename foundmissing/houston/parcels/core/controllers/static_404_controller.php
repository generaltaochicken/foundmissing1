<?php
namespace Core\Controllers;

class Static_404_Controller extends \Core\Controllers\Base_Controller {
	
	function controller() {
		$this->add_asset('css', 'reset.css', 'core');
        $this->add_asset('js', 'jquery.min.js', 'core');
		
		$this->add_asset('css', 'bootstrap.min.css', true);
		$this->add_asset('css', 'font-awesome.min.css', true);
		$this->add_asset('css', 'bootstrap-theme.min.css', true);
		$this->add_asset('js', 'bootstrap.min.js', true);
		
        $this->add_asset('js', 'master.js', true);
		$this->add_asset('css', 'master.css', true);
		
	    header('HTTP/1.0 404 Not Found');
		$this->title = '404 Page Not Found';
	    $this->set_view('Core\View\Static_404');
	}
	
}