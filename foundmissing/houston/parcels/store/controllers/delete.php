<?php
namespace Store\Controllers;

class Delete extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$store = \Store\Models\Store::find($data["store_id"]);

		if($store){
			$store->deleted = 1;
			$store->save();
			$this->add_undo(array("message" => "Store has been deleted!", "action" => $store->link_restore()));

			redirect_to($store->link_all());
		}
	}
}
