<?php
namespace Color\Controllers;

class All extends \Manage\Controllers\Manage{

	private $colors;

	public $colors_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Colors";
		$this->link_new = \Color\Models\Color::link_new();

		$this->colors = \Color\Models\Color::all(array("conditions" => "deleted = 0" , "order" => "ordering"));
		$archived = \Color\Models\Color::count(array("conditions" => "deleted = 1"));
		if($archived > 0){
			$this->archive_button = array("url" => \Color\Models\Color::link_archive(), "title" => "Archive");
		}

		foreach($this->colors as $color){
			$this->has_data = true;
			$this->colors_data[] = array(
				"id" => $color->id,
				"title" => $color->title,
				"hex" => $color->hex,
				"link_edit" => $color->link_edit(),
				"link_delete" => $color->link_delete(),
				"ordering" => $color->ordering,
			);
		}

	}

	public function controller(){
		$this->set_view("Color\Views\All");
	}

}

