<?php
namespace Comment\Controllers;

class All extends \Manage\Controllers\Manage{

	private $comments;

	public $comments_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Comments";
		$this->link_new = \Comment\Models\Comment::link_new();

		$this->comments = \Comment\Models\Comment::all(array("conditions" => "deleted = 0" , "order" => "ordering"));
		$archived = \Comment\Models\Comment::count(array("conditions" => "deleted = 1"));
		if($archived > 0){
			$this->archive_button = array("url" => \Comment\Models\Comment::link_archive(), "title" => "Archive");
		}

		foreach($this->comments as $comment){
			$this->has_data = true;
			$this->comments_data[] = array(
				"id" => $comment->id,
				"parent_id" => $comment->parent_id,
				"user_id" => $comment->user_id,
				"comment_body" => $comment->comment_body,
				"replied_to" => $comment->replied_to,
				"sub_replied_to" => $comment->sub_replied_to,
				"entry_datetime" => $comment->entry_datetime,
				"update_datetime" => $comment->update_datetime,
				"post_type" => $comment->post_type,
				"link_edit" => $comment->link_edit(),
				"link_delete" => $comment->link_delete(),
				"ordering" => $comment->ordering,
			);
		}

	}

	public function controller(){
		$this->set_view("Comment\Views\All");
	}

}

