<?php

namespace Frontend\Controllers;

class Journal extends \Frontend\Controllers\Frontend {

    public $photos = array();

    public function __construct($uri, $data){
        parent::__construct($uri, $data);
		$this->title = 'Journal | FM';
		$this->add_asset('css', 'main_css.css', true);
		$this->add_asset('css', 'journal.css', true);
		
		$this->journals = \Journal\Models\Journal::all(array('conditions' => 'deleted = 0 AND publish_date = 1', 'order' => 'entry_datetime DESC'));
    }

    public function controller(){
		$this->get_journals();
        $this->set_view('Frontend\Views\Journal');
    }
	
	protected function get_journals() {
		$count = 0;
		$newrow = false;
		
		foreach ($this->journals as $journ) {
			$count++;
			
			if ($count % 2 == 0) {
				$newrow = false;
			} else {
				$newrow = true;
			}
			
			$this->journal[] = array(
				'id' => $journ->id,
				'cover' => JOURNAL_IMAGES . $journ->slug . '/' . $journ-> main_image,
				'author' => $journ->author,
				'title' => $journ->title,
				'description' => $journ->excerpt,
				'date' => time_to_friendly_date($journ->entry_datetime, 'Y.m.d'),
				'slug' => "/journal/" . $journ->slug,
				'newrow' => $newrow
			);
		}
	}
}