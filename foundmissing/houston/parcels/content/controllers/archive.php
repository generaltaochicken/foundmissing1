<?php
namespace Content\Controllers;

class Archive extends \Manage\Controllers\Manage{

	private $contents;

	public $contents_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Deleted Contents";

		$this->contents = \Content\Models\Content::all(array("conditions" => "deleted = 1"));
		$this->archive_button = array("url" => \Content\Models\Content::link_all(), "title" => "Back");
		foreach($this->contents as $content){
			$this->has_data = true;
			$this->contents_data[] = array(
				"id" => $content->id,
				"k" => $content->k,
				"v" => $content->v,
				"link_restore" => $content->link_restore(),
			);
		}

	}

	public function controller(){
		$this->set_view("Content\Views\All");
	}

}

