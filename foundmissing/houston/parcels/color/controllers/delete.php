<?php
namespace Color\Controllers;

class Delete extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$color = \Color\Models\Color::find($data["color_id"]);

		if($color){
			$color->deleted = 1;
			$color->save();
			$this->add_undo(array("message" => "Color has been deleted!", "action" => $color->link_restore()));

			redirect_to($color->link_all());
		}
	}
}
