<?php
namespace Frontend\Controllers;

class Interview extends \Frontend\Controllers\Frontend {
    
	private $data;
	
	public $current_user;
	public $post_type = 2;
	
    public function __construct($uri, $data) {
        parent::__construct($uri, $data);
		$this->data = $data;
		$this->current_user = $this->_user->id;
		
		// Add the assets
		$this->add_asset('css', 'interview.css', true);
		$this->add_asset('js', 'interview.js', true);
		$this->add_asset('js', 'comments.js', true);
		
		if (isset($this->data['slug'])) {
			
			$this->interview = \Interview\Models\Interview::first(array('conditions' => 'deleted = 0 AND slug = "' . $this->data['slug'] . '"'));
			$this->title = $this->interview->title . ' | FM';
			if ($this->interview->id == null) {
				redirect_to('/404');
			}
		}
		
		if (isset($_POST['submit_comment'])) {
			$this->submit_comment();
		}
    }

    public function controller() {
		
		$this->article = array(
			'id' => $this->interview->id,
			'title' => $this->interview->title,
			'main_image' => ARTICLE_IMAGES . $this->interview->main_image,
			'date' => time_to_friendly_date($this->interview->publish_date, 'Y.m.d'),
			'author_name' => $this->interview->author_name,
			'quote' => $this->interview->excerpt,
			'slug' => $this->interview->slug,
			'excerpt' => $this->interview->excerpt,
			'body' => $this->interview->body
		);
		
		// Get the shares data
		$query = $this->domain_url . 'interviews/' . $this->article['slug'];
		
		$this->fb_likes = $this->get_facebook_likes($query);
		$this->fb_shares = $this->get_facebook_shares($query);
		$this->facebook_data = array(
			'likes' => $this->fb_likes[0]['like_count'],
			'shares' => $this->fb_shares['shares']
		);
		$this->su_views = $this->get_stumbleupon_views($query);
		$this->gplus = $this->get_google_plus($query);
		$this->tweets = $this->get_tweets($query);
		
		
		$this->comments = \Comment\Models\Comment::getCommentsByID($this->interview->id, $this->post_type);
		
		if ($this->comments != null) {
			$this->has_comments = true;
		}
		
		$count = \Comment\Models\Comment::getCommentCount($this->interview->id, $this->post_type);
			
		if ($count > 1) {
			$this->comments_count = $count . ' comments';
		} else {
			$this->comments_count = $count . ' comment';
		}
		
		$this->set_view('Frontend\Views\Interview');
	}
	
	public function submit_comment() {
		$comment = new \Comment\Models\Comment;
		$user = \User\Models\User::find($this->_user->id);
		
		$comment->parent_id = $this->interview->id;
		$comment->user_id = $this->_user->id;
		$comment->comment_body = $_POST['comment'];
		$comment->entry_datetime = time();
		$comment->post_type = $this->post_type;
		
		$user->karma += 1;
		$comment->save();
		$user->save();
		
		$this->update_activity_feed($comment->id);
		
		redirect_to('/interviews/' . $this->data['slug']);
	}
	
	private function update_activity_feed($comment_id) {
		$activity = new \Activity\Models\Activity;
		$activity->user_id = $this->_user->id;
		$activity->activity_type = 'interview_post';
		$activity->source_id = $this->interview->id;
		$activity->child_id = $comment_id;
		$activity->entry_datetime = time();
		
		$activity->save();
	}
}