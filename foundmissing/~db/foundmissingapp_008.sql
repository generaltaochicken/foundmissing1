-- phpMyAdmin SQL Dump
-- version 4.0.0
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 23, 2013 at 07:00 PM
-- Server version: 5.6.11
-- PHP Version: 5.3.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `foundmissingapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `attachments`
--

DROP TABLE IF EXISTS `attachments`;
CREATE TABLE IF NOT EXISTS `attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) DEFAULT NULL,
  `image_filename` varchar(255) DEFAULT NULL,
  `image_width` int(11) DEFAULT NULL,
  `image_height` int(11) DEFAULT NULL,
  `image_mime` varchar(20) DEFAULT NULL,
  `remote` int(1) NOT NULL DEFAULT '0',
  `extension` varchar(10) DEFAULT NULL,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `entry_datetime` int(11) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `caption` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=77 ;

--
-- Dumping data for table `attachments`
--

INSERT INTO `attachments` (`id`, `type`, `image_filename`, `image_width`, `image_height`, `image_mime`, `remote`, `extension`, `ordering`, `entry_datetime`, `deleted`, `title`, `caption`) VALUES
(32, '', 'b9i1fmafnfat.jpg', 690, 355, 'image/jpeg', 0, '', 0, 1366309639, 0, '', ''),
(33, '', 'f2zpj06cf3v3.jpg', 690, 355, 'image/jpeg', 0, '', 1, 1366309654, 0, '', ''),
(34, '', '1ydsrmeuer8q.jpg', 690, 355, 'image/jpeg', 0, '', 2, 1366309662, 0, '', ''),
(35, '', '142hi8c8l8ai.jpg', 690, 355, 'image/jpeg', 0, '', 3, 1366317999, 0, 'Nike/Jordan', 'Blah blah'),
(36, '', 'nin6n3cuw846.jpg', 2048, 968, 'image/jpeg', 0, '', 4, 1366663994, 0, '', ''),
(37, '', '9zdsahg4jver.jpg', 612, 612, 'image/jpeg', 0, '', 5, 1366755711, 0, 'Mirror', 'this is cool'),
(38, '', 'whrwqxhpz38b.png', 1455, 349, 'image/jpeg', 0, '', 6, 1366755732, 0, '', ''),
(39, '', 'dyjxy1fgs82m.jpg', 192, 192, 'image/jpeg', 0, '', 7, 1366765019, 0, '', ''),
(40, '', 'j0dnvf7s8c49.jpg', 612, 612, 'image/jpeg', 0, '', 8, 1366766895, 0, '', ''),
(41, '', 'b4kt8pvv60vr.jpg', 960, 640, 'image/jpeg', 0, '', 9, 1367256544, 0, '', ''),
(42, '', 'abn8t10avbom.jpg', 192, 192, 'image/jpeg', 0, '', 10, 1367257199, 0, '', ''),
(43, '', '5182dbf29e754.jpg', 0, 0, '', 0, '', 11, 1367530500, 0, 'daft punk', 'daft punk shiny'),
(44, '', '5182e8ed5f6af.jpg', 0, 0, '', 0, '', 12, 1367533846, 0, 'The Great Owl', 'The great owl nimh'),
(45, '', '5183ec5d67016.jpg', 0, 0, '', 0, '', 13, 1367600244, 0, 'Video 1 thumbnail', 'Video 1 thumbnail caption'),
(46, '', '5183ec5d67016.jpg', 0, 0, '', 0, '', 14, 1367600275, 0, 'Video 2 thumb', 'Video 2 thumbnail caption'),
(47, '', '5183ef71d2c08.jpg', 0, 0, '', 0, '', 15, 1367601030, 0, 'Video 1 thumb', 'Video 1 thumbnail caption'),
(48, '', '518a7e6ff3917.jpg', 0, 0, '', 0, '', 16, 1368030839, 0, '', ''),
(49, '', '518a80f907640.jpg', 0, 0, '', 0, '', 17, 1368031492, 0, 'Supra Skytop II', ''),
(50, '', '518a81814ed16.jpg', 0, 0, '', 0, '', 18, 1368031647, 0, 'Supra Skytop II', ''),
(51, '', '518a84096372c.jpg', 0, 0, '', 0, '', 19, 1368032269, 0, '', ''),
(52, '', '518a84b09e747_cropped.jpg', 0, 0, '', 0, '', 20, 1368032435, 0, '', ''),
(53, '', '518a968ff2ce8_cropped.jpg', 0, 0, '', 0, '', 21, 1368037009, 0, '', ''),
(54, '', '518a9919a70e9.jpg', 0, 0, '', 0, '', 22, 1368037660, 0, '', ''),
(55, '', '518a9b7b89681.jpg', 0, 0, '', 0, '', 23, 1368038279, 0, 'Clarks Beeswax Leather Desert Boot', ''),
(56, '', '518aad19614e3.jpg', 0, 0, '', 0, '', 24, 1368042778, 0, '', ''),
(57, '', '518ab00a85285.jpg', 0, 0, '', 0, '', 25, 1368043533, 0, '', ''),
(58, '', '518ab1f1a9a7e.jpg', 0, 0, '', 0, '', 26, 1368044019, 0, '', ''),
(59, '', '518ab38d1e29d.jpg', 0, 0, '', 0, '', 27, 1368044431, 0, '', ''),
(60, '', '518ab50808e3b.jpg', 0, 0, '', 0, '', 28, 1368044815, 0, 'Adidas Dragon', ''),
(61, '', '518ab61da43f4.jpg', 0, 0, '', 0, '', 29, 1368045088, 0, '', ''),
(62, '', '518ab9c130956_cropped.jpg', 0, 0, '', 0, '', 30, 1368046019, 0, '', ''),
(63, '', '518abb2d4973b_cropped.jpg', 0, 0, '', 0, '', 31, 1368046383, 0, '', ''),
(64, '', '518abe935bda7_cropped.jpg', 0, 0, '', 0, '', 32, 1368047254, 0, '', ''),
(65, '', '518abffc64278_cropped.jpg', 0, 0, '', 0, '', 33, 1368047618, 0, '', ''),
(66, '', '518ac1b97d41b_cropped.jpg', 0, 0, '', 0, '', 34, 1368048061, 0, '', ''),
(67, '', '518ac4a6cb673.jpg', 0, 0, '', 0, '', 35, 1368048810, 0, '', ''),
(68, '', '518ac6cb7a005.jpg', 0, 0, '', 0, '', 36, 1368049358, 0, '', ''),
(69, '', '518ac9af37e6c_cropped.jpg', 0, 0, '', 0, '', 37, 1368050097, 0, '', ''),
(70, '', '518acae371b84_cropped.jpg', 0, 0, '', 0, '', 38, 1368050406, 0, '', ''),
(71, 'image', '519e7af466e24.jpg', NULL, NULL, NULL, 0, NULL, 39, 1369340667, 0, 'Sky Machine', 'Test'),
(72, 'image', '519e7e9bae040.jpg', NULL, NULL, NULL, 0, NULL, 40, 1369341597, 0, '', ''),
(73, 'image', '519e8748200e2.jpg', NULL, NULL, NULL, 0, NULL, 41, 1369343818, 0, '', ''),
(74, 'image', '519e8954d5d54.jpg', NULL, NULL, NULL, 0, NULL, 42, 1369344342, 0, '', ''),
(75, 'image', '519e8c733b087.jpg', NULL, NULL, NULL, 0, NULL, 43, 1369345143, 0, '', ''),
(76, 'image', '519e8d97e5bf3.jpg', NULL, NULL, NULL, 0, NULL, 44, 1369345433, 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

DROP TABLE IF EXISTS `brands`;
CREATE TABLE IF NOT EXISTS `brands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `description` text,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `title`, `description`, `ordering`, `deleted`) VALUES
(1, 'Air Jordan', '', 0, 0),
(2, 'Converse', 'These are the best shoes!', 1, 0),
(3, 'Timberland', 'Makin boots', 2, 0),
(4, 'Supra', 'fun hightops!', 3, 0),
(5, 'Nike', '', 4, 0),
(6, 'Clarks', '', 5, 0),
(7, 'Rockport', '', 6, 0),
(8, 'Adidas', '', 7, 0),
(9, 'Allen Edmonds', '', 8, 0),
(10, 'K-Swiss', '', 9, 0),
(11, 'Android Homme', '', 10, 0),
(12, 'Kriss Kross', '', 11, 0);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `slug` varchar(100) DEFAULT NULL,
  `attachment_id` int(11) DEFAULT NULL,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `entry_datetime` int(11) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `parent_id`, `slug`, `attachment_id`, `ordering`, `entry_datetime`, `deleted`) VALUES
(1, 'Footwear', 0, 'footwear', 49, 0, 0, 0),
(2, 'Fashion', 0, 'fashion', 0, 1, 0, 0),
(3, 'Lifestyle', 0, 'lifestyle', 0, 2, 0, 0),
(4, 'Design', 0, 'design', 0, 3, 0, 0),
(5, 'Miscellaneous', 0, 'miscellaneous', 0, 4, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

DROP TABLE IF EXISTS `colors`;
CREATE TABLE IF NOT EXISTS `colors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `hex` varchar(10) DEFAULT NULL,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `colors`
--

INSERT INTO `colors` (`id`, `title`, `hex`, `ordering`, `deleted`) VALUES
(1, 'Black', '#000000', 0, 0),
(2, 'White', '#ffffff', 1, 0),
(3, 'Red', '#FF0000', 2, 0),
(4, 'Beige', '#F7FCC5', 3, 0),
(5, 'Brown', '#8C6246', 4, 0),
(6, 'Silver', '#E0E0E0', 5, 0),
(7, 'Blue', '#1188FF', 6, 0),
(8, 'Magenta', '#FF00FF', 7, 0);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_type` varchar(20) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `body` text,
  `rating_comfort` int(4) DEFAULT NULL,
  `rating_style` int(4) DEFAULT NULL,
  `rating_affordability` int(4) DEFAULT NULL,
  `rating_overall` int(4) DEFAULT NULL,
  `entry_datetime` int(11) DEFAULT NULL,
  `ordering` int(11) DEFAULT '1',
  `status` varchar(10) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `parent_type`, `parent_id`, `user_id`, `body`, `rating_comfort`, `rating_style`, `rating_affordability`, `rating_overall`, `entry_datetime`, `ordering`, `status`, `deleted`) VALUES
(9, 'item', 9, 1, 'This shoe is yummy', NULL, NULL, NULL, NULL, 1369327512, 1, 'a', 0),
(10, 'item', 41, 1, 'Another Review', NULL, NULL, NULL, NULL, 1369328290, 1, 'a', 0),
(11, 'item', 19, 1, 'Ok!', NULL, NULL, NULL, NULL, 1369328356, 1, 'a', 0),
(12, 'item', 33, 1, 'awesome!', NULL, NULL, NULL, NULL, 1369328568, 1, 'a', 0),
(13, 'item', 43, 1, 'neato', NULL, NULL, NULL, NULL, 1369328640, 1, 'a', 0),
(14, 'item', 33, 1, 'Love it', 3, 0, 7, 3, 1369328691, 1, 'a', 0),
(15, 'item', 45, 1, 'gfdgdsfdsafds', 8, 8, 7, 8, 1369328706, 1, 'a', 0),
(16, 'item', 31, 1, 'pink', 6, 6, 8, 7, 1369328862, 1, 'a', 0),
(17, 'item', 52, 6, 'This shoe is just lovely!', 8, 9, 7, 8, 1369343963, 1, 'p', 0);

-- --------------------------------------------------------

--
-- Table structure for table `contents`
--

DROP TABLE IF EXISTS `contents`;
CREATE TABLE IF NOT EXISTS `contents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `k` varchar(255) DEFAULT NULL,
  `v` text,
  `entry_datetime` int(11) NOT NULL DEFAULT '0',
  `update_datetime` int(11) NOT NULL DEFAULT '0',
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `contents`
--

INSERT INTO `contents` (`id`, `k`, `v`, `entry_datetime`, `update_datetime`, `deleted`) VALUES
(1, 'title', 'PLAYFIGHT IS A VFX STUDIO', 0, 0, 0),
(2, 'subtitle', 'WE PRODUCE TOP TIER VISUAL EFFECTS, ANIMATION AND PRE-VISUALIZATION FOR <span>FEATURE FILMS</span>, <span>TELEVISION</span> AND <span>ADVERTISING</span>. OUR OFFICES ARE LOCATED IN DOWNTOWN TORONTO', 0, 0, 0),
(3, 'facebook', 'http://facebook.com/foundmissing', 0, 0, 0),
(4, 'twitter', 'http://twitter.com/foundmissing', 0, 0, 0),
(5, 'instagram', 'http://instagram.com/foundmissing', 0, 0, 0),
(6, 'rss', 'http://rss.com/foundmissing', 0, 0, 0),
(7, 'contact_subtitle', 'We are so fun.', 0, 0, 0),
(8, 'address1', '489 Queen Street East', 0, 0, 1),
(9, 'address2', 'Toronto, On M5A 1V1', 0, 0, 1),
(10, 'phone', '416-809-6594', 0, 0, 0),
(11, 'email', 'hello@playfight.com', 0, 0, 0),
(12, 'job_email', 'jobs@playfight.com', 0, 0, 0),
(13, 'mission_statement', 'So the word is out and the Jordan III white/cement will make another visit come 2011. From pictures that have surfaced one has to be disappointed at the use of what looks like a glossy cheap leather. Outside of that and the fact that there is no Nike Air (wasnâ€™t on the 2003 release either) the shoe looks OK. Take a look at the side pictures below before you make your decision, and at the very least check out the different releases side by side. Enjoy.', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `designers`
--

DROP TABLE IF EXISTS `designers`;
CREATE TABLE IF NOT EXISTS `designers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `description` text,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `designers`
--

INSERT INTO `designers` (`id`, `title`, `description`, `ordering`, `deleted`) VALUES
(1, 'Cole Haan', '', 0, 0),
(2, 'Nike/Jordan', '', 1, 0),
(3, 'Converse', '', 2, 0),
(4, 'Adidas', '', 3, 0),
(5, 'New Balance', '', 4, 0),
(6, 'Red Wing', '', 5, 0),
(7, 'Clarks Original', '', 6, 0),
(8, 'Timberland', '', 7, 0),
(9, 'Android Homme', '', 8, 0),
(10, 'Billy the Moose', '', 9, 0);

-- --------------------------------------------------------

--
-- Table structure for table `favorites`
--

DROP TABLE IF EXISTS `favorites`;
CREATE TABLE IF NOT EXISTS `favorites` (
  `user_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  UNIQUE KEY `user_id` (`user_id`,`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `favorites`
--

INSERT INTO `favorites` (`user_id`, `item_id`) VALUES
(1, 9),
(1, 19),
(1, 29),
(1, 30),
(1, 36);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
CREATE TABLE IF NOT EXISTS `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `skew` varchar(255) DEFAULT NULL,
  `excerpt` text,
  `body` text,
  `price` decimal(11,2) DEFAULT NULL,
  `rating` int(5) DEFAULT NULL,
  `internal_notes` text,
  `category_id` int(5) DEFAULT NULL,
  `tags` text,
  `ordering` int(11) DEFAULT '1',
  `entry_datetime` int(11) DEFAULT NULL,
  `update_datetime` int(11) DEFAULT NULL,
  `publish_datetime` int(11) DEFAULT NULL,
  `unpublish_datetime` int(11) DEFAULT NULL,
  `deleted` int(1) DEFAULT '0',
  `type` varchar(20) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `page` varchar(20) DEFAULT NULL,
  `new_release` varchar(1) DEFAULT NULL,
  `brand_id` int(11) DEFAULT '0',
  `style_id` int(11) DEFAULT '0',
  `designer_id` int(11) DEFAULT '0',
  `colors` varchar(255) DEFAULT '0',
  `store_id` int(11) DEFAULT '0',
  `year` int(5) DEFAULT NULL,
  `purchase_url` text,
  `video_url` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=56 ;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `user_id`, `title`, `slug`, `skew`, `excerpt`, `body`, `price`, `rating`, `internal_notes`, `category_id`, `tags`, `ordering`, `entry_datetime`, `update_datetime`, `publish_datetime`, `unpublish_datetime`, `deleted`, `type`, `status`, `page`, `new_release`, `brand_id`, `style_id`, `designer_id`, `colors`, `store_id`, `year`, `purchase_url`, `video_url`) VALUES
(8, 6, 'Home Slider', 'home-slider', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1366309595, NULL, 1366171200, NULL, 0, 'slider', 'u', NULL, NULL, 0, 0, 0, '0', 0, 0, NULL, ''),
(9, 6, 'Nike Air Jordan IV - Retro 2012', 'nike-air-jordan-iv-retro-2012', '23213', 'So the word is out and the Jordan III white/cement will make another visit come 2011.', 'So the word is out and the Jordan III white/cement will make another visit come 2011. From pictures that have surfaced one has to be disappointed at the use of what looks like a glossy cheap leather. Outside of that and the fact that there is no Nike Air (wasnâ€™t on the 2003 release either) the shoe looks OK. Take a look at the side pictures below before you make your decision, and at the very least check out the different releases side by side. Enjoy!', '0.00', 0, '', 0, '', 1, 1366317636, NULL, 1295326800, NULL, 0, 'product', 'p', NULL, 'y', 1, 1, 2, '1,2,6', 4, 1999, 'http://www.test.com', ''),
(10, 6, 'Timberland 6" Men''s Premium', 'timberland-6-mens-premium', 'ffcc55', 'dasfdsgfdsgdfs gfdfdsfdas', 'dsfadsd sfadsf dsfdsf adsfdsfd', '55.00', NULL, '', NULL, 'adsf,sadsad,fdfd', 2, 1366663975, NULL, 1365480000, NULL, 0, 'product', 'p', NULL, NULL, 3, 2, 2, '4', 2, NULL, 'http://www.test.com', ''),
(11, 6, 'The Wreckoning', 'the-wreckoning', NULL, 'So the word is out and the Jordan III white/cement will make another visit come 2011.', 'So the word is out and the Jordan III white/cement will make another visit come 2011. From pictures that have surfaced one has to be disappointed at the use of what looks like a glossy cheap leather. Outside of that and the fact that there is no Nike Air (wasnâ€™t on the 2003 release either) the shoe looks OK.\r\n\r\nSo the word is out and the Jordan III white/cement will make another visit come 2011. From pictures that have surfaced one has to be disappointed at the use of what looks like a glossy cheap leather. Outside of that and the fact that there is no Nike Air (wasnâ€™t on the 2003 release either) the shoe looks OK.\r\n\r\nSo the word is out and the Jordan III white/cement will make another visit come 2011. From pictures that have surfaced one has to be disappointed at the use of what looks like a glossy cheap leather. Outside of that and the fact that there is no Nike Air (wasnâ€™t on the 2003 release either) the shoe looks OK.', NULL, NULL, NULL, 1, 'tag1', 3, 1366912176, NULL, 1366776000, NULL, 0, 'article', 'p', 'b', NULL, 0, 0, 0, '0', 0, NULL, NULL, ''),
(12, 6, 'Photo1', 'photo1', NULL, 'Photo 1 excerpt', 'Photo 1 body', NULL, NULL, NULL, 0, 'gallery photo', 4, 1367530465, NULL, 1368072000, NULL, 0, 'photo', NULL, NULL, NULL, 0, 0, 0, '0', 0, NULL, NULL, ''),
(13, 1, 'Photo 2', 'photo-2', NULL, 'Photo 2 excerpt', 'Photo 2 body', NULL, NULL, NULL, 0, 'photo two', 5, 1367533780, NULL, 1368763200, NULL, 0, 'photo', NULL, NULL, NULL, 0, 0, 0, '0', 0, NULL, NULL, ''),
(14, 6, 'Video 1', 'video-1', NULL, 'Cosby sweater you probably haven''t heard of them mlkshk.', 'Bicycle rights pork belly shoreditch sustainable, plaid wes anderson food truck four loko flannel brunch brooklyn post-ironic.  Pitchfork PBR flexitarian, yr cred put a bird on it hella YOLO biodiesel selvage.  Lomo squid occupy, four loko neutra umami bushwick actually synth pitchfork try-hard portland gastropub organic master cleanse.  3 wolf moon high life echo park, ugh trust fund etsy tattooed meh.  Craft beer church-key swag whatever, tousled portland etsy intelligentsia typewriter stumptown williamsburg thundercats direct trade.', NULL, NULL, NULL, 0, 'Video 1 tag', 6, 1367595955, NULL, 1367553600, NULL, 0, 'video', 'u', NULL, NULL, 0, 0, 0, '0', 0, NULL, NULL, 'http://player.vimeo.com/video/20997055'),
(15, 6, 'Video 2', 'video-2', NULL, 'Chillwave letterpress lo-fi kale chips.  Marfa carles salvia YOLO.', 'PBR VHS whatever ethnic, truffaut pop-up readymade brunch try-hard post-ironic small batch farm-to-table ethical dreamcatcher cosby sweater.  Chillwave retro put a bird on it, bespoke four loko vice stumptown.  Cred kale chips keffiyeh banh mi, yr thundercats cray four loko actually seitan marfa occupy.  Meh food truck umami, +1 forage mixtape keytar photo booth.  Gluten-free messenger bag PBR, mixtape cosby sweater fixie wolf tumblr brunch tousled keffiyeh single-origin coffee umami.  Swag lomo jean shorts marfa, typewriter lo-fi odd future tonx ennui raw denim.  Try-hard wayfarers food truck pinterest lo-fi.', NULL, NULL, NULL, 0, 'Video  2 tag', 7, 1367598657, NULL, 1367640000, NULL, 0, 'video', 'p', NULL, NULL, 0, 0, 0, '0', 0, NULL, NULL, 'http://www.youtube.com/watch?v=5NV6Rdv1a3I'),
(16, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 1368031297, NULL, NULL, NULL, 0, 'item', NULL, NULL, NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(17, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 1368031592, NULL, NULL, NULL, 0, 'item', NULL, NULL, NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(18, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, 1368031738, NULL, NULL, NULL, 0, 'item', NULL, NULL, NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(19, 6, 'Supra Skytop II', 'supra-skytop-ii', '003', 'These are grey high-tops!', 'Fixie vegan bicycle rights seitan shoreditch high life.  Leggings etsy messenger bag plaid semiotics lomo.  Austin shoreditch fap vice, selfies ugh butcher.  8-bit dreamcatcher tousled mcsweeney''s vegan.  +1 gastropub put a bird on it, actually artisan bespoke lo-fi.  Bicycle rights keffiyeh street art PBR.  Skateboard cardigan occupy, VHS gluten-free salvia semiotics chambray fashion axe craft beer jean shorts master cleanse terry richardson selvage try-hard.', '149.99', NULL, '', NULL, 'high-tops', 11, 1368032250, NULL, 1367380800, NULL, 0, 'product', 'p', NULL, NULL, 4, 1, 0, '3', 0, NULL, 'http://www.suprafootwear.com/products/S01030', NULL),
(20, 6, 'dfhgfjdf', 'dfhgfjdf', NULL, '', '', NULL, NULL, NULL, 0, '', 12, 1368033333, NULL, 0, NULL, 1, 'article', 'p', NULL, NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(21, 6, 'chcxvb', 'chcxvb', NULL, '', '', NULL, NULL, NULL, 0, '', 12, 1368033481, NULL, 0, NULL, 1, 'photo', 'p', NULL, NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(22, 6, 'gjdghjd', 'gjdghjd', NULL, '', '', NULL, NULL, NULL, 0, '', 12, 1368033910, NULL, 0, NULL, 1, 'article', 'p', NULL, NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(23, 6, 'fjdfbcnv', 'fjdfbcnv', NULL, '', '', NULL, NULL, NULL, 0, '', 13, 1368033963, NULL, 0, NULL, 1, 'article', 'p', NULL, NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(24, 6, 'fgjhmh', 'fgjhmh', 'asdsa', 'fgsd', 'fgfdgsfdg', '43.00', NULL, 'dsfadasf', NULL, '', 12, 1368036792, NULL, 1367985600, NULL, 1, 'product', 'p', NULL, NULL, 0, 0, 0, '0', 0, NULL, 'fdasfads', NULL),
(25, 6, 'fgjhmh', 'fgjhmh', 'asdsa', 'fgsd', 'fgfdgsfdg', '43.00', NULL, 'dsfadasf', NULL, '', 13, 1368036837, NULL, 1367985600, NULL, 1, 'product', 'p', NULL, NULL, 0, 0, 0, '0', 0, NULL, 'fdasfads', NULL),
(26, 6, 'fgjhmh', 'fgjhmh', 'asdsa', 'fgsd', 'fgfdgsfdg', '43.00', NULL, 'dsfadasf', NULL, '', 14, 1368036862, NULL, 1367985600, NULL, 1, 'product', 'p', NULL, NULL, 0, 0, 0, '0', 0, NULL, 'fdasfads', NULL),
(27, 6, 'fgjhmh', 'fgjhmh', 'asdsa', 'fgsd', 'fgfdgsfdg', '43.00', NULL, 'dsfadasf', NULL, '', 15, 1368036936, NULL, 1367985600, NULL, 1, 'product', 'p', NULL, NULL, 0, 0, 0, '0', 0, NULL, 'fdasfads', NULL),
(28, 6, 'fgjhmh', 'fgjhmh', 'asdsa', 'fgsd', 'fgfdgsfdg', '43.00', NULL, 'dsfadasf', NULL, '', 16, 1368036976, NULL, 1367985600, NULL, 1, 'product', 'p', NULL, NULL, 0, 2, 0, '1,4', 3, NULL, 'fdasfads', NULL),
(29, 6, 'Nike Womens Free Run 2 EXT', 'nike-womens-free-run-2-ext', '843751', 'Nike releases a new classic, featuring their famous lava color underneath a black structure.', 'Lomo quinoa pop-up, flannel art party tofu meggings banh mi stumptown four loko cray mcsweeney''s messenger bag.  Bespoke helvetica mlkshk, high life narwhal odd future ennui mcsweeney''s jean shorts selfies pug pour-over deep v.  Art party brooklyn tumblr blog occupy whatever pug wayfarers banksy fingerstache, carles lo-fi locavore portland fap.  Mumblecore intelligentsia fap letterpress swag.  Mumblecore next level street art bespoke.  Jean shorts intelligentsia vegan irony, pug leggings fashion axe forage thundercats occupy 8-bit.  VHS typewriter tattooed street art keytar.', '100.00', NULL, '', NULL, 'pink and black, nike, sneakers', 12, 1368037352, NULL, 1367380800, NULL, 0, 'product', 'p', NULL, 'y', 5, 1, 2, '1', 5, NULL, 'http://shop.cncpts.com/products/nike-womens-free-run-2-ext-total-crimson-bright-mango', NULL),
(30, 6, 'Clarks Beeswax Leather Desert Boot', 'clarks-beeswax-leather-desert-boot', '', 'dfsghdfgj', 'Lomo quinoa pop-up, flannel art party tofu meggings banh mi stumptown four loko cray mcsweeney''s messenger bag.  Bespoke helvetica mlkshk, high life narwhal odd future ennui mcsweeney''s jean shorts selfies pug pour-over deep v.  Art party brooklyn tumblr blog occupy whatever pug wayfarers banksy fingerstache, carles lo-fi locavore portland fap.  Mumblecore intelligentsia fap letterpress swag.  Mumblecore next level street art bespoke.  Jean shorts intelligentsia vegan irony, pug leggings fashion axe forage thundercats occupy 8-bit.  VHS typewriter tattooed street art keytar.', '140.00', NULL, '', NULL, 'clarks, brown, boots', 13, 1368038098, NULL, 1368244800, NULL, 0, 'product', 'p', NULL, NULL, 6, 2, 0, '5', 0, NULL, 'http://www.clarkscanada.com/products/ranges/mens-original/desert-boot/desert-boot-beeswax-leather', NULL),
(31, 6, 'RACHEL RACHEL ROY DEANDRAH DRESS FLAT', 'rachel-rachel-roy-deandrah-dress-flat', '765689', 'Gray and lime green loafers', 'Mcsweeney''s pug ethical blue bottle.  Chambray trust fund brunch, cliche VHS neutra gluten-free mustache.  Photo booth chambray jean shorts, 90''s ennui kogi occupy.  DIY 8-bit +1 trust fund selvage dreamcatcher, mumblecore umami typewriter helvetica hoodie mixtape brunch meggings.  Austin readymade flexitarian squid, fashion axe dreamcatcher literally salvia narwhal messenger bag farm-to-table fingerstache selvage.  Cardigan sustainable leggings seitan try-hard mumblecore fap.  Artisan bicycle rights stumptown bespoke keytar.', '91.00', NULL, '', NULL, 'loafers, lime green', 14, 1368038825, NULL, 1368590400, NULL, 0, 'product', 'p', NULL, NULL, 0, 3, 0, '3', 6, NULL, 'http://www.thebay.com/eng/shoes-loafersoxfords-_DEANDRAH_DRESS_FLAT-thebay/291526', NULL),
(32, 6, 'Rockport Men''s Margin Oxford', 'rockport-mens-margin-oxford', '345986', 'Brown and black oxfords', 'Williamsburg high life ugh, pork belly organic retro 90''s flexitarian.  Blog photo booth american apparel, authentic carles raw denim 8-bit letterpress viral gentrify.  Synth freegan jean shorts tonx, tousled kale chips selvage locavore vice.  Mumblecore synth YOLO pitchfork, ethical 8-bit occupy flannel ethnic mixtape chambray.  Messenger bag art party etsy fanny pack, craft beer tattooed wes anderson.  Marfa DIY literally, mcsweeney''s sriracha blue bottle kogi single-origin coffee farm-to-table mixtape 90''s.  Aesthetic fingerstache four loko, +1 food truck farm-to-table next level narwhal.', '100.00', NULL, '', NULL, 'rockports, oxfords, brown and black', 15, 1368043322, NULL, 1367467200, NULL, 0, 'product', 'p', NULL, NULL, 7, 4, 6, '1,5', 7, NULL, 'http://www.amazon.com/Rockport-Margin-Mens-Oxford/dp/B000EY5ORQ', NULL),
(33, 6, 'Supra Vaider Blue', 'supra-vaider-blue', '897524', 'All blue supra high tops', 'Williamsburg high life ugh, pork belly organic retro 90''s flexitarian.  Blog photo booth american apparel, authentic carles raw denim 8-bit letterpress viral gentrify.  Synth freegan jean shorts tonx, tousled kale chips selvage locavore vice.  Mumblecore synth YOLO pitchfork, ethical 8-bit occupy flannel ethnic mixtape chambray.  Messenger bag art party etsy fanny pack, craft beer tattooed wes anderson.  Marfa DIY literally, mcsweeney''s sriracha blue bottle kogi single-origin coffee farm-to-table mixtape 90''s.  Aesthetic fingerstache four loko, +1 food truck farm-to-table next level narwhal.', '80.00', NULL, '', NULL, 'all blue, supra, vaider', 16, 1368043893, NULL, 1367467200, NULL, 0, 'product', 'p', NULL, NULL, 4, 1, 0, '7', 0, NULL, 'http://www.supra2013.net/supra-vaider-high-tops-all-blue-p-912.html', NULL),
(34, 6, 'Nike Presto', 'nike-presto', '897340', 'Nike prestos from grade 7!', 'Artisan fanny pack high life vice, vinyl cliche actually fap gastropub plaid mlkshk post-ironic squid brunch.  Fashion axe tofu literally, put a bird on it locavore freegan street art kogi fanny pack fixie food truck american apparel salvia.  Locavore farm-to-table flannel ethical fanny pack bespoke.  Master cleanse hashtag banksy you probably haven''t heard of them, mumblecore tonx pickled meh portland dreamcatcher YOLO keffiyeh.  Gluten-free literally sartorial, fingerstache occupy raw denim artisan pickled cliche umami.  Keffiyeh DIY vinyl PBR neutra trust fund.  Artisan williamsburg selvage wayfarers, 8-bit mustache brooklyn blue bottle mlkshk aesthetic food truck.', '69.99', NULL, '', NULL, 'presto', 17, 1368044195, NULL, 1367553600, NULL, 0, 'product', 'p', NULL, NULL, 5, 1, 2, '1,2', 8, NULL, 'http://www.footlocker.eu/gb/en/Shoes/Nike-Presto-42.aspx/4206662604', NULL),
(35, 6, 'Adidas Dragon', 'adidas-dragon', '846832', 'Adidas dragons -- for dragons.', 'Salvia godard lomo bushwick.  DIY biodiesel cardigan fanny pack typewriter fixie, sartorial semiotics small batch tattooed cliche stumptown brunch literally.  Intelligentsia fashion axe YOLO keytar, chambray ugh blog typewriter post-ironic selfies artisan.  Keytar williamsburg gluten-free beard truffaut, 90''s neutra occupy sustainable helvetica gastropub viral echo park.  Wolf lo-fi swag DIY vice sartorial, artisan cosby sweater twee brunch hella.  Pop-up art party try-hard blue bottle viral, letterpress mustache aesthetic disrupt kogi tumblr raw denim occupy stumptown fashion axe.  Terry richardson biodiesel fixie authentic vinyl banh mi.', '65.00', NULL, '', NULL, 'dragon, runner', 18, 1368044704, NULL, 1368417600, NULL, 0, 'product', 'p', NULL, NULL, 8, 1, 4, '7', 0, NULL, 'http://www.adidas.com/us/product/mens-originals-dragon-shoes/EW391', NULL),
(36, 6, 'Nike Air Jordan XX8', 'nike-air-jordan-xx8', '094467', 'The new Air Jordan XX8''s!', 'Flannel biodiesel disrupt, YOLO chambray aesthetic hashtag ethnic.  Cosby sweater terry richardson YOLO DIY, pug artisan retro.  PBR cray helvetica blog, intelligentsia irony ethical godard mcsweeney''s portland bicycle rights.  Pitchfork bicycle rights single-origin coffee vice dreamcatcher, banh mi lomo pinterest fingerstache shoreditch four loko aesthetic.  Twee fanny pack american apparel flannel vice.  Hoodie brunch banh mi leggings, meggings polaroid fanny pack hashtag forage echo park mixtape meh.  American apparel stumptown try-hard swag, intelligentsia fixie forage high life echo park blue bottle banksy hella retro.', '200.00', NULL, '', NULL, 'wrapped, jordan', 19, 1368045025, NULL, 1368244800, NULL, 0, 'product', 'p', NULL, NULL, 1, 1, 2, '1,3', 0, NULL, 'http://www.footlocker.eu/gb/en/Shoes/Nike-Air-Jordan-Xx8-41.aspx/4101988104', NULL),
(37, 6, 'Men''s Earthkeepers Stormbuck Lite Chukka Boot', 'mens-earthkeepers-stormbuck-lite-chukka-boot', '462895', 'Cool low cut boots', 'Mumblecore typewriter tousled sartorial tattooed lo-fi PBR cray street art vice flannel.  Hashtag single-origin coffee intelligentsia, craft beer godard trust fund thundercats jean shorts marfa quinoa pitchfork.  Hella tumblr brunch banh mi messenger bag, disrupt irony bespoke before they sold out umami.  Shoreditch art party skateboard ugh, 90''s DIY bicycle rights messenger bag street art fixie.  Brooklyn next level wayfarers, williamsburg portland selvage meh selfies flannel seitan blue bottle fingerstache cray mlkshk.  DIY next level helvetica, photo booth farm-to-table kogi post-ironic wolf pinterest bespoke.  Brunch put a bird on it cliche fixie, pitchfork stumptown mixtape.', '110.00', NULL, '', NULL, 'low cut, boat shoes', 20, 1368046011, NULL, 1367985600, NULL, 0, 'product', 'p', NULL, NULL, 3, 2, 8, '4,6', 0, NULL, 'http://shop.timberland.com/product/index.jsp?c=1106674&productId=12885755&prodFindSrc=paramNav', NULL),
(38, 6, 'Allen Edmonds Men''s McAllister Wing Tip', 'allen-edmonds-mens-mcallister-wing-tip', '949573', 'For manly men only', 'Freegan art party authentic wayfarers disrupt, letterpress meh.  Gentrify readymade fingerstache gastropub retro typewriter blue bottle.  Selfies yr direct trade  intelligentsia chillwave.  Aesthetic hashtag readymade bespoke.  Hoodie ennui banksy synth authentic.  Williamsburg tumblr chambray readymade, master cleanse retro sriracha yr viral echo park seitan swag thundercats gentrify before they sold out.  Beard blog 90''s swag tonx, butcher forage meh 8-bit quinoa bespoke.', '345.00', NULL, '', NULL, 'leather, fine detailing', 21, 1368046375, NULL, 1367899200, NULL, 0, 'product', 'p', NULL, NULL, 9, 4, 0, '5', 7, NULL, 'http://www.amazon.com/Allen-Edmonds-Mens-McAllister-Walnut/dp/B001TDKXR2/ref=sr_1_5?s=shoes&ie=UTF8&qid=1368046234&sr=1-5', NULL),
(39, 6, 'Converse Chuck Taylor', 'converse-chuck-taylor', '839058', 'The classic chuck taylor', 'Godard helvetica semiotics lo-fi, +1 portland small batch biodiesel blog hashtag plaid mixtape.  Locavore cardigan banksy, food truck bespoke pug VHS hashtag narwhal whatever mlkshk blog terry richardson farm-to-table fingerstache.  Jean shorts forage deep v 8-bit street art.  Pickled bespoke fixie, wayfarers 90''s plaid try-hard ugh.  Typewriter gluten-free ugh fap, cliche mumblecore quinoa gentrify stumptown hashtag helvetica tofu.  Dreamcatcher banjo gluten-free, selfies 8-bit trust fund fap keffiyeh gentrify beard forage quinoa deep v you probably haven''t heard of them.  Portland tumblr yr shoreditch organic cosby sweater.', '55.00', NULL, '', NULL, 'classic, chuck taylor', 22, 1368047218, NULL, 1367380800, NULL, 0, 'product', 'p', NULL, NULL, 2, 1, 0, '1', 0, NULL, 'http://www.net-a-porter.com/product/311584?cm_mmc=LinkshareUS-_-Hy3bqNL2jtQ-_-Custom-_-LinkBuilder&siteID=Hy3bqNL2jtQ-WqYL0guMFbG9Fy1Lvs4xZw', NULL),
(40, 6, 'Nike Dunk SB', 'nike-dunk-sb', '9579755', 'Nike''s staple, the dunk', 'Pinterest blog you probably haven''t heard of them, banjo keytar hashtag aesthetic williamsburg fap meggings yr wolf trust fund occupy.  Hashtag odd future deep v ugh ennui.  Meh flexitarian sustainable, jean shorts cosby sweater wayfarers selfies whatever Austin farm-to-table art party terry richardson bushwick literally banksy.  Craft beer helvetica irony fanny pack, brooklyn ugh banjo pickled intelligentsia occupy.  Tumblr echo park williamsburg fap, squid cosby sweater viral vegan craft beer sustainable blue bottle dreamcatcher fashion axe YOLO.  Marfa 90''s echo park, chillwave sustainable PBR you probably haven''t heard of them pitchfork gluten-free.  Thundercats readymade cardigan, odd future cliche bespoke photo booth disrupt polaroid +1.', '250.00', NULL, 'Internal note on Nike Dunk SB', NULL, 'dunks', 23, 1368047607, NULL, 1368590400, NULL, 0, 'product', 'p', NULL, NULL, 5, 1, 2, '1', 0, NULL, 'http://www.flightclubny.com/p.php?fc=ny&c=sb&i=080120', NULL),
(41, 6, 'K-Swiss Clean Classic', 'kswiss-clean-classic', '385059', 'Classic K-Swisses', 'Bespoke VHS tumblr leggings bicycle rights hoodie.  8-bit chambray trust fund flexitarian beard PBR.  Chambray retro ethical meggings marfa post-ironic.  Marfa fingerstache organic, craft beer hella vice gastropub blog.  Raw denim you probably haven''t heard of them direct trade, skateboard bicycle rights scenester salvia flexitarian retro tofu.  Banjo vegan gastropub gentrify, pug fingerstache williamsburg viral bicycle rights street art carles tofu sustainable mcsweeney''s.  Tofu authentic kale chips, american apparel before they sold out stumptown sriracha blog twee plaid 3 wolf moon pop-up.', '70.00', NULL, '', NULL, 'classic, plain', 24, 1368048050, NULL, 1368763200, NULL, 0, 'product', 'p', NULL, NULL, 10, 1, 6, '1,2', 9, NULL, 'http://www.kswiss.com/shop/footwear/02874-035/Men/Clean_Classic/stingray_white/', NULL),
(42, 6, 'The Abington Work Ox Shoe', 'the-abington-work-ox-shoe', '305943', 'Cool shoes', 'Next level lo-fi mumblecore helvetica swag wayfarers, quinoa meh gluten-free ethnic.  Dreamcatcher cosby sweater ugh authentic intelligentsia etsy.  Synth raw denim keffiyeh Austin, letterpress brooklyn retro whatever aesthetic single-origin coffee ethnic cray mixtape fingerstache deep v.  Ethnic pug brunch tattooed raw denim small batch.  Twee whatever pitchfork artisan.  Flannel meh forage, Austin food truck +1 chillwave viral cardigan.  Stumptown bespoke kale chips narwhal.', '180.00', NULL, '', NULL, 'work, summer, boat', 25, 1368048801, NULL, 1367985600, NULL, 0, 'product', 'p', NULL, NULL, 3, 4, 8, '4,5', 10, NULL, 'http://www.karmaloop.com/product/The-Abington-Work-Ox-Shoe-in-Brown-Smooth/313664', NULL),
(43, 6, 'Adidas Opening Ceremony', 'adidas-opening-ceremony', '869203', 'This is an intense shoe', 'Church-key quinoa meh hoodie direct trade.  Actually literally cliche single-origin coffee.  Cray mumblecore pork belly thundercats, brunch food truck wes anderson typewriter etsy lo-fi.  Irony gastropub fashion axe food truck, selvage next level scenester echo park meh DIY godard.  Pug farm-to-table art party +1 tofu.  Messenger bag tumblr small batch bespoke.  Farm-to-table single-origin coffee post-ironic polaroid, etsy flexitarian 3 wolf moon selfies aesthetic helvetica tofu tumblr cardigan quinoa.', '235.00', NULL, '', NULL, 'opening ceremony', 26, 1368049349, NULL, 1369281600, NULL, 0, 'product', 'p', NULL, NULL, 8, 1, 4, '7', 10, NULL, 'http://www.karmaloop.com/product/The-Crazy-8-Tennis-Sneaker-in-Black-Multi/384218', NULL),
(44, 6, 'Android Homme Propulsion High 1.5', 'android-homme-propulsion-high-15', '294765', 'Speckled high-tops!', 'Before they sold out viral YOLO, portland dreamcatcher PBR banjo tumblr helvetica mcsweeney''s lo-fi whatever cred.  Cliche occupy try-hard, shoreditch yr ennui truffaut aesthetic.  3 wolf moon VHS vice, chambray small batch blue bottle literally +1 keytar blog.  DIY food truck direct trade  four loko etsy.  Vegan messenger bag salvia, retro four loko organic lomo iphone irony before they sold out blog.  Meggings portland wes anderson occupy high life.  Whatever Austin occupy thundercats meh.', '219.00', NULL, '', NULL, 'android homme, high-tops', 27, 1368050003, NULL, 1368590400, NULL, 0, 'product', 'p', NULL, NULL, 11, 1, 9, '1', 11, NULL, 'http://www.mypys.com/android-homme-propulsion-high-1-5-grey-fur', NULL),
(45, 6, 'ObyO JS Ostrich Slim - Jeremy Scott', 'obyo-js-ostrich-slim-jeremy-scott', '249634', 'Futury boots', 'Food truck plaid hashtag, twee marfa high life authentic keffiyeh american apparel craft beer lo-fi brooklyn whatever.  Neutra banksy vegan hella, helvetica godard 90''s tonx fanny pack master cleanse gastropub stumptown pork belly.  Mumblecore meh kale chips, letterpress bespoke before they sold out four loko master cleanse brunch readymade selvage single-origin coffee mcsweeney''s cardigan.  Fanny pack keffiyeh vegan, mixtape sustainable authentic bushwick.  Deep v wes anderson pop-up fap you probably haven''t heard of them.  High life etsy mumblecore narwhal, portland 3 wolf moon blog pop-up keffiyeh aesthetic flannel vegan wayfarers.  Small batch thundercats pitchfork wolf yr lo-fi craft beer, put a bird on it actually raw denim godard tumblr bushwick.', '239.99', NULL, '', NULL, 'boots, thick sole', 28, 1368050360, NULL, 1368417600, NULL, 0, 'product', 'p', NULL, NULL, 8, 2, 4, '1,5', 11, NULL, 'http://www.pickyourshoes.com/item.asp?itemname=38584', NULL),
(46, 6, 'Editorials Article', 'editorials-article', NULL, 'Before they sold out church-key tousled, viral hella freegan kogi direct trade  keytar craft beer skateboard shoreditch pickled cray.  Viral pork belly narwhal before they sold out, chambray plaid brooklyn small batch.  Pug +1 before they sold out, skateboard american apparel selvage cred umami tumblr irony actually.', 'Direct trade  actually deep v post-ironic single-origin coffee selfies.  Art party pork belly stumptown, +1 hella deep v keytar tousled high life neutra quinoa pickled lo-fi chambray you probably haven''t heard of them.  Narwhal gluten-free readymade fixie banjo farm-to-table.  Freegan polaroid brunch, marfa whatever williamsburg trust fund echo park craft beer yr mustache.  Pickled flannel etsy pork belly, lo-fi sartorial synth mcsweeney''s hashtag 3 wolf moon gastropub farm-to-table Austin quinoa.  Swag hoodie church-key, art party mixtape beard biodiesel pop-up.  Tumblr american apparel +1 blue bottle typewriter plaid meggings, vinyl ugh YOLO.', NULL, NULL, NULL, 1, 'editorial', 29, 1368213707, NULL, 1368158400, NULL, 0, 'article', 'p', 'e', NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(47, 6, 'shoesssss', 'shoesssss', 'sdhsh', 'rethrh', 'rethregher', '0.00', NULL, '', NULL, 'ertyertye', 30, 1368483164, NULL, 0, NULL, 1, 'product', 'p', NULL, NULL, 0, 0, 0, '0', 0, NULL, '', NULL),
(48, 6, 'funfunfun', 'funfunfun', NULL, '', '', NULL, NULL, NULL, 0, '', 30, 1368824401, NULL, 0, NULL, 0, 'photo', 'p', NULL, NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(49, 6, 'Test Photot', 'test-photot', NULL, 'fadsfadsf', 'gasfdasf', NULL, NULL, NULL, 1, 'dafdfds', 31, 1368827443, NULL, 1368676800, NULL, 0, 'photo', 'p', NULL, NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(50, 6, 'The Newest Article', 'the-newest-article', NULL, 'Newest article excerpt...', 'Newest article body', NULL, NULL, NULL, 1, 'newest, article', 32, 1369256425, NULL, 1369195200, NULL, 1, 'article', 'p', 'b', NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(51, 6, 'Official Article', 'official-article', NULL, 'This is the first official article of the site.', 'Keffiyeh shabby chic direct trade, put a bird on it Godard Echo Park Etsy typewriter McSweeney''s.  Intelligentsia butcher yr stumptown McSweeney''s.  Banksy trust fund four loko sriracha shoreditch.  Flexitarian 8-bit Odd Future cardigan Godard fap, Marfa pitchfork hella fixie.  Echo Park you probably haven''t heard of them food truck scenester iPhone.  Tattooed ugh dreamcatcher, skateboard farm-to-table fanny pack mlkshk Tonx asymmetrical scenester plaid messenger bag.  Semiotics kitsch forage meggings ethnic cornhole, readymade narwhal whatever viral authentic.', NULL, NULL, NULL, 5, 'OFFICIAL', 32, 1369340386, NULL, 1369281600, NULL, 0, 'article', 'p', 'b', NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(52, 6, 'Nike Lunar Pegasus 89', 'nike-lunar-pegasus-89', '3959204', 'The Nike Lunar Pegasus 89 comes in a navy, pink and orange colourway, and makes use of Nike''s most recent cushioning technology, the Lunarlon midsole.', 'Nike have repackaged one of their classic running silhouettes.\r\n\r\nThe Nike Lunar Pegasus 89 comes in a navy, pink and orange colourway, and makes use of Nike''s most recent cushioning technology, the Lunarlon midsole.\r\n\r\nNavy suede and mesh make up the upper part of the shoe alongside bright pink and orange accents, all finished off with neon pink contrast-stitching.\r\n\r\nThere is no release date for these at this moment in time, but you can ogle them in our gallery below and then head to the Nike website for more details.', '130.00', NULL, '', NULL, 'lunar, pegasus, nike', 33, 1369343804, NULL, 1369281600, NULL, 0, 'product', 'p', NULL, 'y', 5, 1, 2, '1,3,6,7', 10, NULL, '', NULL),
(53, 6, 'Adam''s Slider', 'adams-slider', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 34, 1369344161, NULL, 1369281600, NULL, 0, 'slider', 'p', NULL, NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(54, 6, 'Official Photo', 'official-photo', NULL, 'First official photo!', 'This is the newest photo on the site.', NULL, NULL, NULL, 3, '', 35, 1369345091, NULL, 1369281600, NULL, 0, 'photo', 'p', NULL, NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(55, 6, 'Official Video', 'official-video', NULL, 'Kai is the man.', 'sdfgbldfuhbgosdfbgoy bdfosy boduyfbg ob doyufbg ouyb sud fbgyofdgiuysdfbgh uiy ouyfdbg oudfg yg sdoufyg ldsug ly gldfg lysdg ludgfy u doy goyg oygouyg uyfu yfguoyguoyg uyg uygdfuysgfyg yusdfgyug ', NULL, NULL, NULL, 3, '', 36, 1369345389, NULL, 1369281600, NULL, 0, 'video', 'p', NULL, NULL, 0, 0, 0, '0', 0, NULL, NULL, 'http://www.youtube.com/watch?v=-Xa0NfCdLk4');

-- --------------------------------------------------------

--
-- Table structure for table `item_attachments`
--

DROP TABLE IF EXISTS `item_attachments`;
CREATE TABLE IF NOT EXISTS `item_attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) DEFAULT NULL,
  `attachment_id` int(11) DEFAULT NULL,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `entry_datetime` int(11) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=109 ;

--
-- Dumping data for table `item_attachments`
--

INSERT INTO `item_attachments` (`id`, `item_id`, `attachment_id`, `ordering`, `entry_datetime`, `deleted`) VALUES
(50, 8, 32, 0, 0, 0),
(51, 8, 33, 1, 0, 0),
(52, 8, 34, 2, 0, 0),
(53, 9, 35, 3, 0, 0),
(54, 9, 33, 4, 0, 0),
(55, 10, 36, 5, 0, 1),
(56, 9, 37, 6, 0, 1),
(57, 9, 38, 7, 0, 0),
(58, 9, 34, 8, 0, 0),
(59, 9, 39, 9, 0, 0),
(60, 9, 40, 10, 0, 0),
(61, 11, 33, 11, 0, 0),
(62, 11, 41, 12, 0, 0),
(63, 11, 42, 13, 0, 0),
(64, 12, 43, 14, 0, 0),
(65, 13, 44, 15, 0, 0),
(66, 15, 45, 16, 0, 1),
(67, 15, 46, 17, 0, 0),
(68, 14, 47, 18, 0, 0),
(69, 10, 48, 19, 0, 0),
(70, 17, 50, 20, 0, 0),
(71, 19, 51, 22, 0, 0),
(72, 19, 52, 21, 0, 0),
(73, 28, 53, 23, 0, 0),
(74, 29, 54, 24, 0, 0),
(75, 30, 55, 25, 0, 0),
(76, 31, 56, 26, 0, 0),
(77, 32, 57, 27, 0, 0),
(78, 33, 58, 28, 0, 0),
(79, 34, 59, 29, 0, 0),
(80, 35, 60, 30, 0, 0),
(81, 36, 61, 31, 0, 0),
(82, 37, 62, 32, 0, 0),
(83, 38, 63, 33, 0, 0),
(84, 39, 64, 34, 0, 0),
(85, 40, 65, 35, 0, 0),
(86, 41, 66, 36, 0, 0),
(87, 42, 67, 37, 0, 0),
(88, 43, 68, 38, 0, 0),
(89, 44, 69, 39, 0, 0),
(90, 45, 70, 40, 0, 0),
(91, 46, 36, 41, 0, 0),
(92, 51, 72, 42, NULL, 0),
(93, 52, 73, 43, NULL, 0),
(94, 53, 68, 44, NULL, 0),
(95, 53, 43, 45, NULL, 0),
(96, 53, 54, 46, NULL, 0),
(97, 53, 74, 47, NULL, 0),
(98, 51, 73, 48, NULL, 1),
(99, 29, 43, 48, NULL, 1),
(100, 29, 34, 49, NULL, 1),
(101, 29, 45, 50, NULL, 1),
(102, 29, 67, 51, NULL, 1),
(103, 29, 68, 52, NULL, 1),
(104, 29, 55, 53, NULL, 1),
(105, 54, 75, 48, NULL, 0),
(106, 49, 63, 49, NULL, 0),
(107, 48, 72, 50, NULL, 0),
(108, 55, 76, 51, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `stores`
--

DROP TABLE IF EXISTS `stores`;
CREATE TABLE IF NOT EXISTS `stores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `description` text,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `stores`
--

INSERT INTO `stores` (`id`, `title`, `description`, `ordering`, `deleted`) VALUES
(1, 'Chippewa', '', 0, 0),
(2, 'Sebago', '', 1, 0),
(3, 'Puma', '', 2, 0),
(4, 'Under Armour', '', 3, 0),
(5, 'CNCPTS', '', 4, 0),
(6, 'The Bay', '', 5, 0),
(7, 'Amazon', '', 6, 0),
(8, 'Foot Locker', '', 7, 0),
(9, 'K-Swiss', '', 8, 0),
(10, 'Karmaloop', '', 9, 0),
(11, 'Pick Your Shoes', '', 10, 0),
(12, 'The Pop Shop', '', 11, 0);

-- --------------------------------------------------------

--
-- Table structure for table `styles`
--

DROP TABLE IF EXISTS `styles`;
CREATE TABLE IF NOT EXISTS `styles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `description` text,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `styles`
--

INSERT INTO `styles` (`id`, `title`, `description`, `ordering`, `deleted`) VALUES
(1, 'Athletic', '', 0, 0),
(2, 'Boots', '', 1, 0),
(3, 'Loafers', '', 2, 0),
(4, 'Oxfords', '', 3, 0),
(5, 'Vegan', 'Shoes for vegans', 4, 0);

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

DROP TABLE IF EXISTS `subscribers`;
CREATE TABLE IF NOT EXISTS `subscribers` (
  `email` varchar(100) NOT NULL,
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscribers`
--

INSERT INTO `subscribers` (`email`) VALUES
('tyler@hortm.com');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `data` text,
  `admin` int(1) NOT NULL DEFAULT '0',
  `gender` varchar(10) DEFAULT NULL,
  `shoe_size` varchar(25) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `twitter` varchar(50) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `image` varchar(50) DEFAULT NULL,
  `entry_datetime` int(11) DEFAULT '0',
  `update_datetime` int(11) NOT NULL DEFAULT '0',
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `firstname`, `lastname`, `data`, `admin`, `gender`, `shoe_size`, `city`, `twitter`, `website`, `image`, `entry_datetime`, `update_datetime`, `deleted`) VALUES
(1, 'tylersavery', 'sdgdasfds', 'f5c38375cd3ffcbfe0957cb9ae6dce35', 'Tyler', 'Savery', '{"email":"fdfdsf@sdf","phone":"4169974264"}', 1, 'sgsdaf', 'sadgdsf', 'sdgsadf', 'asdfdsf', 'dsgdsaf', NULL, 0, 0, 0),
(2, 'dfad', 'dfsg', '1530d0e446ff74e728158fd5fe89e592', 'dfadsf', 'dsf', '', 0, 'dsfasd', 'fgdsfg', 'fdgdf', 'fdsg', 'fdsg', NULL, 0, 0, 1),
(3, 'adsfdasf', 'dsaf', '47df24e6aa353e1876e81388bfb24515', 'dasfadsf', 'das', '', 0, 'gfsdg', 'fdgf', 'fgs', 'fds', 'hdgfh', NULL, 0, 0, 1),
(4, 'asfdsf', 'retw', '0745da98f74d2927db1f7151bbdbdce1', 'gsdaf', 'das', '', 0, 'dsf', 'gfd', 'hgfdtr', 'bvcbc', 'sdgf', NULL, 0, 0, 1),
(5, 'adsfds', 'fdsg', 'eeab8d82f794c109fc842353426daee1', 'fgsdfg', 'fgds', '', 0, 'fdsg', 'fdsg', 'dfsg', 'dsfg', 'hdsfg', NULL, 0, 0, 1),
(6, 'adam', '', '0282c80acb3ef9ef52aad7ff1bd9a4fb', 'adam', 'gulyas', '', 0, '', '', '', '', '', NULL, 0, 0, 0),
(7, 'bobbyb', 'bobb@asdsa.com', 'f5c38375cd3ffcbfe0957cb9ae6dce35', 'bobby', 'b', '', 0, 'male', '12', 'Toronto', 'bobbyyhhh', 'hey.com', NULL, 0, 0, 0),
(8, 'heyaaasd', 'dfa@sd.com', 'f5c38375cd3ffcbfe0957cb9ae6dce35', 'adsf', 'sdfsdaf', NULL, 0, 'male', '21', 'Toronto', 'somet', 'dasdsad.com', NULL, 0, 0, 0),
(9, 'arny', NULL, 'e2c40727bbbefdda527baadf8d929f7a', 'Arnold', 'Palmer', 'My name is Arnold Pamler.', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
