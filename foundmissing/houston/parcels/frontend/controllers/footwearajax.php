<?php

namespace Frontend\Controllers;

class FootwearAjax extends \Frontend\Controllers\Frontend {
	
    public function __construct($uri, $data){
        parent::__construct($uri, $data);

        $this->data = $data;
		
		if (isset($_POST['type'])) {
			if ($_POST['type'] == 'get_categories') {
				$this->get_categories();
			} else if ($_POST['type'] == 'get_brands') {
				$this->get_brands();
			} else if ($_POST['type'] == 'get_brands_autocomplete') {
				$this->autocomplete_brand();
			} else if ($_POST['type'] == 'refine_search') {
				$this->refine_search();
			} else if ($_POST['type'] == 'add_favorites') {
				$this->add_favorites();
			} else if ($_POST['type'] == 'delete_favorites') {
				$this->delete_favorites();
			}
		}
    }

    public function controller(){
	
    }
	
	protected function get_categories() {
		$categories 	= \Category\Models\Category::all(array('select' => 'title', 'conditions' => 'deleted = 0'));
		$styles 		= \Style\Models\Style::all(array('conditions' => 'deleted = 0', 'order' => 'category_id'));
		$output 		= '<div class="row" style="border-bottom: 1px solid #F1F2F2;">';
		$sneakers		= array();
		$boots			= array();
		$dress			= array();
		$count			= 0;
		
		foreach ($categories as $cat) {
			$count++;
			
			if ($count < count($categories)) {
				$output .= '<div class="col-md-4" style="border-right: 1px solid #F1F2F2; padding-top: 5px; padding-bottom: 5px;">' . $cat->title . '</div>';
			} else {
				$output .= '<div class="col-md-4" style="padding-top: 5px; padding-bottom: 5px;">' . $cat->title . '</div>';
			}
		}
		
		$output .= '</div>';
		$output .= '<div class="row">';
		
		foreach ($styles as $style) {
			switch ($style->category_id) {
				case 1:
					$sneakers[] = array(
						'id' => $style->id,
						'title' => $style->title
					);
					break;
				
				case 2:
					$boots[] = array(
						'id' => $style->id,
						'title' => $style->title
					);
					break;
					
				case 3:
					$dress[] = array(
						'id' => $style->id,
						'title' => $style->title
					);
					break;
				
				default:
					break;
			}
		}
		
		// Output sneakers
		$output .= '<div class="col-md-4" style="border-right: 1px solid #F1F2F2"><ul>';
		foreach ($sneakers as $sneaker) {
			$output .= '<li class="styles_id" data-id="' . $sneaker['id'] . '">' . $sneaker['title'] . '</li>';
		}
		$output .= '</ul></div>';
		
		// Output Boots
		$output .= '<div class="col-md-4" style="border-right: 1px solid #F1F2F2"><ul>';
		foreach ($boots as $boot) {
			$output .= '<li class="styles_id" data-id="' . $boot['id'] . '">' . $boot['title'] . '</li>';
		}
		$output .= '</ul></div>';
		
		// Output Dress
		$output .= '<div class="col-md-4"><ul>';
		foreach ($dress as $shoe) {
			$output .=  '<li class="styles_id" data-id="' . $shoe['id'] . '">'. $shoe['title'] . '</li>';
		}
		$output .= '</ul></div>';
		
		$output .= '</div>';
		
		echo $output;
	}
	
	protected function get_brands() {
		$brands = \Brand\Models\Brand::all(array('conditions' => 'deleted = 0', 'order' => 'title asc'));
		$output = '<div class="row"><div class="col-md-12"><ul>';
		
		foreach ($brands as $brand) {
			$output .= '<li class="brand_id" data-id="' . $brand->id . '">' . $brand->title . '</li>';
		}
		
		$output .= '</ul></div></div>';
		
		echo $output;
	}
	
	protected function autocomplete_brand() {
		$brands = \Brand\Models\Brand::all(array('select' => 'id, title', 'conditions' => 'deleted = 0 AND title LIKE "' . $_POST['brand_name'] . '%"'));
		
		if (count($brands) > 0) {
		foreach ($brands as $brand) {
			echo '<span class="user_brand_id">' . $brand->title . '</span><br />';
		}
		} else {
			echo '<span>No results found</span>';
		}
	}
	
	protected function refine_search() {
	
		if (isset($_POST['brands'])) {
			$brands = array_map('ltrim', explode(',', $_POST['brands']));
			
			foreach ($brands as $brand) {
				$this->brand_result[] = array(
					'id' => \Brand\Models\Brand::get_brand_id_by_name($brand),
					'title' => $brand
				);
			}
		}
		
		if (isset($_POST['categories'])) {
			$styles	= array_map('ltrim', explode(',', $_POST['categories']));
			
			foreach ($styles as $style) {
				$this->styles_result[] = array(
					'id' => \Style\Models\Style::get_style_id_by_name($style),
					'title' => $style
				);
			}
		}
		
		if (isset($_POST['colors'])) {
			$colors	= array_map('ltrim', explode(',', $_POST['colors']));
			
			$this->colors = $colors;
		}
		
		$this->parse_search_results();
	}
	
	protected function get_shoes() {
		if ($this->brand_result[0]['id']) {
			$footwear = $this->get_shoes_by_brands();
			
			if ($this->styles_result[0]['id'] && $footwear[0]) {
			
				foreach ($footwear as $shoe) {
					$explode_style_id = explode(',', $shoe['style_id']);
					
					foreach ($this->styles_result as $style) {
						
						if (in_array($style['id'], $explode_style_id)) {
							$filtered_shoes[] = array(
								'id' => $shoe['id'],
								'brand_name' => $shoe['brand_name'],
								'model_name' => $shoe['model_name'],
								'price' => $shoe['price'],
								'colors' => $shoe['colors'],
								'slug' => $shoe['slug'],
								'image' => $shoe['image']
							);
							
							break;
						}
					}
				}
				$footwear = $filtered_shoes;
			}
			
			if ($this->colors[0] && $footwear[0]) {
				foreach ($footwear as $shoe) {
					$explode_color_id = explode(',', $shoe['color_id']);
					
					foreach ($this->colors as $color) {
						
						if (in_array($color, $explode_color_id)) {
							$filtered_shoes[] = array(
								'id' => $shoe['id'],
								'brand_name' => $shoe['brand_name'],
								'model_name' => $shoe['model_name'],
								'price' => $shoe['price'],
								'colors' => $shoe['colors'],
								'slug' => $shoe['slug'],
								'image' => $shoe['image']
							);
							
							break;
						}
					}
				}
				
				$footwear = $filtered_shoes;
			}
			
			$this->footwear = $footwear;
			
		} else if ($this->styles_result[0]['id']) {
			$footwear = $this->get_shoes_by_styles();
			
			if ($this->colors[0]) {
				foreach ($footwear as $shoe) {
					$explode_color_id = explode(',', $shoe['color_id']);
					
					foreach ($this->colors as $color) {
						
						if (in_array($color, $explode_color_id)) {
							$filtered_shoes[] = array(
								'id' => $shoe['id'],
								'brand_name' => $shoe['brand_name'],
								'model_name' => $shoe['model_name'],
								'price' => $shoe['price'],
								'colors' => $shoe['colors'],
								'slug' => $shoe['slug'],
								'image' => $shoe['image']
							);
							
							break;
						}
					}
				}
				
				$footwear = $filtered_shoes;
			}
			
			$this->footwear = $footwear;
			
		} else if ($this->colors[0]) {
			$this->footwear = $this->get_shoes_by_colors();
		} else {
			$footwear = \Footwear\Models\Footwear::all(array('conditions' => 'deleted = 0 AND publish = 1', 'order' => 'model_name'));
			
			foreach ($footwear as $shoe) {
			
				$color_names = '';
				$explode = explode(",", $shoe->color_id);
				$count = 1;
				
				foreach ($explode as $color) {
					if ($count == count($explode)) {
						$color_names .= \Color\Models\Color::get_color_name($color);
					} else {
						$color_names .= \Color\Models\Color::get_color_name($color) . '/';
					}
					
					$count++;
				}
				
				$this->footwear[] = array(
					'id' => $shoe->id,
					'brand_name' => \Brand\Models\Brand::get_brand_name_by_id($shoe->brand_id),
					'model_name' => $shoe->model_name,
					'price' => number_format($shoe->price, 2, '.', ''),
					'colors' => $color_names,
					'slug' => $shoe->slug,
					'image' => $this->get_cover_image($shoe->id)
				);
			}
		}
		
		$price = json_decode($_POST['price']);
		$this->min = (is_numeric($price->min)? $price->min : null);
		$this->max = (is_numeric($price->max)? $price->max : null);
	}
	
	private function get_shoes_by_brands() {
		$footwear 		= array();
		$query 			= '';
		$count 			= 1;
		
		foreach ($this->brand_result as $brand) {
			
			if ($count == count($this->brand_result)) {
				$query .= 'brand_id = ' . $brand['id'];
			} else {
				$query .= 'brand_id = ' . $brand['id'] . ' OR ';
			}
			
			$count++;
		}
		
		return $this->custom_query_shoe($query);
	}
	
	private function get_shoes_by_styles() {
		$query 			= '';
		$count 			= 1;
		
		foreach ($this->styles_result as $style) {
			if ($count == count($this->styles_result)) {
				$query .= 'style_id = "' . $style['id'] . '" OR style_id LIKE "' . $style['id'] . ',%" OR style_id LIKE "%,' . $style['id'] . ',%" OR style_id LIKE "%,' . $style['id'] . '"';
			} else {
				$query .= 'style_id = "' . $style['id'] . '" OR style_id LIKE "' . $style['id'] . ',%" OR style_id LIKE "%,' . $style['id'] . ',%" OR style_id LIKE "%,' . $style['id'] . '" OR ';
			}
			
			$count++;
		}
		
		return $this->custom_query_shoe($query);
	}
	
	private function get_shoes_by_colors() {
		$query 			= '';
		$count 			= 1;
		
		foreach ($this->colors as $color) {
			if ($count == count($this->colors)) {
				$query .= 'color_id = "' . $color . '" OR color_id LIKE "' . $color . ',%" OR color_id LIKE "%,' . $color . ',%" OR color_id LIKE "%,' . $color . '"';
			} else {
				$query .= 'color_id = "' . $color . '" OR color_id LIKE "' . $color . ',%" OR color_id LIKE "%,' . $color . ',%" OR color_id LIKE "%,' . $color . '" OR ';
			}
			
			$count++;
		}
		
		return $this->custom_query_shoe($query);
	}
	
	private function custom_query_shoe($query) {
		$shoes = \Footwear\Models\Footwear::all(array('conditions' => 'deleted = 0 AND publish = 1 AND ' . $query));
		
		foreach ($shoes as $shoe) {
			if ($shoe) {
				
				$color_names = '';
				$explode = explode(",", $shoe->color_id);
				$count = 1;
				
				foreach ($explode as $color) {
					if ($count == count($explode)) {
						$color_names .= \Color\Models\Color::get_color_name($color);
					} else {
						$color_names .= \Color\Models\Color::get_color_name($color) . '/';
					}
					
					$count++;
				}
				
				$footwear[] = array(
					'id' => $shoe->id,
					'brand_name' => \Brand\Models\Brand::get_brand_name_by_id($shoe->brand_id),
					'model_name' => $shoe->model_name,
					'price' => number_format($shoe->price, 2, '.', ''),
					'style_id' => $shoe->style_id,
					'color_id' => $shoe->color_id,
					'colors' => $color_names,
					'slug' => $shoe->slug,
					'image' => $this->get_cover_image($shoe->id)
				);
			}
		}
		
		return $footwear;
	}
	
	private function get_cover_image($id) {
		$item_attachments = \Footwear_Attachment\Models\Footwear_Attachment::first(array('select' => 'attachment_id', 'conditions' => 'deleted = 0 and footwear_id = "' . $id . '"'));
		$attachments = \Attachment\Models\Attachment::get_cover_attachment($item_attachments->attachment_id);

		return $attachments;
	}
	
	private function parse_search_results() {
		echo '<div class="row">
			<div class="footwear-page-fix"></div>
			<div class="outer-shoes">';
			
		$this->get_shoes();
		
		if ($this->footwear) {
			foreach ($this->footwear as $shoe) {
				if ($shoe['price'] >= $this->min) {
					if ($this->max) {
						if ($shoe['price'] <= $this->max) {
							echo '<div class="footwear_results">';
					
							if ($this->has_favorited($shoe['id'])) {
								echo '<div class="footwear_bookmark footwear_bookmarked" data-shoeid="' . $shoe['id'] . '" title="Unfavorite This"></div>';
							} else {
								echo '<div class="footwear_bookmark" data-shoeid="' . $shoe['id'] . '" title="Favorite This"></div>';
							}
							
							echo '<div class="row footwear_single_info">
										<div class="col-md-12"><a href="/footwear/' . $shoe['slug'] . '">' . $shoe['model_name'] . '</a> - ' . $shoe['colors'] . '<br /><span class="fm_grey">' . $shoe['brand_name'] . '</span><br />' . $shoe['price'] . '</div>
									  </div>
									  <img src="/images/footwear/' . $shoe['image'] . '" />';
									  
							echo '</div>'; // End Footwear_Results
						}
					} else {
						echo '<div class="footwear_results">';
						
						if ($this->has_favorited($shoe['id'])) {
							echo '<div class="footwear_bookmark footwear_bookmarked" data-shoeid="' . $shoe['id'] . '" title="Unfavorite This"></div>';
						} else {
							echo '<div class="footwear_bookmark" data-shoeid="' . $shoe['id'] . '" title="Favorite This"></div>';
						}
						
						echo '<div class="row footwear_single_info">
									<div class="col-md-12"><a href="/footwear/' . $shoe['slug'] . '">' . $shoe['model_name'] . '</a> - ' . $shoe['colors'] . '<br /><span class="fm_grey">' . $shoe['brand_name'] . '</span><br />' . $shoe['price'] . '</div>
								  </div>
								  <img src="/images/footwear/' . $shoe['image'] . '" />';
								  
						echo '</div>'; // End Footwear_Results
					}
				}
			}
		} else {
			echo '<div class="no_results">found nothing</div>';
		}
		
		echo  '</div></div>';
	}
	
	private function has_favorited($shoe_id) {
		$favs = \Favorites\Models\Favorites::first(array('conditions' => 'user_id = "' . $this->_user->id . '" AND item_id = "' . $shoe_id . '"'));
		
		if ($favs) {
			return true;
		} else {
			return false;
		}
	}
	
	private function add_favorites() {
		
		if ($this->_user) {
			$logged_in = true;
			$id = $_POST['id'];
			
			$fav = new \Favorites\Models\Favorites();
			
			$fav->user_id = $this->_user->id;
			$fav->item_id = $id;
			$fav->entry_datetime = time();
			
			$fav->save();
			
			$message = "Item favorited";
		} else {
			$logged_in = false;
			$message = 'You must be logged in to bookmark!';
		}
		
		echo json_encode(array(
			'status' => $logged_in,
			'message' => $message
		));
	}
	
	private function delete_favorites() {
		
		if ($this->_user) {
			$logged_in = true;
			$id = $_POST['id'];
			
			$fav = \Favorites\Models\Favorites::first(array('conditions' => 'user_id = "' . $this->_user->id . '" AND item_id = "' . $id . '"'));
			$fav->delete();

			$message = "Item removed";
		} else {
			$logged_in = false;
			$message = 'You must be logged in!';
		}
		
		echo json_encode(array(
			'status' => $logged_in,
			'message' => $message
		));
	}
}

?>