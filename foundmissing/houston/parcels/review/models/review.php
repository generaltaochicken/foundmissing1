<?php
namespace Review\Models;

class Review extends \Core\Models\Base_Model{

	protected static $manage_prefix = "/manage/reviews/";

	public function link_edit(){
		return self::$manage_prefix . "edit/" . $this->id;
	}

	public function link_delete(){
		return self::$manage_prefix . "delete/" . $this->id;
	}

	public function link_restore(){
		return self::$manage_prefix . "restore/" . $this->id;
	}

	public function link_new(){
		return self::$manage_prefix . "new";
	}

	public function link_all(){
		return self::$manage_prefix;
	}

	public function link_archive(){
		return self::$manage_prefix . "archive";
	}

	public function get_frontend_data(){
		return array(
			"id" => $this->id,
			"author" => $this->author,
			"title" => $this->title,
			"slug" => $this->slug,
			"excerpt" => $this->excerpt,
			"body" => $this->body,
			"shoe_id" => $this->shoe_id,
			"entry_datetime" => $this->entry_datetime,
		);
	}
	
	public function get_main_image() {
		$item_attachment = \Footwear_attachment\Models\Footwear_attachment::first(array('conditions' => 'deleted = 0 AND footwear_id = ' . $this->shoe_id));
		$attachment = \Attachment\Models\Attachment::first(array('conditions' => 'deleted = 0 AND id = ' . $item_attachment->attachment_id));
		
		return $attachment->image_filename;
	}
}