<?php

namespace Frontend\Controllers;

class Frontend extends \Core\Controllers\Base_Controller {
	
	protected $_user;
	
    public $links_data 				= array();
    public $social_links 			= array();
	public $logged_in 				= false;
	
	public $logged_in_user;
    public $pages;
	public $pages2;
    public $dev;
	public $user_data;
	public $user_points;

	//public $domain_url = 'http://24.212.213.222:7755/';
	public $domain_url = 'http://foundmissing.ca/';
	
	public function __construct($uri, $data){
        parent::__construct($uri, $data);
		
        $this->title = "F O U N D M I S S I N G";

        $this->dev = DEV;

        $this->add_asset('css', 'reset.css', 'core');
        $this->add_asset('js', 'jquery.min.js', 'core');
		
		$this->add_asset('css', 'bootstrap.min.css', true);
		$this->add_asset('css', 'font-awesome.min.css', true);
		$this->add_asset('css', 'bootstrap-theme.min.css', true);
		$this->add_asset('js', 'bootstrap.js', true);
		$this->add_asset('js', 'jc.js', true);
		
        //$this->add_asset('js', 'master.js', true);
		$this->add_asset('css', 'master.css', true);

        // Social links for footer
        $conditions = "deleted = 0 AND
                       k = 'facebook' OR
                       k = 'instagram' OR
					   k = 'tumblr' OR
                       k = 'twitter' OR
					   k = 'youtube'";

        $links = \Content\Models\Content::all(array("conditions" => $conditions, 'order' => 'k ASC'));

        foreach($links as $link){
            $this->links_data[] = array(
                'social_link' => $link->v
            );
        }

        $this->social_links = array(
            'facebook' 	=> $this->links_data[0]['social_link'],
            'instagram' 	=> $this->links_data[1]['social_link'],
			'tumblr' 		=> $this->links_data[2]['social_link'],
            'twitter' 		=> $this->links_data[3]['social_link'],
            'youtube' 		=> $this->links_data[4]['social_link']
        );

        $this->check_login();

        $pages = \Page\Models\Page::all(array('conditions' => 'deleted = 0'));
		$count = 0;
		
        foreach($pages as $page){
			if ($count < 4) {
				$this->pages[] = array(
					'title' => $page->title,
					'slug' => $page->slug
				);
			} else {
				$this->pages2[] = array(
					'title' => $page->title,
					'slug' => $page->slug
				);
			}
			$count++;
        }
		
		// Get notifications
		/*if ($this->logged_in) {
			$notifications = \Notification\Models\Notification::all(array("conditions" => "is_unread = 1 AND recipient_id = " . $this->_user->id));
			
			if (!empty($notifications)) {
				$this->has_notification = true;
				$this->notification_count = count($notifications);
				
				$notification_id = array();
				
				foreach ($notifications as $notify) {
					$notification_id[] = $notify->id;
				}
				
				$this->notify_ids = json_encode($notification_id);
				
			}
		}*/
		
		$colors = \Color\Models\Color::all(array("select" => 'id, title, hex', 'conditions' => 'deleted = 0'));
		
		foreach ($colors as $color) {
			$this->color[] = array(
				'id' => $color->id,
				'title' => $color->title,
				'hex' => $color->hex
			);
		}
    }

    protected function check_login() {

        $user = new \User\Models\User();

        if(isset($_SESSION[$user->login_session_name])){
            $this->_user = $user->get_logged_in_user();
			$this->user_data = array(
				'id' => $this->_user->id,
				'username' => $this->_user->username,
				'fullname' => $this->_user->fullname(),
				'image' => '/uploads/images/' . $this->_user->image,
				'karma' => $this->_user->karma
			);
        }
		
		if ($this->user_data['karma'] > 0) {
			$this->user_data['karma'] = '+' . $this->user_data['karma'];
		}
		
		if ($this->_user->image == null) {
			$this->user_data['image'] = '/images/default_avatar.png';
		}
		
        if($this->_user){
        	$this->logged_in = true;
			$this->logged_in_user = $this->_user->id;
        }
    }
	
	public function time_elapsed_string($ptime) {
		$etime = time() - $ptime;

		if ($etime < 1)
		{
			return '0 seconds';
		}

		$a = array( 12 * 30 * 24 * 60 * 60  =>  'year',
					30 * 24 * 60 * 60       =>  'month',
					24 * 60 * 60            =>  'day',
					60 * 60                 =>  'hour',
					60                      =>  'minute',
					1                       =>  'second'
					);

		foreach ($a as $secs => $str)
		{
			$d = $etime / $secs;
			if ($d >= 1)
			{
				$r = round($d);
				return $r . ' ' . $str . ($r > 1 ? 's' : '') . ' ago';
			}
		}
	}
	
	public function get_facebook_likes($query) {
		$url = 'https://api.facebook.com/method/fql.query?query=select%20like_count%20from%20link_stat%20where%20url=%27' . $query . '%27&format=json';
		$contents = file_get_contents($url);
		
		return json_decode($contents, true);
	}
	
	public function get_facebook_shares($query) {
		$contents = file_get_contents('http://graph.facebook.com/?id=' . $query);
		
		return json_decode($contents, true);
	}
	
	public function get_stumbleupon_views($query) {
		$contents = file_get_contents("http://www.stumbleupon.com/services/1.01/badge.getinfo?url=" . $query);
		$views = json_decode($contents);
		
		if ($views->result->views != null) {
			return $views->result->views;
		} else {
			return 0;
		}
	}
	
	public function get_google_plus($query) {
		$contents = file_get_contents("https://plusone.google.com/_/+1/fastbutton?url=" . urlencode($query));
		
		preg_match('/window\.__SSR = {c: ([\d]+)/', $contents, $matches);
		if(isset($matches[0])) {
			return (int) str_replace( 'window.__SSR = {c: ', '', $matches[0]);
		}
		return 0;
	}
	
	public function get_tweets($query){
		$contents = file_get_contents("http://urls.api.twitter.com/1/urls/count.json?url=" . urlencode($query));
		$tweets = json_decode($contents, true);
		$count 	= $tweets['count'];
		return $count;
	}
}