<?php
namespace Comment_karma\Controllers;

class Archive extends \Manage\Controllers\Manage{

	private $comments_karma;

	public $comments_karma_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Deleted Comments_karma";

		$this->comments_karma = \Comment_karma\Models\Comment_karma::all(array("conditions" => "deleted = 1"));
		$this->archive_button = array("url" => \Comment_karma\Models\Comment_karma::link_all(), "title" => "Back");
		foreach($this->comments_karma as $comment_karma){
			$this->has_data = true;
			$this->comments_karma_data[] = array(
				"id" => $comment_karma->id,
				"type" => $comment_karma->type,
				"comment_id" => $comment_karma->comment_id,
				"user_id" => $comment_karma->user_id,
				"entry_datetime" => $comment_karma->entry_datetime,
				"link_restore" => $comment_karma->link_restore(),
			);
		}

	}

	public function controller(){
		$this->set_view("Comment_karma\Views\All");
	}

}

