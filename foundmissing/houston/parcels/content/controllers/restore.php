<?php
namespace Content\Controllers;

class Restore extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$content = \Content\Models\Content::find($data["content_id"]);

		if($content){
			$content->deleted = 0;
			$content->save();
			$this->add_flash(array("message" => "Content has been restored!"));

			redirect_to($content->link_all());
		}
	}
}
