<?php
namespace Newsletter\Controllers;

class All extends \Manage\Controllers\Manage{

	private $newsletters;

	public $newsletters_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Newsletters";
		$this->link_new = \Newsletter\Models\Newsletter::link_new();

		$this->newsletters = \Newsletter\Models\Newsletter::all(array("conditions" => "deleted = 0" , "order" => "ordering"));
		$archived = \Newsletter\Models\Newsletter::count(array("conditions" => "deleted = 1"));
		if($archived > 0){
			$this->archive_button = array("url" => \Newsletter\Models\Newsletter::link_archive(), "title" => "Archive");
		}

		foreach($this->newsletters as $newsletter){
			$this->has_data = true;
			$this->newsletters_data[] = array(
				"id" => $newsletter->id,
				"entry_datetime" => $newsletter->entry_datetime,
				"title" => $newsletter->title,
				"body" => $newsletter->body,
				"link_edit" => $newsletter->link_edit(),
				"link_delete" => $newsletter->link_delete(),
				"ordering" => $newsletter->ordering,
			);
		}

	}

	public function controller(){
		$this->set_view("Newsletter\Views\All");
	}

}

