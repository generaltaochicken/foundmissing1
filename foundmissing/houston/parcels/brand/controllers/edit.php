<?php
namespace Brand\Controllers;

class Edit extends \Manage\Controllers\Manage{

	private $brand;

	public $page_title;
	public $link_back;
	public $form;
	public $existing;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		if(isset($_POST["submit"])){
			$this->submit_form();
		}

		if(isset($data["brand_id"])){
			$this->brand = \Brand\Models\Brand::find($data["brand_id"]);
			$this->page_title = "Editing Brand";
		} else {
			$this->brand = new \Brand\Models\Brand;
			$this->page_title = "New Brand";
		}

		$this->link_back = $this->brand->link_all();

		$form = new \Form\Models\Form("edit", "POST", "", "well");

		$item = new \Form\Models\FormItemHidden(array("id" => "id", "title" => "Id", "value" => $this->brand->id));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "title", "title" => "Title", "value" => $this->brand->title));
		$form->add_item($item);

		$item = new \Form\Models\FormItemTextarea(array("id" => "description", "title" => "Description", "value" => $this->brand->description));
		$form->add_item($item);

		$item = new \Form\Models\FormItemSubmit(array("value" => "Save", "id" => "submit", "class" => "btn btn-primary", "cancel_url" => $this->brand->link_all()));
		$form->add_item($item);

		$this->form = $form->render();

	}

	public function controller(){
		$this->set_view("Brand\Views\Edit");
	}

	private function submit_form(){

		if($_POST["id"] > 0){
			$brand = \Brand\Models\Brand::find($_POST["id"]);
			$flash_msg = "Brand has been updated successfully.";
		} else {
			$brand = new \Brand\Models\Brand;
			$flash_msg = "Brand has been created successfully.";
			$brand->ordering = \Brand\Models\Brand::get_next_ordering_number();
		}

		$brand->title = $_POST["title"];
		$brand->description = $_POST["description"];

		$brand->save();

		$this->add_flash(array("message" => $flash_msg, "type" => "success", "Heading" => "Great!"));
		
		redirect_to($brand->link_edit());
	}

}
