<?php
namespace Gallery_attachment\Controllers;

class Archive extends \Manage\Controllers\Manage{

	private $gallery_attachments;

	public $gallery_attachments_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Deleted Gallery_attachments";

		$this->gallery_attachments = \Gallery_attachment\Models\Gallery_attachment::all(array("conditions" => "deleted = 1"));
		$this->archive_button = array("url" => \Gallery_attachment\Models\Gallery_attachment::link_all(), "title" => "Back");
		foreach($this->gallery_attachments as $gallery_attachment){
			$this->has_data = true;
			$this->gallery_attachments_data[] = array(
				"id" => $gallery_attachment->id,
				"gallery_id" => $gallery_attachment->gallery_id,
				"attachment_id" => $gallery_attachment->attachment_id,
				"entry_datetime" => $gallery_attachment->entry_datetime,
				"link_restore" => $gallery_attachment->link_restore(),
			);
		}

	}

	public function controller(){
		$this->set_view("Gallery_attachment\Views\All");
	}

}

