<?php
namespace Frontpage\Controllers;

class Edit extends \Manage\Controllers\Manage{

	private $frontpage;

	public $page_title;
	public $link_back;
	public $form;
	public $existing;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		if(isset($_POST["submit"])){
			$this->submit_form();
		}

		if(isset($data["frontpage_id"])){
			$this->frontpage = \Frontpage\Models\Frontpage::find($data["frontpage_id"]);
			$this->page_title = "Editing Frontpage";
		} else {
			$this->frontpage = new \Frontpage\Models\Frontpage;
			$this->page_title = "New Frontpage";
		}

		$this->link_back = $this->frontpage->link_all();

		$form = new \Form\Models\Form("edit", "POST", "", "well");

		$item = new \Form\Models\FormItemHidden(array("id" => "id", "title" => "Id", "value" => $this->frontpage->id));
		$form->add_item($item);

		$item = new \Form\Models\FormItemCheckbox(array("id" => "slider", "title" => "Add to Slider", "value" => $this->frontpage->slider));
		$form->add_item($item);

		$item = new \Form\Models\FormItemSelect(array("id" => "position", "title" => "Position", "options" => $this->get_positions_options($this->frontpage->position)));
		$form->add_item($item);

		$item = new \Form\Models\FormItemSelect(array("id" => "type", "title" => "Type", "options" => $this->get_type_options($this->frontpage->type)));
		$form->add_item($item);

		$item = new \Form\Models\FormItemSelect(array("id" => "parent_id", "title" => "Name", "options" => $this->get_type_title()));
		$form->add_item($item);

		$item = new \Form\Models\FormItemSubmit(array("value" => "Save", "id" => "submit", "class" => "btn btn-primary", "cancel_url" => $this->frontpage->link_all()));
		$form->add_item($item);

		$this->form = $form->render();

	}

	public function controller(){
		$this->set_view("Frontpage\Views\Edit");
	}

	private function submit_form(){
		if($_POST["id"] > 0){
			$frontpage = \Frontpage\Models\Frontpage::find($_POST["id"]);
			$flash_msg = "Frontpage has been updated successfully.";
		} else {
			$frontpage = new \Frontpage\Models\Frontpage;
			$flash_msg = "Frontpage has been created successfully.";
			$frontpage->ordering = \Frontpage\Models\Frontpage::get_next_ordering_number();
		}
		
		if (empty($_POST['position'])) {
			$_POST["position"] = 0;
		}
		
		if (empty($_POST['slider'])) {
			$_POST['slider'] = 0;
		}

		$frontpage->slider = $_POST["slider"];
		$frontpage->position = $_POST["position"];
		$frontpage->type = $_POST["type"];
		$frontpage->parent_id = $_POST["parent_id"];

		$frontpage->save();
		$this->add_flash(array("message" => $flash_msg, "type" => "success", "Heading" => "Great!"));
		redirect_to($frontpage->link_all());
	}
	
	private function get_positions_options($current){
		$position = array(
			array('value' => 1, 'title' => 'first box', 'selected' => 0),
			array('value' => 2, 'title' => 'second box', 'selected' => 0),
			array('value' => 3, 'title' => 'third box', 'selected' => 0),
			array('value' => 4, 'title' => 'fourth box', 'selected' => 0),
			array('value' => 5, 'title' => 'fifth box', 'selected' => 0)
		);
		
		for ($i = 0; $i < count($position); $i++) {
			if ($current == $position[$i]['value']) {
				$position[$i]['selected'] = 1;
			}
		}
		
		return $position;
	}
	
	private function get_type_options($current){
		$type = array(
			array('value' => 'article', 'title' => 'Article', 'selected' => 0),
			array('value' => 'review', 'title' => 'Review', 'selected' => 0),
			array('value' => 'interview', 'title' => 'Interview', 'selected' => 0),
			array('value' => 'gallery', 'title' => 'Gallery', 'selected' => 0),
		);
		
		for ($i = 0; $i < count($type); $i++) {
			if ($current == $type[$i]['value']) {
				$type[$i]['selected'] = 1;
			}
		}
		
		return $type;
	}
	
	private function get_type_title() {
		$options = array();
		
		if ($this->frontpage->id != null) {
			switch($this->frontpage->type) {
				case 'article':
					$articles = \Article\Models\Article::all(array('conditions' => 'deleted = 0 AND publish_date > 1', 'order' => 'id DESC'));
					break;
				case 'review':
					$articles = \Review\Models\Review::all(array('conditions' => 'deleted = 0 AND publish_date > 1', 'order' => 'id DESC'));
					break;
				case 'interview':
					$articles = \Interview\Models\Interview::all(array('conditions' => 'deleted = 0 AND publish_date > 1', 'order' => 'id DESC'));
					break;
				case 'gallery':
					$articles = \Gallery\Models\Gallery::all(array('conditions' => 'deleted = 0 AND publish_date > 1', 'order' => 'id DESC'));
					break;
				default:
					break;
			}
		} else {
			$articles = \Article\Models\Article::all(array('conditions' => 'deleted = 0', 'order' => 'id DESC'));
		}
		
		foreach ($articles as $article) {
			$option = array(
				'value' => $article->id, 'title' => ucwords($article->title), 'selected' => 0
			);
			
			$options[] = $option;
		}
		
		for ($i = 0; $i < count($options); $i++) {
			if ($this->frontpage->parent_id == $options[$i]['value']) {
				$options[$i]['selected'] = 1;
			}
		}
		
		return $options;
	}

}
