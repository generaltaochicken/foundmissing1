<?php
namespace Comment\Models;

class Comment extends \Core\Models\Base_Model{

	protected static $manage_prefix = "/manage/comments/";

	public function link_edit(){
		return self::$manage_prefix . "edit/" . $this->id;
	}

	public function link_delete(){
		return self::$manage_prefix . "delete/" . $this->id;
	}

	public function link_restore(){
		return self::$manage_prefix . "restore/" . $this->id;
	}

	public function link_new(){
		return self::$manage_prefix . "new";
	}

	public function link_all(){
		return self::$manage_prefix;
	}

	public function link_archive(){
		return self::$manage_prefix . "archive";
	}

	public function get_frontend_data(){
		return array(
			"id" => $this->id,
			"parent_id" => $this->parent_id,
			"user_id" => $this->user_id,
			"comment_body" => $this->comment_body,
			"replied_to" => $this->replied_to,
			"sub_replied_to" => $this->sub_replied_to,
			"entry_datetime" => $this->entry_datetime,
			"update_datetime" => $this->update_datetime,
			"post_type" => $this->post_type,
			"deleted" => $this->deleted,
			"ordering" =>$this->ordering
		);
	}
	
	public function getCommentCount($parent_id, $post_type) {
		$count = \Comment\Models\Comment::all(array('conditions' => 'deleted = 0 AND post_type = ' . $post_type . ' AND parent_id = "' . $parent_id . '"'));
		
		return count($count);
	}
	
	public function getCommentsByID($parent_id, $post_type) {
		$this->parent_id = $parent_id;
		$this->post_type = $post_type;
		$this->user = new \User\Models\User;
		
		$this->all_comments = \Comment\Models\Comment::all(array('conditions' => 'deleted = 0 AND post_type = ' . $this->post_type . ' AND parent_id = "' . $this->parent_id . '" AND replied_to = 0'));
		
		$replies = \Comment\Models\Comment::getCommentReplies();
		$comments = \Comment\Models\Comment::get_all_comments();
		
		for ($i = 0; $i <= count($comments); $i++) {
			foreach ($replies as $reply) {
				if ($comments[$i]['id'] == $reply['replied_to']) {
					if ($reply['sub_replied_to']) {
						$parent_user = \Comment\Models\Comment::first(array('conditions' =>'id = ' . $reply['sub_replied_to']));
						$sub_parent = \User\Models\User::find($parent_user->user_id);
					}
					
					$comments[$i]['replies'][] = array(
						'id' => $reply['id'],
						'user_id' => $reply['user_id'],
						'fullname' => $reply['fullname'],
						'username' => $reply['username'],
						'user_image' => $reply['user_image'],
						'comment_body' => $reply['comment_body'],
						'comment_karma' => $reply['comment_karma'],
						'replied_to' => $reply['replied_to'],
						'sub_replied_to' => $reply['sub_replied_to'],
						'entry_datetime' => $reply['entry_datetime'],
						'update_datetime' => $reply['update_datetime'],
						'user_voted_up_reply' => $reply['user_voted_up_reply'],
						'user_voted_down_reply' => $reply['user_voted_down_reply'],
						'users_reply' => $reply['users_reply'],
						'parent_user' => ucwords($comments[$i]['fullname']),
						'parent_username' => $comments[$i]['username'],
						'child_user' => ucwords($sub_parent->firstname . ' ' . $sub_parent->lastname),
						'child_username' => $sub_parent->username
					);
				}
			}
		}
		
		return $comments;
	}
	
	private function getCommentReplies() {
		$replies = \Comment\Models\Comment::all(array('conditions' => 'deleted = 0 AND post_type = ' . $this->post_type . ' AND parent_id = "' . $this->parent_id . '" AND replied_to > 0'));
		
		$reply_array = array();
		
		foreach ($replies as $reply) {
			$user_info = $this->user->get_user_info_by_id($reply->user_id);
			
			// Get karma count for the comment
			$karma_count = 0;
			$karma = \Comment_Karma\Models\Comment_Karma::all(array('conditions' => 'deleted = 0 AND parent_id = ' . $this->parent_id . ' AND post_type = ' . $this->post_type . ' AND comment_id = ' . $reply->id));
			
			foreach ($karma as $karm) {
				if ($karm != null) {
					$karma_count += $karm->type;
					
					if ($karm->user_id == $this->current_user) {
						if ($karm->type == 1) {
							$up = true;
						} else if ($karm->type == -1) {
							$down = true;
						}
					}
				}
			}
			
			if ($reply->user_id == $this->current_user) {
				$their_own = true;
			} else {
				$their_own = false;
			}
			
			$reply_array[] = array(
				'id' => $reply->id,
				'user_id' => $reply->user_id,
				'fullname' => $user_info['fullname'],
				'username' => $user_info['username'],
				'user_image' => '/uploads/images/' . $user_info['image'],
				'comment_body' => $reply->comment_body,
				'comment_karma' => $karma_count,
				'replied_to' => $reply->replied_to,
				'sub_replied_to' => $reply->sub_replied_to,
				'entry_datetime' =>  $this->time_elapsed_string($reply->entry_datetime),
				'update_datetime' => $reply->update_datetime,
				'user_voted_up_reply' => $up,
				'user_voted_down_reply' => $down,
				'users_reply' => $their_own
			);
			
			$up = false;
			$down = false;
		}
		
		return $reply_array;
	}
	
	private function get_all_comments() {
		$comments = array();
		
		foreach ($this->all_comments as $comment) {
			$karma_count = 0;
			$user_info = $this->user->get_user_info_by_id($comment->user_id);
			
			$karma = \Comment_Karma\Models\Comment_Karma::all(array('conditions' => 'deleted = 0 AND parent_id = ' . $this->parent_id . ' AND post_type = ' . $this->post_type . ' AND comment_id = ' . $comment->id));
			
			foreach ($karma as $karm) {
				if ($karm != null) {
					$karma_count += $karm->type;
					
					if ($karm->user_id == $this->current_user) {
						if ($karm->type == 1) {
							$up = true;
						} else if ($karm->type == -1) {
							$down = true;
						}
					}
				}
			}
			
			if ($comment->user_id == $this->current_user) {
				$is_own = true;
			} else {
				$is_own = false;
			}
			
			$comments[] = array(
				'id' => $comment->id,
				'user_id' => $comment->user_id,
				'fullname' => $user_info['fullname'],
				'username' => $user_info['username'],
				'user_image' => '/uploads/images/' . $user_info['image'],
				'comment_body' => $comment->comment_body,
				'comment_karma' => $karma_count,
				'replied_to' => $comment->replied_to,
				'sub_replied_to' => $comment->sub_replied_to,
				'entry_datetime' =>  $this->time_elapsed_string($comment->entry_datetime),
				'update_datetime' => $comment->update_datetime,
				'user_voted_up' => $up,
				'user_voted_down' => $down,
				'replies' => '',
				'users_comment' => $is_own
			);
			
			$up = false;
			$down = false;
			$is_own = false;
		}
		
		return $comments;
	}
	
}