<?php
namespace Comment\Controllers;

class Archive extends \Manage\Controllers\Manage{

	private $comments;

	public $comments_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Deleted Comments";

		$this->comments = \Comment\Models\Comment::all(array("conditions" => "deleted = 1"));
		$this->archive_button = array("url" => \Comment\Models\Comment::link_all(), "title" => "Back");
		foreach($this->comments as $comment){
			$this->has_data = true;
			$this->comments_data[] = array(
				"id" => $comment->id,
				"parent_id" => $comment->parent_id,
				"user_id" => $comment->user_id,
				"comment_body" => $comment->comment_body,
				"replied_to" => $comment->replied_to,
				"entry_datetime" => $comment->entry_datetime,
				"update_datetime" => $comment->update_datetime,
				"post_type" => $comment->post_type,
				"link_restore" => $comment->link_restore(),
			);
		}

	}

	public function controller(){
		$this->set_view("Comment\Views\All");
	}

}

