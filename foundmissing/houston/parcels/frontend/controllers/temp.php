<?php
namespace Frontend\Controllers;

class Temp extends \Frontend\Controllers\Frontend {
    
	private $data;
	
    public function __construct($uri, $data) {
        parent::__construct($uri, $data);
		
		$this->title = 'Reviews | FM';
		
	}
	
	public function controller() {
		$this->set_view('Frontend\Views\Temp');
	}
	
}