<?php
namespace Category\Controllers;

class Restore extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$Category = \Category\Models\Category::find($data["Category_id"]);

		if($Category){
			$Category->deleted = 0;
			$Category->save();
			$this->add_flash(array("message" => "Category has been restored!"));

			redirect_to($Category->link_all());
		}
	}
}
