<?php

namespace Manage\Controllers;

class NewParcel extends \Manage\Controllers\Manage{

	public $permission_error = false;

	public function __construct($uri, $data){
        parent::__construct($uri, $data);

        $this->add_asset('js', 'newparcel.js', 'manage');

        if(!$this->check_permissions(PARCEL_ROOT)){
        	$this->permission_error = "<strong>Permission Error.</strong> Please ensure " . PARCEL_ROOT . " is writable. <br /><strong>Run:</strong> <em>sudo chmod 777 " . PARCEL_ROOT . "</em>";
        }

        if(!$this->check_permissions(CONFIG_ROOT.'app.json')){
            $this->permission_error .= "<br /><strong>Permission Error.</strong> Please ensure " . CONFIG_ROOT . 'app.json ' . " is writable. <br /><strong>Run:</strong> <em>sudo chmod 777 " . CONFIG_ROOT . 'app.json</em>';
        }

    }


    public function controller(){

    	$this->set_view('Manage\Views\NewParcel');

    }

    protected function check_permissions($dir){

    	if(is_writable($dir)){
    		return true;
    	} else {
    		return false;
    	}

    }

}
