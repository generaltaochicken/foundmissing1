<?php
namespace Subscriber\Controllers;

class All extends \Manage\Controllers\Manage{

	private $subscribers;

	public $subscribers_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Subscribers";
		
		if (isset($_GET["add_users"]) && $_GET["add_users"] == 1) {
			$user_emails = \User\Models\User::all(array('conditions' => 'deleted = 0', 'select' => 'email'));
			
			foreach ($user_emails as $email) {
				// Does it exist?
				$existence = \Subscriber\Models\Subscriber::find('all', array('conditions' => 'email = "' . $email->email . '"'));
				
				if (!$existence) {
					$subscriber = new \Subscriber\Models\Subscriber;
					$subscriber->ordering = \Subscriber\Models\Subscriber::get_next_ordering_number();
					$subscriber->entry_datetime = time();
					$subscriber->email = $email->email;
					
					$subscriber->save();
				}
			}
		}
		
		$this->link_new = \Subscriber\Models\Subscriber::link_new();

		$this->subscribers = \Subscriber\Models\Subscriber::all(array("conditions" => "deleted = 0" , "order" => "ordering"));
		$archived = \Subscriber\Models\Subscriber::count(array("conditions" => "deleted = 1"));
		if($archived > 0){
			$this->archive_button = array("url" => \Subscriber\Models\Subscriber::link_archive(), "title" => "Archive");
		}

		foreach($this->subscribers as $subscriber){
			$this->has_data = true;
			$this->subscribers_data[] = array(
				"id" => $subscriber->id,
				"entry_datetime" => $subscriber->entry_datetime,
				"email" => $subscriber->email,
				"link_edit" => $subscriber->link_edit(),
				"link_delete" => $subscriber->link_delete(),
				"ordering" => $subscriber->ordering,
			);
		}

	}

	public function controller(){
		$this->set_view("Subscriber\Views\All");
	}

}

