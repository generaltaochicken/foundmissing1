<?php

namespace Frontend\Controllers;

class Footwear extends \Frontend\Controllers\Frontend {
	
    public function __construct($uri, $data){
        parent::__construct($uri, $data);

        $this->data = $data;
		
		if (isset($this->data['slug'])) {
			
			$this->footwear = \Footwear\Models\Footwear::first(array('conditions' => 'deleted = 0 AND slug = "' . $this->data['slug'] . '"'));
			$this->set_values();
			
			$this->title = $this->brand . ' ' . $this->footwear->model_name . ' | FM';
			
			if (strpos($this->link, 'deadstock')) {
				$this->store = 'livestock';
			} else if (strpos($this->link, 'harryrosen')) {
				$this->store = 'harry rosen';
			} else if (strpos($this->link, 'oliberte')) {
				$this->store = 'Oliberté';
			}
			
			if ($this->footwear->id == null) {
				redirect_to('/404');
			}
		}
		
	}
	
	public function controller(){
        $this->set_view('Frontend\Views\Footwear');
    }
	
	private function set_values() {
		$this->brand = \Brand\Models\Brand::get_brand_name_by_id($this->footwear->brand_id);
		$this->name = $this->footwear->model_name;
		$this->description = $this->footwear->description;
		$this->cover = $this->get_cover_image();
		$this->release_date = time_to_friendly_date($this->footwear->release_date, 'Y.m.d');
		$this->rating = $this->footwear->rating;
		$this->tags = ucwords($this->footwear->tags);
		$this->price = number_format($this->footwear->price, 2, '.', '');
		$this->brand_description = \Brand\Models\Brand::get_brand_description($this->footwear->brand_id);
		$this->category = $this->get_style();
		$this->images = $this->get_images();
		$this->colors = $this->get_colors();
		$this->link = $this->footwear->external;
	}
	
	private function get_cover_image() {
		$item_attachments = \Footwear_Attachment\Models\Footwear_Attachment::first(array('select' => 'attachment_id', 'conditions' => 'deleted = 0 and footwear_id = "' . $this->footwear->id . '"'));
		$attachments = \Attachment\Models\Attachment::get_cover_attachment($item_attachments->attachment_id);

		return $attachments;
	}
	
	private function get_style() {
		$styles = explode(',', $this->footwear->style_id);
		$count = 1;
		$result_style = '';
		
		foreach ($styles as $style) {
			
			if ($count == count($styles)) {
				$result_style .= \Style\Models\Style::get_style_name_by_id($style);
			} else {
				$result_style .= \Style\Models\Style::get_style_name_by_id($style) . ', ';
			}
			
			$count++;
		}
		
		return $result_style;
	}
	
	private function get_images() {
		$fw_attachments = \Footwear_Attachment\Models\Footwear_Attachment::find('all', array('conditions' => 'footwear_id = ' . $this->footwear->id));
		
		foreach ($fw_attachments as $fwa) {
			$attachment = \Attachment\Models\Attachment::find($fwa->attachment_id);
			$fw_results[] = array(
				'image' => $attachment->image_filename
			);
		}
		
		return $fw_results;
	}
	
	private function get_colors() {
		$colors = $this->footwear->color_id;
		$colors = explode(',', $colors);
		
		foreach ($colors as $color) {
			$color_info = \Color\Models\Color::find($color);
			
			if ($color_info->hex == '#FFFFFF' || $color_info->hex == '#ffffff') {
				$is_white = true;
			} else {
				$is_white = false;
			}
			
			$color_array[] = array(
				'name' => $color_info->title,
				'hex' => $color_info->hex,
				'white' => $is_white
			);
		}
		
		return $color_array;
	}
}