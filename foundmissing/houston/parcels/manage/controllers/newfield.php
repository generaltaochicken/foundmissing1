<?php

namespace Manage\Controllers;

class NewField extends \Core\Controllers\Base_Controller{

	public $i;
	public $types;

	public function __construct($uri, $data){
        parent::__construct($uri, $data);

        $this->i = $data['i'];

        $this->types = array(
        	array('k' => 'varchar', 'v' => 'String'),
        	array('k' => 'text', 'v' => 'Long Text'),
        	array('k' => 'int', 'v' => 'Integer'),
        	array('k' => 'float', 'v' => 'Float'),
        	array('k' => 'file', 'v' => 'File'),
        	array('k' => 'date', 'v' => 'Date'),
        	array('k' => 'pk', 'v' => 'Primary Key')

        );

    }

    public function controller(){
    	$this->set_view('Manage\Views\NewField');

    }

}
