<?php
namespace Color\Models;

class Color extends \Core\Models\Base_Model{


	public function get_hex_with_title(){
		return $this->hex . " " . $this->title;
	}

	protected static $manage_prefix = "/manage/colors/";

	public function link_edit(){
		return self::$manage_prefix . "edit/" . $this->id;
	}

	public function link_delete(){
		return self::$manage_prefix . "delete/" . $this->id;
	}

	public function link_restore(){
		return self::$manage_prefix . "restore/" . $this->id;
	}

	public function link_new(){
		return self::$manage_prefix . "new";
	}

	public function link_all(){
		return self::$manage_prefix;
	}

	public function link_archive(){
		return self::$manage_prefix . "archive";
	}

	public function get_frontend_data(){
		return array(
			"id" => $this->id,
			"title" => $this->title,
			"hex" => $this->hex,
		);
	}
	
	public function get_color_name($id) {
		$color = \Color\Models\Color::first(array('select' => 'title', 'conditions' => 'deleted = 0 AND id = "' . $id . '"'));
		
		return $color->title;
	}
}