<?php
namespace Page\Controllers;

class All extends \Manage\Controllers\Manage{

	private $pages;

	public $pages_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Pages";
		$this->link_new = \Page\Models\Page::link_new();

		$this->pages = \Page\Models\Page::all(array("conditions" => "deleted = 0" , "order" => "ordering"));
		$archived = \Page\Models\Page::count(array("conditions" => "deleted = 1"));
		if($archived > 0){
			$this->archive_button = array("url" => \Page\Models\Page::link_archive(), "title" => "Archive");
		}

		foreach($this->pages as $page){
			$this->has_data = true;
			$this->pages_data[] = array(
				"id" => $page->id,
				"title" => $page->title,
				"slug" => $page->slug,
				"body" => word_trim($page->body, 20),
				"entry_datetime" => $page->entry_datetime,
				"link_edit" => $page->link_edit(),
				"link_delete" => $page->link_delete(),
				"ordering" => $page->ordering,
			);
		}

	}

	public function controller(){
		$this->set_view("Page\Views\All");
	}

}

