<?php
namespace Frontpage\Controllers;

class Delete extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$frontpage = \Frontpage\Models\Frontpage::find($data["frontpage_id"]);

		if($frontpage){
			$frontpage->deleted = 1;
			$frontpage->save();
			$this->add_undo(array("message" => "Frontpage has been deleted!", "action" => $frontpage->link_restore()));

			redirect_to($frontpage->link_all());
		}
	}
}
