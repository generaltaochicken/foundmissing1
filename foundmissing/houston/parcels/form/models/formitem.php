<?php

namespace Form\Models;

class FormItem extends \ActiveRecord\Model{

    protected $attributes;
    protected $type;
    protected $template;

    protected $form_item_id;
    protected $class;
    protected $value;
    protected $title;
    protected $required;
    protected $disabled;
    protected $selected;
    protected $options;
    protected $redirect;
    protected $multiple;

    protected $template_data;
    protected $output;

    public function __construct($attributes){

        $this->attributes = $attributes;

        $this->form_item_id = (isset($this->attributes['id']) ? $this->attributes['id'] : '');
        $this->class = (isset($this->attributes['class']) ? $this->attributes['class'] : '');
        $this->value = (isset($this->attributes['value']) ? $this->attributes['value'] : '');
        $this->title = (isset($this->attributes['title']) ? $this->attributes['title'] : '');
        $this->required = (isset($this->attributes['required']) ? $this->attributes['required'] : false);
        $this->disabled = (isset($this->attributes['disabled']) ? $this->attributes['disabled'] : false);
        $this->selected = (isset($this->attributes['selected']) ? $this->attributes['selected'] : false);
        $this->options = (isset($this->attributes['options']) ? $this->attributes['options'] : false);
        $this->multiple = (isset($this->attributes['multiple']) ? $this->attributes['multiple'] : false);

        $this->redirect = (isset($this->attributes['redirect']) ? $this->attributes['redirect'] : '/manage');

        $this->set_template_data();

        $template_file =  PARCEL_ROOT.'form'.DS.'views'.DS.strtolower($this->template) .'.html';
        if (is_file($template_file)) {
            $m = new \Mustache_Engine();
            $this->output = $m->render(file_get_contents($template_file), $this->template_data);
        } else {
            if(DEBUG){
                $this->output = '**Form Template ("'. $template_file .'") Not Found**';
            } else {
                $this->output = '';
            }

        }

    }

    protected function set_template_data(){

        $this->template_data = array(
          'id'          => $this->form_item_id,
          'class'       => $this->class,
          'value'       => $this->value,
          'title'       => $this->title,
          'required'    => $this->required,
          'disabled'    => $this->disabled,
          'selected'    => $this->selected,
          'options'     => $this->options,
          'multiple'    => $this->multiple,
          'redirect'    => $this->redirect
        );

    }

    public function get_output(){
        return $this->output;
    }
}