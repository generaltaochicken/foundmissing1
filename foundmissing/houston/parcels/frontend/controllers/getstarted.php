<?php

namespace Frontend\Controllers;

class GetStarted extends \Frontend\Controllers\Frontend {
	
	private $data;
	
	public function __construct($uri, $data) {
        parent::__construct($uri, $data);
		
		$this->data = $data;
		$this->add_asset('css', 'getstarted.css', true);
		$this->add_asset('js', 'getstarted.js', true);
	}
	
	public function controller() {
		$this->set_view('Frontend\Views\GetStarted');
	}
}
?>