<?php

namespace Form\Models;

class FormItemFile extends FormItem {
    
    public function __construct($attributes){
        $this->type = 'file';
        $this->template = 'file';
        parent::__construct($attributes);
    }
    
    
    
}