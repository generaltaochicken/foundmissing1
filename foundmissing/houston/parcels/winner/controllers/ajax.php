<?php
namespace Winner\Controllers;

class Ajax extends \Manage\Controllers\Manage{


	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		if (isset($_POST['type'])) {
			$this->pick_winner();
		}
	}

	public function controller(){
	
	}
	
	protected function pick_winner() {
		$users = \User\Models\User::all(array('conditions' => 'deleted = 0 AND admin = 0'));
		
		foreach ($users as $user) {
		
			$user_array[] = array(
				"id" => $user->id,
				"first" => $user->firstname,
				"last" => $user->lastname,
				"email" => $user->email,
				"code" => $user->code
			);
		}
		
		$count_users = count($users);
		$winning = rand(0, $count_users);
		
		$winner = $user_array[$winning];
		
		echo json_encode($winner);
	}
}
