<?php
namespace Style\Controllers;

class Archive extends \Manage\Controllers\Manage{

	private $styles;

	public $styles_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Deleted Styles";

		$this->styles = \Style\Models\Style::all(array("conditions" => "deleted = 1"));
		$this->archive_button = array("url" => \Style\Models\Style::link_all(), "title" => "Back");
		foreach($this->styles as $style){
			$this->has_data = true;
			$this->styles_data[] = array(
				"id" => $style->id,
				"title" => $style->title,
				"category" => $style->category_id,
				"link_restore" => $style->link_restore(),
			);
		}

	}

	public function controller(){
		$this->set_view("Style\Views\All");
	}

}

