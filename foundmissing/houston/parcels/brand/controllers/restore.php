<?php
namespace Brand\Controllers;

class Restore extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$brand = \Brand\Models\Brand::find($data["brand_id"]);

		if($brand){
			$brand->deleted = 0;
			$brand->save();
			$this->add_flash(array("message" => "Brand has been restored!"));

			redirect_to($brand->link_all());
		}
	}
}
