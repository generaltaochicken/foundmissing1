<?php
namespace Frontend\Controllers;

class Profile extends \Frontend\Controllers\Frontend {
    
	private $data;
	public $is_user = false;
	public $profile_page = true;
	
    public function __construct($uri, $data) {
        parent::__construct($uri, $data);
		
		$this->data = $data;
    }

    public function controller(){
		$user = \User\Models\User::find(array('conditions' => 'username = "' . $this->data['username'] . '"'));
		if ($user->image == null) {
			$user_image = '/images/default_profile.jpg';
		} else {
			$user_image = '/uploads/images/' . $user->image;
		}
		
		if (empty($user->id)) {
			redirect_to('/');
		} else if ($user->id == $this->_user->id) {
			redirect_to('/account/me');
		} else {
			$this->profile_data = array(
				'user_id' => $user->id,
				'username' => $user->username,
				'fullname' => $user->fullname(),
				'city' => $user->city . ', ',
				'country' => $user->country . '  /  ',
				'age' => $user->age,
				'title' => '/  ' . $user->title . '  ',
				'bio' => $user->bio,
				'email' => $user->email,
				'website' => $user->website,
				'facebook' => $user->facebook,
				'twitter' => $user->twitter,
				'image' => $user_image,
				'karma' => $user->karma,
				'date_joined' => time_to_friendly_date($user->entry_datetime, 'Y.m.d'),
				'last_login' => $this->time_elapsed_string($user->update_datetime),
				'comments' => count(\Comment\Models\Comment::all(array('conditions' => 'deleted = 0 AND user_id = "' . $user->id . '"'))),
				'reviews' => count(\Review\Models\Review::all(array('conditions' => 'deleted = 0 AND user_id = "' . $user->id . '"')))
			);
			
			if ($user->city == null) {
				$this->profile_data['city'] = '';
			}
			
			if ($user->country == null) {
				$this->profile_data['country'] = '';
				if ($user->city != null) {
					$this->profile_data['city'] = $user->city;
				}
			}
			
			if ($user->age == null) {
				$this->profile_data['age'] = '';
				if ($user->country != null) {
					$this->profile_data['country'] = $user->country;
				}
			}
			
			if ($user->title == null) {
				$this->profile_data['title'] = '';
			}
			
			if ($this->_user->username == $user->username) {
				$this->is_user = true;
			}
			
			$this->set_view('Frontend\Views\Profile');
		}
		
		$this->title = $this->profile_data['fullname']  . '\'s Profile | FM';
		
		$view = \Account\Controllers\View::get_user_activities();
	}
}