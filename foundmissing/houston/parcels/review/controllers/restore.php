<?php
namespace Review\Controllers;

class Restore extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$review = \Review\Models\Review::find($data["review_id"]);

		if($review){
			$review->deleted = 0;
			$review->save();
			$this->add_flash(array("message" => "Review has been restored!"));

			redirect_to($review->link_all());
		}
	}
}
