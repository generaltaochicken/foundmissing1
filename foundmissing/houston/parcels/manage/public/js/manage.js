var Manage = {};

Manage.Init = function() {

    Manage.Cache();
    Manage.BindListeners();
    Manage.HideFirstAndLastReorder();
    Manage.HideFirstAndLastReorderThumbs();

};

Manage.Cache = function() {

    Manage.dom = {};

    //btns
    Manage.dom.btnDanger = $(".btn-danger");
    Manage.dom.btnReorderUp = $(".reorder.up");
    Manage.dom.btnReorderDown = $(".reorder.down");

    Manage.dom.btnReorderUpThumb = $(".reorder-thumb.up");
    Manage.dom.btnReorderDownThumb = $(".reorder-thumb.down");


    //widgets
    Manage.dom.datePicker = $(".datepicker");
    Manage.dom.colorPiker = $(".colorpicker");
    Manage.dom.tip = $(".tip");
    Manage.dom.fillslug = $(".fillslug");


};

Manage.BindListeners = function(){

    Manage.dom.btnDanger.on('click', function(){
        return confirm("Are you sure?"); 
    });

    Manage.dom.btnReorderUp.on('click', function(){
        var parcel = $(this).attr('rel'); 
        Manage.Reorder($(this), 'up', parcel);
        return false;
    });

    Manage.dom.btnReorderDown.on('click', function(){
        var parcel = $(this).attr('rel') ;
        Manage.Reorder($(this), 'down', parcel);
        return false;
    });


    Manage.dom.btnReorderUpThumb.on('click', function(){

        Manage.ReorderAttachment($(this), 'up');
        return false;
    });

    Manage.dom.btnReorderDownThumb.on('click', function(){

        Manage.ReorderAttachment($(this), 'down');
        return false;
    });

    Manage.dom.fillslug.on('change', function(){

        $("#slug").val(Util.TitleToSlug($(this).val()));

    });

    if(Manage.dom.datePicker){
        Manage.dom.datePicker.datepicker({ dateFormat: "MM d, yy"});
    }
        
    if(Manage.dom.colorPiker){
        Manage.dom.colorPiker.miniColors();
    }

    if(Manage.dom.tip){
        Manage.dom.tip.tooltip();  
    }


    $('#attachmentModalTrigger').click(function(e) {
        e.preventDefault();
        //if(!Manage.modalOpen){
            Manage.modalOpen = true;
            var url = $(this).attr('href');
            //var modal_id = $(this).attr('data-target');
            $.get(url, function(data) {
                $(data).modal();

                setTimeout(function(){
                    $(".useAttachment").on('click', function(e){
                        e.preventDefault();
                        var attachment_id = parseInt($(this).data('attachment'));
                        var item_id = parseInt($(this).data('item'));
                        var url = '/manage/items/attach/' + item_id + '/' + attachment_id;

                        $.post(url, function(d){
                                
                            $(".attachment ul.thumbnails").append(d);
                            $("button.close").click();

                            Manage.Cache();
                            Manage.BindListeners();
                            Manage.HideFirstAndLastReorderThumbs();
                            Manage.modalOpen = false;

                        });

                    });
                }, 800);
                

            });
       // }
    });


    $('#attachmentModalTriggerSimple').click(function(e) {
        e.preventDefault();
        //if(!Manage.modalOpen){
            Manage.modalOpen = true;
            var url = $(this).attr('href');
            //var modal_id = $(this).attr('data-target');
            $.get(url, function(data) {
                $(data).modal();

                setTimeout(function(){
                    $(".useAttachment").on('click', function(e){
                        e.preventDefault();
                        var attachment_id = parseInt($(this).data('attachment'));
                        
                        Manage.SetAttachmentId(attachment_id);
                        $("button.close").click();
                        $("#submit").click();


                    });
                }, 800);
                

            });
       // }
    });

};

// Interactions


Manage.SetAttachmentId = function(id){
    $("#attachment_id").val(id);
}

Manage.Reorder = function($btn, direction, parcel){
    
    var $row = $btn.parent().parent();
    var this_id = parseInt($row.attr('rel'));
    
    //get swap id
    if(direction == 'up'){
        var $swap_row = $row.prev();
        $swap_row.before($row);
        
    } else{
        var $swap_row = $row.next();
        $swap_row.after($row);
    }
    
    Manage.FixReorderedNumbers();

    var swap_id = parseInt($swap_row.attr('rel'));
    
    $.ajax({
        type: "POST",
        url: "/manage/"+parcel+"/reorder/" + this_id + "/" + swap_id,
        data: { }
    }).done(function( msg ) {
        Manage.HideFirstAndLastReorder();
    });
    
    
}


Manage.ReorderAttachment = function($btn, direction, parcel){
    
    var $thumbnail = $btn.parent().parent().parent();

    var this_id = parseInt($thumbnail.attr('rel'));
    
    //get swap id
    if(direction == 'up'){
        var $swap_row = $thumbnail.prev();
        $swap_row.before($thumbnail);
    } else{
        var $swap_row = $thumbnail.next();
        $swap_row.after($thumbnail);
    }

    var swap_id = parseInt($swap_row.attr('rel'));
    
    $.ajax({
        type: "POST",
        url: "/manage/item_attachments/reorder/" + this_id + "/" + swap_id,
        data: { }
    }).done(function( msg ) {

        Manage.HideFirstAndLastReorderThumbs();

    });
    
    
}


Manage.FixReorderedNumbers = function(){
    $tables = $("table");
    $tables.each(function(){
        var i = -1;
        $rows = $(this).find('tr');
        $rows.each(function(){
            i++;
            $(this).children('td.ordering').html(i + '.');    
        }); 
    });
    
}


Manage.HideFirstAndLastReorder = function(){
  
    $tables = $("table");
    $tables.each(function(){
        $first = $(this).find(".reorder:first");
        $last = $(this).find(".reorder:last");
        
        $(this).find(".reorder").show();
        
        $first.hide();
        $last.hide();
        
    });
      
}

Manage.HideFirstAndLastReorderThumbs = function(){

    $thumbs = $("ul.thumbnails");
    $thumbs.each(function(){
        $first = $(this).find(".reorder-thumb:first");
        $last = $(this).find(".reorder-thumb:last");
        
        $(this).find(".reorder-thumb").show();
        
        $first.hide();
        $last.hide();
        
    });

}


//Classes

// On Load
$(document).ready(function(){

    Manage.Init();
	var url = "/ajax/admin";
	
	$('#filter_brand').live('change', function() {
		$(".loading_image").fadeIn();
		var brand_id = $('#filter_brand').val();
		var type = "brand_filter";
		
		var data = { type: type, brand: brand_id };
		
		$.post(url, data, function(response) {
			$(".footwear_collections").fadeOut(function() {
				$(".footwear_collections").html(response).fadeIn();
				$(".loading_image").fadeOut();
			});
		});
	});

});

/* UTILITY FUNCTIONS */

var Util = {};



Util.TitleToSlug = function(title){
    return title.toLowerCase().replace(/[^\w ]+/g,'').replace(/ +/g,'-');
}
