<?php
namespace Frontpage\Controllers;

class All extends \Manage\Controllers\Manage{

	private $frontpages;

	public $frontpages_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Frontpages";
		$this->link_new = \Frontpage\Models\Frontpage::link_new();

		$this->frontpages = \Frontpage\Models\Frontpage::all(array("conditions" => "deleted = 0" , "order" => "ordering"));
		$archived = \Frontpage\Models\Frontpage::count(array("conditions" => "deleted = 1"));
		if($archived > 0){
			$this->archive_button = array("url" => \Frontpage\Models\Frontpage::link_archive(), "title" => "Archive");
		}

		foreach($this->frontpages as $frontpage){
			$this->has_data = true;
			
			switch($frontpage->type) {
				case 'review':
					$review = \Review\Models\Review::find($frontpage->parent_id);
					$title = $review->title;
					break;
				case 'article':
					$article = \Article\Models\Article::find($frontpage->parent_id);
					$title = $article->title;
					break;
				case 'interview':
					$interview = \Interview\Models\Interview::find($frontpage->parent_id);
					$title = $interview->title;
					break;
				case 'gallery':
					$gallery = \Gallery\Models\Gallery::find($frontpage->parent_id);
					$title = $gallery->title;
					break;
			}
			$this->frontpages_data[] = array(
				"id" => $frontpage->id,
				"slider" => $frontpage->slider,
				"position" => $frontpage->position,
				"type" => $frontpage->type,
				"parent_id" => $title,
				"entry_datetime" => $frontpage->entry_datetime,
				"link_edit" => $frontpage->link_edit(),
				"link_delete" => $frontpage->link_delete(),
				"ordering" => $frontpage->ordering,
			);
		}

	}

	public function controller(){
		$this->set_view("Frontpage\Views\All");
	}

}

