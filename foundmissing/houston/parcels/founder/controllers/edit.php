<?php
namespace Founder\Controllers;

class Edit extends \Manage\Controllers\Manage{

	public $page_title;
	public $link_back;
	public $form;
	public $existing;
	
	public $aritcle_title;
	public $article;
	public $published = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		if(isset($_POST["submit"])){
			$this->submit_form();
		}
		
		if(isset($data["article_id"])){
			$this->article = \Founder\Models\Founder::find($data["article_id"]);
			$this->page_title = "Editing Founder";
		} else {
			$this->article = new \Founder\Models\Founder;
			$this->page_title = "New Founder";
		}

		$this->link_back = $this->article->link_all();

		if ($this->article->publish_date != 0 || $this->article->publish_date != null) {
			$this->published = true;
		}
		
		if($this->article->bg_image_header){
			$this->preview = ARTICLE_IMAGES . $this->article->bg_image_header;
		}

	}

	public function controller(){
		$this->set_view("Founder\Views\Edit");
	}
	
	private function get_publish_options() {
		$published = array(
			array('value' => '1', 'title' => 'Yes', 'selected' => 0),
			array('value' => '0', 'title' => 'No', 'selected' => 1)
		);
		
		if  ($this->article->publish_date != 0 || $this->article->publish_date != null) {
			$published[0]['selected'] = 1;
			$published[1]['selected'] = 0;
		}
		
		return $published;
	}

	private function submit_form(){
		if($_POST["id"] > 0){
			$article = \Founder\Models\Founder::find($_POST["id"]);
			$flash_msg = "Founder has been updated successfully.";
		} else {
			$article = new \Founder\Models\Founder;
			$flash_msg = "Founder has been created successfully.";
			$article->ordering = \Founder\Models\Founder::get_next_ordering_number();
			$article->entry_datetime = time();
		}
		
		$article->author_name = $_POST["author_name"];
		$article->title = $_POST["title"];
		$article->slug = $_POST["slug"];
		$article->excerpt = $_POST["excerpt"];
		$article->body = $_POST["body"];
		
		if(isset($_FILES['main_image']['tmp_name']) && $_FILES['main_image']['tmp_name'] != ''){
			$image_slug = time_to_friendly_date($article->entry_datetime, 'Ymd') . '_' . $article->slug;
			$image = $this->upload_article($_FILES['main_image'], array('jpg', 'png', 'gif', 'jpeg'), $image_slug);

			if($image) {
				$article->bg_image_header = $image_slug . '/' . $image['filename'];
			}
		}
		
		$article->publish_date = $_POST["publish_date"];
		if ($article->publish_date == 1) {
			if ($this->article->publish_date <= 1) {
				$article->publish_date = time();
			}
		}

		$article->save();
		$this->add_flash(array("message" => $flash_msg, "type" => "success", "Heading" => "Great!"));
		redirect_to($article->link_edit());
	}

}
