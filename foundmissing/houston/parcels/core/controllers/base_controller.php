<?php
/**
 * ---------------------------------------------------------------
 * Houston v1.1
 * ---------------------------------------------------------------
 * @author Tyler Savery | The Young Astronauts
 * @see http://www.houstonmvc.com
 * @license Open Source
 * ---------------------------------------------------------------
 * 
 * Controller for http requests
 *
 */

namespace Core\Controllers;

class Base_Controller {
    
    public $uri;
    public $title;
    public $meta;
    public $css;
    public $less;
    public $head_js;
    public $foot_js;
    public $errors;
    public $flashes;
    public $undos;
    
    protected $json_responses;
    
    protected $template;
    protected $view;
    
    public function __construct($uri, $data) {
        $this->uri          = $uri;
        $this->data         = $data;
        $this->css          = new \SplFixedArray(0);
        $this->less         = new \SplFixedArray(0);
        $this->head_js      = new \SplFixedArray(0);
        $this->foot_js      = new \SplFixedArray(0);
        $this->meta         = new \SplFixedArray(0);
        $this->called_class = get_called_class();
        $this->view         = '';
        
        $this->title = '';
        $this->website = '';
        $this->handle_flashes();
        $this->handle_undos();
    }
    
    public function add_asset($type, $file, $public = false, $minify = true, $attributes = '', $position = 'head') {
        $parcel = explode('\\', get_called_class());
        
        switch ($type) {
            case 'js':
                if ($public === true) {
                    $file = '/js/'.$file;
                } else if($public == 'remote'){
                    $file = $file;
                } else if(is_string($public)){
                    $file =  '/js/'.strtolower($public).'/'.$file;
                } else {
                    $file = '/js/'.strtolower($parcel[0]).'/'.$file.(($minify) ? '?min=true' : '');
                }

                $position .= '_js';
                $this->{$position}->setSize($this->{$position}->getSize() + 1);
                $this->{$position}[$this->{$position}->getSize() - 1] = 'src="'.$file.'" '.$attributes;
                break;
            case 'css':
                if ($public === true) {
                    $file = 'href="/css/'.$file.'" ';
                } else if(is_string($public)){
                    $file = 'href="/css/'.strtolower($public).'/'.$file.'" ';
                } else {
                    $file = 'href="/css/'.strtolower($parcel[0]).'/'.$file.'" '.$attributes;
                }
                /*
                if($minify){
                    $file .= "?min=true";
                }
                */
                
                $this->css->setSize($this->css->getSize() + 1);
                $this->css[$this->css->getSize() - 1] = $file;
                break;
            case 'less':
                if ($public === true) {
                    $file = 'href="/css/'.$file.'" ';
                } else if(is_string($public)){
                    $file = 'href="/css/'.strtolower($public).'/'.$file.'" ';
                } else {
                    $file = 'href="/css/'.strtolower($parcel[0]).'/'.$file.'" '.$attributes;
                }
                /*
                if($minify){
                    $file .= "?min=true";
                }
                */
                
                $this->less->setSize($this->less->getSize() + 1);
                $this->less[$this->less->getSize() - 1] = $file;
                break;
        }
    }

    public function __set($key, $val) {
        $this->$key = $val;
    }

    public function view() {
        $this->template->__construct($this);
        return $this->template;
    }

    public function add_meta($name, $content, $id = '', $attributes = '') {
        $this->meta->setSize($this->meta->getSize() + 1);
        $this->meta[$this->meta->getSize() - 1] = (($id != '') ? 'id="'.$id.'"' : '').'name="'.$name.'" content="'.$content.'" '.$attributes;
    }
    
    
    public function set_view($name) {
        $this->view = $name;
    }
    
    public function set_errors($error_array){
        $this->errors = $error_array;
    }
    
    public function add_error($error){
        $this->errors[] = array("heading" => "Oops.", "message" => $error);
    }
    
    
    public function set_flashes($flash_array){
        $_SESSION['flashes'] = $flash_array;
    }
    
    public function add_flash($flash){
        $_SESSION['flashes'][] = $flash;
    }
    

    public function add_undo($array){
        $_SESSION['undos'][] = $array;
    }
    
    public function handle_undos(){
        if(isset($_SESSION['undos'])){
            $this->undos = $_SESSION['undos'];
            unset($_SESSION['undos']);
        }
    }

    
    protected function handle_flashes(){
        if(isset($_SESSION['flashes'])){
            $this->flashes = $_SESSION['flashes'];
            unset($_SESSION['flashes']);
        }
    }

    public function controller() {
        // Should be overwritten
    }

    public function render_view() {
        $this->template = new \Core\Controllers\Template_Controller($this);
        if (! empty($this->view)) {
            $this->template->view = $this->view;
        } else {
            $this->template->view = str_replace('Controllers', 'Views', get_called_class());
        }

        echo $this->template->render();

    }


    public function get_partial($view) {
        $template = new \Core\Controllers\Template_Controller($this);
        $template->view = $view;
 
        return $template->render();

    }
    
    public function add_to_title($str){
        if(!$this->title){
            $this->title = $str;
            return;
        }
        
        $this->title .= ' - ' . $str;
    }
    
    public function return_reponse(){
        header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        
        die(json_encode($this->json_responses));
    }
    
}