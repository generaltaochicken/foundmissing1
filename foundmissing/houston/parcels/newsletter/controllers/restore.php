<?php
namespace Newsletter\Controllers;

class Restore extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$newsletter = \Newsletter\Models\Newsletter::find($data["newsletter_id"]);

		if($newsletter){
			$newsletter->deleted = 0;
			$newsletter->save();
			$this->add_flash(array("message" => "Newsletter has been restored!"));

			redirect_to($newsletter->link_all());
		}
	}
}
