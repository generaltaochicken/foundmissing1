<?php
namespace Frontend\Controllers;

class Temp2 extends \Frontend\Controllers\Frontend {
    
	private $data;
	
    public function __construct($uri, $data) {
        parent::__construct($uri, $data);
		
		$this->title = 'Interviews | FM';
		
	}
	
	public function controller() {
		$this->set_view('Frontend\Views\Temp2');
	}
	
}