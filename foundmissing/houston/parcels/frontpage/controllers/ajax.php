<?php
namespace Frontpage\Controllers;

class Ajax extends \Manage\Controllers\Manage{
	public function __construct($uri, $data){
		parent::__construct($uri, $data);
		
		if(isset($_POST['type'])) {
			if ($_POST['type'] == 'parent_id') {
				$this->change_names($_POST['article_type']);
			}
			
			return;
		}
		
	}
	
	private function change_names($name) {
		$output = '';
		
		switch($name) {
			case 'article':
				$articles = \Article\Models\Article::all(array('conditions' => 'deleted = 0 AND publish_date > 0', 'order' => 'id DESC'));
				break;
			case 'review':
				$articles = \Review\Models\Review::all(array('conditions' => 'deleted = 0 AND publish_date > 0', 'order' => 'id DESC'));
				break;
			case 'interview':
				$articles = \Interview\Models\Interview::all(array('conditions' => 'deleted = 0 AND publish_date > 0', 'order' => 'id DESC'));
				break;
			case 'gallery':
				$articles = \Gallery\Models\Gallery::all(array('conditions' => 'deleted = 0', 'order' => 'id DESC'));
				break;
			default:
				break;
		}
		
		foreach ($articles as $article) {
			$output .= '<option value="' . $article->id . '">' . ucwords($article->title) . '</option>';
		}
		
		echo $output;
	}
}

?>