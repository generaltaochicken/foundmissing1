<?php
namespace Api\Controllers;

class Comments extends \Api\Controllers\Api{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->response = $this->get_comments();
		$this->send_response();

	}

	protected function get_comments(){

		$conditions = array('deleted' => 0);

		if($this->request('get', 'user_id')){
			$conditions['user_id'] = $this->request('get', 'user_id');
		}

		if($this->request('get', 'parent_id')){
			$conditions['parent_id'] = $this->request('get', 'parent_id');
		}

		if($this->request('get', 'parent_type')){
			$conditions['parent_type'] = $this->request('get', 'parent_type');
		}

		$args = array('conditions' => $conditions);

		$comments = \Comment\Models\Comment::all($args);
		$array = array();
		foreach($comments as $comment){
			$array[] = array(
				"id" => $comment->id,
				"user_id" => $comment->user_id,
				"parent_type" => $comment->parent_type,
				"parent_id" => $comment->parent_id,
				"body" => $comment->body,
				"entry_datetime" => $comment->entry_datetime,
			);
		}

		return $array;
		
	}




}