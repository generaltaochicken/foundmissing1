<?php
namespace Brand\Controllers;

class All extends \Manage\Controllers\Manage{

	private $brands;

	public $brands_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Brands";
		$this->link_new = \Brand\Models\Brand::link_new();

		$this->brands = \Brand\Models\Brand::all(array("conditions" => "deleted = 0" , "order" => "ordering"));
		$archived = \Brand\Models\Brand::count(array("conditions" => "deleted = 1"));
		if($archived > 0){
			$this->archive_button = array("url" => \Brand\Models\Brand::link_archive(), "title" => "Archive");
		}

		foreach($this->brands as $brand){
			$this->has_data = true;
			$this->brands_data[] = array(
				"id" => $brand->id,
				"title" => $brand->title,
				"description" => $brand->description,
				"link_edit" => $brand->link_edit(),
				"link_delete" => $brand->link_delete(),
				"ordering" => $brand->ordering,
			);
		}

	}

	public function controller(){
		$this->set_view("Brand\Views\All");
	}

}

