<?php
/**
 * ---------------------------------------------------------------
 * Houston v1.1
 * ---------------------------------------------------------------
 * @author Tyler Savery | The Young Astronauts
 * @see http://www.houstonmvc.com
 * @license Open Source
 * ---------------------------------------------------------------
 * 
 * Controller for asset requests
 *
 */

namespace Core\Controllers;

class Asset_Controller {
	
	protected $minimize;

    public function __construct($uri, $data) {
        $this->data = $data;
        $this->uri  = $uri;
		$this->minimize = false;
    }
    
    public function controller() {

		if (isset($_GET['min'])) {
			$this->minimize = $_GET['min'];
		}
		
		$parcel = explode('/', $this->uri);
		$parcel = $parcel[2];
		
		if ($this->data['type'] == 'views') {
			$folder = '';
		} else {
			$folder = DS.'public';
		}

		$file = HOUSTON_ROOT.'parcels'.DS.$parcel.$folder.DS.$this->data['type'].DS.$this->data['asset'];
		
		// Check for the resource in the houston root
		if (! is_file($file)) {
			$file = HOUSTON_ROOT.'parcels'.DS.$parcel.$folder.DS.$this->data['type'].DS.$this->data['asset'];
		}

		if (is_file($file)) {
		    switch ($this->data['type']) {
		    	case 'views': $mime = 'text/html';       break;
		        case 'js':    $mime = 'text/javascript'; break;
		        case 'css':   $mime = 'text/css';        break;
		        case 'img':
		            switch (substr($file, -3)) {
		                case 'jpg':
		                case 'peg':
		                    $mime = 'image/jpeg';
		                    break;
		                case 'png':
		                    $mime = 'image/png';
		                    break;
		                case 'gif':
		                    $mime = 'image/gif';
		                    break;
		                default:
		                    $mime = 'application/octet-stream';
		                    break;
		            }
		        break;
		    }
		    header('Content-Type: ' . $mime);
		    header('Expires: Mon, 26 Jul 2025 05:00:00 GMT');
		    header('Cache-Control: max-age=315360000');
		    header('Pragma: no-cache');
						
			// Minimize the output code TODO: include the JS and CSS min scripts
			if ($this->minimize) {
				$output = $this->sanitize_output(file_get_contents($file));
			} else {
				$output = file_get_contents($file);
			}
            
			
		    die($output);
		}
    }

    /**
     * Sanitizes the output buffer
     *
     * @return string Buffer output to sanitize and return.
     */
    public function sanitize_output($buffer) {
        $search = array(
            '/\>[^\S ]+/s', //strip whitespaces after tags, except space
            '/[^\S ]+\</s', //strip whitespaces before tags, except space
            '/(\s)+/s'      // shorten multiple whitespace sequences
        );
        $replace = array('>', '<', '\\1');
        $buffer = preg_replace($search, $replace, $buffer);
        return $buffer;
    }
    
    public function render_view() {
		return;
    }
    
}