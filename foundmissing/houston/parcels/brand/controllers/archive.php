<?php
namespace Brand\Controllers;

class Archive extends \Manage\Controllers\Manage{

	private $brands;

	public $brands_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Deleted Brands";

		$this->brands = \Brand\Models\Brand::all(array("conditions" => "deleted = 1"));
		$this->archive_button = array("url" => \Brand\Models\Brand::link_all(), "title" => "Back");
		foreach($this->brands as $brand){
			$this->has_data = true;
			$this->brands_data[] = array(
				"id" => $brand->id,
				"title" => $brand->title,
				"description" => $brand->description,
				"link_restore" => $brand->link_restore(),
			);
		}

	}

	public function controller(){
		$this->set_view("Brand\Views\All");
	}

}

