<?php
namespace Article\Controllers;

class Archive extends \Manage\Controllers\Manage{

	private $articles;

	public $articles_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Deleted Articles";

		$this->articles = \Article\Models\Article::all(array("conditions" => "deleted = 1"));
		$this->archive_button = array("url" => \Article\Models\Article::link_all(), "title" => "Back");
		foreach($this->articles as $article){
			$this->has_data = true;
			$this->articles_data[] = array(
				"id" => $article->id,
				"entry_datetime" => $article->entry_datetime,
				"author_name" => $article->author_name,
				"title" => $article->title,
				"slug" => $article->slug,
				"excerpt" => $article->excerpt,
				"body" => $article->body,
				"bg_image_header" => $article->bg_image_header,
				"link_restore" => $article->link_restore(),
			);
		}

	}

	public function controller(){
		$this->set_view("Article\Views\All");
	}

}

