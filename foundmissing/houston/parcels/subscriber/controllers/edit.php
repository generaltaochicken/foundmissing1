<?php
namespace Subscriber\Controllers;

class Edit extends \Manage\Controllers\Manage{

	private $subscriber;

	public $page_title;
	public $link_back;
	public $form;
	public $existing;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		if(isset($_POST["submit"])){
			$this->submit_form();
		}

		if(isset($data["subscriber_id"])){
			$this->subscriber = \Subscriber\Models\Subscriber::find($data["subscriber_id"]);
			$this->page_title = "Editing Subscriber";
		} else {
			$this->subscriber = new \Subscriber\Models\Subscriber;
			$this->page_title = "New Subscriber";
		}

		$this->link_back = $this->subscriber->link_all();

		$form = new \Form\Models\Form("edit", "POST", "", "well");

		$item = new \Form\Models\FormItemHidden(array("id" => "id", "title" => "Id", "value" => $this->subscriber->id));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "email", "title" => "Email", "value" => $this->subscriber->email));
		$form->add_item($item);

		$item = new \Form\Models\FormItemSubmit(array("value" => "Save", "id" => "submit", "class" => "btn btn-primary", "cancel_url" => $this->subscriber->link_all()));
		$form->add_item($item);

		$this->form = $form->render();

	}

	public function controller(){
		$this->set_view("Subscriber\Views\Edit");
	}

	private function submit_form(){
		if($_POST["id"] > 0){
			$subscriber = \Subscriber\Models\Subscriber::find($_POST["id"]);
			$flash_msg = "Subscriber has been updated successfully.";
			$subscriber->email = $_POST["email"];
			$subscriber->entry_datetime = time();
			$subscriber->save();
		} else {
			$subscriber = new \Subscriber\Models\Subscriber;
			$flash_msg = "Subscriber has been created successfully.";
			$subscriber->ordering = \Subscriber\Models\Subscriber::get_next_ordering_number();
			
			$existence = \Subscriber\Models\Subscriber::find('all', array('conditions' => 'email = "' . $email->email . '"'));
			if (!$existence) {
				$subscriber->email = $_POST["email"];
				$subscriber->entry_datetime = time();
				$subscriber->save();
			} else {
				$flash_msg = "Subscriber exists";
			}
		}
		
		$this->add_flash(array("message" => $flash_msg, "type" => "success", "Heading" => "Great!"));
		redirect_to($subscriber->link_new());
		//redirect_to($subscriber->link_all());
	}

}
