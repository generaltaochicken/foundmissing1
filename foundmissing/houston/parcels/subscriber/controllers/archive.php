<?php
namespace Subscriber\Controllers;

class Archive extends \Manage\Controllers\Manage{

	private $subscribers;

	public $subscribers_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Deleted Subscribers";

		$this->subscribers = \Subscriber\Models\Subscriber::all(array("conditions" => "deleted = 1"));
		$this->archive_button = array("url" => \Subscriber\Models\Subscriber::link_all(), "title" => "Back");
		foreach($this->subscribers as $subscriber){
			$this->has_data = true;
			$this->subscribers_data[] = array(
				"id" => $subscriber->id,
				"entry_datetime" => $subscriber->entry_datetime,
				"name" => $subscriber->name,
				"email" => $subscriber->email,
				"link_restore" => $subscriber->link_restore(),
			);
		}

	}

	public function controller(){
		$this->set_view("Subscriber\Views\All");
	}

}

