<?php
namespace Game\Controllers;

class Delete extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$game = \Game\Models\Game::find($data["game_id"]);

		if($game){
			$game->deleted = 1;
			$game->save();
			$this->add_undo(array("message" => "Game has been deleted!", "action" => $game->link_restore()));

			redirect_to($game->link_all());
		}
	}
}
