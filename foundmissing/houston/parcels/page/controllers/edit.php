<?php
namespace Page\Controllers;

class Edit extends \Manage\Controllers\Manage{

	private $page;

	public $page_title;
	public $link_back;
	public $form;
	public $existing;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		if(isset($_POST["submit"])){
			$this->submit_form();
		}

		if(isset($data["page_id"])){
			$this->page = \Page\Models\Page::find($data["page_id"]);
			$this->page_title = "Editing Page";
		} else {
			$this->page = new \Page\Models\Page;
			$this->page_title = "New Page";
		}

		$this->link_back = $this->page->link_all();

		$form = new \Form\Models\Form("edit", "POST", "", "well");

		$item = new \Form\Models\FormItemHidden(array("id" => "id", "title" => "Id", "value" => $this->page->id));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "title", "title" => "Title", "class"=>"fillslug", "value" => $this->page->title));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "slug", "title" => "Slug", "value" => $this->page->slug));
		$form->add_item($item);

		$item = new \Form\Models\FormItemTextarea(array("id" => "body", "title" => "Body", "value" => $this->page->body));
		$form->add_item($item);

		$item = new \Form\Models\FormItemSubmit(array("value" => "Save", "id" => "submit", "class" => "btn btn-primary", "cancel_url" => $this->page->link_all()));
		$form->add_item($item);

		$this->form = $form->render();

	}

	public function controller(){
		$this->set_view("Page\Views\Edit");
	}

	private function submit_form(){
		if($_POST["id"] > 0){
			$page = \Page\Models\Page::find($_POST["id"]);
			$flash_msg = "Page has been updated successfully.";
		} else {
			$page = new \Page\Models\Page;
			$flash_msg = "Page has been created successfully.";
			$page->ordering = \Page\Models\Page::get_next_ordering_number();
			$page->entry_datetime = time();

		}

		$page->title = $_POST["title"];
		$page->slug = $_POST["slug"];
		$page->body = $_POST["body"];

		$page->save();
		$this->add_flash(array("message" => $flash_msg, "type" => "success", "Heading" => "Great!"));
		redirect_to($page->link_all());
	}

}
