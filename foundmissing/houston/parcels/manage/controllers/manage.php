<?php

namespace Manage\Controllers;

class Manage extends \Core\Controllers\Base_Controller {

    protected $_user;
    public $_recent_parcel;
    public $_recent_id;

    public $parcels = array();
    protected $item_types = array();

    public function __construct($uri, $data){
        parent::__construct($uri, $data);

        $this->title = 'FoundMissing :: Control Panel';

        $this->add_asset('css', 'bootstrap.min.css', 'manage');
        $this->add_asset('css', 'bootstrap-fileupload.css', 'manage');

        $this->add_asset('css', 'minicolors.css', 'manage');
        $this->add_asset('css', 'jquery.ui.css', true);
        
        $this->add_asset('css', 'manage.css', 'manage');

        $this->add_asset('js', 'jquery.min.js', 'core');
        $this->add_asset('js', 'bootstrap.min.js', 'manage');
        $this->add_asset('js', 'jquery.ui.core.js', 'manage');
        $this->add_asset('js', 'jquery.ui.widget.js', 'manage');
        $this->add_asset('js', 'jquery.ui.datepicker.js', 'manage');
        $this->add_asset('js', 'minicolors.js', 'manage');
        $this->add_asset('js', 'bootstrap-fileupload.js', 'manage');
        $this->add_asset('js', 'bootstrap-modalmanager.js', 'manage');

		$this->add_asset('js', 'manage.js', 'manage');

        $this->unprotected_controllers = array('User\\Controllers\\Login', 'User\\Controllers\\Logout');

        $this->authenticate();
        $this->load_parcels();
        $this->item_types = $this->get_item_types();

        $this->get_session_variables();

    }

    public function controller(){
		return;
	}


    protected function get_session_variables(){
        if(isset($_SESSION['_recent_parcel'])){
            $this->_recent_parcel = $_SESSION['_recent_parcel'];
        }

        if(isset($_SESSION['_recent_id'])){
            $this->_recent_id = $_SESSION['_recent_id'];
        }
    }

    protected function set_session_variable($key, $val){
        $_SESSION[$key] = $val;
    }


    public function authenticate() {

        $user = new \User\Models\User();

        if(!isset($_SESSION[$user->login_session_name]) || $_SESSION[$user->login_session_name] < 1){

            if(!in_array(get_called_class(), $this->unprotected_controllers)){
            $this->add_flash(array('message' => 'You are not permitted to view this page'));
                redirect_to('/users/login');
            }
        }

        if(isset($_SESSION[$user->login_session_name])){
            $this->_user = $user->get_logged_in_user();

             if(!in_array(get_called_class(), $this->unprotected_controllers)){
                if($this->_user->admin == 0){
                    $this->add_flash(array('message' => 'You are not permitted to view this page'));
                    redirect_to('/users/login');
                }
            }
        }

    }

    public function load_parcels(){

        $app_config_file = CONFIG_ROOT.'app.json';
        $app_config_array = json_decode(file_get_contents($app_config_file), true);

        $parcel_array = $app_config_array['parcels'];
		
        foreach($parcel_array as $parcel){

            if($parcel == 'core' || $parcel == 'manage' || $parcel == 'attachment' || $parcel == 'item_attachment' || $parcel == 'api'){
                continue;
            }

            $parcel_config_file = PARCEL_ROOT.$parcel.DS.$parcel.'.json';
            if(!file_exists($parcel_config_file)){
                continue;
            }

            $parcel_config_array = json_decode(file_get_contents($parcel_config_file), true);

            if(isset($parcel_config_array['disabled']) && $parcel_config_array['disabled']){
               continue;
            }

            $this->parcels[] = $parcel_config_array;

        }

    }


    protected function upload($field, $types, $is_article = false, $gallery_slug = '', $is_footwear = false) {

        $ext = end(explode(".", $field["name"]));
        $e = false;
        if (!in_array($ext,  $types)){
            $this->add_error("Invalid Filetype.");
            $e = true;
        }
        if ($field["error"] > 0){
            $this->add_error(join(',', $field['error']));
            $e = true;

        }
        if ($field['size'] >= MAX_UPLOAD_SIZE){
            $this->add_error("File too large - must be less than " . format_file_size(MAX_UPLOAD_SIZE) . ".");
            $e = true;
        }

        if($e){
            return false;
        }

		if ($is_footwear) {
			$filename = $field["name"];
		} else {
			$filename = random_string(12) . "." . $ext;
		}

        if(!is_dir(IMAGE_UPLOAD_DIRECTORY)){ mkdir(IMAGE_UPLOAD_DIRECTORY); }

        list($w, $h, $type) = @getimagesize($field["tmp_name"]);

        switch($type) {
            case 1:
                $img = @imagecreatefromgif($field["tmp_name"]);
                break;
            case 2:
                $img = @imagecreatefromjpeg($field["tmp_name"]);
                break;
            case 3:
                $img = @imagecreatefrompng($field["tmp_name"]);
                break;
        }

        $max_w = MAX_UPLOAD_WIDTH;
        $max_h = MAX_UPLOAD_HEIGHT;

        $new_width = $w;
        $new_height = $h;

        if($w > $max_w || $h > $max_h){

            if($w > $h){
                $ratio = $max_w / $w;
                $new_width = $max_w;
                $new_height = $h * $ratio;
            } else {
                $ratio = $max_h / $h;
                $new_width = $max_w * $ratio;
                $new_height = $max_h;
            }

        }

        $new_img = @imagecreatetruecolor($new_width, $new_height);
        @imagefilledrectangle($new_img, 0, 0, $new_width, $new_height, 0);
        @imagecopyresampled($new_img, $img, 0, 0, 0, 0, $new_width, $new_height, $w, $h);

        ob_start();
        imagejpeg($new_img, NULL, 80);
        $image_contents = ob_get_contents();
        ob_end_clean();

		if (!$is_article) {
			if (!$gallery_slug) {
				if ($is_footwear) {
					$fh = fopen(PUBLIC_ROOT . FOOTWEAR_IMAGES . $filename, "a+");
				} else {
					$fh = fopen(IMAGE_UPLOAD_DIRECTORY . $filename, "a+");
				}
			} else if ($gallery_slug) {
				if(!is_dir(PUBLIC_ROOT . GALLERY_IMAGES . $gallery_slug . DS)){ mkdir(PUBLIC_ROOT . GALLERY_IMAGES . $gallery_slug . DS); }
				$fh = fopen(PUBLIC_ROOT . GALLERY_IMAGES . $gallery_slug . DS . $filename, "a+");
			}
		} else {
			$fh = fopen(PUBLIC_ROOT . ARTICLE_IMAGES . $filename, "a+");
		}
		
        fwrite($fh, $image_contents);
        fclose($fh);

        return array('filename' => $filename, 'width' => $w, 'height' => $h, 'extension' => $ext, 'mime' => 'image/jpeg');

    }
	
	protected function upload_article($field, $types, $article_slug, $review = false) {
		$ext = end(explode(".", $field["name"]));
        $e = false;
        if (!in_array($ext,  $types)){
            $this->add_error("Invalid Filetype.");
            $e = true;
        }
        if ($field["error"] > 0){
            $this->add_error(join(',', $field['error']));
            $e = true;

        }
        if ($field['size'] >= MAX_UPLOAD_SIZE){
            $this->add_error("File too large - must be less than " . format_file_size(MAX_UPLOAD_SIZE) . ".");
            $e = true;
        }

        if($e){
            return false;
        }

		if ($is_footwear) {
			$filename = $field["name"];
		} else {
			$filename = random_string(12) . "." . $ext;
		}

        if(!is_dir(IMAGE_UPLOAD_DIRECTORY)){ mkdir(IMAGE_UPLOAD_DIRECTORY); }

        list($w, $h, $type) = @getimagesize($field["tmp_name"]);

        switch($type) {
            case 1:
                $img = @imagecreatefromgif($field["tmp_name"]);
                break;
            case 2:
                $img = @imagecreatefromjpeg($field["tmp_name"]);
                break;
            case 3:
                $img = @imagecreatefrompng($field["tmp_name"]);
                break;
        }

        $max_w = MAX_UPLOAD_WIDTH;
        $max_h = MAX_UPLOAD_HEIGHT;

        $new_width = $w;
        $new_height = $h;

        if($w > $max_w || $h > $max_h){

            if($w > $h){
                $ratio = $max_w / $w;
                $new_width = $max_w;
                $new_height = $h * $ratio;
            } else {
                $ratio = $max_h / $h;
                $new_width = $max_w * $ratio;
                $new_height = $max_h;
            }

        }

        $new_img = @imagecreatetruecolor($new_width, $new_height);
        @imagefilledrectangle($new_img, 0, 0, $new_width, $new_height, 0);
        @imagecopyresampled($new_img, $img, 0, 0, 0, 0, $new_width, $new_height, $w, $h);

        ob_start();
        imagejpeg($new_img, NULL, 80);
        $image_contents = ob_get_contents();
        ob_end_clean();
		
		if ($review == false) {
			if(!is_dir(PUBLIC_ROOT . ARTICLE_IMAGES . $article_slug . DS)){ mkdir(PUBLIC_ROOT . ARTICLE_IMAGES . $article_slug . DS); }
				
			$fh = fopen(PUBLIC_ROOT . ARTICLE_IMAGES . $article_slug . DS . $filename, "a+");
		} else {
			if(!is_dir(PUBLIC_ROOT . REVIEW_IMAGES . $article_slug . DS)){ mkdir(PUBLIC_ROOT . REVIEW_IMAGES . $article_slug . DS); }
			$fh = fopen(PUBLIC_ROOT . REVIEW_IMAGES . $article_slug . DS . $filename, "a+");
		}
		
        fwrite($fh, $image_contents);
        fclose($fh);

        return array('filename' => $filename, 'width' => $w, 'height' => $h, 'extension' => $ext, 'mime' => 'image/jpeg');
	}
	
	protected function upload_journal($field, $types, $journal_slug) {
		$ext = end(explode(".", $field["name"]));
        $e = false;
        /*if (!in_array($ext,  $types)){
            $this->add_error("Invalid Filetype.");
            $e = true;
        }
        if ($field["error"] > 0){
            $this->add_error(join(',', $field['error']));
            $e = true;

        }
        if ($field['size'] >= MAX_UPLOAD_SIZE){
            $this->add_error("File too large - must be less than " . format_file_size(MAX_UPLOAD_SIZE) . ".");
            $e = true;
        }*/

        if($e){
            return false;
        }

		//if ($is_footwear) {
			$filename = $field["name"];
		//} else {
			//$filename = random_string(12) . "." . $ext;
		//}

        if(!is_dir(IMAGE_UPLOAD_DIRECTORY)){ mkdir(IMAGE_UPLOAD_DIRECTORY); }

        list($w, $h, $type) = @getimagesize($field["tmp_name"]);

        switch($type) {
            case 1:
                $img = @imagecreatefromgif($field["tmp_name"]);
                break;
            case 2:
                $img = @imagecreatefromjpeg($field["tmp_name"]);
                break;
            case 3:
                $img = @imagecreatefrompng($field["tmp_name"]);
                break;
        }

        $max_w = MAX_UPLOAD_WIDTH;
        $max_h = MAX_UPLOAD_HEIGHT;

        $new_width = $w;
        $new_height = $h;

        if($w > $max_w || $h > $max_h){

            if($w > $h){
                $ratio = $max_w / $w;
                $new_width = $max_w;
                $new_height = $h * $ratio;
            } else {
                $ratio = $max_h / $h;
                $new_width = $max_w * $ratio;
                $new_height = $max_h;
            }

        }

        $new_img = @imagecreatetruecolor($new_width, $new_height);
        @imagefilledrectangle($new_img, 0, 0, $new_width, $new_height, 0);
        @imagecopyresampled($new_img, $img, 0, 0, 0, 0, $new_width, $new_height, $w, $h);

        ob_start();
        imagejpeg($new_img, NULL, 80);
        $image_contents = ob_get_contents();
        ob_end_clean();
		
		if(!is_dir(PUBLIC_ROOT . JOURNAL_IMAGES . $journal_slug . DS)){ mkdir(PUBLIC_ROOT . JOURNAL_IMAGES . $journal_slug . DS); }
		
		$fh = fopen(PUBLIC_ROOT . JOURNAL_IMAGES . $journal_slug . DS . $filename, "a+");
		
        fwrite($fh, $image_contents);
        fclose($fh);

        return array('filename' => $filename, 'width' => $w, 'height' => $h, 'extension' => $ext, 'mime' => 'image/jpeg');
	}
	
	protected function multiple_uploads($array, $types, $slug = '') {
		$names = $array['files']['name'];
		$tmp_name = $array['files']['tmp_name'];
		$count = 0;
		
		if ($types == 'footwear') {
			$url = FOOTWEAR_IMAGES;
		} else if ($types == 'album') {
			$temp = GALLERY_IMAGES . $slug;
			if(!is_dir(PUBLIC_ROOT . $temp . DS)){ mkdir(PUBLIC_ROOT . $temp . DS); }
			
			$url = $temp . DS . $slug .  '_';
		}
		
		foreach ($array['files']['name'] as $name) {
			
			$file_name = $name;
			$file_tmp = $tmp_name[$count];
			
			$count++;
			move_uploaded_file($file_tmp, PUBLIC_ROOT . $url . $file_name);
		}
		
		return $names;
	}

    protected function get_item_types(){
        $types = array(
            /*
            'item' => array(
                'value' => 'item',
                'title' => 'Generic Item',
                'icon'  => 'list',
                'fields' => '*'
            ),
            */
            'article' => array(
                'value' => 'article',
                'title' => 'Article',
                'icon' => 'align-center',
                'fields' => array('user_id', 'title', 'slug', 'excerpt', 'body', 'category_id', 'tags', 'publish_datetime', 'status', 'page')
            ),
            'product' => array(
                'value' => 'product',
                'title' => 'Product',
                'icon'  => 'tags',
                'fields' => array('user_id', 'title', 'slug', 'skew', 'excerpt', 'body', 'price', 'purchase_url', 'internal_notes', 'tags', 'publish_datetime', 'brand_id', 'style_id', 'designer_id', 'colors', 'store_id', 'status', 'new_release')
            ),
            'slider' => array(
                'value' => 'slider',
                'title' => 'Slider',
                'icon' => 'film',
                'fields' => array('user_id', 'title', 'slug', 'publish_datetime', 'status')
            ),
            'photo' => array(
                'value' => 'photo',
                'title' => 'Photo',
                'icon' => 'picture',
                'fields' => array('user_id', 'title', 'slug', 'excerpt', 'body', 'category_id', 'tags', 'publish_datetime', 'status')
            ),
            'video' => array(
                'value' => 'video',
                'title' => 'Video',
                'icon' => 'facetime-video',
                'fields' => array('user_id', 'title', 'slug', 'video_url', 'excerpt', 'body', 'category_id', 'tags', 'publish_datetime', 'status')
            )
        );

        return $types;
    }

    protected function type_allows_field($type, $field){

        if(!$this->type_exists($type)){
            return false;
        }

        $fields = $this->item_types[$type]['fields'];

        if($fields == '*'){
            return true;
        }

        if(in_array($field, $fields)){
            return true;
        }

        return false;

    }

    protected function type_exists($type){

        if(isset($this->item_types[$type])){
            return true;
        }

        return false;

    }


    protected function get_options($objects, $current, $multiple = false, $allow_none = true, $value = 'id', $title = 'title'){

        $return = array();
        if($allow_none){
            $return[] = array('value' => 0, 'title' => 'None');
        }
        foreach($objects as $object){
            if($multiple){
                $current_array = explode(',', $current);
                $selected = (in_array($object->id, $current_array)) ? 'selected' : '';
            } else {
                $selected = ($current == $object->id) ? 'selected' : '';
            }
            $return[] = array(
                'value' => $object->$value,
                'title' => $object->$title,
                'selected' => $selected,
            );
        }

        return $return;

    }

}