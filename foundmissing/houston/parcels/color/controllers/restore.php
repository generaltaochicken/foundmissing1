<?php
namespace Color\Controllers;

class Restore extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$color = \Color\Models\Color::find($data["color_id"]);

		if($color){
			$color->deleted = 0;
			$color->save();
			$this->add_flash(array("message" => "Color has been restored!"));

			redirect_to($color->link_all());
		}
	}
}
