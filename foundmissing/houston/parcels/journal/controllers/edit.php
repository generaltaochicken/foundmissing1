<?php
namespace Journal\Controllers;

class Edit extends \Manage\Controllers\Manage{

	public $Journal;

	public $page_title;
	public $link_back;
	public $form;
	public $existing;
	
	public $aritcle_title;
	public $article;
	public $published = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		if(isset($_POST["submit"])){
			$this->submit_form();
		}

		if(isset($data["Journal_id"])){
			$this->Journal = \Journal\Models\Journal::find($data["Journal_id"]);
			$this->page_title = "Editing Journal";
		} else {
			$this->Journal = new \Journal\Models\Journal;
			$this->page_title = "New Journal";
		}

		$this->link_back = $this->Journal->link_all();
		
		if ($this->Journal->publish_date != 0 || $this->article->publish_date != null) {
			$this->published = true;
		}
		
		if($this->Journal->main_image){
			$this->preview = JOURNAL_IMAGES . $this->Journal->main_image;
		}
		
		/*$form = new \Form\Models\Form("edit", "POST", "", "well");

		$item = new \Form\Models\FormItemHidden(array("id" => "id", "title" => "Id", "value" => $this->Journal->id));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "title", "title" => "Title", "value" => $this->Journal->title));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "author", "title" => "Author", "value" => $this->Journal->author));
		$form->add_item($item);

		$item = new \Form\Models\FormItemTextarea(array("id" => "body", "title" => "Body", "value" => $this->Journal->body));
		$form->add_item($item);

		$item = new \Form\Models\FormItemTextarea(array("id" => "excerpt", "title" => "Excerpt", "value" => $this->Journal->excerpt));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "main_image", "title" => "Main Image", "value" => $this->Journal->main_image));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "publish_date", "title" => "Publish Date", "value" => $this->Journal->publish_date));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "slug", "title" => "Slug", "value" => $this->Journal->slug));
		$form->add_item($item);

		$item = new \Form\Models\FormItemSubmit(array("value" => "Save", "id" => "submit", "class" => "btn btn-primary", "cancel_url" => $this->Journal->link_all()));
		$form->add_item($item);

		$this->form = $form->render();*/

	}

	public function controller(){
		$this->set_view("Journal\Views\Edit");
	}

	private function get_publish_options() {
		$published = array(
			array('value' => '1', 'title' => 'Yes', 'selected' => 0),
			array('value' => '0', 'title' => 'No', 'selected' => 1)
		);
		
		if  ($this->article->publish_date != 0 || $this->article->publish_date != null) {
			$published[0]['selected'] = 1;
			$published[1]['selected'] = 0;
		}
		
		return $published;
	}
	
	private function submit_form(){
		if($_POST["id"] > 0){
			$Journal = \Journal\Models\Journal::find($_POST["id"]);
			$flash_msg = "Journal has been updated successfully.";
		} else {
			$Journal = new \Journal\Models\Journal;
			$flash_msg = "Journal has been created successfully.";
			$Journal->ordering = \Journal\Models\Journal::get_next_ordering_number();
			$Journal->entry_datetime = time();
		}

		$Journal->title = $_POST["title"];
		$Journal->author = $_POST["author_name"];
		$Journal->body = $_POST["body"];
		$Journal->excerpt = $_POST["excerpt"];
		$Journal->slug = $_POST["slug"];

		if(isset($_FILES['main_image']['tmp_name']) && $_FILES['main_image']['tmp_name'] != ''){
			$image_slug = $Journal->slug;
			$image = $this->upload_journal($_FILES['main_image'], array('jpg', 'png', 'gif', 'jpeg'), $image_slug);

			if($image) {
				$Journal->main_image = $image_slug . '/' . $image['filename'];
			}
		}
		
		$Journal->publish_date = $_POST["publish_date"];
		if ($Journal->publish_date == 1) {
			if ($this->Journal->publish_date <= 1) {
				$Journal->publish_date = time();
			}
		}
		
		$Journal->save();
		$this->add_flash(array("message" => $flash_msg, "type" => "success", "Heading" => "Great!"));
		//redirect_to($Journal->link_all());
	}

}
