<?php

namespace Form\Models;

class FormItemSubmit extends FormItem {
    
    public function __construct($attributes){
        $this->type = 'submit';
        $this->template = 'submit';
        parent::__construct($attributes);
    }
    
    
    
}