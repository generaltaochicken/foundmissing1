<?php
namespace Designer\Controllers;

class Delete extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$designer = \Designer\Models\Designer::find($data["designer_id"]);

		if($designer){
			$designer->deleted = 1;
			$designer->save();
			$this->add_undo(array("message" => "Designer has been deleted!", "action" => $designer->link_restore()));

			redirect_to($designer->link_all());
		}
	}
}
