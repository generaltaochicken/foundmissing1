<?php
namespace Winner\Controllers;

class Delete extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$winner = \Winner\Models\Winner::find($data["winner_id"]);

		if($winner){
			$winner->deleted = 1;
			$winner->save();
			$this->add_undo(array("message" => "Winner has been deleted!", "action" => $winner->link_restore()));

			redirect_to($winner->link_all());
		}
	}
}
