<?php
namespace Frontend\Controllers;

class Home extends \Frontend\Controllers\Frontend {
	
	public function __construct($uri, $data){
		parent::__construct($uri, $data);
		$this->add_asset('css', 'main.css', true);
		$this->add_asset('js', 'main.js', true);
		
		$this->get_slider_content();
		
		$this->box_one = $this->get_Box(1);
		$this->box_two = $this->get_Box(2);
		$this->box_three = $this->get_Box(3);
		$this->box_four = $this->get_Box(4);
		$this->box_five = $this->get_Box(5);
		$this->box_six = $this->get_Box(6);
		$this->box_seven = $this->get_Box(7);
	}
	
	public function controller(){
		$this->set_view('Frontend\Views\Home');
	}
	
	public function get_slider_content() {
		$slider = \Frontpage\Models\Frontpage::all(array('conditions' => 'deleted = 0 AND slider = 1', 'order' => 'ordering ASC'));
		$first = true;
		
		foreach ($slider as $content) {
			if ($content->type == 'article') {
				$articles = \Article\Models\Article::first(array('select' => 'title, slug, bg_image_header', 'conditions' => 'id = ' . $content->parent_id));
				
				$article = array(
					'title' => $articles->title,
					'slug' => '/articles/' . $articles->slug,
					'image' => ARTICLE_IMAGES . $articles->bg_image_header,
					'icon' => 5,
					'first' => $first
				);
			} else if ($content->type == 'review') {
				$reviews = \Review\Models\Review::first(array('select' => 'title, slug, cover_image', 'conditions' => 'id = ' . $content->parent_id));
				
				$article = array(
					'title' => $reviews->title,
					'slug' => '/reviews/' . $reviews->slug,
					'image' => REVIEW_IMAGES . $reviews->cover_image,
					'icon' => 7,
					'first' => $first
				);
				
			} else if ($content->type == 'interview') {
				$interview = \Interview\Models\Interview::first(array('select' => 'title, slug, main_image', 'conditions' => 'id = ' . $content->parent_id));
				$article = array(
					'title' => $interview->title,
					'slug' => '/interviews/' . $interview->slug,
					'image' => ARTICLE_IMAGES . $interview->main_image,
					'icon' => 6,
					'first' => $first
				);
			} else if ($content->type = 'gallery') {
				$gallery = \Gallery\Models\Gallery::first(array('select' => 'id, title, slug', 'conditions' => 'id = ' . $content->parent_id));
				
				$gallery_attachment = \Gallery_attachment\Models\Gallery_attachment::first(array('select' => 'attachment_id', 'conditions' => 'deleted = 0 AND gallery_id = ' . $content->parent_id));
				
				$attachment = GALLERY_IMAGES . $gallery->slug . '/' . \Attachment\Models\Attachment::get_cover_attachment($gallery_attachment->attachment_id);
				
				$article = array(
					'title' => $gallery->title,
					'slug' => '/gallery#' . $gallery->id,
					'image' => $attachment,
					'icon' => 8,
					'first' => $first
				);
			}
			
			$this->slider_content[] = $article;
			$first = false;
			
		}
	}
	
	protected function get_Box($boxNum) {
		$box = \Frontpage\Models\Frontpage::first(array('conditions' => 'deleted = 0 AND position = "' . $boxNum . '"', 'order' => 'id DESC'));
		
		if ($box)
			return $this->get_frontpage_data($box);
	}
	
	protected function get_frontpage_data($content) {
	
		if ($content->type == 'article') {
			$articles = \Article\Models\Article::first(array('select' => 'title, slug, bg_image_header, entry_datetime, author_name, excerpt', 'conditions' => 'id = ' . $content->parent_id));
			
			$article = array(
				'title' => $articles->title,
				'slug' => '/articles/' . $articles->slug,
				'image' => ARTICLE_IMAGES . $articles->bg_image_header,
				'icon' => 5,
				'date' => time_to_friendly_date($articles->entry_datetime, 'Y.m.d'),
				'author' => ucwords($articles->author_name),
				'description' => (strlen($articles->excerpt) > 350) ? substr($articles->excerpt,0,350).'...' : $articles->excerpt,
				'type' => 'article'
			);
		} else if ($content->type == 'review') {
			$reviews = \Review\Models\Review::first(array('select' => 'title, slug, cover_image, entry_datetime, excerpt, user_id', 'conditions' => 'id = ' . $content->parent_id));
			
			$article = array(
				'title' => $reviews->title,
				'slug' => '/reviews/' . $reviews->slug,
				'image' => REVIEW_IMAGES . $reviews->cover_image,
				'icon' => 7,
				'date' => time_to_friendly_date($reviews->entry_datetime, 'Y.m.d'),
				'description' => (strlen($reviews->excerpt) > 350) ? substr($reviews->excerpt,0,350).'...' : $reviews->excerpt,
				'author' => ucwords(\User\Models\User::get_user_fullname_by_id($reviews->user_id)),
				'type' => 'review'
			);
			
		} else if ($content->type == 'interview') {
			$interview = \Interview\Models\Interview::first(array('select' => 'title, slug, main_image, entry_datetime, excerpt, author_name', 'conditions' => 'id = ' . $content->parent_id));
			$article = array(
				'title' => $interview->title,
				'slug' => '/interviews/' . $interview->slug,
				'image' => ARTICLE_IMAGES . $interview->main_image,
				'icon' => 6,
				'date' => time_to_friendly_date($interview->entry_datetime, 'Y.m.d'),
				'description' => (strlen($interview->excerpt) > 350) ? substr($interview->excerpt,0,350).'...' : $interview->excerpt,
				'author' => ucwords($interview->author_name),
				'type' => 'interview'
			);
		} else if ($content->type = 'gallery') {
			$gallery = \Gallery\Models\Gallery::first(array('conditions' => 'id = ' . $content->parent_id));
			
			$gallery_attachment = \Gallery_attachment\Models\Gallery_attachment::first(array('select' => 'attachment_id', 'conditions' => 'deleted = 0 AND gallery_id = ' . $content->parent_id));
				
			$attachment = GALLERY_IMAGES . $gallery->slug . '/' . \Attachment\Models\Attachment::get_cover_attachment($gallery_attachment->attachment_id);
			
			$article = array(
				'title' => $gallery->title,
				'slug' => '/gallery#' . $gallery->id,
				'image' => $attachment,
				'icon' => 8,
				'date' => time_to_friendly_date($gallery->entry_datetime, 'Y.m.d'),
				'author' => ucwords($gallery->author_name),
				'description' => (strlen($gallery->description) > 350) ? substr($gallery->description,0,350).'...' : $gallery->description,
				'type' => 'photoshoot'
			);
		}
		
		$contents[] = $article;
		
		return $contents;
	}
	
}

?>