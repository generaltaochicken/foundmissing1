<?php
namespace Account\Controllers;

class Settings extends \Account\Controllers\Account {

    public function __construct($uri, $data) {
    	parent::__construct($uri, $data);

    	if(isset($_POST["submit"])){
    		$this->submit_form();
    	}

    	$form = new \Form\Models\Form("edit", "POST", "", "well");

    	$item = new \Form\Models\FormItemText(array("id" => "firstname", "title" => "First Name", "value" => $this->_user->firstname));
    	$form->add_item($item);

    	$item = new \Form\Models\FormItemText(array("id" => "lastname", "title" => "Last Name", "value" => $this->_user->lastname));
    	$form->add_item($item);

    	$item = new \Form\Models\FormItemText(array("id" => "password", "title" => "Change Password", "value" => ""));
    	$form->add_item($item);

    	$item = new \Form\Models\FormItemSubmit(array("value" => "Save", "id" => "submit", "class" => "btn btn-primary", "cancel_url" => '/account'));
    	$form->add_item($item);

    	$this->form = $form->render();


    }

    private function submit_form(){

		$this->_user->firstname = $_POST["firstname"];
		$this->_user->lastname = $_POST["lastname"];

		if($_POST['password'] != ''){
            $this->_user->password = md5(DB_SALT . $_POST['password']);
        }

		$this->_user->save();
		$this->add_flash(array("message" => $flash_msg, "type" => "success", "Heading" => "Great!"));

		redirect_to('/account');
	
	}
    
}