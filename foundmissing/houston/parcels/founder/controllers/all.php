<?php
namespace Founder\Controllers;

class All extends \Manage\Controllers\Manage{

	private $articles;

	public $articles_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Founders";
		$this->link_new = \Founder\Models\Founder::link_new();

		$this->articles = \Founder\Models\Founder::all(array("conditions" => "deleted = 0 AND publish_date > 1" , "order" => "ordering"));
		$unpublished = \Founder\Models\Founder::all(array("conditions" => "deleted = 0 AND publish_date = 0" , "order" => "ordering"));
		$archived = \Founder\Models\Founder::count(array("conditions" => "deleted = 1"));
		if($archived > 0){
			$this->archive_button = array("url" => \Founder\Models\Founder::link_archive(), "title" => "Archive");
		}

		foreach($this->articles as $article){
			$this->has_data = true;
			$this->articles_data[] = array(
				"id" => $article->id,
				"entry_datetime" => time_to_friendly_date($article->entry_datetime),
				"author_name" => $article->author_name,
				"title" => $article->title,
				"slug" => $article->slug,
				"excerpt" => substr($article->excerpt, 0, 100) . '...',
				"bg_image_header" => $article->bg_image_header,
				"link_edit" => $article->link_edit(),
				"link_delete" => $article->link_delete(),
				"ordering" => $article->ordering,
			);
		}
		
		foreach($unpublished as $article){
			$this->has_data = true;
			$this->articles_unpublished[] = array(
				"id" => $article->id,
				"entry_datetime" => time_to_friendly_date($article->entry_datetime),
				"author_name" => $article->author_name,
				"title" => $article->title,
				"slug" => $article->slug,
				"excerpt" => substr($article->excerpt, 0, 100) . '...',
				"bg_image_header" => $article->bg_image_header,
				"link_edit" => $article->link_edit(),
				"link_delete" => $article->link_delete(),
				"ordering" => $article->ordering,
			);
		}

	}

	public function controller(){
		$this->set_view("Founder\Views\All");
	}

}

