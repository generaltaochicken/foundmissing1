<?php

namespace Frontend\Controllers;

class ActivityAjax extends \Frontend\Controllers\Frontend {
	
    public function __construct($uri, $data){
        parent::__construct($uri, $data);

        $this->data = $data;
		$this->user_id = $this->_user->id;
		
		if (isset($_POST['position'])) {
			$this->offset = $_POST['position'];
			$this->get_next_activities();
		}
    }

    public function controller(){
	
    }
	
	protected function get_next_activities() {
		if ($_POST['isProfile'] == 1) {
			$user_id = $_POST['profile_user'];
			$identifier = \User\Models\User::find($_POST['profile_user'])->fullname() . '\'s';
		} else {
			$user_id = $this->_user->id;
			$identifier  = 'your';
		}
		
		$activities = \Activity\Models\Activity::all(array('conditions' => 'deleted = 0 AND user_id = "' . $user_id  . '" OR parent_id = "' . $user_id  . '"', 'order' => 'entry_datetime DESC', 'limit' => 5, 'offset' => $this->offset));
		
		$all_activities = \Activity\Models\Activity::all(array('conditions' => 'deleted = 0 AND user_id = "' . $user_id  . '" OR parent_id = "' . $user_id  . '"'));
		$activity_count = count($all_activities) - $this->offset;
		
		if ($activity_count < 5) {
			$more_activities = false;
		} else {
			$more_activities = true;
		}
		
		$output = '';

		foreach ($activities as $act) {
			$date = \Comment\Models\Comment::first(array('select' => 'entry_datetime', 'conditions' => 'id = ' . $act->child_id));
			$json = json_decode($date->to_json(array('only' => 'entry_datetime')));
			$converted_date = \Frontend\Controllers\Frontend::time_elapsed_string($json->entry_datetime);
			
			$output .= '<div class="row">
						<div class="col-md-12 activity_loaded">
							<div class="row">';
			$output .= '			<div class="col-xs-3 col-md-3">';
			
			if ($act->activity_type == 'article_post') {
				$article_info = \Article\Models\Article::first(array('select' => 'slug, title, bg_image_header', 'conditions' => 'id = ' . $act->source_id));
				$comment = \Comment\Models\Comment::first(array('select' => 'comment_body', 'conditions' => 'id = ' . $act->child_id . ' AND post_type = 1'));
				
				$output .= '			<div class="activity-image" style="background-image: url(' . ARTICLE_IMAGES . $article_info->bg_image_header . '); -ms-background-position-x: center; -ms-background-position-y: center; background-position: center center; background-size: cover;"></div>
								</div>'; // End of col-md-3
				$output .= '	<div class="col-xs-1 col-md-1 activity_icon_container">
								<i class="fm_icon activity_icon">&#45;</i>
							</div>';
				
				$output .= '<div class="col-xs-8 col-md-8 mobile-activity-content">
							<span class="activity_title">Commented on <a href="/articles/' . $article_info->slug . '">' . $article_info->title . '</a></span>
						
							<span class="activity_date">&nbsp;/&nbsp;' . $converted_date . '</span>
							</br>
							<span class="activity_body">&ldquo;' . $comment->comment_body . '&rdquo;</i></span>
						</div>';
			} else if ($act->activity_type == 'article_comment') {
				$comment = \Comment\Models\Comment::first(array('select' => 'comment_body', 'conditions' => 'id = ' . $act->child_id . ' AND post_type = 1'));
				$article_slug = \Article\Models\Article::first(array('select' => 'slug', 'conditions' => 'id = ' . $act->source_id));
			
				if ($act->parent_id != null && $act->parent_id == $user_id) {
					$users_image = \User\Models\User::find($act->user_id)->get_image_reply();
					$usersname = \User\Models\User::find($act->user_id)->get_uusername();
					$user_fullname = \User\Models\User::find($act->user_id)->fullname();
					$replied_to = true;
				} else {
					$users_image = \User\Models\User::find($act->parent_id)->get_image_reply();
					$usersname =  \User\Models\User::find($act->parent_id)->get_uusername();
					$user_fullname = \User\Models\User::find($act->parent_id)->fullname();
					$replied_to = false;
				}
				
				if ($users_image == null) {
					$users_image = 'default_avatar.png';
				}
				
				if ($act->parent_id == $act->user_id) {
					if ($_POST['isProfile'] == 0) {
						$user_fullname = 'you';
						$identifier = 'your';
					} else {
							$user_fullname = '';
						}
				}
			
					
				$output .= '<div class="activity-image" style="background-image: url(/uploads/images/' . $users_image . '); -ms-background-position-x: center; -ms-background-position-y: center; background-position: center center; background-size: cover;"></div>';
				$output .= '</div>'; // End of col-md-3
				$output .= '	<div class="col-xs-1 col-md-1 activity_icon_container">
								<i class="fm_icon activity_icon">&#45;</i>
							</div>';
							
				$output .= '<div class="col-xs-8 col-md-8 mobile-activity-content">';
				if ($replied_to) {
					$output .= '
						<span class="activity_title">
							<a href="/profile/' . $usersname . '">' . $user_fullname . '</a> replied to ' . $identifier . ' <a href="/articles/' . $article_slug->slug . '#comment_' . $act->child_id . '">comment</a>
						</span>';
				} else {
					$output .= '
						<span class="activity_title">
							replied to <a href="/profile/' . $usersname . '">' . $user_fullname . '\'s</a> <a href="/articles/' . $article_slug->slug . '#comment_' . $act->child_id . '">comment</a>
						</span>';
				}
					
				$output .= '
							<span class="activity_date">&nbsp;/&nbsp;' . $converted_date . '</span>
							</br>
							<span class="activity_body">&ldquo;' . $comment->comment_body . '&rdquo;</i></span>
						</div>';
			} else if ($act->activity_type == 'interview_post') {
				$article_info = \Interview\Models\Interview::first(array('select' => 'slug, title, main_image', 'conditions' => 'id = ' . $act->source_id));
				$comment = \Comment\Models\Comment::first(array('select' => 'comment_body', 'conditions' => 'id = ' . $act->child_id . ' AND post_type = 2'));
				
				$output .= '			<div class="activity-image" style="background-image: url(' . ARTICLE_IMAGES . $article_info->main_image . '); -ms-background-position-x: center; -ms-background-position-y: center; background-position: center center; background-size: cover;"></div>
								</div>'; // End of col-md-3
				$output .= '	<div class="col-xs-1 col-md-1 activity_icon_container">
								<i class="fm_icon activity_icon">&#45;</i>
							</div>';
				
				$output .= '<div class="col-xs-8 col-md-8 mobile-activity-content">
							<span class="activity_title">Commented on <a href="/interviews/' . $article_info->slug . '">' . $article_info->title . '</a></span>
						
							<span class="activity_date">&nbsp;/&nbsp;' . $converted_date . '</span>
							</br>
							<span class="activity_body">&ldquo;' . $comment->comment_body . '&rdquo;</i></span>
						</div>';
			} else if ($act->activity_type == 'interview_comment') {
				$comment = \Comment\Models\Comment::first(array('select' => 'comment_body', 'conditions' => 'id = ' . $act->child_id . ' AND post_type = 2'));
				$article_slug = \Interview\Models\Interview::first(array('select' => 'slug', 'conditions' => 'id = ' . $act->source_id));
			
				if ($act->parent_id != null && $act->parent_id == $user_id) {
					$users_image = \User\Models\User::find($act->user_id)->get_image_reply();
					$usersname = \User\Models\User::find($act->user_id)->get_uusername();
					$user_fullname = \User\Models\User::find($act->user_id)->fullname();
					$replied_to = true;
				} else {
					$users_image = \User\Models\User::find($act->parent_id)->get_image_reply();
					$usersname =  \User\Models\User::find($act->parent_id)->get_uusername();
					$user_fullname = \User\Models\User::find($act->parent_id)->fullname();
					$replied_to = false;
				}
				
				if ($users_image == null) {
					$users_image = 'default_avatar.png';
				}
				
				if ($act->parent_id == $act->user_id) {
					if ($_POST['isProfile'] == 0) {
						$user_fullname = 'you';
						$identifier = 'your';
					} else {
							$user_fullname = '';
						}
				}
			
					
				$output .= '<div class="activity-image" style="background-image: url(/uploads/images/' . $users_image . '); -ms-background-position-x: center; -ms-background-position-y: center; background-position: center center; background-size: cover;"></div>';
				$output .= '</div>'; // End of col-md-3
				$output .= '	<div class="col-xs-1 col-md-1 activity_icon_container">
								<i class="fm_icon activity_icon">&#45;</i>
							</div>';
							
				$output .= '<div class="col-xs-8 col-md-8 mobile-activity-content">';
				if ($replied_to) {
					$output .= '
						<span class="activity_title">
							<a href="/profile/' . $usersname . '">' . $user_fullname . '</a> replied to ' . $identifier . ' <a href="/interviews/' . $article_slug->slug . '#comment_' . $act->child_id . '">comment</a>
						</span>';
				} else {
					$output .= '
						<span class="activity_title">
							replied to <a href="/profile/' . $usersname . '">' . $user_fullname . '\'s</a> <a href="/interviews/' . $article_slug->slug . '#comment_' . $act->child_id . '">comment</a>
						</span>';
				}
					
				$output .= '
							<span class="activity_date">&nbsp;/&nbsp;' . $converted_date . '</span>
							</br>
							<span class="activity_body">&ldquo;' . $comment->comment_body . '&rdquo;</i></span>
						</div>';
			} else if ($act->activity_type == 'review_post') {
				$article_info = \Review\Models\Review::first($act->source_id);
				$article_image = $article_info->cover_image;
				
				$comment = \Comment\Models\Comment::first(array('select' => 'comment_body', 'conditions' => 'id = ' . $act->child_id . ' AND post_type = 3'));
				
				$output .= '			<div class="activity-image" style="background-image: url(' . ARTICLE_IMAGES . $article_image . '); -ms-background-position-x: center; -ms-background-position-y: center; background-position: center center; background-size: cover;"></div>
								</div>'; // End of col-md-3
				$output .= '	<div class="col-xs-1 col-md-1 activity_icon_container">
								<i class="fm_icon activity_icon">&#45;</i>
							</div>';
				
				$output .= '<div class="col-xs-8 col-md-8 mobile-activity-content">
							<span class="activity_title">Commented on <a href="/reviews/' . $article_info->slug . '">' . $article_info->title . '</a></span>
						
							<span class="activity_date">&nbsp;/&nbsp;' . $converted_date . '</span>
							</br>
							<span class="activity_body">&ldquo;' . $comment->comment_body . '&rdquo;</i></span>
						</div>';
			} else if ($act->activity_type == 'review_comment') {
				$comment = \Comment\Models\Comment::first(array('select' => 'comment_body', 'conditions' => 'id = ' . $act->child_id . ' AND post_type = 3'));
				$article_slug = \Review\Models\Review::first(array('select' => 'slug', 'conditions' => 'id = ' . $act->source_id));
			
				if ($act->parent_id != null && $act->parent_id == $user_id) {
					$users_image = \User\Models\User::find($act->user_id)->get_image_reply();
					$usersname = \User\Models\User::find($act->user_id)->get_uusername();
					$user_fullname = \User\Models\User::find($act->user_id)->fullname();
					$replied_to = true;
				} else {
					$users_image = \User\Models\User::find($act->parent_id)->get_image_reply();
					$usersname =  \User\Models\User::find($act->parent_id)->get_uusername();
					$user_fullname = \User\Models\User::find($act->parent_id)->fullname();
					$replied_to = false;
				}
				
				if ($users_image == null) {
					$users_image = 'default_avatar.png';
				}
				
				if ($act->parent_id == $act->user_id) {
					if ($_POST['isProfile'] == 0) {
						$user_fullname = 'you';
						$identifier = 'your';
					} else {
							$user_fullname = '';
						}
				}
			
					
				$output .= '<div class="activity-image" style="background-image: url(/uploads/images/' . $users_image . '); -ms-background-position-x: center; -ms-background-position-y: center; background-position: center center; background-size: cover;"></div>';
				$output .= '</div>'; // End of col-md-3
				$output .= '	<div class="col-xs-1 col-md-1 activity_icon_container">
								<i class="fm_icon activity_icon">&#45;</i>
							</div>';
							
				$output .= '<div class="col-xs-8 col-md-8 mobile-activity-content">';
				if ($replied_to) {
					$output .= '
						<span class="activity_title">
							<a href="/profile/' . $usersname . '">' . $user_fullname . '</a> replied to ' . $identifier . ' <a href="/reviews/' . $article_slug->slug . '#comment_' . $act->child_id . '">comment</a>
						</span>';
				} else {
					$output .= '
						<span class="activity_title">
							replied to <a href="/profile/' . $usersname . '">' . $user_fullname . '\'s</a> <a href="/reviews/' . $article_slug->slug . '#comment_' . $act->child_id . '">comment</a>
						</span>';
				}
					
				$output .= '
							<span class="activity_date">&nbsp;/&nbsp;' . $converted_date . '</span>
							</br>
							<span class="activity_body">&ldquo;' . $comment->comment_body . '&rdquo;</i></span>
						</div>';
			}
			
			$output .= '</div></div></div>';
		}
		
		$output = array('activities' => $output, 'more_activities' => $more_activities);
		echo json_encode($output);
	}

}