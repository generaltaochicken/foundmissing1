<?php
namespace Interview\Controllers;

class All extends \Manage\Controllers\Manage{

	private $interviews;

	public $interviews_data = array();
	public $link_new;
	public $page_title;
	public $has_unpublished = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Interviews";
		$this->link_new = \Interview\Models\Interview::link_new();

		$this->interviews = \Interview\Models\Interview::all(array("conditions" => "deleted = 0 AND publish_date > 0" , "order" => "ordering"));
		$this->unpublished_interviews =  \Interview\Models\Interview::all(array("conditions" => "deleted = 0 AND publish_date = 0" , "order" => "ordering"));
		$archived = \Interview\Models\Interview::count(array("conditions" => "deleted = 1"));
		if($archived > 0){
			$this->archive_button = array("url" => \Interview\Models\Interview::link_archive(), "title" => "Archive");
		}

		foreach($this->interviews as $interview){
			$this->has_data = true;
			$this->interviews_data[] = array(
				"id" => $interview->id,
				"entry_datetime" => time_to_friendly_date($interview->entry_datetime),
				"author_name" => $interview->author_name,
				"title" => $interview->title,
				"slug" => $interview->slug,
				"excerpt" => $interview->excerpt,
				"main_image" => $interview->main_image,
				"publish" => $interview->publish_date,
				"link_edit" => $interview->link_edit(),
				"link_delete" => $interview->link_delete(),
			);
		}
		
		foreach ($this->unpublished_interviews as $interview) {
			$this->has_data = true;
			$this->interview_unpublish[] = array(
				"id" => $interview->id,
				"entry_datetime" => time_to_friendly_date($interview->entry_datetime),
				"author_name" => $interview->author_name,
				"title" => $interview->title,
				"slug" => $interview->slug,
				"excerpt" => $interview->excerpt,
				"main_image" => $interview->main_image,
				"publish" => $interview->publish_date,
				"link_edit" => $interview->link_edit(),
				"link_delete" => $interview->link_delete(),
			);
		}

	}

	public function controller(){
		$this->set_view("Interview\Views\All");
	}

}

