<?php
namespace Newsletter\Controllers;

class Delete extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$newsletter = \Newsletter\Models\Newsletter::find($data["newsletter_id"]);

		if($newsletter){
			$newsletter->deleted = 1;
			$newsletter->save();
			$this->add_undo(array("message" => "Newsletter has been deleted!", "action" => $newsletter->link_restore()));

			redirect_to($newsletter->link_all());
		}
	}
}
