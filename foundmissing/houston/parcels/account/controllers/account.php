<?php

namespace Account\Controllers;

class Account extends \Frontend\Controllers\Frontend {

    protected $_user;
    protected $unprotected_controllers;
    protected $not_account_page = false;

	public function __construct($uri, $data){
        parent::__construct($uri, $data);

        $this->unprotected_controllers = array('Account\\Controllers\\Login', 'Account\\Controllers\\Logout', 'Account\\Controllers\\Register');
        $this->authenticate();

    }

    public function controller(){
		return;
	}


	 protected function authenticate() {

        $user = new \User\Models\User();

        if(!isset($_SESSION[$user->login_session_name]) || $_SESSION[$user->login_session_name] < 1){

            if(!in_array(get_called_class(), $this->unprotected_controllers)){
                if(!$this->not_account_page){
                    $this->add_flash(array('message' => 'You are not permitted to view this page'));
                       redirect_to('/account/login');
                    } 
                }
           
        }

        if(isset($_SESSION[$user->login_session_name])){
            $this->_user = $user->get_logged_in_user();
        }

    }

}