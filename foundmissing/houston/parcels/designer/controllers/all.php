<?php
namespace Designer\Controllers;

class All extends \Manage\Controllers\Manage{

	private $designers;

	public $designers_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Designers";
		$this->link_new = \Designer\Models\Designer::link_new();

		$this->designers = \Designer\Models\Designer::all(array("conditions" => "deleted = 0" , "order" => "ordering"));
		$archived = \Designer\Models\Designer::count(array("conditions" => "deleted = 1"));
		if($archived > 0){
			$this->archive_button = array("url" => \Designer\Models\Designer::link_archive(), "title" => "Archive");
		}

		foreach($this->designers as $designer){
			$this->has_data = true;
			$this->designers_data[] = array(
				"id" => $designer->id,
				"title" => $designer->title,
				"link_edit" => $designer->link_edit(),
				"link_delete" => $designer->link_delete(),
				"ordering" => $designer->ordering,
			);
		}

	}

	public function controller(){
		$this->set_view("Designer\Views\All");
	}

}

