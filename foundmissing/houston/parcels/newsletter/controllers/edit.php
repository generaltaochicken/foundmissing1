<?php
namespace Newsletter\Controllers;

class Edit extends \Manage\Controllers\Manage{

	private $newsletter;

	public $page_title;
	public $link_back;
	public $form;
	public $existing;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		if(isset($_POST["submit"])){
			$this->submit_form();
		}

		if(isset($data["newsletter_id"])){
			$this->newsletter = \Newsletter\Models\Newsletter::find($data["newsletter_id"]);
			$this->page_title = "Editing Newsletter";
		} else {
			$this->newsletter = new \Newsletter\Models\Newsletter;
			$this->page_title = "New Newsletter";
		}

		$this->link_back = $this->newsletter->link_all();

		$form = new \Form\Models\Form("edit", "POST", "", "well");

		$item = new \Form\Models\FormItemHidden(array("id" => "id", "title" => "Id", "value" => $this->newsletter->id));
		$form->add_item($item);
		
		$item = new \Form\Models\FormItemText(array("id" => "title", "title" => "Title", "value" => $this->newsletter->title));
		$form->add_item($item);
		
		$item = new \Form\Models\FormItemTextarea(array("id" => "body", "title" => "Body", "value" => $this->newsletter->body));
		$form->add_item($item);

		$item = new \Form\Models\FormItemSubmit(array("value" => "Save", "id" => "submit", "class" => "btn btn-primary", "cancel_url" => $this->newsletter->link_all()));
		$form->add_item($item);

		$this->form = $form->render();

	}

	public function controller(){
		$this->set_view("Newsletter\Views\Edit");
	}

	private function submit_form(){
		if($_POST["id"] > 0){
			$newsletter = \Newsletter\Models\Newsletter::find($_POST["id"]);
			$flash_msg = "Newsletter has been updated successfully.";
		} else {
			$newsletter = new \Newsletter\Models\Newsletter;
			$flash_msg = "Newsletter has been created successfully.";
			$newsletter->ordering = \Newsletter\Models\Newsletter::get_next_ordering_number();
		}

		$newsletter->title = $_POST["title"];
		$newsletter->body = $_POST["body"];
		$newsletter->entry_datetime = time();
		$newsletter->save();
		$this->add_flash(array("message" => $flash_msg, "type" => "success", "Heading" => "Great!"));
		redirect_to($newsletter->link_all());
	}

}
