<?php
namespace Core\Controllers;

/**
 * ---------------------------------------------------------------
 * Houston v1.1
 * ---------------------------------------------------------------
 * @author Tyler Savery | The Young Astronauts
 * @see http://www.houstonmvc.com
 * @license Open Source
 * ---------------------------------------------------------------
 * 
 * Templating base controller, this can be swapped out with whichever
 * engine you prefer.
 *
 */
class Template_Controller {

    /**
     * Reference back to the main controller.
     *
     * @var class
     */
    protected $main_controller;

    /**
     * The current view to be rendered out.
     *
     * @var string
     */
    public $view;
    
    /**
     * Constructor sets the main controller.
     *
     * @param class The controller being rendered out.
     */
    public function __construct($controller) {
        $this->main_controller = $controller;
    }
    
    /**
     * Defines the isset value for dynamic calls against the template.
     *
     * @param string The name of the parameter being received.
     *
     * @return boolean Always return true to allow access
     *                 to the __get() method.
     */
    public function __isset($name) {
        return true;
    }

    /**
     * Responds to dynamic property requests from Mustache.
     *
     * @var class
     *
     * @return string Spits out the rendered HTML from the template.
     */
    public function __get($name) {
        $parcel = explode('_', $name);
        if ($parcel[0] == 'base') {
            $parcel[0] = BASE_PARCEL;
        }
        $this->view = HOUSTON_ROOT.'\\'.$parcel[0].'\views\\'.$parcel[1];
        return $this->render();
    }
    
    /**
     * Actually renders the value back.
     *
     * @return string The generated HTML from the template.
     */
    public function render() {

        $view = array_slice(explode('\\', strtolower($this->view)), -3, 3);
        $file = \easy_file(array(HOUSTON_ROOT, 'parcels', $view[0], 'views', $view[2] . '.html'));
            
        if (is_file($file)) {
            $m = new \Mustache_Engine();
            $output = file_get_contents($file);
            if (MINIFY_HTML == 'true') {
                $output = $this->sanitize_output($output);
            }
            
            return $m->render($output, $this->main_controller);
        }
        return '';
    }
    
    /**
     * Sanitizes the output buffer
     *
     * @return string Buffer output to sanitize and return.
     */
    public function sanitize_output($buffer) {
        $search = array(
            '/\>[^\S ]+/s', //strip whitespaces after tags, except space
            '/[^\S ]+\</s', //strip whitespaces before tags, except space
            '/(\s)+/s'      // shorten multiple whitespace sequences
        );
        $replace = array('>', '<', '\\1');
        $buffer = preg_replace($search, $replace, $buffer);
        return $buffer;
    }
    
}