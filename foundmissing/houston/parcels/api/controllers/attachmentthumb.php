<?php
namespace Api\Controllers;

class AttachmentThumb{

	protected $max_width = 600;

	public function __construct($uri, $data){
		header("Content-Type: image/jpeg");

		try {
			$attachment = \Attachment\Models\Attachment::find($data['attachment_id']);
		} 
		catch (\ActiveRecord\RecordNotFound $e){
			return false;
		}

		$width = (isset($data['width'])) ? $data['width'] : 150;

		$width = ($width > $this->max_width) ? $this->max_width : $width;

		$img_str = $attachment->thumbnail($width);
		$img = imagecreatefromstring(base64_decode(str_replace('data:image/x-icon;base64,', '', $img_str)));

		imagejpeg($img, NULL, 80);
		die();

	}

	


	



}