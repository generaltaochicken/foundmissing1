$(document).ready(function() {
	$("#the_album").live("click", function() {
		var elem = $(this);
		var carousel_target = elem.data("local");
		
		$(carousel_target).css("display", "block");
	});
	
	 $(document).on('hidden.bs.modal', '.modal', function () {
		$(".carousel").css("display", "none");
		
		/* 
		// EXIT FULL SCREEN
		var docElement, request;
		docElement = document;
		request = docElement.cancelFullScreen|| docElement.webkitCancelFullScreen || docElement.mozCancelFullScreen || docElement.msCancelFullScreen || docElement.exitFullscreen;
		if (typeof request!="undefined" && request){
			request.call(docElement);
		}
		*/
	});
	
	var album_height;
	var album_margin;
	var album_top;
	var album_left;
	var album_width;
	
	$(".fullscreen").toggle(function() {
		var elem = $(this);
		
		var modal = elem.data("modal");
		var carousel = elem.data("carousel");
		var window_height = $(window).height();
		
		
		album_height = $(modal + " .view_album_content").height();
		album_margin = $(modal + " .view_album_inner").css("margin-top");
		album_top = $(modal + " .view_album_inner").css("top");
		album_left = $(modal + " .view_album_inner").css("left");
		album_width = $(modal + " .view_album_inner").css("width");
		
		$(modal + " .view_album_content").css("height", window_height);
		$(modal + " .view_album_inner").css("margin-top", "0");
		$(modal + " .view_album_inner").css("top", "0");
		$(modal + " .view_album_inner").css("left", "0");
		$(modal + " .view_album_inner").css("width", "100%");
		$(carousel + " .item").css("height", window_height);
		
		/*
		// ENTER FULL SCREEN
		var docElement, request;
		docElement = document.documentElement;
		request = docElement.requestFullScreen || docElement.webkitRequestFullScreen || docElement.mozRequestFullScreen || docElement.msRequestFullScreen;
		if (typeof request!="undefined" && request){
			request.call(docElement);
			$(modal + " .view_album_content").css("height", screen.height);
			$(carousel + " .item").css("height", screen.height);
		}*/
	
	}, function(){
		var elem = $(this);
		
		var modal = elem.data("modal");
		var carousel = elem.data("carousel");
		
		var window_height = $(window).height();
		
		$(modal + " .view_album_content").css("height", album_height);
		$(modal + " .view_album_inner").css("margin-top", album_margin);
		$(modal + " .view_album_inner").css("top", album_top);
		$(modal + " .view_album_inner").css("left", album_left);
		$(modal + " .view_album_inner").css("width", album_width);
		$(carousel + " .item").css("height", album_height);
		
		/*
		// EXIT FULL SCREEN
		var docElement, request;

		docElement = document;
		request = docElement.cancelFullScreen|| docElement.webkitCancelFullScreen || docElement.mozCancelFullScreen || docElement.msCancelFullScreen || docElement.exitFullscreen;
		if (typeof request!="undefined" && request){
			request.call(docElement);
		}*/
	});
});

$(window).load(function () {
	if (window.location.hash) {
		var hash = window.location.hash;
		var withoutHash = hash.slice(1);
		
		$('[data-target="#album_modal_' + withoutHash + '"]').click();
	};
});