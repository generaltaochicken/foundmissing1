<?php
namespace Gallery_attachment\Controllers;

class Delete extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$gallery_attachment = \Gallery_attachment\Models\Gallery_attachment::find($data["gallery_attachment_id"]);

		if($gallery_attachment){
			$gallery_attachment->deleted = 1;
			$gallery_attachment->save();
			$this->add_undo(array("message" => "Gallery_attachment has been deleted!", "action" => $gallery_attachment->link_restore()));

			redirect_to($gallery_attachment->link_all());
		}
	}
}
