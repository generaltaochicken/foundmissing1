<?php
namespace User\Controllers;

class Logout extends \User\Controllers\User {

    public function __construct($uri, $data) {
        $user = new \User\Models\User();
		//$this->_user>update_logintime();
        unset($_SESSION[$user->login_session_name]);
        $this->add_flash(array('message' => 'Logged out successfully', 'type' => 'success'));
        redirect_to('/users/login');
    }
    
}