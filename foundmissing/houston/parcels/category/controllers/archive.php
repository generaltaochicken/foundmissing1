<?php
namespace Category\Controllers;

class Archive extends \Manage\Controllers\Manage{

	private $categories;

	public $categories_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Deleted Categories";

		$this->categories = \Category\Models\Category::all(array("conditions" => "deleted = 1"));
		$this->archive_button = array("url" => \Category\Models\Category::link_all(), "title" => "Back");
		foreach($this->categories as $Category){
			$this->has_data = true;
			$this->categories_data[] = array(
				"id" => $Category->id,
				"title" => $Category->title,
				"entry_datetime" => $Category->entry_datetime,
				"link_restore" => $Category->link_restore(),
			);
		}

	}

	public function controller(){
		$this->set_view("Category\Views\All");
	}

}

