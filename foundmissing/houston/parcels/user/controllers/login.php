<?php
namespace User\Controllers;

class Login extends \User\Controllers\User {
    
    public $login_form_partial;
    
    public function __construct($uri, $data) {
        parent::__construct($uri, $data);
        
        $username = '';

        if(isset($_POST['submit']) || isset($_POST['api'])){
            $this->submit_form();
            $email = $_POST['email'];
        }
        
        $this->set_view('User\Views\Login');
        
    }
    
    
    protected function submit_form(){
        
        $user = new \User\Models\User();
        $user->email = $_POST['email'];
        $user->password = $_POST['password'];
        
        $api = false;
        if(isset($_POST['api']) && (int)$_POST['api'] == 1){
            $api = true;
        }
        
        $login = $user->login();
        
        if($login){
            
            $this->add_flash(array('message' => 'Welcome Back ' . $login->firstname . "!", 'type' => 'success'));
            redirect_to("/manage");
      
        } else {
            if($api){
                $this->json_responses = array('code' => "500");
                $this->return_reponse();
            } else {
                $this->set_errors($user->errors); 
            }
            
            
        }
        
    }
    
    
}