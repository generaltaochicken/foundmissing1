-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 14, 2014 at 04:16 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `foundmissingapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE IF NOT EXISTS `activities` (
  `id` int(14) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `activity_type` varchar(30) DEFAULT NULL,
  `source_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `child_id` int(11) DEFAULT NULL,
  `entry_datetime` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`id`, `user_id`, `activity_type`, `source_id`, `parent_id`, `child_id`, `entry_datetime`, `deleted`) VALUES
(1, 15, 'article_post', 5, NULL, 34, 1403395330, 0),
(2, 15, 'article_comment', 5, 12, 30, 1403403071, 0),
(3, 15, 'article_post', 1, NULL, 37, 1403406045, 0),
(4, 15, 'article_comment', 5, 17, 42, 1403406806, 0),
(5, 15, 'article_post', 3, NULL, 43, 1403409202, 0),
(6, 17, 'article_comment', 1, 15, 44, 1403409552, 0),
(7, 12, 'article_comment', 1, 17, 45, 1403444219, 0),
(8, 12, 'article_comment', 1, 15, 46, 1403444231, 0),
(9, 12, 'article_post', 1, NULL, 47, 1403444247, 0),
(10, 15, 'article_post', 3, NULL, 48, 1403469606, 0),
(11, 15, 'article_comment', 3, 15, 49, 1403469621, 0),
(12, 17, 'article_comment', 3, 15, 50, 1403469790, 0),
(13, 17, 'article_comment', 3, 15, 51, 1403469797, 0),
(14, 17, 'article_comment', 3, 15, 52, 1403469841, 0),
(15, 15, 'article_post', 2, NULL, 53, 1403469888, 0),
(16, 15, 'article_comment', 2, 15, 54, 1403473188, 0),
(17, 15, 'article_post', 3, NULL, 55, 1403719590, 0),
(20, 15, 'interview_post', 1, NULL, 59, 1404612982, 0),
(21, 15, 'interview_post', 1, NULL, 60, 1404613074, 0),
(22, 15, 'interview_comment', 1, 15, 61, 1404613502, 0),
(23, 15, 'interview_comment', 1, 15, 62, 1404613504, 0),
(24, 15, 'interview_comment', 1, 15, 63, 1404613617, 0),
(25, 15, 'interview_comment', 1, 15, 64, 1404613762, 0),
(26, 15, 'interview_comment', 1, 15, 65, 1404613770, 0),
(27, 15, 'interview_comment', 1, 15, 66, 1404613808, 0),
(28, 15, 'interview_comment', 1, 15, 67, 1404613940, 0),
(29, 15, 'interview_comment', 1, 15, 68, 1404613997, 0),
(30, 12, 'review_post', 1, NULL, 70, 1404625692, 0),
(31, 12, 'review_comment', 1, 12, 71, 1404625835, 0),
(32, 12, 'interview_post', 1, NULL, 72, 1404626098, 0),
(33, 12, 'interview_post', 1, NULL, 73, 1404626187, 0),
(34, 12, 'review_post', 1, NULL, 74, 1404626346, 0),
(35, 12, 'review_comment', 1, 12, 75, 1404627292, 0),
(36, 12, 'review_post', 3, NULL, 76, 1404627335, 0),
(37, 12, 'article_post', 4, NULL, 77, 1404682865, 0),
(38, 12, 'article_comment', 4, 12, 78, 1404682874, 0),
(39, 12, 'article_comment', 4, 12, 79, 1404682884, 0),
(40, 15, 'interview_comment', 1, 12, 80, 1404683099, 0);

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entry_datetime` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '1',
  `author_name` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `excerpt` text NOT NULL,
  `body` text NOT NULL,
  `bg_image_header` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `entry_datetime`, `deleted`, `ordering`, `author_name`, `title`, `slug`, `excerpt`, `body`, `bg_image_header`) VALUES
(1, 1403052526, 0, 0, 'Mike Nguyen', 'Bricks', 'bricks', 'Nintendo of America president Reggie Fils-Aime attended my wedding. His date was Piper Chapman, the main character from Orange is the New Black, who''s also really good friends with my one-and-a-half year old daughter Charlotte. My kid lives on her own, in an apartment that looks like the bottom of the ocean, and just this morning she got really excited when I gave her a rhino horn to wear on her head.', '<h1>Nintendo</h1> of America president Reggie Fils-Aime attended my wedding. His date was Piper Chapman, the main character from Orange is the New Black, who''s also really good friends with my one-and-a-half year old daughter Charlotte. My kid lives on her own, in an apartment that looks like the bottom of the ocean, and just this morning she got really excited when I gave her a rhino horn to wear on her head.\n\nTomodachi Life for the Nintendo 3DS is the kind of game that''s hard to explain. Even calling it a â€œgameâ€ seems wrong. It looks a bit like The Sims starring your Mii avatar, but really it''s a mixture of a virtual pet and a toy â€” sort of like a Tamagotchi filled with tiny humans that like to play games. Itâ€™s also unlike anything youâ€™ve ever played. Nintendo is typically a conservative company that relies on sequels to known franchises like Mario Kart and The Legend of Zelda. But itâ€™s also a developer that knows how to get weird with games like WarioWare, and Tomodachi Life might just be the weirdest thing it''s ever made.', 'IMG_8670.jpg'),
(2, 1402015315, 0, 1, 'Arthur Jones', 'Brooklyn', 'brooklyn', 'They said that they couldnâ€™t restock the Brooklyn  sneakers fast enough. The Italians have long been lovers of â€œlo stile Inglese,â€ or English sartorial style, with its tweeds, herringbone fabrics and pops of silk and print. The English are returning the compliment this week with â€œThe Glamour of Italian Fashion 1945-2014,â€ a show dedicated to postwar Italian style at the Victoria and Albert Museum. The tightly edited show â€” there are only 100 pieces of menâ€™s and womenâ€™s fashion on display â€” is an earnest and ', 'They said that they couldnâ€™t restock the Brooklyn  sneakers fast enough. The Italians have long been lovers of â€œlo stile Inglese,â€ or English sartorial style, with its tweeds, herringbone fabrics and pops of silk and print. The English are returning the compliment this week with â€œThe Glamour of Italian Fashion 1945-2014,â€ a show dedicated to postwar Italian style at the Victoria and Albert Museum. The tightly edited show â€” there are only 100 pieces of menâ€™s and womenâ€™s fashion on display â€” is an earnest and enthusiastic tale of the rapid journey Italy made from postwar ruin to modern-day prosperitÃ . The show begins with a large, black-and-white image of Florence in 1946, all rubble and crumbling buildings, and ends with a peacockâ€™s display of  opulence including the liquid white gown and showy gold dragon brooch from Tom Fordâ€™s fall 2004 Gucci collection, and Roberto Capucciâ€™s rippling fuchsia dress. In between thereâ€™s jewelry, fur, cashmere, glitter and Pucci prints galore. On display are some of the Bulgari gems that Richard Burton used to woo Elizabeth Taylor while the two were filming â€œCleopatraâ€ in Rome the early Sixties. Thereâ€™s also a display dedi-\r\ncated to the seminal Sala Bianca fashion shows at Palazzo Pitti in Florence; a nod to menâ€™s tailoring, and myriad examples of Italyâ€™s technical prowess â€” from industrially produced knitwear. There are a myriad of examples that display Italyâ€™s technical prowess. In between thereâ€™s jewelry, fur, cashmere, glitter and Pucci prints galore. On display are \r\n\r\nsome of the Bulgari gems that Richard Burton used to woo Elizabeth Taylor while the two \r\n\r\nwere filming â€œCleopatraâ€ in Rome the early Sixties. Thereâ€™s also a display dedicated to the \r\n\r\nseminal Sala Bianca fashion shows at Palazzo Pitti in Florence; a nod to menâ€™s tailoring, \r\n\r\nand myriad examples of Italyâ€™s technical prowess â€” from industrially produced knitwear \r\n\r\nto handcrafted Fendi furs. On display are some of the Bulgari gems that Richard Burton \r\n\r\nused to woo Elizabeth Taylor while the two were filming everybody who walked on by.', 'site_2.jpg'),
(3, 1402115315, 0, 2, 'Dwayne Wade', 'Wade', 'wade', 'Dwyane Tyrone Wade, Jr. (born January 17, 1982) is an American professional basketball player who currently plays for the Miami Heat of the NBA. He has established himself as one of the most well-known and popular players in the league. Wade had the top selling jersey in the NBA for nearly two years, leading the NBA in jersey sales from the 2005 NBA Playoffs, until the midpoint of the 2006â€“07 season.[1] His first name is pronounced /dwÉ›Éªn/, the same as the more common spellings "Duane" and "Dwayne"', 'Dwyane Tyrone Wade, Jr. (born January 17, 1982) is an American professional basketball player who currently plays for the Miami Heat of the NBA. He has established himself as one of the most well-known and popular players in the league. Wade had the top selling jersey in the NBA for nearly two years, leading the NBA in jersey sales from the 2005 NBA Playoffs, until the midpoint of the 2006â€“07 season.[1] His first name is pronounced /dwÉ›Éªn/, the same as the more common spellings "Duane" and "Dwayne".[2]\r\n\r\nAfter entering the league as the fifth pick in the 2003 NBA Draft, Wade was named to the All-Rookie team and the All-Star team the following ten seasons. In his third season, Wade led the Miami Heat to their first NBA championship in franchise history. He was named the 2006 NBA Finals MVP as the Heat won the series 4-2 over the Dallas Mavericks. At the 2008 Summer Olympics, Wade led the United States men''s basketball team, commonly known as the "Redeem Team", in scoring, and helped them captured gold medal honors in Beijing, China. In the 2008â€“09 season, Wade led the league in scoring and earned his first NBA scoring title.\r\n\r\nAfter Lebron James and Chris Bosh joined the Heat, Wade was part of Miami''s second championship win in the 2012 NBA Finals, when Miami defeated the Oklahoma City Thunder. He won his third NBA championship in 2013, when the Heat defeated the San Antonio Spurs in the 2013 NBA Finals.', 'features2.jpg'),
(4, 1402135315, 0, 3, 'Marlon Wayans', 'Tokyo fresh', 'tokyo-fresh', 'TOKYO-One of central Tokyo''s largest new developments has almost fully filled its office space before opening its doors next week, underscoring strength in the city''s property market. Mori Building Co., a major Japanese developer known for projects such as Roppongi Hills and the Shanghai World Financial Center, said Wednesday it has completed the multiuse development, called Toranomon Hills. Mori Building said it has invested 140 billion yen ($1.36 billion) in the project.', 'TOKYO-One of central Tokyo''s largest new developments has almost fully filled its office space before opening its doors next week, underscoring strength in the city''s property market.\r\n\r\nMori Building Co., a major Japanese developer known for projects such as Roppongi Hills and the Shanghai World Financial Center, said Wednesday it has completed the multiuse development, called Toranomon Hills. Mori Building said it has invested 140 billion yen ($1.36 billion) in the project.\r\n\r\nTokyo''s property market has revived recently, supported by Prime Minister Shinzo Abe''s economic programs. Demand for office space is rising as companies expand, and the amount of space available for rent has fallen sharply in recent months, bringing rents up. Relatively few new buildings are coming on the market, helping tighten conditions.\r\n\r\nToranomon Hills, located a few minutes'' walk from the U.S. Embassy and just south of the central government district, also houses retail shops, luxury private residences, conference space and a 164-room hotel carrying the Andaz brand name of Hyatt Hotels Corp. H +0.05%  The complex stands on a recently completed highway extension that will eventually connect it to Tokyo Bay where many Olympic events are planned in 2020.', 'tumblr_n106y2ngIF1qiprgbo1_1280.jpg'),
(5, 1402135315, 0, 4, 'Sean Carter', 'Jigga', 'jigga', 'The track-listing for the soundtrack to the upcoming Baz Luhrmann pic, The Great Gatsby, has finally been revealed Andre 3000 and Beyonce would be covering Amy Winehouseâ€™s 2006 Back To Black single along with other major names.\r\n\r\nThe soundtrack opener comes from executive producer Jay-Z, himself, on a track called 100$ Bill. Other guests include Q-Tip, Lana Del Rey, Bryan Ferry (Roxy Music)  and Florence + The Machine.\r\n\r\nThe film premieres on May 10, with the soundtrack dropping on May 7th.', 'The track-listing for the soundtrack to the upcoming Baz Luhrmann pic, The Great Gatsby, has finally been revealed Andre 3000 and Beyonce would be covering Amy Winehouseâ€™s 2006 Back To Black single along with other major names.\r\n\r\nThe soundtrack opener comes from executive producer Jay-Z, himself, on a track called 100$ Bill. Other guests include Q-Tip, Lana Del Rey, Bryan Ferry (Roxy Music)  and Florence + The Machine.\r\n\r\nThe film premieres on May 10, with the soundtrack dropping on May 7th.', 'jay-z.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `attachments`
--

CREATE TABLE IF NOT EXISTS `attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL,
  `image_filename` varchar(255) NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `entry_datetime` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `caption` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `attachments`
--

INSERT INTO `attachments` (`id`, `type`, `image_filename`, `ordering`, `entry_datetime`, `deleted`, `title`, `caption`) VALUES
(1, 'image', '1987ab89dkdy.jpg', 1, 1402966937, 0, 'main', 'adfadgadf'),
(2, 'image', 'dnzo92479527.jpg', 1, 1402973439, 0, 'main_image', 'lebron 11'),
(3, 'image', '870ey1zhsv4p.jpg', 1, 1403030839, 0, 'main_image', ''),
(4, 'image', 'focnnc83leul.jpg', 1, 1403031203, 0, 'main_image', ''),
(5, 'image', 'l3xzy4ykssap.jpg', 1, 1403031642, 0, 'main_image', ''),
(6, 'image', 'hoffk8yl0hzw.jpeg', 1, 1403031988, 0, 'main_image', ''),
(9, 'image', '2yzpthg66lhh.jpeg', 1, 1404350453, 0, 'Main image', 'pathway'),
(10, 'image', 'ry1876st3u7m.jpg', 1, 1404350511, 0, 'leaf', 'leaf 2nd image'),
(11, 'image', 'ue0uijj2vjj6.jpg', 1, 1404350537, 0, 'grass nature', 'grass nature 3rd'),
(12, 'image', '1ual0nvg89wl.jpg', 1, 1404353865, 0, 'nature 3', 'damn'),
(13, 'image', 'zfq2so6ceo0h.jpg', 1, 1404353883, 0, 'water', 'wa'),
(14, 'image', 'hhq9nz8o0j3l.jpeg', 1, 1404359258, 0, 'Blue Shoes', 'nice eh'),
(15, 'image', 'yq2ws6rmd5mp.jpg', 1, 1404359281, 0, 'gucci', 'shoewsss'),
(16, 'image', 'dhtiov86fb5z.jpg', 1, 1404359397, 0, 'Lake', 'The lake yo'),
(17, 'image', 'fqxnnmfs3o7p.jpg', 1, 1404359418, 0, 'nautre', 'oh shieetttt'),
(18, 'image', 'lc7xeli4oewm.jpg', 1, 1404359446, 0, 'The moon is showing', 'yes it is'),
(19, 'image', 'keq7dv7s0bsu.jpg', 1, 1404359465, 0, 'adfdfa', 'fdafffeeee'),
(20, 'image', 'czoaf3btzy91.jpg', 1, 1404359531, 0, 'shrip', 'shrimmppp'),
(21, 'image', 'a6dwndpn2nb0.jpg', 1, 1405301741, 0, 'shoes', 'dadd');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE IF NOT EXISTS `brands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `description` text,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `title`, `description`, `ordering`, `deleted`) VALUES
(1, 'Air Jordan', '', 0, 0),
(2, 'Converse', 'These are the best shoes!', 1, 0),
(3, 'Timberland', 'Makin boots', 2, 0),
(4, 'Supra', 'fun hightops!', 3, 0),
(5, 'Nike', 'Nike, Inc. is an American multinational corporation that is engaged in the design, development, manufacturing and worldwide marketing and selling of footwear, apparel, equipment, accessories and services. It is one of the world''s largest suppliers of athletic shoes and apparel and a major manufacturer of sports equipment.', 4, 0),
(6, 'Clarks', '', 5, 0),
(7, 'Rockport', '', 6, 0),
(8, 'Adidas', '', 7, 0),
(9, 'Allen Edmonds', '', 8, 0),
(10, 'K-Swiss', '', 9, 0),
(11, 'Android Homme', '', 10, 0),
(12, 'Kriss Kross', '', 11, 1),
(13, 'New Balance', '', 12, 0),
(14, 'Giuseppe Zanotti', '', 13, 0),
(15, 'Gucci', '', 14, 0);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `entry_datetime` int(11) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `ordering`, `entry_datetime`, `deleted`) VALUES
(1, 'Sneaker', 0, 0, 0),
(2, 'Boot', 1, 0, 0),
(3, 'Dress', 2, 0, 0),
(4, 'Design', 3, 0, 1),
(5, 'Miscellaneous', 4, NULL, 1),
(6, 'Random', 5, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

CREATE TABLE IF NOT EXISTS `colors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `hex` varchar(10) DEFAULT NULL,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `colors`
--

INSERT INTO `colors` (`id`, `title`, `hex`, `ordering`, `deleted`) VALUES
(1, 'Black', '#000000', 0, 0),
(2, 'White', '#ffffff', 2, 0),
(3, 'Red', '#FF0000', 3, 0),
(4, 'Beige', '#F7FCC5', 4, 0),
(5, 'Brown', '#8C6246', 5, 0),
(6, 'Silver', '#E0E0E0', 6, 0),
(7, 'Blue', '#0000FF', 1, 0),
(8, 'Purple ', '#FF00FF', 8, 0),
(9, 'Orange', '#FF9100', 9, 0),
(10, 'Infrared ', '#EB1C37', 10, 0),
(11, 'Lime Green ', '#30F000', 11, 0),
(12, 'Royal Blue ', '#4169E1', 12, 0),
(13, 'Navy Blue ', '#000080', 13, 0),
(14, 'Grey ', '#404040', 7, 0),
(15, 'Plum', '#ED1F9A', 14, 0);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(6) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comment_body` varchar(255) NOT NULL,
  `replied_to` int(6) NOT NULL DEFAULT '0',
  `sub_replied_to` int(6) DEFAULT NULL,
  `entry_datetime` int(11) NOT NULL,
  `update_datetime` int(11) NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `post_type` int(1) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `post_type` (`post_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=81 ;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `parent_id`, `user_id`, `comment_body`, `replied_to`, `sub_replied_to`, `entry_datetime`, `update_datetime`, `ordering`, `post_type`, `deleted`) VALUES
(30, 5, 12, 'It''s Jay Hova!', 0, NULL, 1403217425, 0, 1, 1, 0),
(31, 5, 15, 'Nah b, it''s Jigga', 30, 0, 1403217445, 0, 1, 1, 0),
(32, 5, 17, 'I''M BATMAN', 30, 31, 1403217562, 0, 1, 1, 0),
(33, 5, 12, 'Crazy foo...', 30, 32, 1403217769, 0, 1, 1, 0),
(34, 5, 15, 'Comment on the article...', 0, NULL, 1403220105, 0, 1, 1, 0),
(35, 5, 17, 'I already did', 34, 0, 1403226471, 0, 1, 1, 0),
(36, 5, 15, 'Hey Batman, you should get a notification now!', 34, 35, 1403378389, 0, 1, 1, 0),
(37, 1, 15, 'Testing the activity stream', 0, NULL, 1403406045, 0, 1, 1, 0),
(38, 5, 15, 'Haha, activity stream will see this!', 30, 32, 1403406285, 0, 1, 1, 0),
(39, 5, 15, 'It didn''t show last time, but it will show now.', 30, 32, 1403406548, 0, 1, 1, 0),
(40, 5, 15, 'Something went wrong', 30, 39, 1403406626, 0, 1, 1, 0),
(41, 5, 15, 'Get notifications', 34, 35, 1403406698, 0, 1, 1, 0),
(42, 5, 15, 'More notifications for Mr. Bruce Wayne.', 34, 35, 1403406806, 0, 1, 1, 0),
(43, 3, 15, 'Dwayne Wade!', 0, NULL, 1403409201, 0, 1, 1, 0),
(44, 1, 17, 'Bricks on bricks on bricks', 37, 0, 1403409552, 0, 1, 1, 0),
(45, 1, 12, 'Whack!', 37, 44, 1403444219, 0, 1, 1, 0),
(46, 1, 12, 'Good is it working?', 37, 0, 1403444231, 0, 1, 1, 0),
(47, 1, 12, 'Posting a comment to see if it''s showing up in activities stream.', 0, NULL, 1403444247, 0, 1, 1, 0),
(48, 3, 15, 'Need to get more activities', 0, NULL, 1403469606, 0, 1, 1, 0),
(49, 3, 15, 'get more activities test with own reply', 48, 0, 1403469621, 0, 1, 1, 0),
(50, 3, 17, 'More test!', 48, 49, 1403469790, 0, 1, 1, 0),
(51, 3, 17, 'Testing this too!', 43, 0, 1403469797, 0, 1, 1, 0),
(52, 3, 17, 'I like you', 43, 0, 1403469841, 0, 1, 1, 0),
(53, 2, 15, 'Brooklynn!!!!!!!!', 0, NULL, 1403469887, 0, 1, 1, 0),
(54, 2, 15, 'Replying to my own comment', 53, 0, 1403473188, 0, 1, 1, 0),
(55, 3, 15, 'Comment on the article...', 0, NULL, 1403719590, 0, 1, 1, 0),
(59, 1, 15, 'headache..', 0, NULL, 1404612982, 0, 1, 2, 0),
(60, 1, 15, 'replying to this shit....', 59, NULL, 1404613074, 0, 1, 2, 0),
(61, 1, 15, 'another...', 59, 0, 1404613502, 0, 1, 2, 0),
(62, 1, 15, 'another...', 59, 0, 1404613503, 0, 1, 2, 0),
(63, 1, 15, 'test', 59, 0, 1404613616, 0, 1, 2, 0),
(64, 1, 15, 'Test 2', 59, 0, 1404613762, 0, 1, 2, 0),
(65, 1, 15, 'test 3', 59, 60, 1404613770, 0, 1, 2, 0),
(66, 1, 15, 'test 4', 59, 0, 1404613808, 0, 1, 2, 0),
(67, 1, 15, 'test 5', 59, 0, 1404613940, 0, 1, 2, 0),
(68, 1, 15, 'should work now\n', 59, 0, 1404613997, 0, 1, 2, 0),
(70, 1, 12, 'First', 0, NULL, 1404625691, 0, 1, 3, 0),
(71, 1, 12, 'replied!', 70, 0, 1404625835, 0, 1, 3, 0),
(72, 1, 12, 'Test', 0, NULL, 1404626097, 0, 1, 2, 0),
(73, 1, 12, 'Comment on the article...', 0, NULL, 1404626187, 0, 1, 2, 0),
(74, 1, 12, 'Comment on the review', 0, NULL, 1404626345, 0, 1, 3, 0),
(75, 1, 12, 'replied again', 70, 71, 1404627292, 0, 1, 3, 0),
(76, 3, 12, 'Testsssss', 0, NULL, 1404627335, 0, 1, 3, 0),
(77, 4, 12, 'Godzilla g', 0, NULL, 1404682865, 0, 1, 1, 0),
(78, 4, 12, 'g', 77, 0, 1404682874, 0, 1, 1, 0),
(79, 4, 12, 'a', 77, 78, 1404682884, 0, 1, 1, 0),
(80, 1, 15, 'probably last test', 72, 0, 1404683099, 0, 1, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `comment_karmas`
--

CREATE TABLE IF NOT EXISTS `comment_karmas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_type` int(5) NOT NULL,
  `type` int(1) NOT NULL DEFAULT '0',
  `parent_id` int(8) NOT NULL,
  `comment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `entry_datetime` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `ordering` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=63 ;

--
-- Dumping data for table `comment_karmas`
--

INSERT INTO `comment_karmas` (`id`, `post_type`, `type`, `parent_id`, `comment_id`, `user_id`, `entry_datetime`, `deleted`, `ordering`) VALUES
(51, 1, -1, 5, 32, 12, 1403217812, 0, 1),
(52, 1, -1, 5, 32, 15, 1403217838, 0, 1),
(53, 1, 1, 5, 33, 15, 1403218709, 0, 1),
(54, 1, 1, 3, 50, 15, 1403480829, 0, 1),
(55, 1, 1, 3, 52, 15, 1403480850, 0, 1),
(56, 1, 1, 5, 35, 15, 1404460154, 0, 1),
(59, 2, 1, 1, 59, 12, 1404617796, 0, 1),
(60, 2, 1, 1, 60, 12, 1404618014, 0, 1),
(61, 2, -1, 1, 61, 12, 1404618015, 0, 1),
(62, 3, 1, 1, 71, 12, 1404625852, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `contents`
--

CREATE TABLE IF NOT EXISTS `contents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `k` varchar(255) DEFAULT NULL,
  `v` text,
  `entry_datetime` int(11) NOT NULL DEFAULT '0',
  `update_datetime` int(11) NOT NULL DEFAULT '0',
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `id_2` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `contents`
--

INSERT INTO `contents` (`id`, `k`, `v`, `entry_datetime`, `update_datetime`, `deleted`) VALUES
(1, 'title', 'PLAYFIGHT IS A VFX STUDIO', 0, 0, 1),
(2, 'subtitle', 'WE PRODUCE TOP TIER VISUAL EFFECTS, ANIMATION AND PRE-VISUALIZATION FOR <span>FEATURE FILMS</span>, <span>TELEVISION</span> AND <span>ADVERTISING</span>. OUR OFFICES ARE LOCATED IN DOWNTOWN TORONTO', 0, 0, 1),
(3, 'facebook', 'https://www.facebook.com/pages/FoundMissing/158144410869120', 0, 0, 0),
(4, 'twitter', 'https://twitter.com/_foundmissing', 0, 0, 0),
(5, 'instagram', 'http://instagram.com/_foundmissing', 0, 0, 0),
(6, 'rss', 'http://rss.com/foundmissing', 0, 0, 0),
(7, 'contact_subtitle', 'We are so fun.', 0, 0, 1),
(8, 'address1', '489 Queen Street East', 0, 0, 1),
(9, 'address2', 'Toronto, On M5A 1V1', 0, 0, 1),
(10, 'phone', '416-809-6594', 0, 0, 1),
(11, 'email', 'hello@playfight.com', 0, 0, 1),
(12, 'job_email', 'jobs@playfight.com', 0, 0, 1),
(13, 'mission_statement', 'So the word is out and the Jordan III white/cement will make another visit come 2011. From pictures that have surfaced one has to be disappointed at the use of what looks like a glossy cheap leather. Outside of that and the fact that there is no Nike Air (wasnâ€™t on the 2003 release either) the shoe looks OK. Take a look at the side pictures below before you make your decision, and at the very least check out the different releases side by side. Enjoy.', 0, 0, 0),
(14, 'youtube', 'http://www.youtube.com/user/FOUNDMISSINGtv', 0, 0, 0),
(15, 'tumblr', 'http://thefoundmissing.tumblr.com/', 0, 0, 0),
(16, 'pinterest', 'http://pinterest.com', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `designers`
--

CREATE TABLE IF NOT EXISTS `designers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `description` text,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=54 ;

--
-- Dumping data for table `designers`
--

INSERT INTO `designers` (`id`, `title`, `description`, `ordering`, `deleted`) VALUES
(1, 'Cole Haan', '', 2, 0),
(2, 'Nike', '', 1, 0),
(3, 'Converse', '', 3, 0),
(4, 'Adidas', '', 4, 0),
(5, 'New Balance', '', 5, 0),
(6, 'Red Wing', '', 6, 0),
(7, 'Clarks Original', '', 8, 0),
(8, 'Timberland', '', 13, 0),
(9, 'Android Homme', '', 14, 0),
(10, 'Supra II', '', 11, 0),
(11, 'Krisvanassche', '', 16, 0),
(12, 'Balenciaga ', '', 15, 0),
(13, 'Givenchy ', '', 21, 0),
(14, 'Dsquared2 ', '', 18, 0),
(15, 'Ann Demeulemeester ', '', 17, 0),
(16, 'Balmain ', '', 20, 0),
(17, 'Giuseppe Zanotti', '', 12, 0),
(18, 'Raf Simons ', '', 22, 0),
(19, 'Lanvin', '', 23, 0),
(20, 'Saint Laurent ', '', 19, 0),
(21, 'Maison Martin Margiela ', '', 24, 0),
(22, 'Pierre Hardy ', '', 25, 0),
(23, 'MCQ Alexander Mcqueen ', '', 26, 0),
(24, 'Neil Barret ', '', 27, 0),
(25, 'Thom Browne ', '', 28, 0),
(26, 'Paul Smith ', '', 29, 0),
(27, 'Tiger of Sweden ', '', 30, 0),
(28, 'Dr. Martens ', '', 32, 0),
(29, 'Y-3 ', '', 31, 0),
(30, 'Tod''s', '', 33, 0),
(31, 'Asics', '', 34, 0),
(32, 'John Varvatos ', '', 35, 0),
(33, 'Ellen Admonds ', '', 36, 0),
(34, 'Brunello Cucinelli ', '', 37, 0),
(35, 'Oliberte ', '', 39, 0),
(36, 'Toms ', '', 38, 0),
(37, 'Reebok', '', 7, 0),
(38, 'Asics', '', 40, 0),
(39, 'Sorel ', '', 41, 0),
(40, 'Sebago ', '', 42, 0),
(41, 'Sperry Topsider', '', 43, 0),
(42, 'Feit ', '', 44, 0),
(43, 'Danner ', '', 45, 0),
(44, 'Del Toro', '', 46, 0),
(45, 'Chippewa ', '', 47, 0),
(46, 'ETQ ', '', 49, 0),
(47, 'Puma ', '', 10, 0),
(48, 'Jordan ', '', 0, 0),
(49, 'Vans ', '', 9, 0),
(50, 'Christian Louboutin', '', 48, 0),
(51, 'Gucci ', '', 50, 0),
(52, 'Diemme ', '', 51, 0),
(53, 'Yuketen ', '', 52, 0);

-- --------------------------------------------------------

--
-- Table structure for table `favorites`
--

CREATE TABLE IF NOT EXISTS `favorites` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `entry_datetime` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`item_id`),
  KEY `user_id_2` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `favorites`
--

INSERT INTO `favorites` (`id`, `user_id`, `item_id`, `entry_datetime`) VALUES
(3, 15, 6, 1405211818),
(4, 15, 9, 1405211864),
(6, 15, 7, 1405211866),
(7, 15, 8, 1405224796),
(8, 15, 1, 1405224797);

-- --------------------------------------------------------

--
-- Table structure for table `footwears`
--

CREATE TABLE IF NOT EXISTS `footwears` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `model_name` varchar(255) NOT NULL,
  `style_id` varchar(150) NOT NULL,
  `tags` varchar(255) NOT NULL,
  `price` varchar(15) NOT NULL,
  `rating` varchar(5) DEFAULT NULL,
  `color_id` varchar(150) NOT NULL,
  `release_date` int(11) NOT NULL,
  `entry_datetime` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `footwears`
--

INSERT INTO `footwears` (`id`, `slug`, `brand_id`, `model_name`, `style_id`, `tags`, `price`, `rating`, `color_id`, `release_date`, `entry_datetime`, `deleted`, `ordering`) VALUES
(1, 'jordan-xx8-se', 5, 'JORDAN XX8 SE', '11', 'hi-top, jordans, retro', '210.25', '9.1', '1', 1403928000, 1402958103, 0, 0),
(6, 'lebron-11-elite', 5, 'lebron 11 elite', '11,1', 'nike, lebron', '275', '9.1', '1,7,2', 1402891200, 1402973358, 0, 4),
(7, 'free-trainer-50-id', 5, 'Free trainer 5.0 iD', '11,1', 'trainers, nike', '125', '8.5', '12,13', 1402977600, 1403030795, 0, 5),
(8, 'diamente-leather-high-top-sneaker', 15, 'Diamante Leather High-Top Sneaker', '11,10', 'leather, high-tops', '510', '8.5', '1', 1401681600, 1403031179, 0, 6),
(9, 'zoom-kd-vi', 5, 'Zoom KD VI', '11', 'kevin durant, basketball', '250', '8.5', '6,3', 1393650000, 1403031618, 0, 7),
(10, 'melo-m10', 5, 'JORDAN MELO M10', '11,1', 'melo, basketball', '150', '8.5', '5,4', 1401595200, 1403031966, 0, 8);

-- --------------------------------------------------------

--
-- Table structure for table `footwear_attachments`
--

CREATE TABLE IF NOT EXISTS `footwear_attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `footwear_id` int(11) NOT NULL,
  `attachment_id` int(11) NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `entry_datetime` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `footwear_attachments`
--

INSERT INTO `footwear_attachments` (`id`, `footwear_id`, `attachment_id`, `ordering`, `entry_datetime`, `deleted`) VALUES
(1, 1, 1, 1, 0, 0),
(2, 6, 2, 1, 0, 0),
(3, 7, 3, 1, 0, 0),
(4, 8, 4, 1, 0, 0),
(5, 9, 5, 1, 0, 0),
(6, 10, 6, 1, 0, 0),
(7, 1, 21, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `frontpages`
--

CREATE TABLE IF NOT EXISTS `frontpages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slider` int(1) NOT NULL DEFAULT '0',
  `position` int(1) NOT NULL DEFAULT '0',
  `type` varchar(50) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `entry_datetime` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `frontpages`
--

INSERT INTO `frontpages` (`id`, `slider`, `position`, `type`, `parent_id`, `entry_datetime`, `deleted`, `ordering`) VALUES
(1, 1, 0, 'interview', 2, 0, 0, 0),
(2, 1, 0, 'article', 5, 0, 0, 1),
(3, 1, 0, 'review', 3, 0, 0, 2),
(4, 1, 0, 'gallery', 4, 0, 0, 3),
(5, 0, 1, 'article', 2, 0, 0, 4),
(6, 0, 2, 'review', 2, 0, 0, 5),
(7, 0, 3, 'article', 3, 0, 0, 6),
(8, 0, 4, 'review', 6, 0, 0, 7),
(9, 0, 5, 'gallery', 3, 0, 0, 8);

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE IF NOT EXISTS `galleries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `entry_datetime` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id`, `title`, `description`, `entry_datetime`, `deleted`, `ordering`) VALUES
(1, 'No Title', 'No Description Here', 1404349862, 0, 0),
(2, '2nd album', 'siickkkkk', 1404353823, 0, 1),
(3, 'Shoes', 'Shoesssss', 1404359234, 0, 2),
(4, 'More nature stuff', 'ad', 1404359346, 0, 3),
(5, 'nature stuff pt 4', 'these are my backdrops', 1404359373, 0, 4),
(6, 'so hungry', 'this is the desc', 1404359499, 0, 5);

-- --------------------------------------------------------

--
-- Table structure for table `gallery_attachments`
--

CREATE TABLE IF NOT EXISTS `gallery_attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gallery_id` int(11) NOT NULL,
  `attachment_id` int(11) NOT NULL,
  `entry_datetime` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `gallery_attachments`
--

INSERT INTO `gallery_attachments` (`id`, `gallery_id`, `attachment_id`, `entry_datetime`, `deleted`) VALUES
(1, 1, 9, 0, 0),
(2, 1, 10, 0, 0),
(3, 1, 11, 0, 0),
(4, 2, 12, 0, 0),
(5, 2, 13, 0, 0),
(6, 3, 14, 0, 0),
(7, 3, 15, 0, 0),
(8, 4, 16, 0, 0),
(9, 4, 17, 0, 0),
(10, 5, 18, 0, 0),
(11, 5, 19, 0, 0),
(12, 6, 20, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `interviews`
--

CREATE TABLE IF NOT EXISTS `interviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entry_datetime` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `author_name` varchar(55) NOT NULL,
  `title` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `excerpt` text NOT NULL,
  `body` text NOT NULL,
  `main_image` varchar(255) NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `interviews`
--

INSERT INTO `interviews` (`id`, `entry_datetime`, `deleted`, `author_name`, `title`, `slug`, `excerpt`, `body`, `main_image`, `ordering`) VALUES
(1, 1403979222, 0, 'Danica Samuel', 'Eddie Huang', 'eddie-huang', 'I always had the feeling that Dunks were for hippies and that wasn''t me, I was a crispy dude.', '<div class="row">\r\n<div class="col-md-offset-3 col-md-6">\r\nIt seems like a hip young chef is born every hour to jolt the jaded city palate with an inventive olio of Asian-ish street food: the kimchi burrito, the cumin lamb burger, the kung pao pastrami. The last living purist in the game might be Eddie Huang, 30, who holds the keys to BaoHaus, an unassuming East Village bun shop that serves Taiwanese street food and the ever-trendy pork belly. Huang''s M.O., though, has little to do with reinterpreting original flavors, but rather brings it back old school all the way to, say, an Asian grandmother''s kitchen: nostalgic, authentic, with a soup''on of crazy.\r\n</div>\r\n</div>\r\n\r\nYou may be surprised to learn of the circuitous path that took a Taiwanese-American boy out of the suburbs of Orlando and onto the streets of New York, a picaresque journey that earned him two stints in jail, a law degree, and a defunct streetwear label all before the age of 25. Live and learn, as they say. Huang is a fearless raconteur, a talent that has gained him a strong social media following through platforms like his food blog and VICE show, "Fresh Off The Boat," and now the multi-hyphenate can add 2013 TED Fellow and published author to his CV with the newly released Fresh Off The Boat: A Memoir (Spiegel & Grau), a coming-of-age tale of identity and assimilation told in a flambed and unorthodox vernacular all his own. Huang''s writing is at once hilarious and provocative; his incisive wit pulls through like a perfect plate of dan dan noodles.\r\n\r\nWe recently Skyped with the chef to chat about shaking off stereotypes, green juicing, and his last meal on earth. \r\n\r\n\r\nJEANINE CELESTE PANG: [To a blank screen] Eddie? Hi, it''s Jeanine.\r\n\r\nEDDIE HUANG: Cool, just give me one second, I''ve got to put a shirt on. I just got out of the gym, and I''m like, "What am I wearing?" I''m just going to wear my robe. [Laughs and turns on the screen.]\r\n\r\nPANG: So you''re in your bedroom in New York right now?\r\n\r\nHUANG: Yeah, yeah. I went out super hard last night, and woke up at like 10 or 11. It was a disaster.\r\n\r\nPANG: Well, you''ve got a reason to celebrate! Barnes & Noble just put you on their Spring 2013 Discover Great New Writers list. But I have to ask, writing a memoir at 30 can seem a little premature or presumptuous. What was the big draw to publish now?  \r\n\r\nHUANG: I remember talking to an agent who thought my pursuit was crazy and laughable. I told her point-blank, "I''ve lived five times over what you''ve done already, I can just look at your shoes and tell." [laughs] I''ve lived a very, very full life so far. Growing up an ''80s baby, things moved so fast: 30-year-olds are like 50-year-olds now. The midlife crisis happens when you''re 25, two years out of college, and realizing that all you do is go to work and have sex. I wanted to write the book just getting out of that period of my life when I had figured something out, when things were fresh. I mean, I remember what color shirt I wore, or the shoes I wore, when a lot of the things in the book happened.\r\n\r\nPANG: That''s what I found striking; you have very strong memories from your childhood and teen years, with references to the fashion and the music and the books.\r\n\r\nHUANG: I literally went back through my emails, through yearbooks and letters to see what I was saying and doing at that time. The thing is, I really felt that this was the time to write it. I''m sure I''ll read it in five years and be like, Damn, man, did you have to go that hard? People talk about perfect timing, but I think everything is perfect in its moment; you just want to capture that. I think that if I wrote this in five or 10 years, I would be fat and jaded. \r\n\r\nPANG: How do you want to affect your readers? \r\n\r\nHUANG: I''m so sick of people misunderstanding Asians in America and what we''re about. I don''t fit into a lot of the silos that America has created, so by being such an outlier, I think people will naturally think that there exist other weirdos that break the mold and are a new archetype. I hope people will say, "You know what? When that kid Eddie came out, when he dropped, that was some different shit." Like Charles Barkley when he came out and everyone said he was too short to play power forward, but then were like, "Yo, but you can have a shorter power forwardï¿½if he''s explosive." I just want to be that dude that opens that door for others to be the individuals they are.  \r\n\r\nPANG: You''ve made up a lot of pseudonyms for yourself: Rotten Banana, Bang Bros. Connoisseur, Sars Blackmon, The Human Panda. What''s wrong with just Eddie Huang?\r\n\r\nHUANG: I think as an ''80s baby, you grew up with ''90s hip-hop. Everybody had nicknames; all the Wu-Tang cats had aliases. You''re constantly reinventing yourself and playing around. Wordplay was really big for me; I''d always flip things on people. Kids used to make fun of me and say, "Eddie Huang, sitting on a jumbo gong."  And I would say, "Nah, fuck that, I''m Magic Don Huang." It was a way of owning my identity.  \r\n\r\nPANG: The food blogs peg you as the rabble-rouser and the chef who doesn''t play nice with others. Demystify usï¿½is the bad-boy thing just a ruse?\r\n\r\nHUANG: Yeah, you know, the food blogs need to create daily headlines. When Grub Street wrote that Eddie can''t get along with chefs and that he hates Danny Bowien [of Mission Chinese], he and I were literally at BrisketTown, eating barbecue together and just totally laughing about it. I''m good friends with Danny. But my thing is, I''m not an ass patter. I treat the culinary world like I do sports. I hate it when people tell you "good game" when you know you didn''t have one, so I like to talk X''s and O''s with the chefs like they''re players and give constructive criticism. I might say, "Yo, step out more on the pick and roll." Also, the blogs like to paint me as the hip-hop chef, which I hate. I was born in ''82ï¿½if you weren''t listening to hip-hop, you were an idiot.\r\n\r\nPANG: Speaking of hip-hop, your love for that genre is stamped all over the book. You''re constantly quoting rappers, and then you flip it and write about your love for literature, so that Jonathan Swift is referenced in the same graph as Dr. Dre. Where does that dichotomy come from?   \r\n\r\nHUANG: I think it comes from having parents that weren''t immersed in American culture. It was a blessing and a curseï¿½which is a dichotomy in itself. I didn''t even know how to use a semicolon until my senior year of college. But it was also a blessing, because I didn''t have someone handing me a syllabus for life and telling me about the cultural things I needed to watch, worship and hold high. So I went and found Cam''ron. I thought, "I fuck with Killaï¿½but I also like this guy Mark Twain." It was all genius to me. I was able to make my own call, so I think the things that I found inspirational are because I got to go out on that scavenger hunt by myself, with no guide.\r\n\r\nPANG: What do you find inspiring these days?\r\n\r\nHUANG: Right now, I''m definitely on a health kick. The thing is, I feel disgusting eating all that food on the VICE show, so I''ve gotten very interested in cold-pressed juices. You feel great after drinking it. I''ve been working on a juice project and teaching kids in Harlem how to juice. Look at this. [Holds up a plastic tumbler of green juice.] I made it myself.\r\n\r\nPANG: That''s very green! What''s in it?\r\n\r\nHUANG: I put kale, spinach, lemon, ginger, Fujiyama apples and Chinese herbal supplements. I wake up in the morning and make myself a green juice for breakfast. I go to the gym, and after I work out, I can have fat, protein and carbs, so that''s when I go eat at BaoHaus. For me, juicing isn''t about binging and cleansing; I try to incorporate it into a balanced diet.\r\n\r\nPANG: And is it true you nix carbs after 3 pm?\r\n\r\nHUANG: Yeah, I try to just eat protein and fiber after 3 pm. I think chefs have a societal responsibilityï¿½similar to bartendersï¿½you''ve got to cut people off when they''ve had a little too much. So I may end up opening more juice bars. I wonder how people will respond, they might say, "How do you go from pork belly to this?"\r\n\r\nPANG: Probably the biggest fans of pork belly are the ones who will need the juice.\r\n\r\nHUANG: Well, for the next BaoHaus, my brother and I are retooling the menu based on our lifestyleï¿½we''re never going to live one life and sell you another. Our pork belly bao will always be there if you want to indulgeï¿½we''re not the governmentï¿½but we''ll give you healthier options too, and try delivering the same flavors with lean poultry and tofu.\r\n\r\nPANG: If you were to plan your last meal, what would be on the menu?\r\n\r\nHUANG: I would definitely have soup dumplings. I would also love a good Cantonese sticky rice in lotus leaf, Lion''s Head meatballs, hong shao rou (red cooked pork shank), some stir-fried chili cabbage, stir-fried snow pea shoots, eight-treasure glutinous rice dessert, the KFC egg tarts from China, a slice of pizza, fried chicken, and Jamaican ox tail. Ohï¿½and Peking duck from Da Dong and maybe two slices of Peter Luger''s steak and bacon. Just two. And three pieces of sushiï¿½kinmedai, shimmaji and kanpachi. That''s the dinner; that''s a wrap.\r\n\r\nPANG: What do you think about people calling you the new Anthony Bourdain?\r\n\r\nHUANG: I get why they say itï¿½in a lot of ways he''s been my career godfatherï¿½but I don''t think it''s fair to him, because I''m just a kid. When people compared me to David Chang, I was really upset and really didn''t like it. We don''t have the same goals; I thought it was more because he and I looked similar. But I feel like Tony is a kindred spirit. At the core, the thing that ties us is that we''re both very honest and incapable of not saying something. I''ve been out with him on the street and strangers will ask him very uncomfortable things, and you''ll see him stop for a second, flinch, and spit out an answer. He''s very inspiring.\r\n\r\nPANG: In your book, you talk about being an in-betweener. There''s a line that struck me: "Forget countries and boundaries, you can call me international." \r\n\r\nHUANG: I''m so glad you brought up that quote, because that quote is amazing. I was reading the Tao Te Ching last night, high, and I realized it holds the same ideology. Countries and boundaries and whateverï¿½you don''t need to announce it. If you talk about it in terms of New York real estate, you don''t need someone to tell you where the Lower East Side ends and where the Lower East Side begins. I get so disenfranchised reading the news, because global borders and lines we''ve created are completely unnecessary. That''s just another person on the other side, and it''s his bad luck that he was born there and it''s my good fortune that I was born here.  It''s all kind of illogical.', 'eddie.jpg', 1),
(2, 1403980171, 0, 'Danica Samuel', 'Tinker Hatfield', 'tinker-hatfield', 'I''m so old now my bones are all soft and bendable like. It sucks man, I don''t know.', 'Tinker Hatfield is a design guru to thousands of wanna-be sneaker designers, (and designers in general) all over the world. As the Vice President of Design & Special Projects at Nike — which means in his words, he can "design whatever I want," — the maverick 60-year-old is credited for designing Air Jordans, Air Max, Air Huarache, has collaborated with people like Kanye West and even made those Back To The Future shoes. Tinker was in town recently and we caught up with the man himself to talk about imagination, getting inspiration in unlikely places and Nike''s potential space project. Seriously.\r\n\r\nJerico Mandybur: What''s the most satisfying project you''ve ever worked on?\r\nTinker Hatfield: When I look back retrospectively, there''s a shoe that I think is my favourite — it''s the Air Jordan 11. Air Jordan''s are a small part of what I''ve done at Nike. It''s been fun working with him, Michael Jordan, so that shoe has special meaning. There''s a story behind it and I like the design and I also like the circumstances. I like what happened for that design to occur.\r\n\r\nIs this the Air Jordan you designed once Michael had retired and you were supposed to stop designing them?\r\nYeah, he had won three NBA championships in a row and then he retired. He was tired, his father had been murdered — there were just a lot of things going on in his life. He tried to be a professional baseball player. And everybody at Nike just felt like, "Well, it''s done, it''s over. We can''t really sell Air Jordans if he''s retired." And I totally did not agree. So while he was out playing baseball, and he didn''t even know it at first, but I was working harder than ever to do the best new sneaker that I could come up with. There were people within the Nike company who literally were telling me "Don''t do it, stop. We don''t want you spending the time and money on it." And I just didn’t listen. I kept working on it and finally I started tracking Michael down in small towns where he was playing minor league baseball. Just little towns, where there''s this big huge star in the locker room with a bunch of young baseball players. It was just surreal. \r\n\r\nI finally got to show him my idea and we collaborated a little bit more and then the shoe came out and it''s the one that''s most notable because it had patent leather all over, it was shiny, plasticky leather. Much like you''d see on a women''s shoe. I just threw everything at it. So not only was he not playing, it was a very unusual shoe. When it came out there were lines around the block and people breaking into the stores to steal them and beating each other up to get them.  It''s probably the most important Air Jordan of all the 30 years of working with him. So that''s why I like that shoe. Without being real cheeky about it, I can sort of say "I told you so." \r\n\r\nIs it true that when you presented Michael with the first Air Jordan he shed a tear?\r\nIt was the Air Jordan 13 but I do remember having to present my idea to him. I pulled out this big sheet of paper with all these drawings and photographs on it and I started talking about how the way he moved reminded me of a big, powerful, predatory cat. In the images I used a black leopard. And I was barely getting into the presentation, he''s looking at the photos and the drawings and he goes "How did you know?" And I''m like "Know what?" He said "Only my closest friends when I was younger used to call me the black cat." And he got kind of choked up and teary eyed and just thought it was an amazing coincidence but maybe it was also a testament to our relationship, that we knew each other well enough. It was really touching. \r\n\r\nCan you describe your journey from corporate architect...\r\nYou have done some homework! \r\n\r\nGoogle!\r\nVery well played. \r\n\r\nHow did you go from a Nike corporate architect to a designer?\r\nIt''s a great question because I don''t get asked that very often. What happened was, as the corporate architect, I did show some flashes of innovative things and almost at the same time Nike started to plateau out, it wasn''t growing very much, in fact it was shrinking. And our shoes were less interesting and we were starting to lay people off. They were desperate to pull out of this funk where our products weren''t doing so well. I know why — we were listening too much to the retailers and the sales people. It''s not the most creative way to design. They asked me to compete against all of the other shoe designers in a 24 hour contest. We had 24 hours to design a shoe and present it. Essentially I won the contest by a mile and they said "You are now a shoe designer. Forget that architecture stuff."\r\n\r\nDid they do anything with your competition designs?\r\nThey didn''t make it but it''s part of our archives. I designed a sneaker to be ridden on a motor scooter. I went completely away from what anybody else did. I drew the scooter and how the shoe would work on the scooter and yet you could get out and kind of jog around in it a little bit.\r\n\r\nHow did working as an architect influence your shoe designs? \r\nWhether you''re designing someone a home or a company headquarters or even a church, you sit down and you have to understand the religion and the customs and the nature of the people who are gonna be in that building. It''s exactly the same for shoe design and the process for discovery is the same. Even though the way you draw a building is different from drawing a shoe, that''s kind of immaterial. If you can draw, you can draw. It''s more about understanding other things. So it was a really easy switch for me. I still design other things too. Some people want me to design motorcycles, a couple of companies are talking to me. I don''t know. \r\n\r\nSo maybe it''s to aspiring sneaker designers advantage to not necessarily limit their scope in that way?\r\nThat is very insightful because it''s probably the one gem of advice I can give a young person who wants to be a sneaker designer. They’ll come to me and they’ll have a whole notebook full of sneaker designs and nothing else. And I''m going "Have you ever designed a toaster or a ever designed a car?" In order to be good, you should understand more than just sneakers. Your designs will be richer and more interesting.\r\n\r\nCan you tell me about visiting the Pompidou centre in Paris and the kind of influence it had on the first Nike Air Max?\r\nThe architect Renzi Piano essentially turned that building inside out. It''s glass, but a lot of the structures are outside of the glass. It''s like the guts of the building are on the outside. That says a little bit about how that building works. And then all the duct work for all the mechanical systems are painted in bright colours and the whole thing just says "this is what I am, as a building." I thought since we had developed this air bag technology for cushioning and no one had ever seen it before, why don''t we cut a hole in the side of the shoe so you could see that technology and understand it, just like that building did? I also like that building because it''s really provocative. A professor once told me that if people don''t either love or hate your work, you haven''t done anything. Just being daring and provocative even though a lot of people are gonna hate it — it might just change architecture or design.\r\n\r\nSo it''s about being informed of other cultures and reflective of things that aren''t just sport anymore.\r\nAbsolutely. Actually I went to the NGV and Melbourne Museum for two different exhibits of first people art. One was contemporary, one was really just looking at stuff that first people had done over hundreds of years. It was great to see both and that was fun because even functional items like a spear or a digging stick have artistic integrity. There''s usually some storytelling involved. It''s cool, it''s a past of design. People try to be more clean and modern but unadorned. I really dislike Dwell magazine in San Francisco because everything is the same. A building might look beautiful in its sparseness and its clean lines but if you think about it it''s, gonna look terrible once you live in it. That''s not how people live.\r\n\r\nIs there a sneaker you''ve done that you think was really underrated?\r\nYeah I worked on a shoe about 12 years ago and it was called the Sock Dart. I''ve had a few stupid projects, one was called Foot Tent where I tried to design a shoe that was like a backpacking tent for your foot so that you put the foot inside the shoe but it didn''t touch your foot. Like if you''re crawling inside a tent you have space around you. That was the dumbest idea ever. It sounded kind of neat at first. It went to the market too. It did okay, because we didn''t make very many. [laughs] \r\n\r\nIs there one thing you''ve always wanted to design but haven''t had the chance yet? What if it was anything in the world?\r\nHmm you''re good. [laughs] I think I''d like to design a motorcycle because I like motorcycles and it''d be another interesting challenge. \r\n\r\nIt could be a Nike motorcycle!\r\nI was working on a motorcycle for Gran Turismo, which is a PlayStation game, so that''s kind of like designing a real one. Cars are a little harder, they require so much development time and money but I don''t think it''d be much fun because too many people will get involved in that process. I don''t know. Actually, I shouldn''t even tell you this, but Virgin Airlines is going to be starting sending people into space — Richard Branson has this whole thing worked out. So we''re working on — we''re kind of semi-interested in designing a shoe for space — and I''ve already done one. I can''t show it to you though. \r\n\r\nCool.\r\nYeah. It''s a quarter of a million dollars to go to space. So there''s been a little bit of publicity around that in general, but not much about what anybody would wear. We may not do it, but it''s one of those things where you go "Uh, that could be interesting." And designing for movies is often really interesting, ''cos I''ve done that a bit. With some of the Batman movies, we designed the boots and of course, Michael F Fox''s self-lacing shoes in Back To The Future. There''s some other cool stuff we''re working on that I can''t talking about either. Those kind of projects cost a lot of money, but they''re good because they make you think a little more outside your comfort zone.  ', 'tinker.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recipient_id` int(11) NOT NULL,
  `sender_id` int(11) DEFAULT NULL,
  `activity_type` varchar(25) NOT NULL,
  `object_url` varchar(255) NOT NULL,
  `entry_datetime` int(11) NOT NULL,
  `is_unread` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `recipient_id` (`recipient_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `recipient_id`, `sender_id`, `activity_type`, `object_url`, `entry_datetime`, `is_unread`) VALUES
(1, 15, 17, 'comment_reply', '#comment_34', 1403371905, 0),
(2, 15, 1, 'contact_request', 'request_1', 1403374358, 0),
(3, 17, 15, 'comment_reply', 'comment_36', 1403378389, 0),
(4, 17, 15, 'comment_reply', 'comment_38', 1403406285, 0),
(5, 17, 15, 'comment_reply', 'comment_42', 1403406806, 0),
(6, 15, 17, 'comment_reply', 'comment_44', 1403409552, 0),
(7, 17, 12, 'comment_reply', 'comment_45', 1403444219, 0),
(8, 15, 12, 'comment_reply', 'comment_46', 1403444231, 0),
(9, 15, 15, 'comment_reply', 'comment_49', 1403469622, 0),
(10, 15, 17, 'comment_reply', 'comment_50', 1403469790, 0),
(11, 15, 17, 'comment_reply', 'comment_51', 1403469797, 0),
(12, 15, 17, 'comment_reply', 'comment_52', 1403469841, 0),
(13, 15, 15, 'comment_reply', '#comment_54', 1403473188, 0),
(14, 15, 15, 'comment_reply', '#comment_57', 1404610258, 0),
(15, 15, 15, 'comment_reply', '#comment_58', 1404610260, 0),
(16, 15, 15, 'comment_reply', '#comment_61', 1404613502, 0),
(17, 15, 15, 'comment_reply', '#comment_62', 1404613504, 0),
(18, 15, 15, 'comment_reply', '#comment_63', 1404613617, 0),
(19, 15, 15, 'comment_reply', '#comment_64', 1404613762, 0),
(20, 15, 15, 'comment_reply', '#comment_65', 1404613770, 0),
(21, 15, 15, 'comment_reply', '#comment_66', 1404613808, 0),
(22, 15, 15, 'comment_reply', '#comment_67', 1404613940, 0),
(23, 15, 15, 'comment_reply', '#comment_68', 1404613997, 0),
(24, 12, 12, 'comment_reply', '#comment_71', 1404625835, 0),
(25, 12, 12, 'comment_reply', '#comment_75', 1404627292, 0),
(26, 12, 12, 'comment_reply', '#comment_78', 1404682874, 0),
(27, 12, 12, 'comment_reply', '#comment_79', 1404682884, 0),
(28, 12, 15, 'comment_reply', '#comment_80', 1404683099, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `body` text NOT NULL,
  `entry_datetime` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `slug`, `body`, `entry_datetime`, `deleted`, `ordering`) VALUES
(1, 'About Us', 'about-us', 'FoundMissing\r\n\r\nAs you find something, the ravenous cycle of fashion leaves us wanting more. Our collections grow but every piece inspires something new, that one thing missing to make your collection complete.\r\n\r\n\r\nProblem\r\n\r\nâ€¨â€¨Footwear in general is fragmented and displayed across the web in such a way that is is difficult for one to be able to search for a specific hard to find shoe, whether they would like to find information on the product or purchase it. Although, we are not an online store, it our duty to direct you to the information and reach of purchase. â€¨â€¨\r\n\r\nSolution\r\n\r\nâ€¨â€¨FoundMissing solves the search/hunting problem by working closely with selected brands on providing the newest detailed product information that is specifically catered to their preferences. FoundMissing exists to fill the void on allowing viewers to express their opinions on select shoes and provide an ease of access to purchasing the end product.\r\n\r\nCopyright Â© 2014 FoundMissing. All rights reserved. ', 1376084166, 0, 0),
(2, 'Careers', 'careers', 'You feel you have the talent to work with FoundMissing ? If so please give us a shout at \r\n\r\njobs@foundmissing.ca\r\n\r\n', 1376084337, 0, 2),
(3, 'Privacy', 'privacy', 'The following Privacy Policy covers all the websites (â€œwebsitesâ€) owned, affiliated or operated by FoundMissing. (â€œFoundMissingâ€, â€œweâ€, â€œusâ€, â€œourâ€). It is our goal to bring you information tailored to your personal interests but, at the same time, protect your privacy. Please read the following to learn more about our privacy policy.\r\n\r\nBy visiting FoundMising websites, submitting information or using any of our products and services, you are accepting the practiced outlined in this Privacy Policy.\r\n\r\nThis Privacy Policy covers FoundMissingâ€™s treatment of personal information that FoundMissing gathers when you are on our websites and when you are using FoundMissing services. Also, this Privacy Policy covers FoundMissingâ€™s treatment of any personal information that FoundMissingâ€™s business partners share with us. This Privacy Policy does not cover information collected elsewhere, including without limitation offline and on sites linked to and from the website as well as the practices of third parties that FoundMissing does not control or own, or individuals that FoundMissing does not employ or manage.\r\n\r\nIn addition to reviewing this Privacy Policy, please read our User Agreement. Your use of our websites constitutes agreement to its terms and conditions as well.\r\n\r\nThis Privacy Policy may be modified from time to time so check back often. Continued access of our websites by you will constitute your acceptance of the changes or revisions to the Privacy Policy.\r\n\r\n1. Introduction\r\n\r\nThank you for visiting FoundMissing. This Privacy Policy is written to better explain how we collect and use your information. This Privacy Policy is a binding contract and you must agree to it to the same extent as our Terms and Conditions in order to use our services.\r\n\r\n2. Information You Provide to Us\r\n\r\nThe website generally collects personally identifying information with your specific knowledge and consent. This includes but is not limited to when you register for our forums, enter a sweepstakes and complete a survey. You can choose not to provide us with certain information, but you may not be able to take advantage of many of our special features such as posting on our forums. - Registration: In order to use select parts of our websites such as the database, you must complete a registration form. As part of this registration forum, we require your email address, name, city of origin, gender, shoe size, and date of birth. Optional information such as your referrer or time zone may also be requested.\r\n\r\n3. Personal Information We Collect When You Buy from Us\r\n\r\nIn order for you to place an order with us, you must provide us your name, e-mail address, mailing address, date of birth, Paypal information and telephone number. We will not disclose any information that you provide to us except in accordance with the provisions of this Privacy Policy.\r\n\r\n4. Automatic Information\r\n\r\nOur servers may automatically collect information about your computer when you visit our websites, including without limitation the website that referred you, your Internet Protocol (â€œIPâ€) and other usage and traffic information. FoundMissing uses this statistical information to analyze trends, gather demographic information and administer our websites so we may provide improved web experiences to our readers. The information is not in a manner that would identify you personally. We may provide statistical information, not personal information, to our partners about how our users, collectively use our websites. We share this information to our partners, so that they may further understand the usage of their areas and our websites, so that they too could provide you with a better web experience. There may also be occasion when we are legally required to provide access to our database in order to cooperate with police investigations or other legal proceedings. In those instances, the information is provided only for that purpose.\r\n\r\n5. Use of Information\r\n\r\nWe may use your information:\r\n\r\nTo enhance or improve user experience, the website, or service.\r\nTo process transactions and sell, transfer, and exchange your information to a third party for their marketing purposes with no additional consent.\r\nTo send e-mails about the site or respond to inquiries.\r\nTo send e-mails about the website and other products, news, or updates for FoundMissing.ca.\r\nTo send e-mails about third partiesâ€™ products or services.\r\n\r\n6. Cookies\r\n\r\nIn order to distinguish you from other usersâ€”especially other users on the same network as you â€” we need to use cookies to track your specific computer. Specifically, we use cookies to save your preferences, keep track of advertisements, and remember your shopping cart items. Additionally, our cookies may be accessible by third parties, or provided by third parties. For these reasons we require that you keep cookies enabled when using our website.\r\n\r\n7. Third Party Websites\r\n\r\nWe may link to third party websites from our own website. We have no control over, and are not responsible for, these third party websites or their use of your personal information. We recommend that you review their privacy policies and other agreements governing your use of their website.\r\n\r\n8. Third Party Access to Your Information\r\n\r\nWe protect your information from being accessed by third parties. However, since we may employ outside maintenance personnel or use a third party to host our material, there are third party entities which may have custody or access to your information. Because of this, it is necessary that you permit us to give access to your information to third parties to the same extent that you authorize us to do so. For greater certainty, every authorization which you grant to us in this Privacy Policy, you also grant to any third party that we may hire, contract, or otherwise retain the services of for the purpose of operating, maintaining, repairing, or otherwise improving or preserving our website or its underlying files or systems. You agree not to hold us liable for the actions of any of these third parties, even if we would normally be held vicariously liable for their actions, and that you must take legal action against them directly should they commit any tort or other actionable wrong against you. Additionally, we may store your information or share it with certain other third parties for reason that may or may not be related to maintenance, advertising, marketing, or other purposes related to our business. The following is a non-exhaustive list of other entities that we may store, share, or transfer your information with:\r\n\r\nGoogle Analytics\r\nParse.ly\r\n\r\n9. Marketing & Communication\r\n\r\nWhen registering for a FoundMissing database account, you will be given the option to opt-in to updates through the FoundMissing newsletter  ''new in'' and promotional emails. If you would prefer to opt-out from these e-mail, you can do so at any time by using the unsubscribe link at the bottom of the e-mail.\r\n\r\n10. Law Enforcement\r\n\r\nWe may disclose your information to a third party where we believe, in good faith, that it is desirable to do so for the purposes of a civil action, criminal investigation, or other legal matter. In the event that we receive a subpoena affecting your privacy, we may elect to notify you to give you an opportunity to file a motion to quash the subpoena, or we may attempt to quash it ourselves, but we are not obligated to do either.\r\n\r\n11. Security of Information\r\n\r\nWe use SSL certificates to help secure our website. However, we make no representations as to the security or privacy of your information. It is in our interest to keep our website secure, but we recommend that you use anti-virus software, firewalls, and other precautions to protect yourself from security threats.\r\n\r\n12. Amendments\r\n\r\nWe may amend this Privacy Policy from time to time, and the amended version will be posted on our website in place of the old version. We will also include the date that the currently displayed Privacy Policy took effect to help you determine whether there have been any changes since you last used our website. Like our Terms of Service, you must visit this page each time you come to our website and read and agree to it if the date it was last modified is more recent than the last time you agreed to the Privacy Policy. If you do not agree to the new Privacy Policy, you must immediately cease using our service.\r\n\r\n13. Contact\r\n\r\nAny inquiries about your rights under this Privacy Policy, or any other matters regarding your privacy, can be directed to info@foundmissing.ca', 1376084353, 0, 4),
(4, 'Legal Terms', 'legal-terms', '1. Introduction\r\n\r\nWelcome to FoundMissing.ca, a body corporate doing business in Ontario, Canada. This document constitutes a legally-binding agreement ("Agreement") governing the terms of providing you with our service. Throughout this document, the words "FoundMissing," "FoundMissing," "us," "we," and "our," refer to us, our website, FoundMissing.ca, or our service, FoundMissing, as is appropriate in the context of the use of the words. Likewise, the words "you" and "your" refer to you, the person who is being presented with this document for your agreement.\r\n\r\nBy registering for our website as a member, you are agreeing to receive regular newsletters and marketing related emails from FoundMissing. Should you wish to be removed from our email list, you can unsubscribe or terminate your account any time.\r\n\r\n2. Description of Service\r\n\r\nFoundMissing solves the search/hunting problem by working closely with selected brands on providing the newest detailed product information that is specifically catered to their preferences. FoundMissing exists to fill the void on allowing viewers to express their opinions on select shoes and provide an ease of access to purchasing the end product.\r\n\r\n3. Information Supplied\r\n\r\nWhen using our website and reviewing footwear, you will provide your name, e-mail address,city of origin, date of birth, shoe size, gender.\r\n\r\nIn addition to providing us with the above information about yourself, you must be eighteen years of age or older, or, if a higher minimum age of contractual capacity exists in your jurisdiction, you must be at least that age. Additionally, you represent to us that you are not otherwise considered to be under a disability to enter into a contract with us, whether it be because of an embargo in or against your jurisdiction or any other reason.\r\n\r\n4. Image Terms of Use\r\n\r\nEditors typically include images as part of their blog posts and the images that they are authorized to use is the following:\r\n\r\nImages that are licensed from vendors\r\nImages that are supplied to our editors or released into the public domain by public relations and marketing companies for press or other purposes\r\nImages that are supplied by readers, with the implied representation that the person submitting the image owns the copyright in the image and the right to give it to us for use on our site\r\nImages that are published on Photobucket or other public photos sites with licenses granted under Creative Commons, with attribution in accordance with the CC license granted in each case\r\nImages that are commissioned by FoundMissing\r\nImages that we believe to be covered by the Fair Use Doctrine, such as:\r\nThumbnail images of 150 x 150 pixels or less, cropped or reduced in size from the original source\r\nImages that are used to illustrate a newsworthy story, where the image itself tells a story\r\nImages used in a transformative manner, such as parody\r\nImages that are widely distributed that they have become part of the news\r\nIf FoundMissing receives any notice that an image is posted up inappropriately such as not keeping with the terms listed above, we reserve the right to remove that image.\r\n\r\nIf you think we have published an image or text that violates your copyright, we will address your concerns; however, if the image falls into one of the listed categories above and we believe that our use of the image is legitimate, we will not remove it from the site.\r\n\r\n5. Payment\r\n\r\nYou may pay for our services using Paypal. When providing credit card information, the credit card must be your own. If the credit card does not belong to you, you must have permission from the accountholder to use their card for your transactions.\r\n\r\nWe do not accept personal checks, money orders or direct bank transfers. Please note, items will not be shipped until the transaction has been cleared.\r\n\r\nAny use of fraudulent chargebacks simply to get free merchandise is not only strictly prohibited, but also criminal fraud, and will be dealt with by reporting such chargebacks to your local police department and a credit bureau, as well as referring the matter to a court of competent jurisdiction.\r\n\r\n6. Price Changes & Stock Availability\r\n\r\nPrices are subject to change without prior notice.\r\n\r\nWhile we endeavor to provide you with the most accurate and up-to-date information, occasionally ordered items may not be available as listed.\r\n\r\nIn the case that part of your order is unavailable or out of stock, the item(s) will be cancelled from the order with notification and where applicable, a refund shall be applied for the cost of the regarding item(s).\r\n\r\nFoundMissing ensures that all our product descriptions and prices are up-to-date and as accurate as possible. However, in few cases, pricing errors may occur with our listings. Should we discover a pricing error while processing your order and payment, we will immediately rectify the problem before cancelling and refunding you for your order. We will also make sure to inform you about the error with your order as soon as possible.\r\n\r\n7. Promotional & Store Credit Codes\r\n\r\nWhere a promotional code is required for an offer or discounted price, the code should be entered during the checkout process.\r\n\r\nCustomers should take care when entering codes, checking that the offer/credit has been applied. Should you find that the code is unapplicable or invalid, please refer to the specific conditions for the code as the promotion may have already expired. Should you have difficulty in applying store credit to your order, please contact us via e-mail at info@hypebeast.com, Monday to Friday, 10:00am to 6:00pm (Toronto time) for assistance.\r\n\r\n8. Discounts, Sales & Promotions\r\n\r\nFrom time to time, FoundMissing may offer discounts and/or promotions. Discount, sales and/or promotional offers and prices are only applicable when checking out whilst the offer is still valid. Time restrictions will be stated where applicable. FoundMissing reserves the right to amend, extend or terminate these offers at an earlier date without prior notice. For those who purchased over 24 hours before the sale, price adjustments will not be made. For those who purchased within 24 hours, we can refund the discount being offered at the time.\r\n\r\n9. Rules of Conduct\r\n\r\nWe host a review section as well as blogs that we may permit you to comment on. As a part of using our website, you must not:\r\n\r\nPost anything that violates any applicable law, foreign or domestic.\r\nPost anything defamatory, violent, pornographic, hateful, racist, or otherwise objectionable. We have the sole right to determine what is objectionable.\r\nHack, crack, phish, SQL inject, or otherwise threaten or actually harm the security of our systems or those of our users.\r\nInfringe on the intellectual property rights of anyone.\r\nDo anything that infringes on the rights of us or a third party.\r\nCreate more than one account, use another personâ€™s account, or allow another person to use your account.\r\nEngage in unsolicited marketing activities, whether commercial or non-commercial, on our forum or through its private messaging service, or on our blog comments section, or anywhere else on our website.\r\nAdvertise on our website without our permission.\r\nImpersonate another person or organization.\r\nPost any links to content which would, if posted on our own website, violate these Rules of Conduct.\r\nWe reserve the right to terminate our service to you for reasons other than those listed here. We may terminate your account without explanation.\r\n\r\n10. Communications Decency Act\r\n\r\nAs we are located in Toronto, we have certain defenses available to us for liability for defamation posted on our website by third parties, such as in our forums or on our blog.\r\n\r\nOur Rules of Conduct prohibit defamation from being published on our website. In order to complain about defamatory material on our website, please notify us with full details of where the defamation is located on our website (provide us with the URL), your name, and an explanation of how the material is defamatory. We will respond within a reasonable time and, if it appears that there are grounds to believe the material is defamatory, we will remove it. You may send your request to info@foundmissing.ca.\r\n\r\n\r\n11. Contest Disclaimer\r\n\r\nThe maximum number of entries per person allowed during the contest period is as the contest has specified. Any attempt by a person to submit more than the stated maximum number of entries will be disqualified from participation in this contest. FoundMissing are not responsible for lost, late, incomplete, invalid, unintelligible, illegible, misdirected or postage-due entries, which will be disqualified. By participating, all entrants agree to abide by these official contest rules.\r\n\r\nIn consideration for your participation in the contest, the individual, group, organization, business, spectator, or other, does hereby release and forever discharge the FoundMissing, and its officers, board and employees, jointly and severally from any and all actions, cause of actions, claim and demands for, upon or by reason of any damage, loss or injury including death, which hereafter may be sustained by participating in the contest.\r\n\r\nThis release extends and applies to, and also covers and includes, all unknown, unforeseen, unanticipated and unsuspected injuries, damages, loss and liability and the consequences thereof including death, as well as those now disclosed and know to exist. The provision of any state, federal, local or territorial law or state providing substance that releases shall not extend to claims, demands, injuries, or damages which are know or unsuspected to exist at this time, to the person executing such release, are hereby expressly waived.\r\n\r\nIt is further understood and agreed that said participation in the contest not to be construed as an admission of any liability and acceptance of assumption of responsibility by FoundMissing, its officers, board and employees, jointly and severally, for all damages and expenses for which the FoundMissing , its officers, board and employees, become liable as a result of any alleged act of the parade participant.\r\n\r\nThe prize is non-transferable and cannot be exchanged for cash.\r\n\r\nFoundMissing reserves the right to offer alternative prize if any prize is not available or under special situation.\r\n\r\n12. Disclaimer\r\n\r\nWe do not actively screen blog comments and forum posts before they are uploaded to our website by third parties. As a part of your acceptance of this Agreement, you release us from any liability for the actions of third parties on our website, whether or not we would otherwise normally be held liable for such words under the laws of Ontario or any other jurisdiction.\r\n\r\n13. Copyright\r\n\r\nYou agree not to copy, distribute, display, disseminate, or otherwise reproduce any of the information on our website, or our website itself, without our prior written permission. This includes, but is not limited to, a prohibition on aggregating product information reviews and forum content made available on our website.\r\n\r\n14. Trademarks\r\n\r\n"FoundMissing" is a trademark used by us, FoundMissing, to uniquely identify our website, business, and service. You agree not to use this phrase anywhere without our prior written consent. Additionally, you agree not to use our trade dress, or copy the look and feel of our website or its design, without our prior written consent. You agree that this paragraph goes beyond the governing law on intellectual property law, and includes prohibitions on any competition that violates the provisions of this paragraph.\r\n\r\n15. Revocation of Consent\r\n\r\nWhere FoundMissing has given prior written consent for your use of our protected material in accordance with our above â€œCopyrightâ€ and â€œTrademarksâ€ provisions, we may revoke that consent at any time. If we so request, we may require that you immediately take action to remove from circulation, display, publication, or other dissemination, any of the marks, copyrighted content, or other materials that we previously consented for you to use.\r\n\r\n16. Notification of Changes\r\n\r\nWhenever FoundMissing changes its Terms of Use, changes will be posted to ensure users are aware of the new changes.\r\n\r\n17. Contact Information\r\n\r\nIf you have any questions, concerns or notices of violations to these terms and conditions, please contact the editor of FoundMissing by sending an email to info@foundmissing.ca\r\n', 1376084367, 0, 3),
(5, 'Contact Us', 'contact-us', 'For general inquiries collaborations, advertising opportunities on the site.\r\nPlease email:\r\n \r\ninfo@foundmissing.ca', 1376084379, 0, 5),
(6, 'Branding', 'branding', '<link rel="stylesheet" href="/css/pages.css">\r\n<div id="wrapper">\r\n<h1>Logos and Branding</h1>\r\n<div class="panel">\r\n<h2>Our Logo</h2>\r\n<hr />\r\n<h3>Grab one, it''s free! Just don''t be a jabronie â€“ please don''t mess with it.<br />\r\nThese all come in a folder with EPS and PNG files of different sizes. <a href="images/twoople-logos.zip">Click here</a> to download everything at once.<br /><br />\r\n<div class="arrow-down"></div> This is the primary FoundMissing logo. Try to use this one whenever you can.</h3>\r\n    <div class="logowrapper" style="margin-top: 0px;">\r\n        <div id="logo-p">\r\n        <center><img src="/images/fm-primarywhitetrans-64.png" width="372" height="64" alt="foundmissing primary logo red"/></center>\r\n        <a href="images/twoople-primary.zip"><div class="download"></div></a>\r\n		</div>\r\n    </div>\r\n    \r\n    <div class="logowrapper" id="half" style="margin-right:2.5%;">\r\n      <div id="logo-s">\r\n        <center>\r\n          <img src="/images/fm-sectrans-128.png" width="178" height="128" alt="foundmissing secondary logo" />\r\n        </center>\r\n        <a href="images/twoople-secondary.zip"><div class="download"></div></a>\r\n        </div>\r\n        <div class="description" id="half">\r\n        <h3><div class="arrow-up"></div> This is our secondary logo. When things get too tight on space, use this guy.</h3>\r\n        </div>\r\n    </div>\r\n    \r\n  <div class="logowrapper" id="half">\r\n      <div id="logo-i">\r\n        <center>\r\n		  <img src="/images/fm-redtrans-128.png" width="128" height="128" alt="foundmissing icon" />\r\n        </center>\r\n        <a href="images/twoople-icon.zip"><div class="download"></div></a>\r\n        </div>\r\n        <div class="description" id="half">\r\n        <h3><div class="arrow-up"></div> Use these icons for UI stuff and instances with limited space.</h3>\r\n      </div>\r\n    </div>\r\n</div>\r\n\r\n    \r\n    \r\n    \r\n\r\n\r\n\r\n<div class="panel" style="padding-top: 50px; margin-top:40px;">\r\n<h2>Colour</h2>\r\n<hr />\r\n<h3>These are our primary and supporting colours.</h3>\r\n<div class="colour" id="fmred">\r\n    <div class="colourinner">\r\n    <h3>FM Infrared</h3>\r\n    <h4>HEX	#EC1C3A<br />\r\n        RGB	236, 28, 58<br />\r\n        CMYK	1, 99, 80, 0<br />\r\n        PMS	1788 C</h4>\r\n    </div>\r\n</div>\r\n<div class="colour" id="fmdgrey">\r\n	<div class="colourinner">\r\n    <h3>Dark Grey</h3>\r\n    <h4>HEX	#535858<br />\r\n    RGB	83, 88, 88<br />\r\n    CMYK	66, 54, 55, 29</h4>\r\n    </div>\r\n</div>\r\n<div class="colour" id="fmgrey">\r\n	<div class="colourinner">\r\n    <h3>Grey</h3>\r\n    <h4>HEX	#A0A6A6<br />\r\n    RGB	160, 166, 166<br />\r\n	CMYK	40, 29, 31, 0</h4>\r\n    </div>\r\n</div>\r\n<div class="colour" id="fmlgrey">\r\n	<div class="colourinner">\r\n    <h3>Light Grey</h3>\r\n    <h4>HEX	#F1F2F2<br />\r\n	RGB	241, 242, 242<br />\r\n	CMYK	4, 2, 3, 0</h4>\r\n    </div>\r\n</div>\r\n\r\n\r\n\r\n<div class="colour2" id="fmwhite">\r\n	<div class="colourinner">\r\n    </div>\r\n</div>\r\n<div class="colour2" id="fmcharles">\r\n    <div class="colourinner">\r\n    <h3>Charleston Green</h3>\r\n    <h4>HEX	#232B2B<br/>\r\n    	RGB	35, 43, 43<br/>\r\n    	CMYK	76, 63, 64, 66</h4>\r\n    </div>\r\n</div>\r\n<div class="colour2" id="fmpink">\r\n	<div class="colourinner">\r\n    <h3>Pink</h3>\r\n    <h4>HEX	#535858<br />\r\n    RGB	83, 88, 88<br />\r\n    CMYK	66, 54, 55, 29</h4>\r\n    </div>\r\n</div>\r\n<div class="colour2" id="fmgreen">\r\n	<div class="colourinner">\r\n    <h3>Green</h3>\r\n    <h4>HEX	#232B2B<br/>\r\n    	RGB	35, 43, 43<br/>\r\n    	CMYK	76, 63, 64, 66</h4>\r\n    </div>\r\n</div>\r\n</div>\r\n\r\n\r\n\r\n<div class="panel">\r\n<h2>Spacing</h2>\r\n<hr />\r\n<h3>Keep it spacy.<br />\r\nWhen placing the FM logo, try and keep around an icon''s worth of padding between it and adjacent elements.</h3>\r\n<center><img src="/images/fm-safearea.png" width="500" height="128" alt="foundmissing safe area" /></center>\r\n</div>\r\n\r\n\r\n\r\n\r\n\r\n<div class="panel">\r\n<h2>Typography</h2>\r\n<hr />\r\n<h3>We use Georgia for copy, Verdana for the real tiny stuff, and <a href="http://www.jlee.ca/rigby.html">Rigby</a> for titling.</h3>\r\n\r\n    \r\n    <div class="typespec" style="margin-top:40px;">\r\n    <h2 style="font-family: Verdana; margin-bottom:-5px;">Verdana</h2>\r\n    <h3 style="font-family: Verdana; letter-spacing:2.5px; font-size:16px; margin-bottom:-5px;">A B C D E F G H I J K L M N O P Q R S T U V W X Y Z</h3>\r\n    <h3 style="font-family: Verdana; letter-spacing:3.5px; font-size:16px; margin-bottom:-5px;">a b c d e f g h i j k l m n o p q r s t u v w x y z</h3>\r\n    <h3 style="font-family: Verdana; letter-spacing:3.7px; font-size:16px; margin-bottom:-5px;">0 1 2 3 4 5 6 7 8 9 + - = ! @ # $ % ^ & * ( : ? </h3>\r\n    </div>\r\n    \r\n    <div class="typespec">\r\n    <h2 style="font-family: Georgia; margin-bottom:-5px;">Georgia</h2>\r\n    <h3 style="font-family: Georgia; letter-spacing:3.5px; font-size:16px; margin-bottom:-5px;">A B C D E F G H I J K L M N O P Q R S T U V W X Y Z</h3>\r\n    <h3 style="font-family: Georgia; letter-spacing:5px; font-size:16px; margin-bottom:-5px;">a b c d e f g h i j k l m n o p q r s t u v w x y z</h3>\r\n    <h3 style="font-family: Georgia; letter-spacing:5.5px; font-size:16px; margin-bottom:-5px;">0 1 2 3 4 5 6 7 8 9 + - = ! @ # $ % ^ & * ( : ? </h3>\r\n    </div>\r\n    \r\n   	<div class="typespec">\r\n    <h2 style="font-family: rigby; margin-bottom:-5px;">Rigby</h2>\r\n    <h3 style="font-family: rigby; letter-spacing:4px; font-size:16px; margin-bottom:-5px;">A B C D E F G H I J K L M N O P Q R S T U V W X Y Z</h3>\r\n    <h3 style="font-family: rigby; letter-spacing:5.2px; font-size:16px; margin-bottom:-5px;">a b c d e f g h i j k l m n o p q r s t u v w x y z</h3>\r\n    <h3 style="font-family: rigby; letter-spacing:6px; font-size:16px; margin-bottom:-5px;">0 1 2 3 4 5 6 7 8 9 + - = ! @ # $ % ^ & * ( : ? </h3>\r\n    </div>\r\n</div>\r\n</div>', 1401664164, 0, 1),
(7, 'Partners', 'partners', '<a href="http://tinypic.com?ref=ivznlj" target="_blank"><img src="http://i62.tinypic.com/ivznlj.jpg" border="0" alt="Image and video hosting by TinyPic"></a>', 1401664264, 0, 6),
(8, 'Advertising', 'advertising', 'adf', 1401664277, 1, 7);

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE IF NOT EXISTS `reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `excerpt` varchar(150) NOT NULL,
  `body` text NOT NULL,
  `shoe_id` int(11) NOT NULL,
  `cover_image` varchar(100) NOT NULL,
  `entry_datetime` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `ordering` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `user_id`, `title`, `slug`, `excerpt`, `body`, `shoe_id`, `cover_image`, `entry_datetime`, `deleted`, `ordering`) VALUES
(1, 15, 'Jordan XX8 SE', 'jordan-xx8-se', 'The Air Jordan XX8 SE Men''s Basketball Shoe is equipped with with Nike Zoom Air units for elite, low-profile cushioning during play. A lightweight', 'The Air Jordan XX8 SE Men''s Basketball Shoe is equipped with Nike Zoom Air units for elite, low-profile cushioning during play. A lightweight midsole with Flight Plate technology offers stability and support, while a multidirectional outsole delivers excellent traction.\n\nSince his game-winning shot that brought the National Championship to North Carolina, Michael Jordan has been at the forefront of basketball consciousness. He took the court in 1985 wearing the original Air Jordan I, setting a new standard for performance and style, simultaneously breaking league rules and his opponents'' will while capturing the imagination of fans worldwide. Ever evolving, ever at the pinnacle of imagination, the Air Jordan line is the ultimate in basketball footwear.', 1, 'air-jordan-xx8-camo-2.jpg', 1402958468, 0, 0),
(2, 15, 'Lebrons 11 elite', 'lebrons-11-elite', 'he LeBron 11 Elite Men''s Basketball Shoe features Hyperposite construction, which blends the nearly seam-free, lightweight strength of Hyperfuse with ', 'he LeBron 11 Elite Men''s Basketball Shoe features Hyperposite construction, which blends the nearly seam-free, lightweight strength of Hyperfuse with the streamlined durability of Foamposite. Full-length Nike Zoom cushioning provides low-profile, responsive impact protection.', 6, 'nike-lebron-11-elite-team-photos-3.jpg', 1402973876, 0, 1),
(3, 15, 'Nike Free Trainer 5.0 iD', 'nike-free-trainer-50-id', 'The Nike Free Trainer 5.0 iD delivers lightweight flexibility, superior support and a choices of outsoles designed to provide the best traction in the', 'The Nike Free Trainer 5.0 iD delivers lightweight flexibility, superior support and a choices of outsoles designed to provide the best traction in the gym or on the pavement. With NIKEiD, you can truly own your training by customizing the entire shoe with exclusive colors, graphics and materials.\r\n\r\nNike Frees are different than traditional athletic shoes. Theyâ€™re designed to let your feet move more freely and naturally, which means your muscles will be doing a bit more work than demanded of them in some other athletic shoes. Nike encourages you to get used to them gradually for a safer, more effective and more enjoyable experience. Please visit http://nike.com/nikefree for more information on how to get the best from your Nike Free shoes.', 7, 'IMG_6467.jpg', 1403030893, 0, 2),
(4, 15, 'Gucci Diamante Leather High-Top Sneaker', 'gucci-diamante-leather-hightop-sneaker', 'n 1921 Florence, a craftsman named Guccio Gucci opened his store to sell leather goods, and a legend was born. Over the decades, people around the wor', 'n 1921 Florence, a craftsman named Guccio Gucci opened his store to sell leather goods, and a legend was born. Over the decades, people around the world have admired and appreciated works from the House of Gucci---whether it''s women''s ready-to-wear, men''s designs, children''s clothes, accessories, handbags, shoes, or fragrances. Today, designer Frida Giannini holds the reins of this venerable Italian line.', 8, '2013-New!-Gucci-221825-Blue-Crocodile-High-top-Sneaker_1.jpg', 1403031259, 0, 3),
(5, 15, 'nike zoom kd vi', 'nike-zoom-kd-vi', 'The KD VI Men''s Basketball Shoe was built with a new, responsive Nike Zoom unit, a more supportive upper and a lower-cut silhouette for better flex th', ' <p><b>RESPONSIVE PERFORMANCE</b></p>\r\n<br>\r\n<p>The KD VI Men''s Basketball Shoe was built with a new, responsive Nike Zoom unit, a more supportive upper and a lower-cut silhouette for better flex through the ankle&mdash;resulting in KD''s lightest shoe to date.</p>\r\n<br>\r\n<p><b>Responsive Cushioning</b></p>\r\n<p>The KD VI has visible Max Air in the heel for maximum impact protection on hard landings,&#160;plus a Nike Zoom unit in the forefoot for lightweight, low-profile responsiveness. Together with a Phylon midsole, they provide exceptional cushioning for every position on the court.</p>\r\n<br>\r\n<p><b>Ultra-Light Support</b></p>\r\n<p>The shoe''s two-layer upper is ultra-thin and features Flywire technology&mdash;super-light, super-strong nylon cables that support the foot by encasing it in a secure hug. High-wrapped rubber at the outsole adds stability and support during quick cuts.</p>\r\n<br>\r\n<p><b>Lightweight Durability</b></p>\r\n<p>A multidirectional outsole pattern offers excellent traction and durability for total control, with layers of rubber stripped away in some areas to maintain the shoe''s light weight.</p>\r\n<br>\r\n<p><b>Enhanced Flex</b></p>\r\n<p>The collar of the KD VI is lower than that of its predecessor, allowing more flexibility through the ankle.</p>\r\n<br>\r\n<p><b>More Benefits</b></p>\r\n<ul>\r\n<li>Flywire technology on the tongue for an extra-secure fit</li>\r\n<li>Asymmetrical tongue that integrates with the upper for lockdown</li>\r\n</ul>', 9, 'Nike-Zoom-KD-VI-Schwarz-Gruen_b6.jpg', 1403031786, 0, 4),
(6, 15, 'JORDAN MELO M10', 'jordan-melo-m10', 'Carmelo Anthony''s on-court skill was a key influence in the design of the Jordan Melo M10 Men''s Basketball Shoe, featuring revolutionary Flight Plate ', 'SUPERIOR RESPONSIVENESS TO ELEVATE YOUR GAME\r\nCarmelo Anthony''s on-court skill was a key influence in the design of the Jordan Melo M10 Men''s Basketball Shoe, featuring revolutionary Flight Plate technology for maximum responsiveness and signature details that celebrate Melo''s tenth year with Nike Basketball.\r\nMAXIMUM RESPONSIVENESS\r\nJordan''s signature Flight Plate technology harnesses the energy of every step for maximum responsiveness.\r\nSECURE LOCKDOWN\r\nDynamic Fit technology integrates with the laces for a locked-down fit that molds to the unique shape of your foot.\r\nSIGNATURE DETAILS\r\nThe heel counter features a chrome finish that incorporates Melo''s first signature logo, created for the first Jordan Melo shoe ten years ago.\r\nMORE BENEFITS\r\nMolded quarter panels made of full-grain leather for a premium look\r\nPerforated side overlays for breathability and support\r\nRubber outsole for durability and traction\r\nTPU heel counter for stability and support', 10, 'IMG_0015.jpg', 1403032025, 0, 5);

-- --------------------------------------------------------

--
-- Table structure for table `stores`
--

CREATE TABLE IF NOT EXISTS `stores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `description` text,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `stores`
--

INSERT INTO `stores` (`id`, `title`, `description`, `ordering`, `deleted`) VALUES
(1, 'Chippewa', '', 0, 1),
(2, 'Sebago', '', 1, 1),
(3, 'Puma', '', 2, 1),
(4, 'Under Armour', '', 3, 1),
(5, 'CNCPTS', '', 4, 1),
(6, 'The Bay', '', 5, 1),
(7, 'Amazon', '', 6, 1),
(8, 'Foot Locker', '', 7, 1),
(9, 'K-Swiss', '', 8, 1),
(10, 'Karmaloop', '', 9, 1),
(11, 'Pick Your Shoes', '', 10, 1),
(12, 'The Pop Shop', '', 11, 1),
(13, 'The Shoe Company', '', 12, 1),
(14, 'Livestock ', '', 0, 0),
(15, 'Nrml', '', 1, 0),
(16, 'Capsule', '', 2, 0),
(17, 'Serpentine ', '', 3, 0),
(18, 'Adrift ', '', 4, 0),
(19, 'Get Outside', '', 5, 0),
(20, 'Off The Hook', '', 6, 0),
(21, 'Nomad', '', 7, 0),
(22, 'Haven ', '', 8, 0);

-- --------------------------------------------------------

--
-- Table structure for table `styles`
--

CREATE TABLE IF NOT EXISTS `styles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `category_id` int(1) NOT NULL,
  `description` text,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `styles`
--

INSERT INTO `styles` (`id`, `title`, `category_id`, `description`, `ordering`, `deleted`) VALUES
(1, 'Basketball', 1, '', 1, 0),
(2, 'Cross-Trainer', 1, '', 2, 0),
(3, 'Skate', 1, '', 4, 0),
(4, 'Casual', 1, '', 6, 0),
(5, 'Runner', 1, 'Shoes for vegans', 9, 0),
(6, 'Chukka', 2, '', 5, 0),
(7, 'Hiker ', 2, '', 5, 0),
(8, 'Winter ', 2, '', 8, 0),
(9, 'Above Ankle', 2, '', 7, 0),
(10, 'Dress Boots', 2, '', 3, 0),
(11, 'Oxford', 3, '', 0, 0),
(12, 'Monk', 3, NULL, 1, 0),
(13, 'Derby', 3, NULL, 1, 0),
(14, 'Loafer', 3, NULL, 1, 0),
(15, 'Other', 3, NULL, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE IF NOT EXISTS `subscribers` (
  `email` varchar(100) NOT NULL,
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscribers`
--

INSERT INTO `subscribers` (`email`) VALUES
('gully@theyoungastronauts.com'),
('tyler@hortm.com');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `admin` int(1) NOT NULL DEFAULT '0',
  `gender` varchar(10) DEFAULT NULL,
  `age` varchar(60) DEFAULT NULL,
  `shoe_size` varchar(25) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `country` varchar(60) DEFAULT NULL,
  `title` varchar(60) DEFAULT NULL,
  `bio` text,
  `twitter` varchar(50) DEFAULT NULL,
  `facebook` varchar(60) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `image` varchar(50) DEFAULT 'default_profile.jpg',
  `entry_datetime` int(11) DEFAULT '0',
  `update_datetime` int(11) NOT NULL DEFAULT '0',
  `deleted` int(1) NOT NULL DEFAULT '0',
  `karma` int(100) NOT NULL DEFAULT '0',
  `code` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `firstname`, `lastname`, `admin`, `gender`, `age`, `shoe_size`, `city`, `country`, `title`, `bio`, `twitter`, `facebook`, `website`, `image`, `entry_datetime`, `update_datetime`, `deleted`, `karma`, `code`) VALUES
(1, 'tylersavery', 'sdgdasfds', 'f5c38375cd3ffcbfe0957cb9ae6dce35', 'Tyler', 'Savery', 1, 'sgsdaf', NULL, 'sadgdsf', 'sdgsadf', NULL, NULL, NULL, 'asdfdsf', NULL, 'dsgdsaf', NULL, 0, 0, 0, 0, ''),
(2, 'dfad', 'dfsg', '1530d0e446ff74e728158fd5fe89e592', 'dfadsf', 'dsf', 0, 'dsfasd', NULL, 'fgdsfg', 'fdgdf', NULL, NULL, NULL, 'fdsg', NULL, 'fdsg', NULL, 0, 0, 1, 0, ''),
(3, 'adsfdasf', 'dsaf', '47df24e6aa353e1876e81388bfb24515', 'dasfadsf', 'das', 0, 'gfsdg', NULL, 'fdgf', 'fgs', NULL, NULL, NULL, 'fds', NULL, 'hdgfh', NULL, 0, 0, 1, 0, ''),
(4, 'asfdsf', 'retw', '0745da98f74d2927db1f7151bbdbdce1', 'gsdaf', 'das', 0, 'dsf', NULL, 'gfd', 'hgfdtr', NULL, NULL, NULL, 'bvcbc', NULL, 'sdgf', NULL, 0, 0, 1, 0, ''),
(5, 'adsfds', 'fdsg', 'eeab8d82f794c109fc842353426daee1', 'fgsdfg', 'fgds', 0, 'fdsg', NULL, 'fdsg', 'dfsg', NULL, NULL, NULL, 'dsfg', NULL, 'hdsfg', NULL, 0, 0, 1, 0, ''),
(6, 'adam', '', '0282c80acb3ef9ef52aad7ff1bd9a4fb', 'adam', 'gulyas', 0, '', NULL, '', '', NULL, NULL, NULL, '', NULL, '', NULL, 0, 0, 1, 0, ''),
(7, 'bobbyb', 'bobb@asdsa.com', 'f5c38375cd3ffcbfe0957cb9ae6dce35', 'bobby', 'b', 0, 'male', NULL, '12', 'Toronto', NULL, NULL, NULL, 'bobbyyhhh', NULL, 'hey.com', NULL, 0, 0, 1, 0, ''),
(8, 'heyaaasd', 'dfa@sd.com', 'f5c38375cd3ffcbfe0957cb9ae6dce35', 'adsf', 'sdfsdaf', 0, 'male', NULL, '21', 'Toronto', NULL, NULL, NULL, 'somet', NULL, 'dasdsad.com', NULL, 0, 0, 1, 0, ''),
(9, 'arny', NULL, 'e2c40727bbbefdda527baadf8d929f7a', 'Arnold', 'Palmer', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 0, ''),
(10, 'john', NULL, '3e28cf872b83ec263d9de93852c433d4', 'John', 'Doe', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 0, ''),
(11, 'adamg', 'gully@theyoungastronauts.com', '21744fb20bbed76d69100830a9d38107', 'Adam', 'Gulyas', 0, 'M', NULL, '11', 'Whitby', NULL, NULL, NULL, '@meerp', NULL, '', NULL, 0, 0, 1, 0, ''),
(12, 'david', 'david', '00b72fbb0801e3dc426362e3ce8ff17c', 'David', 'Bremang', 1, 'Male', '52', '18', 'toronto', 'canada', NULL, NULL, 'http://www.twitter.com', '', 'http://www.foundmissing.ca', 'edvq7pomucpunv.jpg', 1398175909, 1404616889, 0, 17, ''),
(13, 'Taimoor', NULL, '561a9cdedbf74132b641cf3f2aefa5b6', 'Taimoor', '.', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1398175909, 0, 1, 0, ''),
(14, 'duke', NULL, '00b72fbb0801e3dc426362e3ce8ff17c', 'Duke', 'Sigulinski ', 0, NULL, NULL, NULL, NULL, 'Canada', NULL, NULL, NULL, NULL, NULL, NULL, 1398175909, 0, 1, 0, ''),
(15, 'mike', 'mike', '4d3911052cc5a9d1f929274de9fd6312', 'Mike', 'Nguyen', 1, 'male', '29', '10.0', 'sauga', 'canada', 'master web sensei', '<h2>My Bio</h2>\r\n<p>Et viderer sensibus suscipiantur eum, pri ea suscipit periculis. Nemore aliquip complectitur ea quo, at sea justo dolorum. Velit lobortis cu vix, sit movet iusto veniam in. Iusto scriptorem no ius. Eum doctus posidonium ad, vim id oratio oblique. Usu an accusata constituto. Ut suas prima indoctum mea, sit eu affert incorrupte.</p>\r\n<p>Vel facilisis mnesarchum ut. In eos nisl autem epicuri, vel ei ubique efficiantur. Ad vix errem eirmod democritum, quo alia choro id. Laudem percipitur ei eum, facer dicant doctus in eum, ne mea simul invenire. Porro pertinax cu his, brute tacimates repudiandae et usu, nulla nostrum iracundia eos ei.</p>\r\n\r\n<p>Eam liber nobis nusquam ea. Est tantas fuisset dolores ex, id nec nonumy sapientem, eam labore mediocritatem in. Ex etiam dictas insolens per, an usu consul scaevola, an tota mutat rationibus vix. Consul alterum vix cu. Eu vide equidem definitionem nam.</p>\r\n\r\n<p>Pri no dicit diceret consetetur. Ex ridens bonorum cum. Nam ut meis mutat moderatius, cum doming consequat in. Eum eu zril pericula. Illum paulo elaboraret ne quo.</p>', 'http://twitter.com', '', 'http://www.hoangnguyen.ca', 'jwoo2228d7snxg.jpg', 1401286309, 1405224775, 0, 33, ''),
(16, 'mikenguyen', 'mikenguyen191@gmail.com', '4d3911052cc5a9d1f929274de9fd6312', 'Mike', 'Nguyen', 0, 'male', '28', '10', 'mississauga', 'Canada', 'The Professor', NULL, NULL, NULL, NULL, 'xv9m96vnyrz73v.jpg', 1402198539, 1402470339, 0, 4, ''),
(17, 'batman007', 'batman@batman.com', '4d3911052cc5a9d1f929274de9fd6312', 'bruce', 'wayne', 0, 'male', '32', '10.5', 'Gotham', 'United States', 'The Dark Knight', 'There is a city in the north of the United States by the name of Gotham City, once a major industrial centre it was hit badly by the Great Depression and has never really recovered. Crime rates rose, corrupt officials and despair set in despite the best efforts of the original Green Lantern and Black Canary. The darkness had taken the city and it would take a Dark Knight to reclaim it.\n\n  Young Bruce Wayne is the only son of prominent Gotham City doctor Thomas Wayne and his wife Martha Wayne. Two events in his childhood forever scared the future of Bruce. The first was when he accidentally fell through a hole in the grounds of his ancestral home Wayne Manor and found himself in a cave system that ran under the house full of bats. For years afterwards Bruce would catch sight of one particularly large bat over and over again.\n\n  The second and most tragic event happened just after the family had left a showing of the classic film "The Mark of Zorro" at the cinema and was walking back to their home. They were confronted by a mugger who demanded that they handed over their valuables. Thomas Wayne put up a struggle and in the process he and his wife was shot dead. The mugger ran off leaving the shocked Bruce alone with the bodies of his dead parents. First on the scene was physician Leslie Thompkins who sought to look after Bruce.\n\n  Thomas''s brother Phil Wayne was named Bruce''s guardian and with the help of Thompkins and the family butler Alfred Pennyworth they raised him as best they could. For days after the funeral Bruce was unsettled, it came to a head one night when he ran to his parents grave and swore his vengeance on the criminal world. On that night Bruce Wayne died and the Batman was born.\n\n  Bruce put his utmost into his school work and upon graduation he left the US to travel the world learning from the brightest and the best in each field of speciality. He also travelled to remote schools on the Far East and studied for years under the masters of the various martial arts. Upon returning to the United States, he set out on a fact-finding mission into the depraved parts of Gotham''s seedier districts to see how the criminal element had evolved. While there he became involved in a street brawl where he was seriously injured. He managed to make his way back to Wayne Manor and slumped in his study slowly bleeding he considered the situation. He was injured because the local criminals would not fear him as plain common man on the street, yet his father always maintained that criminals were a superstitious and cowardly lot. He needed to make them fear but how?', 'https://twitter.com/TheBatman', 'https://www.facebook.com/batman', NULL, '2l2gvhlqx9u764.jpg', 1402198539, 1403719914, 0, 7, '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
