<?php

namespace Frontend\Controllers;

class Gallery extends \Frontend\Controllers\Frontend {

    public $photos = array();

    public function __construct($uri, $data){
        parent::__construct($uri, $data);
		$this->add_asset('css', 'gallery.css', true);
		$this->add_asset('js', 'gallery.js', true);
		$this->add_asset('js', 'bootstrap.carousel.modal.js', true);
		$this->title = 'Gallery | FM';
		
		
		$this->albums = \Gallery\Models\Gallery::all(array('conditions' => 'deleted = 0 AND publish_date > 1', 'order' => 'entry_datetime DESC'));
    }

    public function controller(){
		$this->get_albums();
        $this->set_view('Frontend\Views\Gallery');
    }
	
	protected function get_albums() {
		foreach ($this->albums as $bums) {
			
			$album_attachments = \Gallery_attachment\Models\Gallery_attachment::first(array('select'=> 'attachment_id', 'conditions' => 'gallery_id = ' . $bums->id));
			$cover_image = \Attachment\Models\Attachment::first(array('select'=> 'image_filename', 'conditions' => 'id = ' . $album_attachments->attachment_id));
			$this->album[] = array(
				'id' => $bums->id,
				'cover' => GALLERY_IMAGES . $bums->slug . '/' . $cover_image->image_filename,
				'image' => $this->get_images($bums->id, $bums->slug),
				'author' => $bums->author_name,
				'title' => $bums->title,
				'description' => $bums->description,
				'date' => time_to_friendly_date($bums->publish_date, 'Y.m.d')
			);
		}
	}
	
	protected function get_images($album_id, $album_slug) {
	
		$album_attachments = \Gallery_attachment\Models\Gallery_attachment::all(array('select'=> 'attachment_id', 'conditions' => 'gallery_id = ' . $album_id));
		$first = true;
		$count = 0;
		
		foreach ($album_attachments as $aa) {
			$attach = \Attachment\Models\Attachment::first(array('select'=> 'image_filename, title, caption', 'conditions' => 'id = ' . $aa->attachment_id));
			$images[] = array(
				'name' => GALLERY_IMAGES . $album_slug . '/' . $attach->image_filename,
				'caption' => $attach->caption,
				'image_title' => $attach->title,
				'count' => $count,
				'first' => $first
			);
			
			$count++;
			$first = false;
		}
		
		return $images;
	}
}