<?php
namespace Account\Controllers;

class Image extends \Account\Controllers\Account {

    public function __construct($uri, $data) {
    	parent::__construct($uri, $data);

    	$this->add_asset('js', 'jcrop.js', true);

    	if(isset($_POST["filename"])){
    		$this->submit_form();
    	}

    }

    public function controller(){
    	$this->set_view('Account\Views\Image');
    }


    protected function submit_form(){

    	$this->_user->image = $_POST['filename'];
    	$this->_user->save();

    	redirect_to('/account');

    }



}