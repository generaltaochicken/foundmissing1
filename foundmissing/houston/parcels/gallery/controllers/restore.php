<?php
namespace Gallery\Controllers;

class Restore extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$gallery = \Gallery\Models\Gallery::find($data["gallery_id"]);

		if($gallery){
			$gallery->deleted = 0;
			$gallery->save();
			$this->add_flash(array("message" => "Gallery has been restored!"));

			redirect_to($gallery->link_all());
		}
	}
}
