<?php
namespace Content\Controllers;

class All extends \Manage\Controllers\Manage{

	private $contents;

	public $contents_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Contents";
		$this->link_new = \Content\Models\Content::link_new();

		$this->contents = \Content\Models\Content::all(array("conditions" => "deleted = 0"));
		$archived = \Content\Models\Content::count(array("conditions" => "deleted = 1"));
		if($archived > 0){
			$this->archive_button = array("url" => \Content\Models\Content::link_archive(), "title" => "Archive");
		}

		foreach($this->contents as $content){
			$this->has_data = true;
			$this->contents_data[] = array(
				"id" => $content->id,
				"k" => $content->k,
				"v" => $content->v,
				"link_edit" => $content->link_edit(),
				"link_delete" => $content->link_delete(),
			);
		}

	}

	public function controller(){
		$this->set_view("Content\Views\All");
	}

}

