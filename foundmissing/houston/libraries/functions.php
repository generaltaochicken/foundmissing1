<?php

function redirect_to($location){
    header("Location: " . strtolower($location));
    die();
}

function easy_file($file_array, $trailer_slash = false){
	$str = '';
	foreach($file_array as $chunk) {
		$chunk = rtrim($chunk, DS);
		$str .= $chunk . DS;
	}

	if(!$trailer_slash){
		$str = rtrim($str, DS);
	}

	return $str;


}

function get_file_extension($file_name) {
  return substr(strrchr($file_name,'.'),1);
}


function safe_filename($filename) {

    $filename = strtolower($filename);
    $filename = str_replace("#","_",$filename);
    $filename = str_replace(" ","_",$filename);
    $filename = str_replace("'","",$filename);
    $filename = str_replace('"',"",$filename);
    $filename = str_replace("__","_",$filename);
    $filename = str_replace("&","and",$filename);
    $filename = str_replace("/","_",$filename);
    $filename = str_replace("\\","_",$filename);
    $filename = str_replace("?","",$filename);

   return $filename;
}

function get_extension($file){
	return end(explode(".", $file));
}


function word_trim($string, $count, $ellipsis = true){
  $words = explode(' ', $string);
  if (count($words) > $count){
    array_splice($words, $count);
    $string = implode(' ', $words);
    if (is_string($ellipsis)){
      $string .= $ellipsis;
    }
    elseif ($ellipsis){
      $string .= '&hellip;';
    }
  }
  return $string;
}

function char_trim($string, $count, $ellipsis = true){

	$size = strlen($string);
	if ($size > $count){

	$string = substr($string, 0, $count);

    if (is_string($ellipsis)){
      $string .= $ellipsis;
    }
    elseif ($ellipsis){
      $string .= '&hellip;';
    }


  }
  return $string;
}

function remove_special_characters($text){

	$entities = array(128 => 'euro', 130 => 'sbquo', 131 => 'fnof',
					  132 => 'bdquo', 133 => 'hellip', 134 => 'dagger',
					  135 => 'Dagger', 136 => 'circ', 137 => 'permil',
					  138 => 'Scaron', 139 => 'lsaquo', 140 => 'OElig',
					  145 => 'lsquo', 146 => 'rsquo', 147 => 'ldquo',
					  148 => 'rdquo', 149 => 'bull', 150 => '#45',
					  151 => 'mdash', 152 => 'tilde', 153 => 'trade',
					  154 => 'scaron', 155 => 'rsaquo', 156 => 'oelig',
					  159 => 'Yuml', 160 => 'nbsp', 240 => 'nbsp',
					  194 => 'nbsp');

	$new_text = '';

	for($i = 0; $i < strlen($text); $i++) {
		$num = ord($text{$i});
		if (array_key_exists($num, $entities)) {
			switch ($num) {
				case 150:
				$new_text .= '-';
				break;
			default:
				$new_text .= '&'.$entities[$num].';';
			}
		} else if($num < 127 || $num > 159) {
			$new_text .= $text{$i};
		}
	}

	//Your nasty string of words and non ascii chars
	$Contentz = $new_text;

	//Specific string replaces for ellipsis, etc that you dont want removed but replaced
	$theBad = 	array("�","�","�","�","�","�","�");
	$theGood = array("\"","\"","'","'","...","-","-");
	$Contentz = str_replace($theBad,$theGood,$Contentz);

	//Whatever might be left over...
	//Remove all non ascii chars (aka: bad Microsoft Word and Word Perfect Shit shit)
	$Contentz = preg_replace('/[^(\x20-\x7F)\x0A]*/','', $Contentz);

	$Contentz = str_replace('�', '-', $Contentz);

	$new_text = $Contentz;

	return $new_text;

}


function is_email($email) {
    $isValid = true;
    $atIndex = strrpos($email, "@");
    if (is_bool($atIndex) && !$atIndex) {
        $isValid = false;
    } else {
        $domain = substr($email, $atIndex+1);
        $local = substr($email, 0, $atIndex);
        $localLen = strlen($local);
        $domainLen = strlen($domain);
        if ($localLen < 1 || $localLen > 64) {
            // local part length exceeded
            $isValid = false;
        } else if ($domainLen < 1 || $domainLen > 255) {
            // domain part length exceeded
            $isValid = false;
        } else if ($local[0] == '.' || $local[$localLen-1] == '.') {
            // local part starts or ends with '.'
            $isValid = false;
        } else if (preg_match('/\\.\\./', $local)) {
            // local part has two consecutive dots
            $isValid = false;
        } else if (!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain)) {
            // character not valid in domain part
            $isValid = false;
        } else if (preg_match('/\\.\\./', $domain)) {
            // domain part has two consecutive dots
            $isValid = false;
        } else if (!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/', str_replace("\\\\","",$local))) {
            // character not valid in local part unless
            // local part is quoted
            if (!preg_match('/^"(\\\\"|[^"])+"$/', str_replace("\\\\","",$local))) {
                $isValid = false;
            }
        }
        if ($isValid && !(checkdnsrr($domain,"MX") || checkdnsrr($domain,"A"))) {
            // domain not found in DNS
            $isValid = false;
        }
    }
    return $isValid;
}


function timestamp_now(){

	return date("Y-m-d H:i:s");

}

function timestamp($unix){

	return date("Y-m-d H:i:s", $unix);

}


function time_to_friendly_date($time, $format = 'F j, Y'){
	if($time == 0) { return '-'; }

	return date($format, $time);
}

function time_to_friendly_time($time, $format = 'g:i a'){
	if($time == 0) { return '-'; }

	return date($format, $time);
}

function plural($num) {
	if ($num != 1)
		return "s";
}

function get_relative_time($time) {
	$diff = time() - $time;
	if ($diff<60)
		return $diff . " second" . plural($diff) . " ago";
	$diff = round($diff/60);
	if ($diff<60)
		return $diff . " minute" . plural($diff) . " ago";
	$diff = round($diff/60);
	if ($diff<24)
		return $diff . " hour" . plural($diff) . " ago";
	$diff = round($diff/24);
	if ($diff<7)
		return $diff . " day" . plural($diff) . " ago";
	$diff = round($diff/7);
	if ($diff<4)
		return $diff . " week" . plural($diff) . " ago";
	return "on " . date("F j, Y", strtotime($date));
}

function format_file_size($bytes) {
	 if(!empty($bytes)) {
		 $s = array('bytes', 'kb', 'MB', 'GB', 'TB', 'PB');
		 $e = floor(log($bytes)/log(1024));
		 $output = sprintf('%.'.($e == 0 ? 0 : 0).'f '.$s[$e], ($bytes/pow(1024, floor($e))));
		 return $output;
	 }
}


function title_to_slug($title){

    $slug = str_replace(' ', '-', strtolower($title));
    $slug = str_replace('&amp;', 'and', $slug);

    return $slug;

}

function slug_to_title($slug){

	$title = str_replace('-', ' ', $slug);
	$title = str_replace('�', ' ', $title);
	$title = str_replace('and', '&amp;', $title);
	$title = ucwords($title);

	return $title;
}


function getExtension($str) {
         $i = strrpos($str,".");
         if (!$i) { return ""; }
         $l = strlen($str) - $i;
         $ext = substr($str,$i+1,$l);
         return $ext;
}





function debug($obj, $die = false){
	echo "<HR><pre>";
	print_r($obj);
	echo "</pre><HR>";

	if($die) {
		echo "\n---PROCESS ENDED---";
		echo "<HR><pre>";
		echo "\n---BACKTRACE---\n\n";

		print_r(debug_backtrace());

		echo "</pre><HR>";
		die();
	}
}


function random_string($length, $charset='abcdefghijklmnopqrstuvwxyz0123456789') {
    $str = '';
    $count = strlen($charset);
    while ($length--) {
        $str .= $charset[mt_rand(0, $count-1)];
    }
    return $str;
}


function clean_tags($str){
	$str = str_replace(',', ' ', $str);
	$str = str_replace('  ', ' ', $str);

	return $str;

}


function get_video_id_from_url($subject){

	$url = parse_url($subject);
	parse_str($url['query'], $query);

	return $query['v'];

}

function nl2li($text){

	$list = explode("\n", $text);

	$str = '';
	foreach($list as $line) {
		$str .= "<li>" . $line . "</li>";
	}

	return $str;

}

function nl2p($text){

	$items = explode("\n\n", $text);

	$str = '';
	foreach($items as $item) {
		$str .= "<p>" . $item . "</p>";
	}

	return $str;

}

function format_currency($num, $format = '$%.2n'){
	if(!is_numeric($num)){
		return $num;
	}

	return money_format($format, $num);
}

function significan_zeroes($value){

	$value = preg_replace('/(\.[0-9]+?)0*$/', '$1', $value);

	return $value;

}


function validate_phone($phone_number){

    $phone_number = (string)$phone_number;
    $phone_number = str_replace(' ', '', $phone_number);
    $phone_number = str_replace('-', '', $phone_number);
    $phone_number = str_replace('.', '', $phone_number);
    $phone_number = str_replace('(', '', $phone_number);
    $phone_number = str_replace(')', '', $phone_number);
    $phone_number = str_replace('+', '', $phone_number);
    $phone_number = str_replace('-', '', $phone_number);

    if($phone_number[0] != 1){
        $phone_number = "1" . $phone_number;
    }

    if(strlen($phone_number) != 11){
        return false;
    } else {
        return $phone_number;
    }

}


function youtube_url_to_id($url){
	if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match)) {
    	return $match[1];
	}
}

function vimeo_url_to_id($url){
    $result = preg_match('/(\d+)/', $url, $matches);
    if ($result) {
        return $matches[0];
    }

    return false;
}




function indent_json($json) {

    $result      = '';
    $pos         = 0;
    $strLen      = strlen($json);
    $indentStr   = '    ';
    $newLine     = "\n";
    $prevChar    = '';
    $outOfQuotes = true;

    for ($i=0; $i<=$strLen; $i++) {

        // Grab the next character in the string.
        $char = substr($json, $i, 1);

        // Are we inside a quoted string?
        if ($char == '"' && $prevChar != '\\') {
            $outOfQuotes = !$outOfQuotes;

        // If this character is the end of an element,
        // output a new line and indent the next line.
        } else if(($char == '}' || $char == ']') && $outOfQuotes) {
            $result .= $newLine;
            $pos --;
            for ($j=0; $j<$pos; $j++) {
                $result .= $indentStr;
            }
        }

        // Add the character to the result string.
        $result .= $char;

        // If the last character was the beginning of an element,
        // output a new line and indent the next line.
        if (($char == ',' || $char == '{' || $char == '[') && $outOfQuotes) {
            $result .= $newLine;
            if ($char == '{' || $char == '[') {
                $pos ++;
            }

            for ($j = 0; $j < $pos; $j++) {
                $result .= $indentStr;
            }
        }

        $prevChar = $char;
    }

    return $result;
}


function video_type($url) {
    if (strpos($url, 'youtube') > 0) {
        return 'youtube';
    } elseif (strpos($url, 'vimeo') > 0) {
        return 'vimeo';
    } else {
        return 'unknown';
    }
}



