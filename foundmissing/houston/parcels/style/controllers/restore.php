<?php
namespace Style\Controllers;

class Restore extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$style = \Style\Models\Style::find($data["style_id"]);

		if($style){
			$style->deleted = 0;
			$style->save();
			$this->add_flash(array("message" => "Style has been restored!"));

			redirect_to($style->link_all());
		}
	}
}
