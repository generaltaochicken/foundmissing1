<?php
namespace Page\Controllers;

class Delete extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$page = \Page\Models\Page::find($data["page_id"]);

		if($page){
			$page->deleted = 1;
			$page->save();
			$this->add_undo(array("message" => "Page has been deleted!", "action" => $page->link_restore()));

			redirect_to($page->link_all());
		}
	}
}
