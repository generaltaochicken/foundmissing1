<?php
namespace User\Controllers;

class Delete extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$user = \User\Models\User::find($data["user_id"]);

		if($user){
			$user->deleted = 1;
			$user->save();
			$this->add_undo(array("message" => "User has been deleted!", "action" => $user->link_restore()));

			redirect_to($user->link_all());
		}
	}
}
