<?php
namespace Attachment\Controllers;

class Delete extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$attachment = \Attachment\Models\Attachment::find($data["attachment_id"]);

		if($attachment){
			$attachment->deleted = 1;
			$attachment->save();
			$this->add_undo(array("message" => "Attachment has been deleted!", "action" => $attachment->link_restore()));

			redirect_to($attachment->link_all());
		}
	}
}
