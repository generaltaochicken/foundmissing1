<?php
namespace Winner\Controllers;

class Email extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);
		
		$this->winners = \Winner\Models\Winner::all(array("conditions" => "deleted = 0 AND sent = 0" , "order" => "ordering"));
		$this->page_title = "Send Emails";
		
		foreach($this->winners as $winner){
			$name = \User\Models\User::get_user_fullname_by_id($winner->user_id);
			$email = \User\Models\User::get_user_email_by_id($winner->user_id);
			$body = '<img src="http://foundmissing.ca/images/fm-primaryred-64.png" /><p>' . ucwords(\User\Models\User::get_user_fullname_by_id($winner->user_id)) . 
					', Congratulations, you are a winner of FoundMissing�s first raffle giveaway!</p>
					<p><img src="http://foundmissing.ca/images/sponsors.png" /></p>
					<p><img src="http://foundmissing.ca/images/fm_flyer.jpg" /></p>
					<p>The FoundMissing team would like to thank you again for attending our first event 
					launch.We wouldn�t be here without your continuous support.<br />We are excited to have 
					you on board through this great journey and we look forward to growing with you and 
					your favorite brands in a completely new way.</p>
					<p>You have won <b>' . $winner->prize . '</b></p>
					<p>To claim your prize, please reply to this e-mail within 24 hours with your special 
					raffle code, and your preferred contact method. We will be in touch with you shortly on 
					how to pick up your prize!</p>
					<p>Congratulations again on your win!</p>
					<p>Sincerely,</p>
					<p>FoundMissing Team</p>
					<p><img src="http://foundmissing.ca/images/fm-sectrans-128.png" /></p>';
			$this->SendEmails($name, $body, $email);
			
			$win = \Winner\Models\Winner::first($winner->user_id);
			$win->sent = '1';
			$win->save();
			sleep(20);
		}
			
	}
	
	public function controller() {
		$this->set_view("Winner\Views\Email");
	}
	
	private function SendEmails($name, $body, $email) {
		require_once(PARCEL_ROOT . 'libraries/PHPMailer/PHPMailerAutoload.php');
		$mail = new \PHPMailer();
		$mail->isSMTP();                                      												// Set mailer to use SMTP
		$mail->Host = 'smtp.gmail.com';  															// Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               										// Enable SMTP authentication
		$mail->Username = 'info@foundmissing.ca';                 						// SMTP username
		$mail->Password = 'sneakers10';                    										// SMTP password
		$mail->SMTPSecure = 'tls';                            										// Enable encryption, 'ssl' also accepted

		$mail->From = 'info@foundmissing.ca';
		$mail->FromName = 'FoundMissing';
		$mail->addAddress($email, $name);  		// Add a recipient

		$mail->WordWrap = 50;                                 										// Set word wrap to 50 characters

		$mail->isHTML(true);                                  											// Set email format to HTML

		$mail->Subject 		= 'Raffle Winner';
		$mail->Body    		= $body;
		
		if(!$mail->send()) {
			//echo 'Message could not be sent.';
			//echo 'Mailer Error: ' . $mail->ErrorInfo;
		} else {
			//echo 'Message has been sent';
		}
	}
	
}