<?php
namespace Founder\Controllers;

class Reorder extends \Core\Controllers\Ajax_Controller{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this_item = \Founder\Models\Founder::find($data["this_id"]);

		$swap_item = \Founder\Models\Founder::find($data["swap_id"]);

		if($this_item && $swap_item){
			list($this_item->ordering, $swap_item->ordering) = array($swap_item->ordering, $this_item->ordering);
			$this_item->save();

			$swap_item->save();

		}
	}
}
