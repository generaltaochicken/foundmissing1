<?php
namespace Subscriber\Controllers;

class Restore extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$subscriber = \Subscriber\Models\Subscriber::find($data["subscriber_id"]);

		if($subscriber){
			$subscriber->deleted = 0;
			$subscriber->save();
			$this->add_flash(array("message" => "Subscriber has been restored!"));

			redirect_to($subscriber->link_all());
		}
	}
}
