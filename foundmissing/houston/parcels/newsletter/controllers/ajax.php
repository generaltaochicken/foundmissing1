<?php
namespace Newsletter\Controllers;

class Ajax extends \Manage\Controllers\Manage{
	public function __construct($uri, $data){
		require_once(PARCEL_ROOT . 'libraries/PHPMailer/PHPMailerAutoload.php');
		
		parent::__construct($uri, $data);
		
		$output = 0;
		$title = '';
		
		if (isset($_POST['title'])) {
			$title = $_POST['title'];
		}
		
		if (isset($_POST['body'])) {
			$this->mailing_list = \Subscriber\Models\Subscriber::all(array('conditions' => 'deleted = 0', 'select' => 'email'));
			foreach($this->mailing_list as $user){
				$name = '';
				$email = $user->email;
				$body = $_POST['body'];
				$this->SendEmails($name, $body, $email, $title);
				
				$output += 1;
			}
		}
		
		echo $output . ' emails sent';
	}
	
	private function SendEmails($name, $body, $email, $title) {
		$mail = new \PHPMailer();
		$mail->isSMTP();                                      												// Set mailer to use SMTP
		$mail->Host = 'smtp.gmail.com';  															// Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               										// Enable SMTP authentication
		$mail->Username = 'info@foundmissing.ca';                 						// SMTP username
		$mail->Password = 'sneakers10';                    										// SMTP password
		$mail->SMTPSecure = 'tls';                            										// Enable encryption, 'ssl' also accepted

		$mail->From = 'info@foundmissing.ca';
		$mail->FromName = 'FoundMissing';
		$mail->addAddress($email, $name);  		// Add a recipient

		$mail->WordWrap = 50;                                 										// Set word wrap to 50 characters

		$mail->isHTML(true);                                  											// Set email format to HTML

		$mail->Subject 		= $title;
		$mail->Body    		= $body;
		$mail->send();
	}
}

?>