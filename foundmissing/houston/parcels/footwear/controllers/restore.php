<?php
namespace Footwear\Controllers;

class Restore extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$footwear = \Footwear\Models\Footwear::find($data["footwear_id"]);

		if($footwear){
			$footwear->deleted = 0;
			$footwear->save();
			$this->add_flash(array("message" => "Footwear has been restored!"));

			redirect_to($footwear->link_all());
		}
	}
}
