<?php
namespace Account\Controllers;

class ViewAjax extends \Frontend\Controllers\Frontend {
	 public function __construct($uri, $data){
        parent::__construct($uri, $data);

        $this->data = $data;
		$this->user_id = $this->_user->id;

		if (isset($_POST['type'])) {
			if ($_POST['type'] == 'edit') {
				$this->updated_websites 		= array($_POST['website'], $_POST['facebook'], $_POST['twitter']);
				//$this->updated_bio 				= $purifier->purify($_POST['bio']);
				$this->updated_bio 				= $_POST['bio'];
				$this->updated_name 			= $_POST['name'];
				$this->updated_age 				= $_POST['age'];
				$this->updated_city 				= $_POST['city'];
				$this->updated_country 			= $_POST['country'];
				$this->updated_title 				= $_POST['title'];
				
				$this->update_profile();
				
				echo $this->updated_bio;
			} else if ($_POST['type'] == 'password') {
				$this->old_password = filter_var($_POST['old'], FILTER_SANITIZE_STRING);
				$this->new_password = filter_var($_POST['new_pass'], FILTER_SANITIZE_STRING);
				
				$this->change_password();
			}
		}
    }

    public function controller(){
	
    }
	
	protected function change_password() {
		$password = md5(DB_SALT . $this->old_password);
		
		$options = array('conditions' => array("id = '" . $this->user_id . "' AND password = '" . $password . "'"));
		$user = \User\Models\User::find($options);
		echo $user->email;
		
		if (sizeof($user) < 1) {
			return false;
		} else {
			$user->password = md5(DB_SALT . $this->new_password);
			$user->save();
		}
	}
	
	protected function update_profile() {
		// CLEAN DATA
		require_once(PARCEL_ROOT.'libraries/HTMLPurifier/HTMLPurifier.auto.php');
		$config 				= \HTMLPurifier_Config::createDefault();
		$config->set('HTML.Allowed', 'p, h1, h2, h3, a[href], b, i');
		$purifier 				= new \HTMLPurifier($config);
		
		$user = \User\Models\User::find($this->user_id);
		$last_name = '';
		
		$name_array = explode(" ", $this->updated_name);
		$first_name = $name_array[0];
		
		for ($i = 1; $i < count($name_array); $i++) {
			$last_name .= $name_array[$i] . ' ';
		}
		
		$first_name = trim($first_name);
		$last_name = trim($last_name);

		
		for ($i = 0; $i < count($this->updated_websites); $i++) {
			if (strpos($this->updated_websites[$i], 'http') !== 0 && !empty($this->updated_websites[$i])) {
				$this->updated_websites[$i] = 'http://' . $this->updated_websites[$i];
			}
		}
		
		$user->website = $purifier->purify($this->updated_websites[0]);
		$user->facebook = $purifier->purify($this->updated_websites[1]);
		$user->twitter = $purifier->purify($this->updated_websites[2]);
		$user->bio = $purifier->purify($this->updated_bio);
		$user->firstname = $purifier->purify($first_name);
		$user->lastname = $purifier->purify($last_name);
		$user->age = $purifier->purify($this->updated_age);
		$user->city = $purifier->purify($this->updated_city);
		$user->country = $purifier->purify($this->updated_country);
		$user->title = $purifier->purify($this->updated_title);
		
		$user->save();
		
		echo array(
			'website' => $user->website,
			'facebook' => $user->facebook,
			'twitter' => $user->twitter,
			'bio' => $this->updated_bio
		);
	}
}

?>