<?php
namespace Attachment\Controllers;

class Restore extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$attachment = \Attachment\Models\Attachment::find($data["attachment_id"]);

		if($attachment){
			$attachment->deleted = 0;
			$attachment->save();
			$this->add_flash(array("message" => "Attachment has been restored!"));

			redirect_to($attachment->link_all());
		}
	}
}
