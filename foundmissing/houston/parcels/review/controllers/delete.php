<?php
namespace Review\Controllers;

class Delete extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$review = \Review\Models\Review::find($data["review_id"]);

		if($review){
			$review->deleted = 1;
			$review->save();
			$this->add_undo(array("message" => "Review has been deleted!", "action" => $review->link_restore()));

			redirect_to($review->link_all());
		}
	}
}
