<?php
namespace Review\Controllers;

class Archive extends \Manage\Controllers\Manage{

	private $reviews;

	public $reviews_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Deleted Reviews";

		$this->reviews = \Review\Models\Review::all(array("conditions" => "deleted = 1"));
		$this->archive_button = array("url" => \Review\Models\Review::link_all(), "title" => "Back");
		foreach($this->reviews as $review){
			$this->has_data = true;
			$this->reviews_data[] = array(
				"id" => $review->id,
				"user_id" => $review->user_id,
				"title" => $review->title,
				"slug" => $review->slug,
				"excerpt" => $review->excerpt,
				"body" => $review->body,
				"shoe_id" => $review->shoe_id,
				"entry_datetime" => $review->entry_datetime,
				"link_restore" => $review->link_restore(),
			);
		}

	}

	public function controller(){
		$this->set_view("Review\Views\All");
	}

}

