<?php
namespace Frontend\Controllers;

class Review extends \Frontend\Controllers\Frontend {
    
	private $data;
	
	public $review = array();
	public $post_type = 3;
	public $current_user;
	protected $review_images = array();
	
    public function __construct($uri, $data) {
        parent::__construct($uri, $data);
		
		$this->data = $data;
		//$this->add_asset('css', 'review.css', true);
		$this->add_asset('css', 'article.css', true);
		$this->add_asset('js', 'comments.js', true);
		$this->current_user = $this->_user->id;
		
		if (isset($this->data['slug'])) {
			$this->review_info = \Review\Models\Review::first(array('conditions' => 'deleted = 0 AND slug = "' . $this->data['slug'] . '"'));
			$this->title = $this->review_info->title . ' | FM';
			
			$this->get_shoe_info();
			$this->get_review_images();
			$this->get_styles();
			$this->get_colors();
			
			if ($this->review_info->id == null) {
				redirect_to('/404');
			}
		}
		
		if (isset($_POST['submit_comment'])) {
			$this->submit_comment();
		}
		
    }

    public function controller(){
		$this->review[] = array(
			'id' => $this->review_info->id,
			//'author_id' => $this->review_info->user_id,
			'author_name' =>  $this->review_info->author,
			'date' => time_to_friendly_date($this->review_info->publish_date, 'Y.m.d'),
			'title' => $this->review_info->title,
			'body' => $this->review_info->body,
			'slug' => $this->review_info->slug,
			'excerpt' => $this->review_info->excerpt,
			'main_image' => REVIEW_IMAGES . $this->review_info->cover_image
		);
		
		// Get the shares data
		$query = $this->domain_url . 'reviews/' . $this->review['slug'];
		
		$this->fb_likes = $this->get_facebook_likes($query);
		$this->fb_shares = $this->get_facebook_shares($query);
		$this->facebook_data = array(
			'likes' => $this->fb_likes[0]['like_count'],
			'shares' => $this->fb_shares['shares']
		);
		$this->su_views = $this->get_stumbleupon_views($query);
		$this->gplus = $this->get_google_plus($query);
		$this->tweets = $this->get_tweets($query);
		
		
		// Comments
		$this->comments = \Comment\Models\Comment::getCommentsByID($this->review_info->id, $this->post_type);
		
		if ($this->comments != null) {
			$this->has_comments = true;
		}
		$count = \Comment\Models\Comment::getCommentCount($this->review_info->id, $this->post_type);
			
		if ($count > 1) {
			$this->comments_count = $count . ' comments';
		} else {
			$this->comments_count = $count . ' comment';
		}
		
		$this->set_view('Frontend\Views\Review');
	}
	
	public function submit_comment() {
		$comment = new \Comment\Models\Comment;
		$user = \User\Models\User::find($this->_user->id);
		
		$comment->parent_id = $this->review_info->id;
		$comment->user_id = $this->_user->id;
		$comment->comment_body = $_POST['comment'];
		$comment->entry_datetime = time();
		$comment->post_type = $this->post_type;
		
		$user->karma += 1;
		$comment->save();
		$user->save();
		
		$this->update_activity_feed($comment->id);
		
		redirect_to('/reviews/' . $this->data['slug']);
	}
	
	private function update_activity_feed($comment_id) {
		$activity = new \Activity\Models\Activity;
		$activity->user_id = $this->_user->id;
		$activity->activity_type = 'review_post';
		$activity->source_id = $this->review_info->id;
		$activity->child_id = $comment_id;
		$activity->entry_datetime = time();
		
		$activity->save();
	}
	
	protected function get_shoe_info() {
		$this->shoe = \Footwear\Models\Footwear::find($this->review_info->shoe_id);
	}
	
	protected function get_review_images() {
		$footwear_attachments = \Footwear_Attachment\Models\Footwear_Attachment::all(array('conditions' => 'deleted = 0 AND footwear_id = "' . $this->review_info->shoe_id . '"'));

		foreach ($footwear_attachments as $attach) {
			$attachments = \Attachment\Models\Attachment::find($attach->attachment_id);

			$this->review_images[] = array(
				'id' => $attachments->id,
				'image' => $attachments->image_filename,
				'caption' => $attachments->caption
			);
		}
	}
	
	// Get the categories
	protected function get_styles() {
		$this->styles = \Style\Models\Style::all(array('conditions' => 'deleted = 0'));
	}
	
	protected function get_colors() {
		$this->colors = \Color\Models\Color::all(array('conditions' => 'deleted = 0'));
	}
}