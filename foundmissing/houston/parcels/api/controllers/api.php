<?php
namespace Api\Controllers;

class Api extends \Mobile\Controllers\Mobile {
	
	public $data;
	public $uri;
	
	protected $response_format;
	protected $response_code;
	protected $response;

	protected $action;
	protected $method;

	protected $allow_get;
	protected $allow_post;
	protected $allow_put;
	protected $allow_delete;

	protected $put_params;
	protected $delete_params;
	
	protected $articles;
	protected $article_image;
	
	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->data = $data;
		$this->uri = $uri;

		$this->allow_post = true;
		$this->allow_get = true;
		$this->allow_put = true;
		$this->allow_delete = true;
		
		$this->method = strtolower($_SERVER['REQUEST_METHOD']);
		
		if($this->method == 'get'){
			if(!$this->allow_get){
				$this->not_authorized();
			}
			
			if (isset($_GET['articles'])) {
				$type = $_GET['articles'];
				
				switch($type) {
					case "articles":
						echo $this->getArticles();
						//var_dump($this->getArticles());
					/*default:
						$this->not_authorized();*/
				}
			}
			
			
			if (isset($_GET['article_image'])) {
				$this->article_image = $_GET['article_image'];
				$this->getArticleCover();
			}
		}
		
		if($this->method == 'post'){
		
			if (isset($_POST['type'])) {
				$type = $_POST['type'];
				
				switch($type) {
					case "articles":
						echo $this->getArticles();
						//var_dump($this->getArticles());
					/*default:
						$this->not_authorized();*/
				}
			}
		}
	}
	
	public function controller() {
	
	}
	
	private function getArticles() {
		//$articles = \Article\Models\Article::all(array('conditions' => 'deleted = 0 AND publish_date > 1', 'order' => 'publish_date DESC', 'limit' => $this->limits));
		$this->articles = \Article\Models\Article::all(array('conditions' => 'deleted = 0 AND publish_date > 1', 'order' => 'publish_date DESC', 'limit' => 1));
		
		foreach($this->articles as $article){
			/*$articles_data['article'] = array(
				"id" => $article->id,
				"entry_datetime" => time_to_friendly_date($article->entry_datetime),
				"author_name" => $article->author_name,
				"title" => $article->title,
				"slug" => $article->slug,
				"excerpt" => substr($article->excerpt, 0, 100) . '...',
				"cover" => "http://foundmissing/images/articles/" + $article->bg_image_header
			);*/
			
			$articles_data['article'] = $article->title;
		}
		
		return json_encode($articles_data);
		//return $articles_data;
	}
	
	
	private function getArticleCover() {
		echo $this->article_image;
	}

	
			/*
		if($this->method == 'get'){
			if(!$this->allow_get){
				$this->not_authorized();
			}
		}

		if($this->method == 'post'){
			if(!$this->allow_post){
				$this->not_authorized();
			}
		}

		if($this->method == 'put'){
			if(!$this->allow_put){
				$this->not_authorized();
			}
			$this->put_params = $this->get_request_values();
		}

		if($this->method == 'delete'){
			if(!$this->allow_delete){
				$this->not_authorized();
			}
			$this->delete_params = $this->get_request_values();
		}
		
		$this->response_format = 'json';
		
	*/
	
	
	
	/*
	protected function request($method, $key){
		switch(strtolower($method)){
			case 'get':
				if(!$this->allow_get){
					$this->return_code = 401;
					return false;
				}
				if(!isset($_GET[$key])){
					return false;
				}
				return $_GET[$key];
				break;
			case 'post':
				if(!$this->allow_post){
					$this->return_code = 401;
					return false;
				}
				if(!isset($_POST[$key])){
					return false;
				}
				return $_POST[$key];
				break;
			case 'put':
				if(!$this->allow_put){
					$this->return_code = 401;
					return false;
				}
				if(!isset($this->put_params[$key])){
					return false;
				}
				return $this->put_params[$key];
				break;
			case "delete":
				if(!$this->allow_delete){
					$this->return_code = 401;
					return false;
				}
				if(!isset($this->delete_params[$key])){
					return false;
				}
				return $this->delete_params[$key];
				break;


		}
	}
	*/
	protected function send_response() {

		if(!$this->response){
			$this->response_code = 500;
			$this->response = array();
		}

		switch($this->response_format){
			case 'html':
				header('Content-type: text/html');
                header("HTTP/1.0 " . $this->response_code);
				die($this->response);
				break;
			case 'json':
			default:
				header('Content-type: application/json');
                header("HTTP/1.0 " . $this->response_code);
        		die(json_encode($this->response));
        		break;
		}

	}


	protected function not_authorized(){
		$this->response_code = 401;
		$this->response = array('Invalid request. Permission denied');

		$this->send_response();
	}
	/*
	private function get_request_values(){
		
		$putfp = fopen('php://input', 'r');
		$putdata = '';
		while($data = fread($putfp, 1024)){
			$putdata .= $data;
		}
		fclose($putfp);
		parse_str($putdata, $kv);

		return $kv;
	}
	*/
}