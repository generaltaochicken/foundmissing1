<?php
namespace Category\Controllers;

class Delete extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$Category = \Category\Models\Category::find($data["Category_id"]);

		if($Category){
			$Category->deleted = 1;
			$Category->save();
			$this->add_undo(array("message" => "Category has been deleted!", "action" => $Category->link_restore()));

			redirect_to($Category->link_all());
		}
	}
}
