<?php
namespace Interview\Controllers;

class Edit extends \Manage\Controllers\Manage{

	private $interview;

	public $page_title;
	public $link_back;
	public $form;
	public $existing;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		if(isset($_POST["submit"])){
			$this->submit_form();
		}

		if(isset($data["interview_id"])){
			$this->interview = \Interview\Models\Interview::find($data["interview_id"]);
			$this->page_title = "Editing Interview";
		} else {
			$this->interview = new \Interview\Models\Interview;
			$this->page_title = "New Interview";
		}

		$this->link_back = $this->interview->link_all();

		$form = new \Form\Models\Form("edit", "POST", "", "well");

		$item = new \Form\Models\FormItemHidden(array("id" => "id", "title" => "Id", "value" => $this->interview->id));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "author_name", "title" => "Author Name", "value" => $this->interview->author_name));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "title", "title" => "Title", "class" => "fillslug", "value" => $this->interview->title));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "slug", "title" => "Slug", "value" => $this->interview->slug));
		$form->add_item($item);

		$item = new \Form\Models\FormItemTextarea(array("id" => "excerpt", "title" => "Excerpt", "value" => $this->interview->excerpt));
		$form->add_item($item);

		$item = new \Form\Models\FormItemTextarea(array("id" => "body", "title" => "Body", "value" => $this->interview->body));
		$form->add_item($item);

		$item = new \Form\Models\FormItemFile(array("id" => "main_image", "title" => "File", "value" => $this->interview->main_image));
		$form->add_item($item);
		
		$item = new \Form\Models\FormItemSelect(array("id" => "publish_date", "title" => "Publish", "options" => $this->get_publish_options()));
		$form->add_item($item);

		$item = new \Form\Models\FormItemSubmit(array("value" => "Save", "id" => "submit", "class" => "btn btn-primary", "cancel_url" => $this->interview->link_all()));
		$form->add_item($item);

		$this->form = $form->render();
		
		if($this->review->cover_image){
			$this->preview = ARTICLE_IMAGES . $this->interview->main_image;
		}

	}

	public function controller(){
		$this->set_view("Interview\Views\Edit");
	}

	private function submit_form(){
		if($_POST["id"] > 0){
			$interview = \Interview\Models\Interview::find($_POST["id"]);
			$flash_msg = "Interview has been updated successfully.";
		} else {
			$interview = new \Interview\Models\Interview;
			$flash_msg = "Interview has been created successfully.";
			$interview->ordering = \Interview\Models\Interview::get_next_ordering_number();
		}

		$interview->author_name = $_POST["author_name"];
		$interview->title = $_POST["title"];
		$interview->slug = $_POST["slug"];
		$interview->excerpt = $_POST["excerpt"];
		$interview->body = $_POST["body"];
		
		if(isset($_FILES['main_image']['tmp_name']) && $_FILES['main_image']['tmp_name'] != ''){

			$image = $this->upload($_FILES['main_image'], array('jpg', 'png', 'gif', 'jpeg'), true);

			if($image) {
			$interview->main_image = $image['filename'];
			}

		}
		
		
		$interview->publish_date = $_POST["publish_date"];
		if ($interview->publish_date == 1) {
			if ($this->interview->publish_date <= 1) {
				$interview->publish_date = time();
			}
		}
		
		$interview->save();
		$this->add_flash(array("message" => $flash_msg, "type" => "success", "Heading" => "Great!"));
		redirect_to($interview->link_all());
	}
	
	private function get_publish_options() {
		$published = array(
			array('value' => '1', 'title' => 'Yes', 'selected' => 0),
			array('value' => '0', 'title' => 'No', 'selected' => 1)
		);
		
		if  ($this->interview->publish_date != 0 || $this->interview->publish_date != null) {
			$published[0]['selected'] = 1;
			$published[1]['selected'] = 0;
		}
		
		return $published;
	}
	

}
