<?php
namespace Page\Controllers;

class Restore extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$page = \Page\Models\Page::find($data["page_id"]);

		if($page){
			$page->deleted = 0;
			$page->save();
			$this->add_flash(array("message" => "Page has been restored!"));

			redirect_to($page->link_all());
		}
	}
}
