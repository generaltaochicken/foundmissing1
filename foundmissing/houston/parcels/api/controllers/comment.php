<?php
namespace Api\Controllers;

class Comment extends \Api\Controllers\Api{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->allow_delete = false;

		switch($this->method){
			case "get":
				$this->response = $this->get_comment();
			break;
			case "post":
				$this->response = $this->create_comment();
				break;	
			case "put":
				$this->response = $this->update_comment();
			break;
			case "delete":
				$this->response = $this->delete_comment();
				break;
		}
		

		$this->send_response();

	}

	protected function get_comment(){

		try {
			$comment = \Comment\Models\Comment::find($this->data['comment_id']);
		} 
		catch (\ActiveRecord\RecordNotFound $e){
			return false;
		}

		$array = array(
			"id" => $comment->id,
			"user_id" => $comment->user_id,
			"parent_type" => $comment->parent_type,
			"parent_id" => $comment->parent_id,
			"body" => $comment->body,
			"entry_datetime" => $comment->entry_datetime,
		);
	
		return $array;
		
	}

	protected function create_comment(){

		$comment = new \Comment\Models\Comment;

		$comment->user_id = $this->request('post', 'user_id');
		$comment->parent_type = $this->request('post', 'parent_type');
		$comment->parent_id = $this->request('post', 'parent_id');
		$comment->body = $this->request('post', 'body');
		$comment->entry_datetime = time();

		if($comment->validate()){
			$comment->save();
			return array('response' => 200 , 'id' => $comment->id);
		} else {
			return array('response' => 500 , 'message' => 'could not validate');
		}

	}

	protected function update_comment(){

		try {
			$comment = \Comment\Models\Comment::find($this->data['comment_id']);
		} 
		catch (\ActiveRecord\RecordNotFound $e){
			return array('response' => 500 , 'message' => 'comment not found');
		}

		if($this->request('post', 'user_id')){
			$comment->user_id = $this->request('post', 'user_id');
		}

		if($this->request('post', 'parent_type')){
			$comment->parent_type = $this->request('post', 'parent_type');
		}

		if($this->request('post', 'parent_id')){
			$comment->parent_id = $this->request('post', 'parent_id');
		}

		if($this->request('put', 'body')){
			$comment->body = $this->request('put', 'body');
		}

		if($comment->validate()){
			$comment->save();
			return array('response' => 200 , 'id' => $comment->id, 'message' => ' comment updated');
		} else {
			return array('response' => 500 , 'message' => 'could not validate');
		}
	}

	protected function delete_comment(){

		try {
			$comment = \Comment\Models\Comment::find($this->data['comment_id']);
		} 
		catch (\ActiveRecord\RecordNotFound $e){
			return array('response' => 500 , 'message' => 'comment not found');
		}

		$comment->deleted = 1;
		$comment->save();

		return array('response' => 200 , 'message' => ' comment deleted');

	}

}