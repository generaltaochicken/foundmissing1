<?php

namespace Mobile\Controllers;

class AjaxMobile extends \Mobile\Controllers\Mobile {
	private $image_url;
	private $limits = 10;
	
	public function __construct($uri, $data){
		if (isset($_POST['task'])) {
			$task = $_POST['task'];	
			
			switch ($task) {
				case 'Interviews':
					$return_data .= $this->get_articles('interview');
					$type = 'interview';
					break;
				case 'Articles':
					$return_data .= $this->get_articles('article');
					$type = 'article';
					break;
				case 'Reviews';
					$return_data .= $this->get_articles('review');
					$type = 'review';
					break;
				case 'Gallery';
					$return_data .= $this->get_articles('gallery');
					$return_data .= $this->gallery_css();
					$type = 'gallery';
					break;
				default:
					$return_data .= 'found nothing';
					break;
			}
			
			$this->image_url = $this->get_image_url($type);
			$this->type = $type;
			
			$return_data .= $this->apply_css();
			$return_data .= $this->add_script();
			
			echo $return_data;
		} else if (isset($_POST['type'])) {
			$type = $_POST['type'];
			$id = $_POST['id'];
			
			switch ($type) {
				case 'interview':
					$return_data .= $this->get_article($type, $id);
					break;
				case 'article':
					$return_data .= $this->get_article($type, $id);
					break;
				case 'review':
					$return_data .= $this->get_article($type, $id);
					break;
				case 'gallery':
					$return_data .= $this->get_article($type, $id);
					break;
				default:
					$return_data .= 'found nothing';
					break;
			}
			
			$return_data .= $this->add_sub_css();
			$return_data .= $this->add_sub_script();
			echo $return_data;
		} else if (isset($_POST['load_more'])) {
			$type = $_POST['load_more'];
			$offset = $_POST['offset'];
			
			$output .= $this->load_more($type, $offset);
			
			echo $output;
		}
	}
	
	public function controller() {
	
	}
	
	private function get_articles($type) {
		$output = '<div id="type" data-type="' . $type . '" data-main="1"></div>';
		
		switch($type) {
			case "interview":
				$articles = \Interview\Models\Interview::all(array('conditions' => 'deleted = 0 AND publish_date > 1', 'order' => 'publish_date DESC', 'limit' => $this->limits));
				break;
			case "review":
				$articles = \Review\Models\Review::all(array('conditions' => 'deleted = 0 AND publish_date > 1', 'order' => 'publish_date DESC', 'limit' => $this->limits));
				break;
			case "article":
				$articles = \Article\Models\Article::all(array('conditions' => 'deleted = 0 AND publish_date > 1', 'order' => 'publish_date DESC', 'limit' => $this->limits));
				break;
			case "gallery":
				$gallery = \Gallery\Models\Gallery::all(array('conditions' => 'deleted = 0 AND publish_date > 1', 'order' => 'publish_date DESC'));
				$gallery_output = $this->parse_gallery($gallery);
				break;
			default:
				break;
		}
		
		// temp
		if ($type == 'review' || $type == 'interview') {
			$output .= '<div style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3)), url(/images/boots.jpg);background-image: -webkit-gradient(linear, left top, left bottom, from(rgba(0, 0, 0, 0.3)), to(rgba(0, 0, 0, 0.3))), url(/images/boots.jpg);background-image: -webkit-linear-gradient(top, rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3)), url(/images/boots.jpg);background-repeat: no-repeat;width:100%;min-height:100vh;background-position: left center;-webkit-background-position: left;">
							<div style="position: absolute;top: 50%;left: 50%;width: 350px;height: 300px;margin-left: -175px;margin-top: -135px;color:#FFFFFF;line-height: 22px;font-family: georgia, serif;letter-spacing: 0.5px;text-align: center;">Original and real content means a lot to us at FoundMissing. As a new website, we would like to establish a relationship with the right brands before we give you genuine content.<br /><br />Thank you for your patience.</div>
						</div>';
		} else if ($type != 'gallery') {
			foreach ($articles as $article) {
				if ($type == "interview") {
					$image = ARTICLE_IMAGES . $article->main_image;
					$author_name = ucwords($article->author_name);
				} else if ($type == "review") {
					$image = REVIEW_IMAGES . $article->cover_image;
					$author_name = \User\Models\User::get_user_fullname_by_id($article->user_id);
				} else if ($type == "article") {
					$image = ARTICLE_IMAGES . $article->bg_image_header;
					$author_name = ucwords($article->author_name);
				}
				
				$excerpt = substr($article->excerpt, 0, 200) . '...';
				$output .= '
							<div class="row article">
								<div class="col-md-12 sub_section">
									<div class="article_image" style="background-image: url(' . $image . '); background-position: center; background-size: cover;">
										<div class="unactive"></div>
									</div>
									<div class="article_author text-center">' . time_to_friendly_date($article->publish_date, 'Y.m.d') . 
										'<br .>/<br />' . $author_name . '
									</div>
									<div class="article_title">' . $article->title . '</div>
									<div class="article_excerpt text-center">' . $excerpt . '</div>
									<div data-type="' . $type . '" data-id="' . $article->id . '" class="article_link"></div>
								</div>
							</div>';
			}
		} else {
			$output .= $gallery_output;
		}
		
		return $output;
	}
	
	private function parse_gallery($gallery_object) {
		foreach ($gallery_object as $obj) {
			$max_limit = 4;
			$min_limit = 1;
			$limit = rand($min_limit, $max_limit);
			
			while ($limit_temp == $limit || $limit == 2) {
				$limit = rand($min_limit, $max_limit);
			}
			
			$limit_temp = $limit;
				
			$images = array();
			
			$gal_attachments = \Gallery_attachment\Models\Gallery_attachment::all(array('select' => 'attachment_id', 'conditions' => 'deleted = 0 AND gallery_id = ' . $obj->id, 'limit' => $limit));
			
			foreach ($gal_attachments as $ga) {
				$attachment = \Attachment\Models\Attachment::first($ga->attachment_id);
				
				$images[] = $attachment->image_filename;
			}
			
			$output .= '<div class="row article">
							<div id="photos" class="col-md-12 sub_section">';
							
			if ($limit == 1) {
				$output .= 	   '<div class="album_cover style_1-1-1" style="background-image: url(' . GALLERY_IMAGES . $obj->slug . '/' . $images[0] . '); background-position: center; background-size: cover;"></div>';
			} else if ($limit == 2) {
				$output .= 	   '<div class="album_cover style_2-1-1" style="background-image: url(' . GALLERY_IMAGES . $obj->slug . '/' . $images[0] . '); background-position: center; background-size: cover;"></div>
								<div class="album_cover style_2-1-2" style="background-image: url(' . GALLERY_IMAGES . $obj->slug . '/' . $images[1] . '); background-position: center; background-size: cover;"></div>';
			} else if ($limit == 3) {
				$max = 4;
				$min = 1;
				$style = rand($min, $max);
				
				if ($style == 1) {
					$image_1 = 'style_3-1-1';
					$image_2 = 'style_3-1-2';
					$image_3 = 'style_3-1-3';
				} else if ($style == 2) {
					$image_1 = 'style_3-2-1';
					$image_2 = 'style_3-2-2';
					$image_3 = 'style_3-2-3';
				} else if ($style == 3) {
					$image_1 = 'style_3-3-1';
					$image_2 = 'style_3-3-2';
					$image_3 = 'style_3-3-3';
				} else if ($style == 4) {
					$image_1 = 'style_3-4-1';
					$image_2 = 'style_3-4-2';
					$image_3 = 'style_3-4-3';
				}
				
				$output .= 	   '<div class="album_cover ' . $image_1 . '" style="background-image: url(' . GALLERY_IMAGES . $obj->slug . '/' . $images[0] . '); background-position: center; background-size: cover;"></div>
									<div class="album_cover ' . $image_2 . '" style="background-image: url(' . GALLERY_IMAGES . $obj->slug . '/' . $images[1] . '); background-position: center; background-size: cover;"></div>
									<div class="album_cover ' . $image_3 . '" style="background-image: url(' . GALLERY_IMAGES . $obj->slug . '/' . $images[2] . '); background-position: center; background-size: cover;"></div>';
			} else if ($limit == 4) {
				$max = 4;
				$min = 1;
				$style = rand($min, $max);
				
				if ($style == 1) {
					$image_1 = 'style_4-1-1';
					$image_2 = 'style_4-1-2';
					$image_3 = 'style_4-1-3';
					$image_4 = 'style_4-1-4';
				} else if ($style == 2) {
					$image_1 = 'style_4-2-1';
					$image_2 = 'style_4-2-2';
					$image_3 = 'style_4-2-3';
					$image_4 = 'style_4-2-4';
				} else if ($style == 3) {
					$image_1 = 'style_4-3-1';
					$image_2 = 'style_4-3-2';
					$image_3 = 'style_4-3-3';
					$image_4 = 'style_4-3-4';
				} else if ($style == 4) {
					$image_1 = 'style_4-4-1';
					$image_2 = 'style_4-4-2';
					$image_3 = 'style_4-4-3';
					$image_4 = 'style_4-4-4';
				}
				
				$output .= 	   '<div class="album_cover ' . $image_1 . '" style="background-image: url(' . GALLERY_IMAGES . $obj->slug . '/' . $images[0] . '); background-position: center; background-size: cover;"></div>
									<div class="album_cover ' . $image_2 . '" style="background-image: url(' . GALLERY_IMAGES . $obj->slug . '/' . $images[1] . '); background-position: center; background-size: cover;"></div>
									<div class="album_cover ' . $image_3 . '" style="background-image: url(' . GALLERY_IMAGES . $obj->slug . '/' . $images[2] . '); background-position: center; background-size: cover;"></div>
									<div class="album_cover ' . $image_4 . '" style="background-image: url(' . GALLERY_IMAGES . $obj->slug . '/' . $images[3] . '); background-position: center; background-size: cover;"></div>';
			}
							
			$output .= '<div class="unactive"></div>
							<div class="article_author text-center">' . time_to_friendly_date($obj->publish_date, 'Y.m.d') . 
										'<br .>/<br />' . $obj->author_name . '</div>
							<div class="article_title gallery_title">' . $obj->title . '</div>
							<div class="article_excerpt text-center">' . $obj->description . '</div>
							<div data-type="gallery" data-id="' . $obj->id . '" class="article_link"></div>
							';
			$output .= '</div></div>';
		}
						
		return $output;
	}
	
	private function get_article($type, $id) {
		switch($type) {
			case "interview":
				$article = \Interview\Models\Interview::find($id);
				$image = ARTICLE_IMAGES . $article->main_image;
				$author_name = ucwords($article->author_name);
				break;
			case "review":
				$article = \Review\Models\Review::find($id);
				$image = REVIEW_IMAGES . $article->cover_image;
				$author_name = \User\Models\User::get_user_fullname_by_id($article->user_id);
				break;
			case "article":
				$article = \Article\Models\Article::find($id);
				$image = ARTICLE_IMAGES . $article->bg_image_header;
				$author_name = ucwords($article->author_name);
				break;
			case "gallery":
				$article = \Gallery\Models\Gallery::find($id);
				$output .= $this->parse_gallery_album($article);
				break;
			default:
				break;
		}
		
		if ($type != 'gallery') {
			$output .= '<div class="article_wrapper">
							<div class="row">
								<div class="col-xs-12">
									<div class="article_cover" style="background-image: url(' . $image . '); background-position: center bottom; background-size: cover;">
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 article_header">
									<div class="article_author display text-center">
										' . $author_name . '<br />/<br />' . time_to_friendly_date($article->publish_date, 'Y.m.d') . '
									</div>
									<div class="article_heading text-center">
										' . $article->title . '
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 article_body">
									' . $article->body . '
								</div>
							</div>
						</div>';
		}
		
		return $output;
	}
	
	private function parse_gallery_album($article_object, $layout = 'grid') {
	
		$output = '<div class="article_wrapper">
						<div class="row">
							<div class="col-xs-12 article_header album_head">
								<div class="article_author display text-center">
									' . $article_object->author_name . '<br />/<br />' . time_to_friendly_date($article_object->publish_date, 'Y.m.d') . '
								</div>
								<div class="article_heading text-center">
									' . $article_object->title . '
								</div>
								<div class="article_excerpt album_desc text-center">' . $article_object->description . '</div>
							</div>
						</div>
						<div id="album_outer" class="row">';
		
		$gal_attachments = \Gallery_attachment\Models\Gallery_attachment::all(array('select' => 'attachment_id', 'conditions' => 'deleted = 0 AND gallery_id = ' . $article_object->id));
					
		if ($layout == 'full') {
			foreach ($gal_attachments as $ga) {
				$attachment = \Attachment\Models\Attachment::first($ga->attachment_id);
				
				$output .= '<div class="row image album_image_container"><img class="album_image" src="' . GALLERY_IMAGES . $article_object->slug . '/' . $attachment->image_filename . '"/></div>';
			}
		} else if ($layout == 'grid') {
			foreach ($gal_attachments as $ga) {
				$attachment = \Attachment\Models\Attachment::first($ga->attachment_id);
				
				$output .= '<div class="image grid_layout"><img class="album_image_grid" src="' . GALLERY_IMAGES . $article_object->slug . '/' . $attachment->image_filename . '"/></div>';
			}
		}
		
		$output .= '</div></div>';
		$output .= '<div class="full_image"><div class="image_container"></div></div>';
		return $output;
	}
	
	private function gallery_css() {
		$css = '<style>
					#photos {
						overflow: hidden;
					}
					
					.album_cover {
						position: absolute;
					}
					
					.style_1-1-1 {
						height: 250px;
						width: 100%;
					}
					
					.style_2-1-1 {
						height: 250px;
						width: 50%;
						left: 0;
					}
					
					.style_2-1-2 {
						height: 250px;
						width: 50%;
						left: 50%;
					}
					
					.style_3-1-1 {
						height: 250px;
						width: 50%;
					}
					
					.style_3-1-2 {
						height: 125px;
						width: 50%;
						left: 50%;
					}
					
					.style_3-1-3 {
						height: 125px;
						width: 50%;
						left: 50%;
						top: 50%;
					}
					
					.style_3-2-1 {
						height: 250px;
						width: 75%;
					}
					
					.style_3-2-2 {
						height: 125px;
						width: 25%;
						left: 75%;
					}
					
					.style_3-2-3 {
						height: 125px;
						width: 25%;
						left: 75%;
						top: 50%;
					}
					
					.style_3-3-1 {
						height: 125px;
						width: 25%;
					}
					
					.style_3-3-2 {
						height: 125px;
						width: 25%;
						top: 50%;
					}
					
					.style_3-3-3 {
						height: 250px;
						width: 75%;
						left: 25%;
					}
					
					.style_3-4-1 {
						height: 125px;
						width: 50%;
					}
					
					.style_3-4-2 {
						height: 125px;
						width: 50%;
						top: 50%;
					}
					
					.style_3-4-3 {
						height: 250px;
						width: 50%;
						left: 50%;
					}
					
					.style_4-1-1 {
						height: 125px;
						width: 50%;
					}
					
					.style_4-1-2 {
						height: 125px;
						width: 50%;
						top: 50%;
					}
					
					.style_4-1-3 {
						height: 125px;
						width: 50%;
						left: 50%;
					}
					
					.style_4-1-4 {
						height: 125px;
						width: 50%;
						left: 50%;
						top: 50%;
					}
					
					.style_4-2-1 {
						height: 250px;
						width: 75%;
					}
					
					.style_4-2-2 {
						height: 84px;
						width: 25%;
						left: 75%;
					}
					
					.style_4-2-3 {
						height: 84px;
						width: 25%;
						left: 75%;
						top: 33.33%;
					}
					
					.style_4-2-4 {
						height: 84px;
						width: 25%;
						left: 75%;
						top: 66.66%;
					}
					
					.style_4-3-1 {
						height: 84px;
						width: 25%;
					}
					
					.style_4-3-2 {
						height: 84px;
						width: 25%;
						top: 33.33%;
					}
					
					.style_4-3-3 {
						height: 84px;
						width: 25%;
						top: 66.66%;
					}
					
					.style_4-3-4 {
						height: 250px;
						width: 75%;
						left: 25%;
					}
					
					.style_4-4-1 {
						height: 62.5px;
						width: 33.33%;
					}
					
					.style_4-4-2 {
						height: 62.5px;
						width: 33.33%;
						left: 33.33%;
					}
					
					.style_4-4-3 {
						height: 62.5px;
						width: 33.33%;
						left: 66.66%
					}
					
					.style_4-4-4 {
						height: 187.5px;
						width: 100%;
						top: 25%;
					}
				</style>';
				
		return $css;
	}
	
	private function apply_css() {
		$css = '<style>
					.article {
						/*margin-top: 1px;*/
					}
					
					.article_title {
						font-size: 20px;
						letter-spacing: 3px;
						text-transform: uppercase;
						z-index: 10;
						/*text-shadow: 1px 1px #535858;*/
						position: absolute;
						font-family: rigby;
						color: #FFFFFF;
						top: 50%;
						margin-top: -12.5px;
						width: 100%;
						left: 0;
						text-align: center;
						padding-left: 30px;
						padding-right: 30px;
					}
					
					.gallery_title {
						top: 44%;
					}
					
					.unactive {
						background-color: #000;
						opacity: 0.3;
						height: 100%;
						width: 100%;
						top: 0;
						left: 0;
						position: absolute;
					}
					
					.active {
						background-color: #000;
						opacity: 0.4;
						height: 100%;
						width: 100%;
						top: 0;
						left: 0;
						position: absolute;
					}
					
					.article_excerpt {
						position: absolute;
						top: 65%;
						left: 0;
						z-index: 10;
						color: #FFFFFF;
						width: 100%;
						padding-left: 30px;
						padding-right: 30px;
						color: #FFFFFF;
						display: none;
						font-size: 12px;
					}
					
					.article_author {
						position: absolute;
						left: 0;
						top: 15px;
						z-index: 10;
						color: #FFFFFF;
						width: 100%;
						padding-left: 30px;
						display: none;
						font-size: 12px;
						padding-right: 30px;
						line-height: 11px;
					}
					
					.article_link {
						position: absolute;
						height: 100%;
						width: 100%;
						padding: 0 30px;
						top: 0;
						left: 0;
						display: none;
						z-index: 11;
					}
					
					.sub_section {
						position: relative;
						height: 250px;
					}

					.article_image {
						height: 250px;
						width: 100%;
						position: relative;
					}
					
					.loading {
						width: 100%;
						position: relative;
						overflow-x: hidden;
					}
					
					.loading img {
						left: 50%;
						margin-left: -32px;
						position: absolute;
						width: 64px;
						height: 64px;
					}
					
					#album_outer {
						width: 100%;
						height: auto;
						margin: 0;
					}
					
					.album_image_container {
						margin-top: 1px;
					}
					
					.album_image {
						width: 100%;
						height: auto;
					}
					
					.grid_layout {
						height: 130px;
						float: left;
						overflow: hidden;
						margin-top: 1px;
						margin-right: 1px;
						position: relative;
						vertical-align: middle;
					}
					
					.grid_layout:nth-child(3n + 3) {
						margin-right: 0px !important;
					}
					
					.album_image_grid {
						position: absolute;
						/*left: -20%;*/
						left: -18%;
						max-width:250px;
						max-height:250px;
						overflow: hidden;
					}
					
					.album_desc {
						line-height: 11px;
						display: block;
						top: 68%;
					}
					
					.album_head {
						height: 185px !important;
					}
					
				</style>';
				
		return $css;
	}
	
	private function add_sub_css() {
		$css = '<style>
					.display {
						display: block;
					}

					.article_cover {
						height: 300px;
					}
					
					.article_header {
						height: 165px;
						background-color: #EB1C39;
						position: relative;
						color: #FFFFFF;
					}
					
					.article_heading {
						position: absolute;
						top: 66px;
						left: 0;
						padding: 0 40px;
						letter-spacing: 2px;
						font-size: 20px;
						width: 100%;
						font-family: rigby;
						text-transform: uppercase;
					}
					
					.article_body {
						padding: 20px 45px;
					}
				</style>';
				
		return $css;
	}
	
	private function load_more_script() {
		/*$js = '<script>
					var homeWidth = $("body").outerWidth(true);
					var url = "/ajaxmobile/";
					
					$(".sub_section").on("click", function() {
						$(this).children(".article_excerpt").fadeIn("fast");
						$(this).children(".article_author").fadeIn("fast");
						$(this).children(".article_image").children().css("background-color", "#EB1C39");
						$(this).children(".article_link").css("display", "block");
						
						$(this).children(".unactive").css("background-color", "#EB1C39");
					});
					
					$(".article_link").on("click", function() {
					//$(document.body).on("click", "#main .article_link", function(){
						var id = $(this).data("id");
						var type = $(this).data("type");
						var data = { type: type, id: id };
						var mywindow = $(window);
						var mypos = mywindow.scrollTop();
						
						$("#type").data("main", "0");
						
						$("body").append("<header id=\"header2\"><div class=\"header2\"><div id=\"close2\"><i class=\"fa fa-chevron-left darkgrey\"></i></div><img class=\"icon_head\" src=\"' . $this->image_url . '\" /></div></header>");
						
						$("#header2").css("width", homeWidth);
						
						$("#main_article").css("width", homeWidth);
						
						displayLoader("#main_article");
						
						$("#header").fadeOut();
						
						$("#main_article").transition({ x: -homeWidth, queue: false }, function() {
							$("#main").hide();
						});
						
						$.post(url, data, function(response) {
							$(".loading_fm").fadeOut(function() {
								mywindow.scrollTop(0);
								$("#main_article").css({
									"position" : "absolute",
									"margin-top" : "50px"
								});
								$("#main_article").html(response);
								$("#header2").transition({ x: -homeWidth, queue: false });
								$(".header2").show();
								
								if (type == "gallery") {
									var image_width = (homeWidth -2) / 3;
									$(".grid_layout").css({
										"width" : image_width
									});
								}
								
								$("#main_article .loading_fm").remove();
							});
						});
					});
				</script>';*/
		return $js;
	}
	private function add_script() {
		$js = '	<script>
					var homeWidth = $("body").outerWidth(true);
					var url = "/ajaxmobile/";
					
					$(".sub_section").on("click", function() {
						$(this).children(".article_excerpt").fadeIn("fast");
						$(this).children(".article_author").fadeIn("fast");
						$(this).children(".article_image").children().css("background-color", "#EB1C39");
						$(this).children(".article_link").css("display", "block");
						
						$(this).children(".unactive").css("background-color", "#EB1C39");
					});
					
					$(".article_link").on("click", function() {
					//$(document.body).on("click", "#main .article_link", function(){
						var id = $(this).data("id");
						var type = $(this).data("type");
						var data = { type: type, id: id };
						var mywindow = $(window);
						var mypos = mywindow.scrollTop();
						
						$("#type").data("main", "0");
						
						$("body").append("<header id=\"header2\"><div class=\"header2\"><div id=\"close2\"><i class=\"fa fa-chevron-left darkgrey\"></i></div><img class=\"icon_head\" src=\"' . $this->image_url . '\" />';
						
		if ($this->type == 'gallery') {
			$js .= '<span class=\"toggle_layout\"><i class=\"fa fa-th\"></i></span>';
		}
		
				$js .= '</div></header>");
						
						$("#header2").css("width", homeWidth);
						
						$("#main_article").css("width", homeWidth);
						
						displayLoader("#main_article");
						
						$("#header").fadeOut();
						//$("#main").fadeOut(function() { 
							//$("#main").offset({ top: 0, left: -9999 })
						//});
						
						$("#main_article").transition({ x: -homeWidth, queue: false }, function() {
							$("#main").fadeOut();
							//$("#main").offset({ top: 0, left: -9999 });
						});
						
						$.post(url, data, function(response) {
							$(".loading_fm").fadeOut(function() {
								mywindow.scrollTop(0);
								$("#main_article").css({
									"position" : "absolute",
									"margin-top" : "50px"
								});
								$("#main_article").html(response);
								$("#header2").transition({ x: -homeWidth, queue: false });
								$(".header2").show();
								
								if (type == "gallery") {
									var image_width = (homeWidth -2) / 3;
									$(".grid_layout").css({
										"width" : image_width
									});
								}
					
								$("#main_article .loading_fm").remove();
							});
						});
					});
					
					$("#close").on("click", function() {
						//$(document.body).off("click", "#main .article_link");
						$(this).children().css("color", "#EB1C39");
						$("#wrapper_home").show();
						
						$("body").scrollTop(0);
						$("#main").transition({ x: homeWidth, queue: false, delay: 200 }, function() {
							$("#main").html("");
							$("#main").css({
								"width" : "0px",
								"position" : "fixed",
								"margin-top" : "0px"
							});
						});
						
						$("#header").transition({ x: homeWidth, queue: false, delay: 200 }, function() {
							$("#header").remove();
						});
						
						$("#main_article").html("");
					});
					
					$(".icon_head").click(function() {
						//$("body").scrollTop(0);
						$("body").animate({scrollTop: 0});
					});
					
					var offset = 5;
					var load_more = true;
					
					/*
					$(window).scroll(function(){
						var elem = $("#type");
						var type = elem.data("type");
						var isMain = elem.data("main");
						
						if (isMain == "1" && load_more) {
							if ($(window).scrollTop() == $(document).height()-$(window).height()){
								
								$("#main").append("<div class=\"loading\"><img src=\"/images/fm-redtrans-128.png\" /></div>");
								
								var data = { load_more: type, offset: offset };
								
								$.post(url, data, function(response) {
									var res = $.parseJSON(response);
									$(".loading").fadeOut(function() {
										//$(".loading").remove();
									});
									
									if (!res[0].last) {
										$("#main").append(res[0].html);
									} else {
										load_more = false;
									}
								});
								
								offset += 3;
							}
						}
					});*/
					
				</script>';
				
		return $js;
		
	}
	
	private function add_sub_script() {
		$js = '	<script>
					$("#close2").on("click", function() {
						$(this).children().css("color", "#EB1C39");
						$("#header").show();
						$("#type").data("main", "1");
						//$("#main").offset({ top: 0, left: 0 });
						$("#main").show();
						
						$("#main_article").transition({ x: homeWidth, queue: false, delay: 200 }, function() {
							$("#main_article").html("");
							$("#main_article").css({
								"width" : "0px",
								"position" : "fixed",
								"margin-top" : "0px"
							});
						});
						
						$("#header2").transition({ x: homeWidth, queue: false, delay: 200 }, function() {
							$("#header2").remove();
							
						});
					
					});
					
					$(".toggle_layout").click(function() {
						
						if ($("#album_outer").children().hasClass("grid_layout")) {
						
							$("#album_outer").fadeOut(function() {
								$(".grid_layout").removeClass("grid_layout").addClass("album_image_container");
								$(".album_image_grid").removeClass("album_image_grid").addClass("album_image");
								
								$(".album_image").css("width", homeWidth);
								$("#album_outer").fadeIn();
							});
							
							$(".toggle_layout").fadeOut(function() {
								
								if ($(".toggle_layout i").hasClass("fa-th")) {
									$(".toggle_layout i").removeClass("fa-th").addClass("fa-stop");
								}
								$(".toggle_layout").fadeIn();
							});
						} else if ($("#album_outer").children().hasClass("album_image_container")) {
						
							$("#album_outer").fadeOut(function() {
								$(".album_image_container").removeClass("album_image_container").addClass("grid_layout");
								$(".album_image").removeClass("album_image").addClass("album_image_grid");
								$("#album_outer").fadeIn();
							});
							
							$(".toggle_layout").fadeOut(function() {
								
								if ($(".toggle_layout i").hasClass("fa-stop")) {
									$(".toggle_layout i").removeClass("fa-stop").addClass("fa-th");
								}
								$(".toggle_layout").fadeIn();
							});
						}
					});
					
					$(".icon_head").click(function() {
						//$("body").scrollTop(0);
						$("body").animate({scrollTop: 0});
					});
				</script>';
				
		return $js;
	}
	
	private function get_image_url($type) {
		if ($type == "article") {
			$icon = "/images/icons/gray/48px/48_article.png";
		} else if ($type == "interview") {
			$icon = "/images/icons/gray/48px/48_interview.png";
		} else if ($type == "review") {
			$icon = "/images/icons/gray/48px/48_review.png";
		} else if ($type == "gallery") {
			$icon = "/images/icons/gray/48px/48_gallery.png";
		} else {
			$icon = "";
		}
			
		return $icon;
		
	}
	
	private function load_more($type, $offset) {
		$output = array();
		
		if ($type == "review") {
			$articles = \Review\Models\Review::all(array('conditions' => 'deleted = 0 AND publish_date > 1', 'order' => 'publish_date DESC', 'limit' => '3', 'offset' => $offset));
		} else if ($type == "article") {
			$articles = \Article\Models\Article::all(array('conditions' => 'deleted = 0 AND publish_date > 1', 'order' => 'publish_date DESC', 'limit' => '3', 'offset' => $offset));
		} else if ($type == "interview") {
			$articles = \Interview\Models\Interview::all(array('conditions' => 'deleted = 0 AND publish_date > 1', 'order' => 'publish_date DESC', 'limit' => '3', 'offset' => $offset));
		} else if ($type == "gallery") {
			$output[] = array('true' => false, 'html' => '');
		}
		
		if ($type != 'gallery') {
			if (empty($articles)) {
				$output[] = array('last' => true, 'html' => '');
			} else {
				foreach ($articles as $article) {
				
					if ($type == "review") {
						$image = REVIEW_IMAGES . $article->cover_image;
						$author_name = \User\Models\User::get_user_fullname_by_id($article->user_id);
					} else if ($type == "article") {
						$image = ARTICLE_IMAGES . $article->bg_image_header;
						$author_name = ucwords($article->author_name);
					} else if ($type == "interview") {
						$image = ARTICLE_IMAGES . $article->main_image;
						$author_name = ucwords($article->author_name);
					}
					
					$excerpt = substr($article->excerpt, 0, 200) . '...';
					
					$html_output .= '<div class="row article"><div class="col-md-12 sub_section"><div class="article_image" style="background-image: url(' . $image . '); background-position: center; background-size: cover;"><div class="unactive"></div></div><div class="article_author text-center">' . time_to_friendly_date($article->publish_date, 'Y.m.d') . '<br .>/<br />' . $author_name . '</div><div class="article_title">' . $article->title . '</div><div class="article_excerpt text-center">' . $excerpt . '</div><div data-type="' . $type . '" data-id="' . $article->id . '" class="article_link"></div></div></div>';
				}
				
				$output[] = array(
					'last' => false, 'html' => $html_output . $this->load_more_script()
				);
			}
		}
		
		
		return json_encode($output);
	}
}

?>