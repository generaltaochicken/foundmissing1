<?php
namespace Attachment\Controllers;

class Edit extends \Manage\Controllers\Manage{

	private $attachment;

	public $page_title;
	public $link_back;
	public $form;
	public $existing;
	public $multiple = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);
		
		if(isset($_GET['item_id'])){
			if ($_GET['type'] == 'footwear') {
				$this->link_back = '/manage/footwear/edit/' . $_GET['item_id'];
				
				if ($_GET['upload'] == 'multiple') {
					$this->multiple = true;
				}
				
			} else if ($_GET['type'] == 'album') {
				$this->link_back = '/manage/gallerys/edit/' . $_GET['item_id'];
				if ($_GET['upload'] == 'multiple') {
					$this->multiple = true;
				}
				$this->gallery_slug = $_GET['slug'];
			}
			
			$this->item_id = $_GET['item_id'];
			$this->type = $_GET['type'];
		}
			
		if(isset($_POST["submit"])){
			$this->submit_form();
		}

		if (isset($data["attachment_id"])){
			$this->attachment = \Attachment\Models\Attachment::find($data["attachment_id"]);
			$this->page_title = "Editing Attachment";
			$this->new = false;

		} else {
			$this->attachment = new \Attachment\Models\Attachment;
			$this->page_title = "New Attachment";
			$this->new = true;
		}
		
		$this->filename = $this->attachment->image_filename;
		
		
		if ($this->multiple == false) {
			$form = new \Form\Models\Form("edit", "POST", "", "well");

			$item = new \Form\Models\FormItemHidden(array("id" => "id", "title" => "Id", "value" => $this->attachment->id));
			$form->add_item($item);

			$item = new \Form\Models\FormItemFile(array("id" => "image_filename", "title" => "File", "value" => $this->attachment->image_filename));
			$form->add_item($item);

			$item = new \Form\Models\FormItemText(array("id" => "title", "title" => "Title", "value" => $this->attachment->title));
			$form->add_item($item);

			$item = new \Form\Models\FormItemTextarea(array("id" => "caption", "title" => "Caption", "value" => $this->attachment->caption));
			$form->add_item($item);

			$item = new \Form\Models\FormItemSubmit(array("value" => "Save", "id" => "submit", "class" => "btn btn-primary", "redirect" => $this->link_back));
			$form->add_item($item);

			$this->form = $form->render();
		} else if ($this->multiple == true) {
			$this->form = '<form action="" method="POST" enctype="multipart/form-data">
								<input type="file" name="files[]" multiple /><br />
								<p>Title</p>
								<input type="text" id="title" name="title" value=""/><br />
								<input type="submit" name="submit"/>
							</form>';
		}
		
		if($this->attachment->image_filename){
			$this->preview = $this->attachment->preview();
			$this->attachment_id = $this->attachment->id;
		}
	}

	public function controller(){
		$this->set_view("Attachment\Views\Edit");
	}

	private function submit_form(){
		if($_POST["id"] > 0){
			$attachment = \Attachment\Models\Attachment::find($_POST["id"]);
			$flash_msg = "Attachment has been updated successfully.";
		} else {
			$attachment = new \Attachment\Models\Attachment;
			$flash_msg = "Attachment has been created successfully.";
			$attachment->entry_datetime = time();
			$this->new = true;
		}
		
		$attachment->title = $_POST["title"];
		$attachment->caption = $_POST["caption"];
		$attachment->type = 'image';
		
		if ($this->multiple == false) {
			if(isset($_FILES['image_filename']['tmp_name']) && $_FILES['image_filename']['tmp_name'] != ''){

				if ($this->type == 'album') {
					$image = $this->upload($_FILES['image_filename'], array('jpg', 'png', 'gif', 'jpeg'), false, $this->gallery_slug);
				} else if ($this->type == 'footwear') {
					$image = $this->upload($_FILES['image_filename'], array('jpg', 'png', 'gif', 'jpeg'), false, '', true);
				} else {
					$image = $this->upload($_FILES['image_filename'], array('jpg', 'png', 'gif', 'jpeg'));
				}

				if($image) {
					$attachment->image_filename = $image['filename'];
				}
			}
			
			$attachment->save();
			
			$this->add_flash(array("message" => $flash_msg, "type" => "success", "Heading" => "Great!"));
		
		
			if(isset($this->item_id)){
				if($this->new){
					if ($this->type == 'footwear') {
						$footwear_attachment = new \Footwear_attachment\Models\Footwear_attachment;
						$footwear_attachment->footwear_id = $this->item_id;
						$footwear_attachment->attachment_id = $attachment->id;
						$footwear_attachment->save();
					} else if ($this->type == 'album') {
						$gallery_attachment = new \Gallery_attachment\Models\Gallery_attachment;
						$gallery_attachment->gallery_id = $this->item_id;
						$gallery_attachment->attachment_id = $attachment->id;
						$gallery_attachment->save();
					}
				}

				redirect_to($this->link_back);
			} else {
				redirect_to($attachment->link_edit());
			}

		} else {
			if ($this->type == 'footwear') {
				$images = $this->multiple_uploads($_FILES, 'footwear');
				
				foreach ($images as $image) {
					$attachment = new \Attachment\Models\Attachment;
					$attachment->entry_datetime = time();
					$attachment->title = $_POST["title"];
					$attachment->image_filename = $image;
					
					$attachment->save();
					
					$footwear_attachment = new \Footwear_attachment\Models\Footwear_attachment;
					$footwear_attachment->footwear_id = $this->item_id;
					$footwear_attachment->attachment_id = $attachment->id;
					$footwear_attachment->save();
				}
			} else if ($this->type == 'album') {
				$images = $this->multiple_uploads($_FILES, 'album', $this->gallery_slug);
				
				foreach ($images as $image) {
					$attachment = new \Attachment\Models\Attachment;
					$attachment->entry_datetime = time();
					
					if ($_POST["title"])
						$attachment->title = $_POST["title"];
						
					$attachment->image_filename = $this->gallery_slug . '_' . $image;
					$attachment->save();
					
					$gallery_attachment = new \Gallery_attachment\Models\Gallery_attachment;
					$gallery_attachment->gallery_id = $this->item_id;
					$gallery_attachment->attachment_id = $attachment->id;
					$gallery_attachment->save();
				}
			}
			
			redirect_to($this->link_back);
		}
		
	}

}
