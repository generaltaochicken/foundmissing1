<?php
namespace Review\Controllers;

class Edit extends \Manage\Controllers\Manage{

	public $review;

	public $page_title;
	public $link_back;
	public $form;
	public $existing;
	public $published = false;
	public $shoes_options;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		if(isset($_POST["submit"])){
			$this->submit_form();
		}

		if(isset($data["review_id"])){
			$this->review = \Review\Models\Review::find($data["review_id"]);
			$this->page_title = "Editing Review";
		} else {
			$this->review = new \Review\Models\Review;
			$this->page_title = "New Review";
		}
		
		if ($this->review->publish_date != 0 || $this->review->publish_date != null) {
			$this->published = true;
		}
		
		$this->shoes_options = $this->get_shoe_name_options($this->review->shoe_id);

		$this->link_back = $this->review->link_all();

		/*
		$form = new \Form\Models\Form("edit", "POST", "", "well");

		$item = new \Form\Models\FormItemHidden(array("id" => "id", "title" => "Id", "value" => $this->review->id));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "author", "title" => "Author Name", "value" => $this->review->author));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "title", "title" => "Title", "class" => "fillslug", "value" => $this->review->title));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "slug", "title" => "Slug", "value" => $this->review->slug));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "excerpt", "title" => "Excerpt", "value" => $this->review->excerpt));
		$form->add_item($item);

		$item = new \Form\Models\FormItemTextarea(array("id" => "body", "title" => "Body", "value" => $this->review->body));
		$form->add_item($item);

		$item = new \Form\Models\FormItemSelect(array("id" => "shoe_id", "title" => "Shoe", "options" => $this->get_shoe_name_options($this->review->shoe_id)));
		$form->add_item($item);

		$item = new \Form\Models\FormItemFile(array("id" => "main_image", "title" => "File", "value" => $this->review->cover_image));
		$form->add_item($item);
		
		$item = new \Form\Models\FormItemSelect(array("id" => "publish_date", "title" => "Publish", "options" => $this->get_publish_options()));
		$form->add_item($item);
		
		$item = new \Form\Models\FormItemSubmit(array("value" => "Save", "id" => "submit", "class" => "btn btn-primary", "cancel_url" => $this->review->link_all()));
		$form->add_item($item);

		$this->form = $form->render();
		*/
		
		if($this->review->cover_image){
			$this->preview = REVIEW_IMAGES . $this->review->cover_image;
		}

	}

	public function controller(){
		$this->set_view("Review\Views\Edit");
	}

	private function submit_form(){
		if($_POST["id"] > 0){
			$review = \Review\Models\Review::find($_POST["id"]);
			$flash_msg = "Review has been updated successfully.";
		} else {
			$review = new \Review\Models\Review;
			$flash_msg = "Review has been created successfully.";
			$review->ordering = \Review\Models\Review::get_next_ordering_number();
			$review->entry_datetime = time();
		}

		$review->author = $_POST["author"];
		$review->title = $_POST["title"];
		$review->slug = $_POST["slug"];
		$review->excerpt = $_POST["excerpt"];
		$review->body = $_POST["body"];
		$review->shoe_id = $_POST["shoe_id"];
		
		if(isset($_FILES['main_image']['tmp_name']) && $_FILES['main_image']['tmp_name'] != ''){

			$image_slug = time_to_friendly_date($review->entry_datetime, 'Ymd') . '_' . $review->slug;
			$image = $this->upload_article($_FILES['main_image'], array('jpg', 'png', 'gif', 'jpeg'), $image_slug, true);

			if($image) {
				$review->cover_image =  $image_slug . '/' . $image['filename'];
			}
		}
		
		$review->publish_date = $_POST["publish_date"];
		if ($review->publish_date == 1) {
			if ($this->review->publish_date <= 1) {
				$review->publish_date = time();
			}
		}

		$review->save();
		$this->add_flash(array("message" => $flash_msg, "type" => "success", "Heading" => "Great!"));
		redirect_to($review->link_edit());
	}
	
	private function get_publish_options() {
		$published = array(
			array('value' => '1', 'title' => 'Yes', 'selected' => 0),
			array('value' => '0', 'title' => 'No', 'selected' => 1)
		);
		
		if  ($this->review->publish_date != 0 || $this->review->publish_date != null) {
			$published[0]['selected'] = 1;
			$published[1]['selected'] = 0;
		}
		
		return $published;
	}
	
	protected function get_shoe_name_options($current){

		$footwear = \Footwear\Models\Footwear::all(array('conditions' => 'deleted = 0', 'order' => 'ordering'));
		
		$shoes = $this->get_options($footwear, $current, false, false, 'id', 'model_name');
		$output = '';
		
		foreach ($shoes as $shoe) {
		
			if ($shoe['selected'] === 'selected') {
				$output .= '<option value="' . $shoe['value'] . '" selected="selected">' . $shoe['title'] . '</option>';
			} else {
				$output .= '<option value="' . $shoe['value'] . '">' . $shoe['title'] . '</option>';
			}
		}
		
		return $output;
	}

}
