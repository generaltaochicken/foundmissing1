<?php
namespace Store\Controllers;

class Edit extends \Manage\Controllers\Manage{

	private $store;

	public $page_title;
	public $link_back;
	public $form;
	public $existing;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		if(isset($_POST["submit"])){
			$this->submit_form();
		}

		if(isset($data["store_id"])){
			$this->store = \Store\Models\Store::find($data["store_id"]);
			$this->page_title = "Editing Store";
		} else {
			$this->store = new \Store\Models\Store;
			$this->page_title = "New Store";
		}

		$this->link_back = $this->store->link_all();

		$form = new \Form\Models\Form("edit", "POST", "", "well");

		$item = new \Form\Models\FormItemHidden(array("id" => "id", "title" => "Id", "value" => $this->store->id));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "title", "title" => "Title", "value" => $this->store->title));
		$form->add_item($item);

		$item = new \Form\Models\FormItemTextarea(array("id" => "description", "title" => "Description", "value" => $this->store->description));
		$form->add_item($item);

		$item = new \Form\Models\FormItemSubmit(array("value" => "Save", "id" => "submit", "class" => "btn btn-primary", "cancel_url" => $this->store->link_all()));
		$form->add_item($item);

		$this->form = $form->render();

	}

	public function controller(){
		$this->set_view("Store\Views\Edit");
	}

	private function submit_form(){
		if($_POST["id"] > 0){
			$store = \Store\Models\Store::find($_POST["id"]);
			$flash_msg = "Store has been updated successfully.";
		} else {
			$store = new \Store\Models\Store;
			$flash_msg = "Store has been created successfully.";
			$store->ordering = \Store\Models\Store::get_next_ordering_number();
		}

		$store->title = $_POST["title"];
		$store->description = $_POST["description"];

		$store->save();
		$this->add_flash(array("message" => $flash_msg, "type" => "success", "Heading" => "Great!"));
		redirect_to($store->link_all());
	}

}
