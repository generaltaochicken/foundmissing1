<?php
namespace User\Controllers;

class All extends \Manage\Controllers\Manage{

	private $users;

	public $users_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Users";
		$this->link_new = \User\Models\User::link_new();

		$this->users = \User\Models\User::all(array("conditions" => "deleted = 0 AND admin = 0"));
		$this->admins = \User\Models\User::all(array("conditions" => "deleted = 0 AND admin = 1"));
		$archived = \User\Models\User::count(array("conditions" => "deleted = 1"));
		
		if($archived > 0){
			$this->archive_button = array("url" => \User\Models\User::link_archive(), "title" => "Archive");
		}

		foreach($this->users as $user){
			$this->has_data = true;
			$this->users_data[] = array(
				"id" => $user->id,
				"username" => $user->username,
				"password" => $user->password,
				"firstname" => $user->firstname,
				"lastname" => $user->lastname,
				"admin" => $user->admin,
				"link_edit" => $user->link_edit(),
				"link_delete" => $user->link_delete(),
				"code" => $user->code,
				"email" => $user->email
			);
		}
		
		foreach ($this->admins as $admin) {
			$this->admin_data[] = array(
				"id" => $admin->id,
				"username" => $admin->username,
				"password" => $admin->password,
				"firstname" => $admin->firstname,
				"lastname" => $admin->lastname,
				"admin" => $admin->admin,
				"link_edit" => $admin->link_edit(),
				"link_delete" => $admin->link_delete(),
				"code" => $admin->code,
				"email" => $admin->email
			);
		}

	}

	public function controller(){
		$this->set_view("User\Views\All");
	}

}

