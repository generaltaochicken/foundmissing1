<?php
namespace Subscriber\Controllers;

class Delete extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$subscriber = \Subscriber\Models\Subscriber::find($data["subscriber_id"]);

		if($subscriber){
			$subscriber->deleted = 1;
			$subscriber->save();
			$this->add_undo(array("message" => "Subscriber has been deleted!", "action" => $subscriber->link_restore()));

			redirect_to($subscriber->link_all());
		}
	}
}
