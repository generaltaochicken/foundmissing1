<?php
namespace User\Controllers;

class Archive extends \Manage\Controllers\Manage{

	private $users;

	public $users_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Deleted Users";

		$this->users = \User\Models\User::all(array("conditions" => "deleted = 1"));
		$this->archive_button = array("url" => \User\Models\User::link_all(), "title" => "Back");
		foreach($this->users as $user){
			$this->has_data = true;
			$this->users_data[] = array(
				"id" => $user->id,
				"username" => $user->username,
				"password" => $user->password,
				"firstname" => $user->firstname,
				"lastname" => $user->lastname,
				"admin" => $user->admin,
				"link_restore" => $user->link_restore(),
			);
		}

	}

	public function controller(){
		$this->set_view("User\Views\All");
	}

}

