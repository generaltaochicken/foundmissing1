<?php
namespace Color\Controllers;

class Archive extends \Manage\Controllers\Manage{

	private $colors;

	public $colors_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Deleted Colors";

		$this->colors = \Color\Models\Color::all(array("conditions" => "deleted = 1"));
		$this->archive_button = array("url" => \Color\Models\Color::link_all(), "title" => "Back");
		foreach($this->colors as $color){
			$this->has_data = true;
			$this->colors_data[] = array(
				"id" => $color->id,
				"title" => $color->title,
				"hex" => $color->hex,
				"link_restore" => $color->link_restore(),
			);
		}

	}

	public function controller(){
		$this->set_view("Color\Views\All");
	}

}

