<?php
namespace Winner\Controllers;

class Restore extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$winner = \Winner\Models\Winner::find($data["winner_id"]);

		if($winner){
			$winner->deleted = 0;
			$winner->save();
			$this->add_flash(array("message" => "Winner has been restored!"));

			redirect_to($winner->link_all());
		}
	}
}
