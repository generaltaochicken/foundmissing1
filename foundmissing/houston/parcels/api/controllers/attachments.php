<?php
namespace Api\Controllers;

class Attachments extends \Api\Controllers\Api{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->response = $this->get_attachments();
		$this->send_response();

	}

	protected function get_attachments(){

		$conditions = array('deleted' => 0);

		if($this->request('get', 'parent_id')){
			$conditions['parent_id'] = $this->request('get', 'parent_id');
		}

		$args = array('conditions' => $conditions);

		$items = \Attachment\Models\Attachment::all($args);
		$array = array();
		foreach($items as $item){
			$array[] = array(
				"id" => $item->id,
				"type" => $item->type,
				"image_src" => $item->image_src(false),
				"image_src_absolute" => $item->image_src(true),
				"title" => $item->title,
				"caption" => $item->caption
			);
		}

		return $array;
		
	}

}