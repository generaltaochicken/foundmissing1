<?php
namespace Article\Models;

class Article extends \Core\Models\Base_Model{

	protected static $manage_prefix = "/manage/articles/";

	public function link_edit(){
		return self::$manage_prefix . "edit/" . $this->id;
	}

	public function link_delete(){
		return self::$manage_prefix . "delete/" . $this->id;
	}

	public function link_restore(){
		return self::$manage_prefix . "restore/" . $this->id;
	}

	public function link_new(){
		return self::$manage_prefix . "new";
	}

	public function link_all(){
		return self::$manage_prefix;
	}

	public function link_archive(){
		return self::$manage_prefix . "archive";
	}

	public function get_frontend_data(){
		return array(
			"id" => $this->id,
			"entry_datetime" => $this->entry_datetime,
			"author_name" => $this->author_name,
			"title" => $this->title,
			"slug" => $this->slug,
			"excerpt" => $this->excerpt,
			"body" => $this->body,
			"bg_image_header" => $this->bg_image_header,
		);
	}
	
	public function get_article_slug($article_id) {
		$article = self::find($article_id);
		return $article->slug;
	}
}