<?php

namespace Manage\Controllers;

class Generate extends \Manage\Controllers\Manage{
    
    protected $config;
    protected $parcel;
    protected $plural;
    protected $models;
    protected $model;
    protected $manage_prefix;
    
    //file writing settings
    protected $write_files;
    protected $backup_files;
    protected $create_db_table;
    protected $update_config;
    
    public $output;

    public $logOutput = array();
    public $sqlOutput;
    public $routesOutput;
    public $parcel_name;
    public $url;

    public $safemode;
    public $file_outputs = array();

    protected $permission_error = false;
    
    public function __construct($uri, $data){
        parent::__construct($uri, $data);
        
        /* **************** */
        /**** SETTINGS **** */
        /* **************** */
        
        $this->manage_prefix = 'manage';
        $this->parcel = $data['parcel_name'];
        $this->plural = $data['parcel_plural'];
     
        $this->write_files = true;
        $this->backup_files = false;
        $this->create_db_table = true;
        $this->update_config = true;
        $this->safemode = false;
        
        /* **************** */


        if($this->safemode){
            $this->log("***SAFE MODE*** Files will not actually be written.");
        }

        //log the settings
        if($this->write_files){
            $directory = PARCEL_ROOT . $this->parcel . DS;
            if(!is_writable($directory . $this->parcel .'.json')){
                $this->permission_error = true;
                $this->add_error('Permission to write file in ' . $directory . " has been denied. Please check permissions.");
            }


            $this->log("Files will be written");
        } else {
            $this->log("Files will NOT be written");
        }

        if($this->backup_files){
            $this->log("Files will be backed-up if neccissary");
        } else {
            $this->log("Files will NOT be backed-up if neccissary");
        }

        if($this->create_db_table){
            $this->log("Database table will be created");
        } else {
            $this->log("Database table will NOT be created");
        }

        if($this->update_config){
            $this->log("Config file will be updated with new routes");
        } else {
            $this->log("Config file will NOT be updated with new routes");
        }
        
        $model = $this->parcel;
        $this->config = json_decode(file_get_contents(PARCEL_ROOT . $this->parcel . DS . $this->parcel . '.json'), false);
        $this->fields = $this->config->models->$model->fields;

        $this->write_file($this->parcel . '.php', 'models', $this->generate_model());
        $this->write_file('all.php', 'controllers', $this->generate_controller_all());
        $this->write_file('archive.php', 'controllers', $this->generate_controller_archive());
        $this->write_file('edit.php', 'controllers', $this->generate_controller_edit());
        $this->write_file('delete.php', 'controllers', $this->generate_controller_delete());
        $this->write_file('restore.php', 'controllers', $this->generate_controller_restore());
        $this->write_file('reorder.php', 'controllers', $this->generate_controller_reorder());
        $this->write_file('all.html', 'views', $this->generate_view_all());
        $this->write_file('table.html', 'views', $this->generate_view_table());
        $this->write_file('edit.html', 'views', $this->generate_view_edit());
        
        $sql = $this->generate_sql();
        
        $this->create_db_table($sql);
        $this->update_config($this->generate_routes());

    }

    public function controller(){

        $this->parcel_name = ucwords($this->parcel);
        $this->url = '/manage/' . $this->plural;

        $this->set_view('Manage\Views\Generate');
    }
    
    protected function write_file($filename, $dir, $contents){

        if($this->safemode){
            $this->file_outputs[] = array('filename' => $dir . '/' . $filename, 'content' => $contents);
            $this->log("*SAFEMODE* " . $filename . " not written, instead file output logged below");
            return true;
        }
        
        if($this->permission_error){
            return false;
        }

        if($this->write_files){
            
            $f_array = explode('.',$filename);
            $ext = $f_array[sizeof($f_array) - 1];
            $file_title = $f_array[0];
            $directory = PARCEL_ROOT . $this->parcel . DS ;
            if($dir){
                $directory .=  $dir . DS;
            }
            
            if($dir){
                if(!is_dir($directory)){
                    mkdir($directory);
                    $this->log('Created directory [' . $directory . ']');
                } else {
                    $this->log('Directory Exists [' . $directory . ']');
                }
            }
            
            $file = $directory.$filename;
            
            if(!is_file($file)){
               $fh = fopen($file, 'w');
               fwrite($fh, $contents);
               fclose($fh);

               chmod($file, 0664);
               
               $this->log('Wrote file [' . $file . ']');
            } else {
                $this->log("File exists [$file]");
                
                if($this->backup_files){
                    $newfile = $directory . $file_title . '_' . time() . '.' . $ext;
                    rename($file, $newfile);
                    $this->log("Renamed old file to " . $newfile);
                } else {
                    $this->log("Overwriting " . $file);
                }
                
                $fh = fopen($file, 'w');
                fwrite($fh, $contents);
                fclose($fh);

                chmod($file, 0664);
                
                $this->log('Wrote file [' . $file . ']');
            }
        
        }
    }
    
    protected function create_db_table($sql){
        
        if($this->safemode){
            $this->log("*SAFEMODE* No DB table created");

            return false;
        }

        if($this->create_db_table){
            
            $con = mysql_connect(DB_HOST, DB_USER, DB_PASS);
            $db = mysql_select_db(DB_NAME, $con);
            $query = mysql_query($sql,$con);
            mysql_close($con);
            
            $this->log("Writing DB Table");
            
        }
        
    }
    
    protected function update_config($routes){
        
        if($this->update_config){
            
            //$this->write_file('routes.json', '', $routes);

            $this->routesOutput = $routes;

            //$this->log("*** COPY CONTENTS OF routes.json into app.json OR the parcel's config file.");
        
        }
    }
    
    protected function log($str){
        
        $this->logOutput[] = $str;

        
    }
    
    protected function lt(){
        if($this->write_files){
            return '<';
        }
        
        return "&lt";
    }
    
    protected function gt(){
        if($this->write_files){
            return '>';
        }
        return "&gt";
    }
    
    protected function nl($num = 1){
        $str = '';
        for($i=0; $i<$num; $i++){
            $str .= "\n";
        }
        return $str;
    }
    
    protected function tab($num = 1){
        $str = '';
        for($i=0; $i<$num; $i++){
            $str .= "\t";
        }
        return $str;
    }
    
    protected function open($php = true){
        $html = '';
        if(!$this->write_files){
            $html = "<pre>";
        }
        
        if($php){
            $html .= $this->lt() . "?php\n";
        }
        return $html;
    }
    
    protected function close(){
        
        if(!$this->write_files){
            $html = "</pre>";
        }
        
        return "";
    }
    
    protected function ref_model(){
        return '\\'. ucwords($this->parcel) .'\\Models\\' . ucwords($this->parcel);
    }
    
    protected function generate_header($title){
        $output .= '<hr />' . $title . '<hr />';
        
        return $output;
    }


    protected function field_exists($field){

        return array_key_exists($field, $this->fields);

    }
    
    
    
    protected function generate_model(){
        $output = $this->open();
        
        //opening
        $output .= "namespace " . ucwords($this->parcel) . '\Models;' . $this->nl(2);
        $output .= "class " . ucwords($this->parcel) . ' extends \Core\Models\Base_Model{' . $this->nl(2) . $this->tab();
        $output .= 'protected static $manage_prefix = "/'. $this->manage_prefix .'/'. $this->plural .'/";' . $this->nl(2) . $this->tab();
        
        //links
        $output .= 'public function link_edit(){' . $this->nl() . $this->tab(2);
        $output .= 'return self::$manage_prefix . "edit/" . $this->id;' . $this->nl() . $this->tab();
        $output .= '}' . $this->nl(2) . $this->tab();
        
        $output .= 'public function link_delete(){' . $this->nl() . $this->tab(2);
        $output .= 'return self::$manage_prefix . "delete/" . $this->id;' . $this->nl() . $this->tab();
        $output .= '}' . $this->nl(2) . $this->tab();
        
        $output .= 'public function link_restore(){' . $this->nl() . $this->tab(2);
        $output .= 'return self::$manage_prefix . "restore/" . $this->id;' . $this->nl() . $this->tab();
        $output .= '}' . $this->nl(2) . $this->tab();
        
        $output .= 'public function link_new(){' . $this->nl() . $this->tab(2);
        $output .= 'return self::$manage_prefix . "new";' . $this->nl() . $this->tab();
        $output .= '}' . $this->nl(2) . $this->tab();
        
        $output .= 'public function link_all(){' . $this->nl() . $this->tab(2);
        $output .= 'return self::$manage_prefix;' . $this->nl() . $this->tab();
        $output .= '}' . $this->nl(2) . $this->tab();
        
        $output .= 'public function link_archive(){' . $this->nl() . $this->tab(2);
        $output .= 'return self::$manage_prefix . "archive";' . $this->nl() . $this->tab();
        $output .= '}' . $this->nl(2);

        $output .= $this->tab(1) .'public function get_frontend_data(){' . $this->nl();
        $output .= $this->tab(2) .'return array(' . $this->nl();

        foreach($this->fields as $key => $attr){
            if($attr->table_view == true){
                $output .= $this->tab(3) . '"' . $key . '" => $this->' . $key . ',' . $this->nl();
            }
        }

        $output .= $this->tab(2) .');' . $this->nl();
        $output .= $this->tab(1) . "}" . $this->nl();


        //close
        $output .= "}";
        $output .= $this->close();
        
        return $output;
        
    }
    
    
    protected function generate_controller_all(){
        
        $output = $this->open();
        
        //opening
        $output .= "namespace " . ucwords($this->parcel) . '\Controllers;' . $this->nl(2);
        $output .= "class All extends \Manage\Controllers\\Manage{" . $this->nl(2) . $this->tab();
        
        $output .= 'private $' . $this->plural . ';' . $this->nl(2) . $this->tab();
        
        $output .= 'public $' . $this->plural . '_data = array();' . $this->nl() . $this->tab(1);
        $output .= 'public $link_new;' . $this->nl();
        $output .= $this->tab() . 'public $page_title;' . $this->nl();
        $output .= $this->tab() . 'public $has_data = false;' . $this->nl();
        $output .= $this->tab() . 'public $archive_button = false;' . $this->nl(2);
        
        $output .= $this->tab() . 'public function __construct($uri, $data){' . $this->nl();
        $output .= $this->tab(2) . 'parent::__construct($uri, $data);' . $this->nl(2);
        $output .= $this->tab(2) . '$this->page_title = "'. ucwords($this->plural) .'";' . $this->nl();
        $output .= $this->tab(2) . '$this->link_new = \\'. ucwords($this->parcel) .'\\Models\\' . ucwords($this->parcel) . '::link_new();' . $this->nl(2);
        
        $output .= $this->tab(2) . '$this->'. $this->plural .' = \\'. ucwords($this->parcel) .'\\Models\\' . ucwords($this->parcel) . '::all(array("conditions" => "deleted = 0" , "order" => "ordering"));' . $this->nl();
        $output .= $this->tab(2) . '$archived = \\'. ucwords($this->parcel) .'\\Models\\' . ucwords($this->parcel) . '::count(array("conditions" => "deleted = 1"));' . $this->nl();
        $output .= $this->tab(2) . 'if($archived > 0){' . $this->nl();
        $output .= $this->tab(3) . '$this->archive_button = array("url" => ' . $this->ref_model() . '::link_archive(), "title" => "Archive");' . $this->nl();
        $output .= $this->tab(2) . '}' . $this->nl(2);
        
        $output .= $this->tab(2) . 'foreach($this->' . $this->plural . ' as $' . $this->parcel . '){' . $this->nl();
        $output .= $this->tab(3) . '$this->has_data = true;' . $this->nl();
        $output .= $this->tab(3) . '$this->' . $this->plural . '_data[] = array(' . $this->nl();
        
        foreach($this->fields as $key => $attr){
            if($attr->table_view == true){
                $output .= $this->tab(4) . '"' . $key . '" => $' . $this->parcel .'->' . $key . ',' . $this->nl();
            }
        }
        
        $output .= $this->tab(4) . '"link_edit" => $' . $this->parcel . '->link_edit(),' . $this->nl();
        $output .= $this->tab(4) . '"link_delete" => $' . $this->parcel . '->link_delete(),' . $this->nl();

        //put if statement here to look for ordering
        if($this->field_exists('ordering')){
            $output .= $this->tab(4) . '"ordering" => $' . $this->parcel . '->ordering,' . $this->nl();
        }
        
        $output .= $this->tab(3) . ');' . $this->nl();
        $output .= $this->tab(2) . '}' . $this->nl(2);
        $output .= $this->tab(1) . '}' . $this->nl(2);
        
        $output .= $this->tab() . 'public function controller(){' . $this->nl();
        $output .= $this->tab(2) . '$this->set_view("' . ucwords($this->parcel) . '\\Views\\All");'  . $this->nl();
        $output .= $this->tab(1) . '}' . $this->nl(2);
        
        $output .= '}' . $this->nl(2);
        
        
        $output .= $this->close();
        
        return $output;
        
    }
    
    protected function generate_controller_archive(){
        
        $output = $this->open();
        
        //opening
        $output .= "namespace " . ucwords($this->parcel) . '\Controllers;' . $this->nl(2);
        $output .= "class Archive extends \Manage\Controllers\\Manage{" . $this->nl(2) . $this->tab();
        
        $output .= 'private $' . $this->plural . ';' . $this->nl(2) . $this->tab();
        
        $output .= 'public $' . $this->plural . '_data = array();' . $this->nl() . $this->tab(1);
        $output .= 'public $link_new;' . $this->nl();
        $output .= $this->tab() . 'public $page_title;' . $this->nl();
        $output .= $this->tab() . 'public $has_data = false;' . $this->nl();
        $output .= $this->tab() . 'public $archive_button = false;' . $this->nl(2);
        
        $output .= $this->tab() . 'public function __construct($uri, $data){' . $this->nl();
        $output .= $this->tab(2) . 'parent::__construct($uri, $data);' . $this->nl(2);
        $output .= $this->tab(2) . '$this->page_title = "Deleted '. ucwords($this->plural) .'";' . $this->nl(2);
        
        $output .= $this->tab(2) . '$this->'. $this->plural .' = \\'. ucwords($this->parcel) .'\\Models\\' . ucwords($this->parcel) . '::all(array("conditions" => "deleted = 1"));' . $this->nl();
        $output .= $this->tab(2) . '$this->archive_button = array("url" => ' . $this->ref_model() . '::link_all(), "title" => "Back");' . $this->nl();
        
        $output .= $this->tab(2) . 'foreach($this->' . $this->plural . ' as $' . $this->parcel . '){' . $this->nl();
        $output .= $this->tab(3) . '$this->has_data = true;' . $this->nl();
        $output .= $this->tab(3) . '$this->' . $this->plural . '_data[] = array(' . $this->nl();
        
        foreach($this->fields as $key => $attr){
            if($attr->table_view == true){
                $output .= $this->tab(4) . '"' . $key . '" => $' . $this->parcel .'->' . $key . ',' . $this->nl();
            }
        }
        
        $output .= $this->tab(4) . '"link_restore" => $' . $this->parcel . '->link_restore(),' . $this->nl();
        
        $output .= $this->tab(3) . ');' . $this->nl();
        $output .= $this->tab(2) . '}' . $this->nl(2);
        $output .= $this->tab(1) . '}' . $this->nl(2);
        
        $output .= $this->tab() . 'public function controller(){' . $this->nl();
        $output .= $this->tab(2) . '$this->set_view("' . ucwords($this->parcel) . '\\Views\\All");'  . $this->nl();
        $output .= $this->tab(1) . '}' . $this->nl(2);
        
        $output .= '}' . $this->nl(2);
        
        
        $output .= $this->close();
        
        return $output;
        
    }
    
    protected function generate_controller_edit(){
        $output = $this->open();
        $output .= "namespace " . ucwords($this->parcel) . '\Controllers;' . $this->nl(2);
        $output .= "class Edit extends \Manage\Controllers\\Manage{" . $this->nl(2);
        
        $output .= $this->tab() . 'private $' . $this->parcel . ';' . $this->nl(2);
        $output .= $this->tab() . 'public $page_title;' . $this->nl();
        $output .= $this->tab() . 'public $link_back;' . $this->nl();
        $output .= $this->tab() . 'public $form;' . $this->nl();
        $output .= $this->tab() . 'public $existing;' . $this->nl(2);
        
        $output .= $this->tab() . 'public function __construct($uri, $data){' . $this->nl();
        $output .= $this->tab(2) . 'parent::__construct($uri, $data);' . $this->nl(2);
        
        $output .= $this->tab(2) . 'if(isset($_POST["submit"])){' . $this->nl();
        $output .= $this->tab(3) . '$this->submit_form();' . $this->nl();
        $output .= $this->tab(2) . '}' . $this->nl(2);
        
        $output .= $this->tab(2) . 'if(isset($data["'. $this->parcel .'_id"])){' . $this->nl();
        $output .= $this->tab(3) . '$this->' . $this->parcel . ' = ' . $this->ref_model() . '::find($data["'. $this->parcel .'_id"]);' . $this->nl();
        $output .= $this->tab(3) . '$this->page_title = "Editing ' . ucwords($this->parcel) . '";' . $this->nl();
        $output .= $this->tab(2) . '} else {' . $this->nl();
        $output .= $this->tab(3) . '$this->' . $this->parcel . ' = new ' . $this->ref_model() . ';' . $this->nl();
        $output .= $this->tab(3) . '$this->page_title = "New ' . ucwords($this->parcel) . '";' . $this->nl();
        $output .= $this->tab(2) . '}' . $this->nl(2);
        
        $output .= $this->tab(2) . '$this->link_back = $this->' . $this->parcel . '->link_all();' . $this->nl(2);
        
        $output .= $this->tab(2) . '$form = new \Form\Models\Form("edit", "POST", "", "well");' . $this->nl(2);
        
        foreach($this->fields as $key => $attr){
            if($attr->edit_view == true || (isset($attr->pk) && $attr->pk == true)){
                
                switch($attr->type){
                    case "int":
                        if(isset($attr->pk) && $attr->pk == true){
                            $type = 'Hidden';
                        } else {
                            $type = 'Text';
                        }
                        break;
                    case "varchar":
                        $type = 'Text';
                        break;
                    case "text":
                        $type = 'Textarea';
                        break;
                    default:
                        $type = 'Text';
                        break;
                }
                
                $output .= $this->tab(2) .'$item = new \Form\Models\FormItem' . $type . '(array("id" => "'. $key .'", "title" => "'. $attr->presentation .'", "value" => $this->'. $this->parcel . '->' . $key .'));'  . $this->nl();
                $output .= $this->tab(2) . '$form->add_item($item);' . $this->nl(2);
            }
        }
        
        $output .= $this->tab(2) . '$item = new \Form\Models\FormItemSubmit(array("value" => "Save", "id" => "submit", "class" => "btn btn-primary", "cancel_url" => $this->' . $this->parcel . '->link_all()));' . $this->nl();
        $output .= $this->tab(2) . '$form->add_item($item);' . $this->nl(2);

        $output .= $this->tab(2) . '$this->form = $form->render();' . $this->nl(2);
        
        $output .= $this->tab() . '}' . $this->nl(2);
        
        $output .= $this->tab() . 'public function controller(){' . $this->nl();
        $output .= $this->tab(2) . '$this->set_view("'. ucwords($this->parcel) .'\Views\Edit");' . $this->nl();
        $output .= $this->tab() . '}' . $this->nl(2);
        
        $output .= $this->tab() . 'private function submit_form(){' . $this->nl();
        $output .= $this->tab(2) . 'if($_POST["id"] > 0){' . $this->nl();
        
        $output .= $this->tab(3) . '$' . $this->parcel . ' = ' . $this->ref_model() . '::find($_POST["id"]);' . $this->nl();
        $output .= $this->tab(3) . '$flash_msg = "' . ucwords($this->parcel) . ' has been updated successfully.";' . $this->nl();  
        
        $output .= $this->tab(2) . '} else {' . $this->nl();
        
        $output .= $this->tab(3) . '$' . $this->parcel . ' = new ' . $this->ref_model() . ';' . $this->nl();
        $output .= $this->tab(3) . '$flash_msg = "' . ucwords($this->parcel) . ' has been created successfully.";' . $this->nl();  
        $output .= $this->tab(3) . '$'. $this->parcel . '->ordering = '. $this->ref_model() . '::get_next_ordering_number();' . $this->nl();
        $output .= $this->tab(2) . '}' . $this->nl(2);
        
        
        foreach($this->fields as $key => $attr){
            if($attr->edit_view){
              $output .= $this->tab(2) . '$' . $this->parcel . '->' . $key . ' = $_POST["'. $key .'"];' . $this->nl();
            }
        }
        
        $output .= $this->nl() . $this->tab(2) . '$' . $this->parcel . '->save();' . $this->nl();
        $output .= $this->tab(2) . '$this->add_flash(array("message" => $flash_msg, "type" => "success", "Heading" => "Great!"));' . $this->nl();
        $output .= $this->tab(2) . 'redirect_to($' . $this->parcel . '->link_all());' . $this->nl();        
        
        $output .= $this->tab() . '}' . $this->nl(2);

        
        $output .= '}' . $this->nl();
        
        $output .= $this->close();
        
        return $output;
        
        
    }
    
    
    
    protected function generate_controller_delete(){
        $output = $this->open();
        $output .= "namespace " . ucwords($this->parcel) . '\Controllers;' . $this->nl(2);
        $output .= "class Delete extends \Manage\Controllers\\Manage{" . $this->nl(2);
        
        $output .= $this->tab() . 'public function __construct($uri, $data){' . $this->nl();
        $output .= $this->tab(2) . 'parent::__construct($uri, $data);' . $this->nl(2);
        $output .= $this->tab(2) . '$' . $this->parcel . ' = ' . $this->ref_model() . '::find($data["'. $this->parcel .'_id"]);' . $this->nl(2);
        $output .= $this->tab(2) . 'if($'. $this->parcel .'){' . $this->nl();
        $output .= $this->tab(3) . '$' . $this->parcel . '->deleted = 1;' . $this->nl();
        $output .= $this->tab(3) . '$' . $this->parcel . '->save();' . $this->nl();
        $output .= $this->tab(3) . '$this->add_undo(array("message" => "'. ucwords($this->parcel) .' has been deleted!", "action" => $'. $this->parcel .'->link_restore()));' . $this->nl(2);
        
        $output .= $this->tab(3) . 'redirect_to($'. $this->parcel .'->link_all());' . $this->nl();
        $output .= $this->tab(2) . '}' . $this->nl();
        $output .= $this->tab() . '}' . $this->nl();
        $output .= '}' . $this->nl();
        
        $output .= $this->close();
        
        return $output;
        
        
    }
    
    protected function generate_controller_restore(){
        $output = $this->open();
        $output .= "namespace " . ucwords($this->parcel) . '\Controllers;' . $this->nl(2);
        $output .= "class Restore extends \Manage\Controllers\\Manage{" . $this->nl(2);
        
        $output .= $this->tab() . 'public function __construct($uri, $data){' . $this->nl();
        $output .= $this->tab(2) . 'parent::__construct($uri, $data);' . $this->nl(2);
        $output .= $this->tab(2) . '$' . $this->parcel . ' = ' . $this->ref_model() . '::find($data["'. $this->parcel .'_id"]);' . $this->nl(2);
        $output .= $this->tab(2) . 'if($'. $this->parcel .'){' . $this->nl();
        $output .= $this->tab(3) . '$' . $this->parcel . '->deleted = 0;' . $this->nl();
        $output .= $this->tab(3) . '$' . $this->parcel . '->save();' . $this->nl();
        $output .= $this->tab(3) . '$this->add_flash(array("message" => "'. ucwords($this->parcel) .' has been restored!"));' . $this->nl(2);
        
        $output .= $this->tab(3) . 'redirect_to($'. $this->parcel .'->link_all());' . $this->nl();
        $output .= $this->tab(2) . '}' . $this->nl();
        $output .= $this->tab() . '}' . $this->nl();
        $output .= '}' . $this->nl();
        
        $output .= $this->close();
        
        return $output;
        
        
    }


    protected function generate_controller_reorder(){
        $output = $this->open();
        $output .= "namespace " . ucwords($this->parcel) . '\Controllers;' . $this->nl(2);
        $output .= "class Reorder extends \Core\Controllers\\Ajax_Controller{" . $this->nl(2);
        
        $output .= $this->tab() . 'public function __construct($uri, $data){' . $this->nl();
        $output .= $this->tab(2) . 'parent::__construct($uri, $data);' . $this->nl(2);
        $output .= $this->tab(2) . '$this_item = ' . $this->ref_model() . '::find($data["this_id"]);' . $this->nl(2);
        $output .= $this->tab(2) . '$swap_item = ' . $this->ref_model() . '::find($data["swap_id"]);' . $this->nl(2);
        $output .= $this->tab(2) . 'if($this_item && $swap_item){' . $this->nl();

        $output .= $this->tab(3) . 'list($this_item->ordering, $swap_item->ordering) = array($swap_item->ordering, $this_item->ordering);' . $this->nl();
    
        $output .= $this->tab(3) . '$this_item->save();' . $this->nl(2);
        $output .= $this->tab(3) . '$swap_item->save();' . $this->nl(2);
        
        $output .= $this->tab(2) . '}' . $this->nl();
        $output .= $this->tab() . '}' . $this->nl();
        $output .= '}' . $this->nl();
        
        $output .= $this->close();
        
        return $output;
        
        
    }
    
    
    protected function generate_view_all(){
        
        $output = $this->open(false);
        $output .= '{{{view.manage_header}}}' . $this->nl(2);
        
        $output .= $this->lt() . 'div class="container"' . $this->gt() . $this->nl(2);
        $output .= $this->lt() . 'div class="row header"' . $this->gt() . $this->nl();
        $output .= $this->tab() . $this->lt() . 'div class="span6"' . $this->gt() . '{{#page_title}}'. $this->lt() .'h2'. $this->gt() .'{{.}}'. $this->lt() .'/h2'. $this->gt() .'{{/page_title}}' . $this->lt() . '/div' . $this->gt() . $this->nl();
        $output .= $this->tab() . $this->lt() . 'div class="span6 right"' . $this->gt() . '{{#link_new}}' . $this->lt() . 'a href="{{.}}" class="btn btn-large"' . $this->gt() . 'Create New ' . ucwords($this->parcel) . ' ' . $this->lt() . 'span class="icon-plus"' . $this->gt() . $this->lt() . '/span' . $this->gt() . $this->lt() . '/a' . $this->gt() . '{{/link_new}}' . $this->lt() .'/div' . $this->gt() . $this->nl();
        $output .= $this->lt() . '/div' . $this->gt() . $this->nl(2);
        
        $output .= '{{#has_data}}' . $this->nl();
        $output .= '{{{view.'. $this->parcel .'_table}}}' . $this->nl();
        $output .= '{{/has_data}}' . $this->nl(2);
        
        $output .= '{{^has_data}}' . $this->nl();
        $output .= $this->lt() . 'div class="alert alert-info"' . $this->gt() . $this->nl();
        $output .= $this->tab() . $this->lt() . 'strong' . $this->gt() . 'There are no ' . $this->plural . ' yet.' . $this->lt() . '/strong' . $this->gt() . ' Create one ' . $this->lt() . 'a href="{{link_new}}"' . $this->gt() . 'now' . $this->lt() . '/a' . $this->gt() . '!' . $this->nl();
        $output .= $this->lt() . '/div' . $this->gt() . $this->nl();
        $output .= '{{/has_data}}' . $this->nl(2);
        
        $output .= '{{#archive_button}}' . $this->nl();
        $output .= $this->lt() . 'a href="{{url}}" class="btn btn-mini"' . $this->gt() . '{{title}}' . $this->lt() . '/a' . $this->gt() . $this->nl();
        $output .= '{{/archive_button}}' . $this->nl(2);
        
        $output .= $this->lt() . '/div' . $this->gt() . $this->nl(2);
        $output .= '{{{view.manage_footer}}}';
        $output .= $this->close();
        
        return $output;
        
    }
    
    
    protected function generate_view_table(){
        
        $output = $this->open(false);
        
        $output .= $this->lt() . 'table class="table table-striped" width="100%"' . $this->gt() .$this->nl();
        $output .= $this->tab() . $this->lt() . 'thead' . $this->gt() . $this->nl();
        $output .= $this->tab(2) . $this->lt() . 'tr' . $this->gt() . $this->nl();
        
        foreach($this->fields as $key => $attr){
            if($attr->table_view == true){
                $output .= $this->tab(3) . $this->lt() . 'th' . $this->gt() . $attr->presentation . $this->lt() . '/th' . $this->gt() . $this->nl();
            }
        }
        
        $output .= $this->tab(3) . $this->lt() . 'th width="230"' . $this->gt() . 'Actions' . $this->lt() . '/th' . $this->gt() . $this->nl();
    
        $output .= $this->tab(2) . $this->lt() . '/tr' . $this->gt() . $this->nl();
        $output .= $this->tab(1) . $this->lt() . '/thead' . $this->gt() . $this->nl();
        $output .= $this->tab(1) . $this->lt() . 'tbody' . $this->gt() . $this->nl();
        
        $output .= $this->tab(1) . '{{#' . $this->plural . '_data}}' . $this->nl();
        
        $output .= $this->tab(2) . $this->lt() . 'tr rel="{{id}}"' . $this->gt() . $this->nl();
        
        foreach($this->fields as $key => $attr){
            if($attr->table_view == true){
                $output .= $this->tab(3) . $this->lt() . 'td' . $this->gt() . '{{'. $key .'}}' . $this->lt() . '/td' . $this->gt() . $this->nl();
            }
        }

        $output .= $this->tab(3) . $this->lt() . 'td' . $this->gt() . $this->nl();
        
        $output .= $this->tab(4) . '{{#link_view}}' . $this->nl();
        $output .= $this->tab(4) . $this->lt() . 'a href="{{.}}" class="btn"' . $this->gt() . 'View' . $this->lt() . '/a' . $this->gt() . $this->nl();
        $output .= $this->tab(4) . '{{/link_view}}' . $this->nl(2);
        
        $output .= $this->tab(4) . '{{#link_edit}}' . $this->nl();
        $output .= $this->tab(4) . $this->lt() . 'a href="{{.}}" class="btn btn-primary"' . $this->gt() . 'Edit' . $this->lt() . '/a' . $this->gt() . $this->nl();
        $output .= $this->tab(4) . '{{/link_edit}}' . $this->nl(2);
        
        $output .= $this->tab(4) . '{{#link_delete}}' . $this->nl();
        $output .= $this->tab(4) . $this->lt() . 'a href="{{.}}" class="btn btn-danger"' . $this->gt() . 'Delete' . $this->lt() . '/a' . $this->gt() . $this->nl();
        $output .= $this->tab(4) . '{{/link_delete}}' . $this->nl(2);
        
        $output .= $this->tab(4) . '{{#link_restore}}' . $this->nl();
        $output .= $this->tab(4) . $this->lt() . 'a href="{{.}}" class="btn btn-primary"' . $this->gt() . 'Restore' . $this->lt() . '/a' . $this->gt() . $this->nl();
        $output .= $this->tab(4) . '{{/link_restore}}' . $this->nl(2);

        //reorder
        $output .= $this->tab(4) .  $this->lt() . 'a href="#" class="reorder up btn" rel=' . $this->plural . $this->gt() . $this->lt() . 'span class="icon-arrow-up"' . $this->gt() . $this->lt() . '/span' .$this->gt() . $this->lt() . '/a' . $this->gt() . $this->nl();
        $output .= $this->tab(4) .  $this->lt() . 'a href="#" class="reorder down btn" rel=' . $this->plural . ' style="float:right;"' . $this->gt() . $this->lt() . 'span class="icon-arrow-down"' . $this->gt() . $this->lt() . '/span' .$this->gt() . $this->lt() . '/a' . $this->gt() . $this->nl();

        $output .= $this->tab(3) . $this->lt() . '/td' . $this->gt() . $this->nl();
        
        $output .= $this->tab(2) . $this->lt() . '/tr' . $this->gt() . $this->nl();
        $output .= $this->tab(1) . '{{/' . $this->plural . '_data}}' . $this->nl();
        $output .= $this->tab(1) . $this->lt() . '/tbody' . $this->gt() . $this->nl();
        $output .= $this->lt() . '/table' . $this->gt();
        
        
        $output .= $this->close();
        
        return $output;
        
        
    }
    
    protected function generate_view_edit(){

        $output = $this->open(false);
        $output .= '{{{view.manage_header}}}' . $this->nl(2);
        
        $output .= $this->lt() . 'div class="container"' . $this->gt() . $this->nl(2);
        $output .= $this->lt() . 'div class="row header"' . $this->gt() . $this->nl();
        $output .= $this->tab() . $this->lt() . 'div class="span6"' . $this->gt() . '{{#page_title}}'. $this->lt() .'h2'. $this->gt() .'{{.}}'. $this->lt() .'/h2'. $this->gt() .'{{/page_title}}' . $this->lt() . '/div' . $this->gt() . $this->nl();
        $output .= $this->tab() . $this->lt() . 'div class="span6 right"' . $this->gt() . '{{#link_back}}' . $this->lt() . 'a href="{{.}}" class="btn btn-large"' . $this->gt() . $this->lt() . 'span class="icon-arrow-left"' . $this->gt() . $this->lt() . '/span' . $this->gt() . ' Back' .$this->lt() . '/a' . $this->gt() . '{{/link_back}}' . $this->lt() .'/div' . $this->gt() . $this->nl();
        $output .= $this->lt() . '/div' . $this->gt() . $this->nl(2);
        
        $output .= '{{{form}}}' . $this->nl(2);
        
        $output .= '{{{view.manage_footer}}}';

        $output .= $this->close();
        
        return $output;
        
        
    }

    
    protected function generate_routes(){
        $output = $this->open(false);
        
        $output .= '"manage/' . $this->plural . '" : "' . ucwords($this->parcel). '\\\Controllers\\\All",' . $this->nl(); 
        $output .= '"manage/' . $this->plural . '/all" : "' . ucwords($this->parcel). '\\\Controllers\\\All",' . $this->nl(); 
        $output .= '"manage/' . $this->plural . '/new" : "' . ucwords($this->parcel). '\\\Controllers\\\Edit",' . $this->nl(); 
        $output .= '"manage/' . $this->plural . '/edit/:'. $this->parcel .'_id" : "' . ucwords($this->parcel). '\\\Controllers\\\Edit",' . $this->nl(); 
        $output .= '"manage/' . $this->plural . '/delete/:'. $this->parcel .'_id" : "' . ucwords($this->parcel). '\\\Controllers\\\Delete",' . $this->nl(); 
        $output .= '"manage/' . $this->plural . '/restore/:'. $this->parcel .'_id" : "' . ucwords($this->parcel). '\\\Controllers\\\Restore",' . $this->nl(); 
        $output .= '"manage/' . $this->plural . '/archive" : "' . ucwords($this->parcel). '\\\Controllers\\\Archive"' . $this->nl(); 
   
        $output .= $this->close();
        return $output;
        
    }
    
    protected function generate_routes_array(){
        
        $routes['manage/' . $this->plural]                                          = ucwords($this->parcel). '\\\Controllers\\\All';
        $routes['manage/' . $this->plural . '/all']                                 = ucwords($this->parcel) . '\\\Controllers\\\All';
        $routes['manage/' . $this->plural . '/new']                                 = ucwords($this->parcel). '\\\Controllers\\\Edit';
        $routes['manage/' . $this->plural . '/edit/:' . $this->parcel .'_id']       = ucwords($this->parcel). '\\\Controllers\\\Edit';
        $routes['manage/' . $this->plural . '/delete/:'. $this->parcel .'_id']      = ucwords($this->parcel). '\\\Controllers\\\Delete';
        $routes['manage/' . $this->plural . '/restore/:'. $this->parcel .'_id']     = ucwords($this->parcel). '\\\Controllers\\\Restore';
        $routes['manage/' . $this->plural . '/archive']                             = ucwords($this->parcel). '\\\Controllers\\\Archive';
        
        return $routes;
        
    }
    
    
    protected function generate_sql(){
        $output = $this->open(false);
    
        $output .= 'CREATE TABLE `'. $this->plural .'` (' . $this->nl();
        foreach($this->fields as $key => $attr){
            
            if($attr->type == 'text'){
                $output .= $this->tab() . '`'. $key .'` ' . $attr->type . ' NOT NULL';
            } else {
                $output .= $this->tab() . '`'. $key .'` ' . $attr->type . '('. $attr->size .') NOT NULL';
            
            }
            if(isset($attr->pk) && $attr->pk == true ){
                $output .= ' AUTO_INCREMENT';
                $pk = $key;
            }
            
            if(isset($attr->default)){
                $output .= " DEFAULT ". $attr->default;
            }
            $output .= ',' . $this->nl();
        }
        
        if($pk){
            $output .= $this->tab() . 'PRIMARY KEY(`'. $pk .'`)' . $this->nl();
        }
        
        $output .= ');';
        
    
        $output .= $this->close(false);

        $this->sqlOutput = $output;

        return $output;
    }   
    
    
}