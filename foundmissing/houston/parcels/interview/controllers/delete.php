<?php
namespace Interview\Controllers;

class Delete extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$interview = \Interview\Models\Interview::find($data["interview_id"]);

		if($interview){
			$interview->deleted = 1;
			$interview->save();
			$this->add_undo(array("message" => "Interview has been deleted!", "action" => $interview->link_restore()));

			redirect_to($interview->link_all());
		}
	}
}
