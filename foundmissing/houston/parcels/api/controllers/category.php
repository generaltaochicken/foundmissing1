<?php
namespace Api\Controllers;

class Category extends \Api\Controllers\Api{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->response = $this->get_category();

		$this->send_response();

	}

	protected function get_category(){

		try {
			$item = \Category\Models\Category::find($this->data['category_id']);
		} 
		catch (\ActiveRecord\RecordNotFound $e){
			return false;
		}
		
		$array = array(
			"id" => $item->id,
			"parent_id" => $item->parent_id,
			"title" => $item->title,
			"slug" => $item->slug,
			"attachment_id" => $item->attachment_id
		);
	
		return $array;
		
	}




}