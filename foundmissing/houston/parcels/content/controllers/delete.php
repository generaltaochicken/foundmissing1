<?php
namespace Content\Controllers;

class Delete extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$content = \Content\Models\Content::find($data["content_id"]);

		if($content){
			$content->deleted = 1;
			$content->save();
			$this->add_undo(array("message" => "Content has been deleted!", "action" => $content->link_restore()));

			redirect_to($content->link_all());
		}
	}
}
