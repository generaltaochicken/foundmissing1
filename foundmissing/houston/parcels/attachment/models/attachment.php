<?php
namespace Attachment\Models;

class Attachment extends \Core\Models\Base_Model{

	protected static $manage_prefix = "/manage/attachments/";

	public function link_edit(){
		return self::$manage_prefix . "edit/" . $this->id;
	}

	public function link_delete(){
		return self::$manage_prefix . "delete/" . $this->id;
	}

	public function link_restore(){
		return self::$manage_prefix . "restore/" . $this->id;
	}

	public function link_new(){
		return self::$manage_prefix . "new";
	}

	public function link_all(){
		return self::$manage_prefix;
	}

	public function link_archive(){
		return self::$manage_prefix . "archive";
	}

	public function get_frontend_data(){
		return array(
			"id" => $this->id,
			"type" => $this->type,
			"image_filename" => $this->image_filename,
			"extension" => $this->extension,
			"entry_datetime" => $this->entry_datetime,
			"title" => $this->title,
			"caption" => $this->caption,
		);
	}
	
	public function get_cover_attachment($id) {
		$attachment = \Attachment\Models\Attachment::first(array('select' => 'image_filename', 'conditions' => 'id = "' . $id . '"'));
		
		return $attachment->image_filename;
	}
}