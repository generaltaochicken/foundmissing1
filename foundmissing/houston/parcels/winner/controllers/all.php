<?php
namespace Winner\Controllers;

class All extends \Manage\Controllers\Manage{

	private $winners;

	public $winners_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Winners";
		$this->link_new = \Winner\Models\Winner::link_new();

		$this->winners = \Winner\Models\Winner::all(array("conditions" => "deleted = 0" , "order" => "ordering"));
		$archived = \Winner\Models\Winner::count(array("conditions" => "deleted = 1"));
		if($archived > 0){
			$this->archive_button = array("url" => \Winner\Models\Winner::link_archive(), "title" => "Archive");
		}

		foreach($this->winners as $winner){
			$this->has_data = true;
			$this->winners_data[] = array(
				"id" => $winner->id,
				"user_id" => $winner->user_id,
				"full_name" => \User\Models\User::get_user_fullname_by_id($winner->user_id),
				"email" => \User\Models\User::get_user_email_by_id($winner->user_id),
				"code" => \User\Models\User::get_user_code_by_id($winner->user_id),
				"entry_datetime" => $winner->entry_datetime,
				"event" => $winner->event,
				"prize" => $winner->prize,
				"link_edit" => $winner->link_edit(),
				"link_delete" => $winner->link_delete(),
				"ordering" => $winner->ordering,
			);
		}

	}

	public function controller(){
		$this->set_view("Winner\Views\All");
	}

}

