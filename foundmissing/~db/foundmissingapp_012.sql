-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 20, 2014 at 01:58 PM
-- Server version: 5.5.32
-- PHP Version: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `foundmissingapp`
--
CREATE DATABASE IF NOT EXISTS `foundmissingapp` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `foundmissingapp`;

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entry_datetime` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '1',
  `author_name` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `excerpt` text NOT NULL,
  `body` text NOT NULL,
  `bg_image_header` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `entry_datetime`, `deleted`, `ordering`, `author_name`, `title`, `slug`, `excerpt`, `body`, `bg_image_header`) VALUES
(1, 1403052526, 0, 0, 'Mike Nguyen', 'Bricks', 'bricks', 'Nintendo of America president Reggie Fils-Aime attended my wedding. His date was Piper Chapman, the main character from Orange is the New Black, who''s also really good friends with my one-and-a-half year old daughter Charlotte. My kid lives on her own, in an apartment that looks like the bottom of the ocean, and just this morning she got really excited when I gave her a rhino horn to wear on her head.', '<h1>Nintendo</h1> of America president Reggie Fils-Aime attended my wedding. His date was Piper Chapman, the main character from Orange is the New Black, who''s also really good friends with my one-and-a-half year old daughter Charlotte. My kid lives on her own, in an apartment that looks like the bottom of the ocean, and just this morning she got really excited when I gave her a rhino horn to wear on her head.\n\nTomodachi Life for the Nintendo 3DS is the kind of game that''s hard to explain. Even calling it a â€œgameâ€ seems wrong. It looks a bit like The Sims starring your Mii avatar, but really it''s a mixture of a virtual pet and a toy â€” sort of like a Tamagotchi filled with tiny humans that like to play games. Itâ€™s also unlike anything youâ€™ve ever played. Nintendo is typically a conservative company that relies on sequels to known franchises like Mario Kart and The Legend of Zelda. But itâ€™s also a developer that knows how to get weird with games like WarioWare, and Tomodachi Life might just be the weirdest thing it''s ever made.', 'IMG_8670.jpg'),
(2, 1402015315, 0, 1, 'Arthur Jones', 'Brooklyn', 'brooklyn', 'They said that they couldnâ€™t restock the Brooklyn  sneakers fast enough. The Italians have long been lovers of â€œlo stile Inglese,â€ or English sartorial style, with its tweeds, herringbone fabrics and pops of silk and print. The English are returning the compliment this week with â€œThe Glamour of Italian Fashion 1945-2014,â€ a show dedicated to postwar Italian style at the Victoria and Albert Museum. The tightly edited show â€” there are only 100 pieces of menâ€™s and womenâ€™s fashion on display â€” is an earnest and ', 'They said that they couldnâ€™t restock the Brooklyn  sneakers fast enough. The Italians have long been lovers of â€œlo stile Inglese,â€ or English sartorial style, with its tweeds, herringbone fabrics and pops of silk and print. The English are returning the compliment this week with â€œThe Glamour of Italian Fashion 1945-2014,â€ a show dedicated to postwar Italian style at the Victoria and Albert Museum. The tightly edited show â€” there are only 100 pieces of menâ€™s and womenâ€™s fashion on display â€” is an earnest and enthusiastic tale of the rapid journey Italy made from postwar ruin to modern-day prosperitÃ . The show begins with a large, black-and-white image of Florence in 1946, all rubble and crumbling buildings, and ends with a peacockâ€™s display of  opulence including the liquid white gown and showy gold dragon brooch from Tom Fordâ€™s fall 2004 Gucci collection, and Roberto Capucciâ€™s rippling fuchsia dress. In between thereâ€™s jewelry, fur, cashmere, glitter and Pucci prints galore. On display are some of the Bulgari gems that Richard Burton used to woo Elizabeth Taylor while the two were filming â€œCleopatraâ€ in Rome the early Sixties. Thereâ€™s also a display dedi-\r\ncated to the seminal Sala Bianca fashion shows at Palazzo Pitti in Florence; a nod to menâ€™s tailoring, and myriad examples of Italyâ€™s technical prowess â€” from industrially produced knitwear. There are a myriad of examples that display Italyâ€™s technical prowess. In between thereâ€™s jewelry, fur, cashmere, glitter and Pucci prints galore. On display are \r\n\r\nsome of the Bulgari gems that Richard Burton used to woo Elizabeth Taylor while the two \r\n\r\nwere filming â€œCleopatraâ€ in Rome the early Sixties. Thereâ€™s also a display dedicated to the \r\n\r\nseminal Sala Bianca fashion shows at Palazzo Pitti in Florence; a nod to menâ€™s tailoring, \r\n\r\nand myriad examples of Italyâ€™s technical prowess â€” from industrially produced knitwear \r\n\r\nto handcrafted Fendi furs. On display are some of the Bulgari gems that Richard Burton \r\n\r\nused to woo Elizabeth Taylor while the two were filming everybody who walked on by.', 'site_2.jpg'),
(3, 1402115315, 0, 2, 'Dwayne Wade', 'Wade', 'wade', 'Dwyane Tyrone Wade, Jr. (born January 17, 1982) is an American professional basketball player who currently plays for the Miami Heat of the NBA. He has established himself as one of the most well-known and popular players in the league. Wade had the top selling jersey in the NBA for nearly two years, leading the NBA in jersey sales from the 2005 NBA Playoffs, until the midpoint of the 2006â€“07 season.[1] His first name is pronounced /dwÉ›Éªn/, the same as the more common spellings "Duane" and "Dwayne"', 'Dwyane Tyrone Wade, Jr. (born January 17, 1982) is an American professional basketball player who currently plays for the Miami Heat of the NBA. He has established himself as one of the most well-known and popular players in the league. Wade had the top selling jersey in the NBA for nearly two years, leading the NBA in jersey sales from the 2005 NBA Playoffs, until the midpoint of the 2006â€“07 season.[1] His first name is pronounced /dwÉ›Éªn/, the same as the more common spellings "Duane" and "Dwayne".[2]\r\n\r\nAfter entering the league as the fifth pick in the 2003 NBA Draft, Wade was named to the All-Rookie team and the All-Star team the following ten seasons. In his third season, Wade led the Miami Heat to their first NBA championship in franchise history. He was named the 2006 NBA Finals MVP as the Heat won the series 4-2 over the Dallas Mavericks. At the 2008 Summer Olympics, Wade led the United States men''s basketball team, commonly known as the "Redeem Team", in scoring, and helped them captured gold medal honors in Beijing, China. In the 2008â€“09 season, Wade led the league in scoring and earned his first NBA scoring title.\r\n\r\nAfter Lebron James and Chris Bosh joined the Heat, Wade was part of Miami''s second championship win in the 2012 NBA Finals, when Miami defeated the Oklahoma City Thunder. He won his third NBA championship in 2013, when the Heat defeated the San Antonio Spurs in the 2013 NBA Finals.', 'features2.jpg'),
(4, 1402135315, 0, 3, 'Marlon Wayans', 'Tokyo fresh', 'tokyo-fresh', 'TOKYO-One of central Tokyo''s largest new developments has almost fully filled its office space before opening its doors next week, underscoring strength in the city''s property market. Mori Building Co., a major Japanese developer known for projects such as Roppongi Hills and the Shanghai World Financial Center, said Wednesday it has completed the multiuse development, called Toranomon Hills. Mori Building said it has invested 140 billion yen ($1.36 billion) in the project.', 'TOKYO-One of central Tokyo''s largest new developments has almost fully filled its office space before opening its doors next week, underscoring strength in the city''s property market.\r\n\r\nMori Building Co., a major Japanese developer known for projects such as Roppongi Hills and the Shanghai World Financial Center, said Wednesday it has completed the multiuse development, called Toranomon Hills. Mori Building said it has invested 140 billion yen ($1.36 billion) in the project.\r\n\r\nTokyo''s property market has revived recently, supported by Prime Minister Shinzo Abe''s economic programs. Demand for office space is rising as companies expand, and the amount of space available for rent has fallen sharply in recent months, bringing rents up. Relatively few new buildings are coming on the market, helping tighten conditions.\r\n\r\nToranomon Hills, located a few minutes'' walk from the U.S. Embassy and just south of the central government district, also houses retail shops, luxury private residences, conference space and a 164-room hotel carrying the Andaz brand name of Hyatt Hotels Corp. H +0.05%  The complex stands on a recently completed highway extension that will eventually connect it to Tokyo Bay where many Olympic events are planned in 2020.', 'tumblr_n106y2ngIF1qiprgbo1_1280.jpg'),
(5, 1402135315, 0, 4, 'Sean Carter', 'Jigga', 'jigga', 'The track-listing for the soundtrack to the upcoming Baz Luhrmann pic, The Great Gatsby, has finally been revealed Andre 3000 and Beyonce would be covering Amy Winehouseâ€™s 2006 Back To Black single along with other major names.\r\n\r\nThe soundtrack opener comes from executive producer Jay-Z, himself, on a track called 100$ Bill. Other guests include Q-Tip, Lana Del Rey, Bryan Ferry (Roxy Music)  and Florence + The Machine.\r\n\r\nThe film premieres on May 10, with the soundtrack dropping on May 7th.', 'The track-listing for the soundtrack to the upcoming Baz Luhrmann pic, The Great Gatsby, has finally been revealed Andre 3000 and Beyonce would be covering Amy Winehouseâ€™s 2006 Back To Black single along with other major names.\r\n\r\nThe soundtrack opener comes from executive producer Jay-Z, himself, on a track called 100$ Bill. Other guests include Q-Tip, Lana Del Rey, Bryan Ferry (Roxy Music)  and Florence + The Machine.\r\n\r\nThe film premieres on May 10, with the soundtrack dropping on May 7th.', 'jay-z.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `attachments`
--

CREATE TABLE IF NOT EXISTS `attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL,
  `image_filename` varchar(255) NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `entry_datetime` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `caption` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `attachments`
--

INSERT INTO `attachments` (`id`, `type`, `image_filename`, `ordering`, `entry_datetime`, `deleted`, `title`, `caption`) VALUES
(1, 'image', '1987ab89dkdy.jpg', 1, 1402966937, 0, 'main', 'adfadgadf'),
(2, 'image', 'dnzo92479527.jpg', 1, 1402973439, 0, 'main_image', 'lebron 11'),
(3, 'image', '870ey1zhsv4p.jpg', 1, 1403030839, 0, 'main_image', ''),
(4, 'image', 'focnnc83leul.jpg', 1, 1403031203, 0, 'main_image', ''),
(5, 'image', 'l3xzy4ykssap.jpg', 1, 1403031642, 0, 'main_image', ''),
(6, 'image', 'hoffk8yl0hzw.jpeg', 1, 1403031988, 0, 'main_image', '');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE IF NOT EXISTS `brands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `description` text,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `title`, `description`, `ordering`, `deleted`) VALUES
(1, 'Air Jordan', '', 0, 0),
(2, 'Converse', 'These are the best shoes!', 1, 0),
(3, 'Timberland', 'Makin boots', 2, 0),
(4, 'Supra', 'fun hightops!', 3, 0),
(5, 'Nike', '', 4, 0),
(6, 'Clarks', '', 5, 0),
(7, 'Rockport', '', 6, 0),
(8, 'Adidas', '', 7, 0),
(9, 'Allen Edmonds', '', 8, 0),
(10, 'K-Swiss', '', 9, 0),
(11, 'Android Homme', '', 10, 0),
(12, 'Kriss Kross', '', 11, 1),
(13, 'New Balance', '', 12, 0),
(14, 'Giuseppe Zanotti', '', 13, 0),
(15, 'Gucci', '', 14, 0);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `slug` varchar(100) DEFAULT NULL,
  `attachment_id` int(11) DEFAULT NULL,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `entry_datetime` int(11) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `parent_id`, `slug`, `attachment_id`, `ordering`, `entry_datetime`, `deleted`) VALUES
(1, 'Footwear', 0, 'footwear', 49, 0, 0, 0),
(2, 'Fashion', 0, 'fashion', 0, 1, 0, 0),
(3, 'Lifestyle', 0, 'lifestyle', 0, 2, 0, 0),
(4, 'Design', 0, 'design', 0, 3, 0, 0),
(5, 'Miscellaneous', 0, 'miscellaneous', 0, 4, NULL, 1),
(6, 'Random', 0, 'random', 0, 5, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

CREATE TABLE IF NOT EXISTS `colors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `hex` varchar(10) DEFAULT NULL,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `colors`
--

INSERT INTO `colors` (`id`, `title`, `hex`, `ordering`, `deleted`) VALUES
(1, 'Black', '#000000', 0, 0),
(2, 'White', '#ffffff', 2, 0),
(3, 'Red', '#FF0000', 3, 0),
(4, 'Beige', '#F7FCC5', 4, 0),
(5, 'Brown', '#8C6246', 5, 0),
(6, 'Silver', '#E0E0E0', 6, 0),
(7, 'Blue', '#0000FF', 1, 0),
(8, 'Purple ', '#FF00FF', 8, 0),
(9, 'Orange', '#FF9100', 9, 0),
(10, 'Infrared ', '#EB1C37', 10, 0),
(11, 'Lime Green ', '#30F000', 11, 0),
(12, 'Royal Blue ', '#4169E1', 12, 0),
(13, 'Navy Blue ', '#000080', 13, 0),
(14, 'Grey ', '#404040', 7, 0),
(15, 'Plum', '#ED1F9A', 14, 0);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(6) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comment_body` varchar(255) NOT NULL,
  `replied_to` int(6) NOT NULL DEFAULT '0',
  `sub_replied_to` int(6) DEFAULT NULL,
  `entry_datetime` int(11) NOT NULL,
  `update_datetime` int(11) NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `post_type` int(1) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `post_type` (`post_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `parent_id`, `user_id`, `comment_body`, `replied_to`, `sub_replied_to`, `entry_datetime`, `update_datetime`, `ordering`, `post_type`, `deleted`) VALUES
(30, 5, 12, 'It''s Jay Hova!', 0, NULL, 1403217425, 0, 1, 1, 0),
(31, 5, 15, 'Nah b, it''s Jigga', 30, 0, 1403217445, 0, 1, 1, 0),
(32, 5, 17, 'I''M BATMAN', 30, 31, 1403217562, 0, 1, 1, 0),
(33, 5, 12, 'Crazy foo...', 30, 32, 1403217769, 0, 1, 1, 0),
(34, 5, 15, 'Comment on the article...', 0, NULL, 1403220105, 0, 1, 1, 0),
(35, 5, 17, 'I already did', 34, 0, 1403226471, 0, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `comment_karmas`
--

CREATE TABLE IF NOT EXISTS `comment_karmas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(1) NOT NULL DEFAULT '0',
  `parent_id` int(8) NOT NULL,
  `comment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `entry_datetime` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `ordering` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=54 ;

--
-- Dumping data for table `comment_karmas`
--

INSERT INTO `comment_karmas` (`id`, `type`, `parent_id`, `comment_id`, `user_id`, `entry_datetime`, `deleted`, `ordering`) VALUES
(51, -1, 5, 32, 12, 1403217812, 0, 1),
(52, -1, 5, 32, 15, 1403217838, 0, 1),
(53, 1, 5, 33, 15, 1403218709, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `contents`
--

CREATE TABLE IF NOT EXISTS `contents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `k` varchar(255) DEFAULT NULL,
  `v` text,
  `entry_datetime` int(11) NOT NULL DEFAULT '0',
  `update_datetime` int(11) NOT NULL DEFAULT '0',
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `id_2` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `contents`
--

INSERT INTO `contents` (`id`, `k`, `v`, `entry_datetime`, `update_datetime`, `deleted`) VALUES
(1, 'title', 'PLAYFIGHT IS A VFX STUDIO', 0, 0, 1),
(2, 'subtitle', 'WE PRODUCE TOP TIER VISUAL EFFECTS, ANIMATION AND PRE-VISUALIZATION FOR <span>FEATURE FILMS</span>, <span>TELEVISION</span> AND <span>ADVERTISING</span>. OUR OFFICES ARE LOCATED IN DOWNTOWN TORONTO', 0, 0, 1),
(3, 'facebook', 'http://facebook.com/foundmissing', 0, 0, 0),
(4, 'twitter', 'https://twitter.com/FoundMissing__/', 0, 0, 0),
(5, 'instagram', 'http://instagram.com/foundmissing__', 0, 0, 0),
(6, 'rss', 'http://rss.com/foundmissing', 0, 0, 0),
(7, 'contact_subtitle', 'We are so fun.', 0, 0, 1),
(8, 'address1', '489 Queen Street East', 0, 0, 1),
(9, 'address2', 'Toronto, On M5A 1V1', 0, 0, 1),
(10, 'phone', '416-809-6594', 0, 0, 1),
(11, 'email', 'hello@playfight.com', 0, 0, 1),
(12, 'job_email', 'jobs@playfight.com', 0, 0, 1),
(13, 'mission_statement', 'So the word is out and the Jordan III white/cement will make another visit come 2011. From pictures that have surfaced one has to be disappointed at the use of what looks like a glossy cheap leather. Outside of that and the fact that there is no Nike Air (wasnâ€™t on the 2003 release either) the shoe looks OK. Take a look at the side pictures below before you make your decision, and at the very least check out the different releases side by side. Enjoy.', 0, 0, 0),
(14, 'youtube', 'http://youtube.com', 0, 0, 0),
(15, 'tumblr', 'http://tumblr.com', 0, 0, 0),
(16, 'pinterest', 'http://pinterest.com', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `designers`
--

CREATE TABLE IF NOT EXISTS `designers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `description` text,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=54 ;

--
-- Dumping data for table `designers`
--

INSERT INTO `designers` (`id`, `title`, `description`, `ordering`, `deleted`) VALUES
(1, 'Cole Haan', '', 2, 0),
(2, 'Nike', '', 1, 0),
(3, 'Converse', '', 3, 0),
(4, 'Adidas', '', 4, 0),
(5, 'New Balance', '', 5, 0),
(6, 'Red Wing', '', 6, 0),
(7, 'Clarks Original', '', 8, 0),
(8, 'Timberland', '', 13, 0),
(9, 'Android Homme', '', 14, 0),
(10, 'Supra II', '', 11, 0),
(11, 'Krisvanassche', '', 16, 0),
(12, 'Balenciaga ', '', 15, 0),
(13, 'Givenchy ', '', 21, 0),
(14, 'Dsquared2 ', '', 18, 0),
(15, 'Ann Demeulemeester ', '', 17, 0),
(16, 'Balmain ', '', 20, 0),
(17, 'Giuseppe Zanotti', '', 12, 0),
(18, 'Raf Simons ', '', 22, 0),
(19, 'Lanvin', '', 23, 0),
(20, 'Saint Laurent ', '', 19, 0),
(21, 'Maison Martin Margiela ', '', 24, 0),
(22, 'Pierre Hardy ', '', 25, 0),
(23, 'MCQ Alexander Mcqueen ', '', 26, 0),
(24, 'Neil Barret ', '', 27, 0),
(25, 'Thom Browne ', '', 28, 0),
(26, 'Paul Smith ', '', 29, 0),
(27, 'Tiger of Sweden ', '', 30, 0),
(28, 'Dr. Martens ', '', 32, 0),
(29, 'Y-3 ', '', 31, 0),
(30, 'Tod''s', '', 33, 0),
(31, 'Asics', '', 34, 0),
(32, 'John Varvatos ', '', 35, 0),
(33, 'Ellen Admonds ', '', 36, 0),
(34, 'Brunello Cucinelli ', '', 37, 0),
(35, 'Oliberte ', '', 39, 0),
(36, 'Toms ', '', 38, 0),
(37, 'Reebok', '', 7, 0),
(38, 'Asics', '', 40, 0),
(39, 'Sorel ', '', 41, 0),
(40, 'Sebago ', '', 42, 0),
(41, 'Sperry Topsider', '', 43, 0),
(42, 'Feit ', '', 44, 0),
(43, 'Danner ', '', 45, 0),
(44, 'Del Toro', '', 46, 0),
(45, 'Chippewa ', '', 47, 0),
(46, 'ETQ ', '', 49, 0),
(47, 'Puma ', '', 10, 0),
(48, 'Jordan ', '', 0, 0),
(49, 'Vans ', '', 9, 0),
(50, 'Christian Louboutin', '', 48, 0),
(51, 'Gucci ', '', 50, 0),
(52, 'Diemme ', '', 51, 0),
(53, 'Yuketen ', '', 52, 0);

-- --------------------------------------------------------

--
-- Table structure for table `favorites`
--

CREATE TABLE IF NOT EXISTS `favorites` (
  `user_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  UNIQUE KEY `user_id` (`user_id`,`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `favorites`
--

INSERT INTO `favorites` (`user_id`, `item_id`) VALUES
(1, 9),
(1, 19),
(1, 29),
(1, 30),
(1, 36),
(11, 9),
(11, 35);

-- --------------------------------------------------------

--
-- Table structure for table `footwears`
--

CREATE TABLE IF NOT EXISTS `footwears` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_id` int(11) NOT NULL,
  `model_name` varchar(255) NOT NULL,
  `style_id` varchar(150) NOT NULL,
  `tags` varchar(255) NOT NULL,
  `price` varchar(15) NOT NULL,
  `rating` varchar(5) DEFAULT NULL,
  `color_id` varchar(150) NOT NULL,
  `release_date` int(11) NOT NULL,
  `entry_datetime` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `footwears`
--

INSERT INTO `footwears` (`id`, `brand_id`, `model_name`, `style_id`, `tags`, `price`, `rating`, `color_id`, `release_date`, `entry_datetime`, `deleted`, `ordering`) VALUES
(1, 5, 'JORDAN XX8 SE', '11', 'hi-top, jordans, retro', '210.25', '9.1', '1', 1403928000, 1402958103, 0, 0),
(6, 5, 'lebron 11 elite', '11,1', 'nike, lebron', '275', '9.1', '1,7,2', 1402891200, 1402973358, 0, 4),
(7, 5, 'Free trainer 5.0 iD', '11,1', 'trainers, nike', '125', '8.5', '12,13', 1402977600, 1403030795, 0, 5),
(8, 15, 'Diamante Leather High-Top Sneaker', '11,10', 'leather, high-tops', '510', '8.5', '1', 1401681600, 1403031179, 0, 6),
(9, 5, 'Zoom KD VI', '11', 'kevin durant, basketball', '250', '8.5', '0', 1393650000, 1403031618, 0, 7),
(10, 5, 'JORDAN MELO M10', '11,1', 'melo, basketball', '150', '8.5', '', 1401595200, 1403031966, 0, 8);

-- --------------------------------------------------------

--
-- Table structure for table `footwear_attachments`
--

CREATE TABLE IF NOT EXISTS `footwear_attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `footwear_id` int(11) NOT NULL,
  `attachment_id` int(11) NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `entry_datetime` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `footwear_attachments`
--

INSERT INTO `footwear_attachments` (`id`, `footwear_id`, `attachment_id`, `ordering`, `entry_datetime`, `deleted`) VALUES
(1, 1, 1, 1, 0, 0),
(2, 6, 2, 1, 0, 0),
(3, 7, 3, 1, 0, 0),
(4, 8, 4, 1, 0, 0),
(5, 9, 5, 1, 0, 0),
(6, 10, 6, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE IF NOT EXISTS `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `skew` varchar(255) DEFAULT NULL,
  `excerpt` text,
  `body` text,
  `price` decimal(11,2) DEFAULT NULL,
  `rating` int(5) DEFAULT NULL,
  `internal_notes` text,
  `category_id` int(5) DEFAULT NULL,
  `tags` text,
  `ordering` int(11) DEFAULT '1',
  `entry_datetime` int(11) DEFAULT NULL,
  `update_datetime` int(11) DEFAULT NULL,
  `publish_datetime` int(11) DEFAULT NULL,
  `unpublish_datetime` int(11) DEFAULT NULL,
  `deleted` int(1) DEFAULT '0',
  `type` varchar(20) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `page` varchar(20) DEFAULT NULL,
  `new_release` varchar(1) DEFAULT NULL,
  `brand_id` int(11) DEFAULT '0',
  `style_id` int(11) DEFAULT '0',
  `designer_id` int(11) DEFAULT '0',
  `colors` varchar(255) DEFAULT '0',
  `store_id` int(11) DEFAULT '0',
  `year` int(5) DEFAULT NULL,
  `purchase_url` text,
  `video_url` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=87 ;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `user_id`, `title`, `slug`, `skew`, `excerpt`, `body`, `price`, `rating`, `internal_notes`, `category_id`, `tags`, `ordering`, `entry_datetime`, `update_datetime`, `publish_datetime`, `unpublish_datetime`, `deleted`, `type`, `status`, `page`, `new_release`, `brand_id`, `style_id`, `designer_id`, `colors`, `store_id`, `year`, `purchase_url`, `video_url`) VALUES
(8, 1, 'Home Slider OLD', 'home-slider-old', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1366309595, NULL, 1366171200, NULL, 1, 'slider', 'p', NULL, NULL, 0, 0, 0, '0', 0, 0, NULL, ''),
(9, 12, 'Nike Air Jordan IV - Retro 2012', 'nike-air-jordan-iv-retro-2012', '23213', 'So the word is out and the Jordan III white/cement will make another visit come 2011.', 'So the word is out and the Jordan III white/cement will make another visit come 2011. From pictures that have surfaced one has to be disappointed at the use of what looks like a glossy cheap leather. Outside of that and the fact that there is no Nike Air (wasnâ€™t on the 2003 release either) the shoe looks OK. Take a look at the side pictures below before you make your decision, and at the very least check out the different releases side by side. Enjoy!', '0.00', 0, '', 0, '', 1, 1366317636, NULL, 1295326800, NULL, 1, 'product', 'p', NULL, 'y', 1, 1, 2, '1,2,6', 0, 1999, 'http://www.test.com', ''),
(10, 6, 'Timberland 6" Men''s Premium', 'timberland-6-mens-premium', 'ffcc55', 'dasfdsgfdsgdfs gfdfdsfdas', 'dsfadsd sfadsf dsfdsf adsfdsfd', '55.00', NULL, '', NULL, 'adsf,sadsad,fdfd', 2, 1366663975, NULL, 1365480000, NULL, 1, 'product', 'p', NULL, NULL, 3, 2, 2, '4', 2, NULL, 'http://www.test.com', ''),
(11, 6, 'The Wreckoning', 'the-wreckoning', NULL, 'So the word is out and the Jordan III white/cement will make another visit come 2011.', 'So the word is out and the Jordan III white/cement will make another visit come 2011. From pictures that have surfaced one has to be disappointed at the use of what looks like a glossy cheap leather. Outside of that and the fact that there is no Nike Air (wasnâ€™t on the 2003 release either) the shoe looks OK.\r\n\r\nSo the word is out and the Jordan III white/cement will make another visit come 2011. From pictures that have surfaced one has to be disappointed at the use of what looks like a glossy cheap leather. Outside of that and the fact that there is no Nike Air (wasnâ€™t on the 2003 release either) the shoe looks OK.\r\n\r\nSo the word is out and the Jordan III white/cement will make another visit come 2011. From pictures that have surfaced one has to be disappointed at the use of what looks like a glossy cheap leather. Outside of that and the fact that there is no Nike Air (wasnâ€™t on the 2003 release either) the shoe looks OK.', NULL, NULL, NULL, 1, 'tag1', 3, 1366912176, NULL, 1366776000, NULL, 1, 'article', 'p', 'b', NULL, 0, 0, 0, '0', 0, NULL, NULL, ''),
(12, 6, 'Photo1', 'photo1', NULL, 'Photo 1 excerpt', 'Photo 1 body', NULL, NULL, NULL, 0, 'gallery photo', 4, 1367530465, NULL, 1368072000, NULL, 1, 'photo', NULL, NULL, NULL, 0, 0, 0, '0', 0, NULL, NULL, ''),
(13, 1, 'Photo 2', 'photo-2', NULL, 'Photo 2 excerpt', 'Photo 2 body', NULL, NULL, NULL, 0, 'photo two', 5, 1367533780, NULL, 1368763200, NULL, 1, 'photo', NULL, NULL, NULL, 0, 0, 0, '0', 0, NULL, NULL, ''),
(14, 6, 'Video 1', 'video-1', NULL, 'Cosby sweater you probably haven''t heard of them mlkshk.', 'Bicycle rights pork belly shoreditch sustainable, plaid wes anderson food truck four loko flannel brunch brooklyn post-ironic.  Pitchfork PBR flexitarian, yr cred put a bird on it hella YOLO biodiesel selvage.  Lomo squid occupy, four loko neutra umami bushwick actually synth pitchfork try-hard portland gastropub organic master cleanse.  3 wolf moon high life echo park, ugh trust fund etsy tattooed meh.  Craft beer church-key swag whatever, tousled portland etsy intelligentsia typewriter stumptown williamsburg thundercats direct trade.', NULL, NULL, NULL, 0, 'Video 1 tag', 6, 1367595955, NULL, 1367553600, NULL, 1, 'video', 'u', NULL, NULL, 0, 0, 0, '0', 0, NULL, NULL, 'http://player.vimeo.com/video/20997055'),
(15, 6, 'Video 2', 'video-2', NULL, 'Chillwave letterpress lo-fi kale chips.  Marfa carles salvia YOLO.', 'PBR VHS whatever ethnic, truffaut pop-up readymade brunch try-hard post-ironic small batch farm-to-table ethical dreamcatcher cosby sweater.  Chillwave retro put a bird on it, bespoke four loko vice stumptown.  Cred kale chips keffiyeh banh mi, yr thundercats cray four loko actually seitan marfa occupy.  Meh food truck umami, +1 forage mixtape keytar photo booth.  Gluten-free messenger bag PBR, mixtape cosby sweater fixie wolf tumblr brunch tousled keffiyeh single-origin coffee umami.  Swag lomo jean shorts marfa, typewriter lo-fi odd future tonx ennui raw denim.  Try-hard wayfarers food truck pinterest lo-fi.', NULL, NULL, NULL, 0, 'Video  2 tag', 7, 1367598657, NULL, 1367640000, NULL, 1, 'video', 'p', NULL, NULL, 0, 0, 0, '0', 0, NULL, NULL, 'http://www.youtube.com/watch?v=5NV6Rdv1a3I'),
(16, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 1368031297, NULL, NULL, NULL, 0, 'item', NULL, NULL, NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(17, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 1368031592, NULL, NULL, NULL, 0, 'item', NULL, NULL, NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(18, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, 1368031738, NULL, NULL, NULL, 0, 'item', NULL, NULL, NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(19, 6, 'Supra Skytop II', 'supra-skytop-ii', '003', 'These are grey high-tops!', 'Fixie vegan bicycle rights seitan shoreditch high life.  Leggings etsy messenger bag plaid semiotics lomo.  Austin shoreditch fap vice, selfies ugh butcher.  8-bit dreamcatcher tousled mcsweeney''s vegan.  +1 gastropub put a bird on it, actually artisan bespoke lo-fi.  Bicycle rights keffiyeh street art PBR.  Skateboard cardigan occupy, VHS gluten-free salvia semiotics chambray fashion axe craft beer jean shorts master cleanse terry richardson selvage try-hard.', '149.99', NULL, '', NULL, 'high-tops', 11, 1368032250, NULL, 1367380800, NULL, 1, 'product', 'p', NULL, NULL, 4, 1, 0, '3', 0, NULL, 'http://www.suprafootwear.com/products/S01030', NULL),
(20, 6, 'dfhgfjdf', 'dfhgfjdf', NULL, '', '', NULL, NULL, NULL, 0, '', 12, 1368033333, NULL, 0, NULL, 1, 'article', 'p', NULL, NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(21, 6, 'chcxvb', 'chcxvb', NULL, '', '', NULL, NULL, NULL, 0, '', 12, 1368033481, NULL, 0, NULL, 1, 'photo', 'p', NULL, NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(22, 6, 'gjdghjd', 'gjdghjd', NULL, '', '', NULL, NULL, NULL, 0, '', 12, 1368033910, NULL, 0, NULL, 1, 'article', 'p', NULL, NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(23, 6, 'fjdfbcnv', 'fjdfbcnv', NULL, '', '', NULL, NULL, NULL, 0, '', 13, 1368033963, NULL, 0, NULL, 1, 'article', 'p', NULL, NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(24, 6, 'fgjhmh', 'fgjhmh', 'asdsa', 'fgsd', 'fgfdgsfdg', '43.00', NULL, 'dsfadasf', NULL, '', 12, 1368036792, NULL, 1367985600, NULL, 1, 'product', 'p', NULL, NULL, 0, 0, 0, '0', 0, NULL, 'fdasfads', NULL),
(25, 6, 'fgjhmh', 'fgjhmh', 'asdsa', 'fgsd', 'fgfdgsfdg', '43.00', NULL, 'dsfadasf', NULL, '', 13, 1368036837, NULL, 1367985600, NULL, 1, 'product', 'p', NULL, NULL, 0, 0, 0, '0', 0, NULL, 'fdasfads', NULL),
(26, 6, 'fgjhmh', 'fgjhmh', 'asdsa', 'fgsd', 'fgfdgsfdg', '43.00', NULL, 'dsfadasf', NULL, '', 14, 1368036862, NULL, 1367985600, NULL, 1, 'product', 'p', NULL, NULL, 0, 0, 0, '0', 0, NULL, 'fdasfads', NULL),
(27, 6, 'fgjhmh', 'fgjhmh', 'asdsa', 'fgsd', 'fgfdgsfdg', '43.00', NULL, 'dsfadasf', NULL, '', 15, 1368036936, NULL, 1367985600, NULL, 1, 'product', 'p', NULL, NULL, 0, 0, 0, '0', 0, NULL, 'fdasfads', NULL),
(28, 6, 'fgjhmh', 'fgjhmh', 'asdsa', 'fgsd', 'fgfdgsfdg', '43.00', NULL, 'dsfadasf', NULL, '', 16, 1368036976, NULL, 1367985600, NULL, 1, 'product', 'p', NULL, NULL, 0, 2, 0, '1,4', 3, NULL, 'fdasfads', NULL),
(29, 6, 'Nike Womens Free Run 2 EXT', 'nike-womens-free-run-2-ext', '843751', 'Nike releases a new classic, featuring their famous lava color underneath a black structure.', 'Lomo quinoa pop-up, flannel art party tofu meggings banh mi stumptown four loko cray mcsweeney''s messenger bag.  Bespoke helvetica mlkshk, high life narwhal odd future ennui mcsweeney''s jean shorts selfies pug pour-over deep v.  Art party brooklyn tumblr blog occupy whatever pug wayfarers banksy fingerstache, carles lo-fi locavore portland fap.  Mumblecore intelligentsia fap letterpress swag.  Mumblecore next level street art bespoke.  Jean shorts intelligentsia vegan irony, pug leggings fashion axe forage thundercats occupy 8-bit.  VHS typewriter tattooed street art keytar.', '100.00', NULL, '', NULL, 'pink and black, nike, sneakers', 12, 1368037352, NULL, 1367380800, NULL, 1, 'product', 'p', NULL, 'y', 5, 1, 2, '1', 5, NULL, 'http://shop.cncpts.com/products/nike-womens-free-run-2-ext-total-crimson-bright-mango', NULL),
(30, 6, 'Clarks Beeswax Leather Desert Boot', 'clarks-beeswax-leather-desert-boot', '', 'dfsghdfgj', 'Lomo quinoa pop-up, flannel art party tofu meggings banh mi stumptown four loko cray mcsweeney''s messenger bag.  Bespoke helvetica mlkshk, high life narwhal odd future ennui mcsweeney''s jean shorts selfies pug pour-over deep v.  Art party brooklyn tumblr blog occupy whatever pug wayfarers banksy fingerstache, carles lo-fi locavore portland fap.  Mumblecore intelligentsia fap letterpress swag.  Mumblecore next level street art bespoke.  Jean shorts intelligentsia vegan irony, pug leggings fashion axe forage thundercats occupy 8-bit.  VHS typewriter tattooed street art keytar.', '140.00', NULL, '', NULL, 'clarks, brown, boots', 13, 1368038098, NULL, 1368244800, NULL, 1, 'product', 'p', NULL, NULL, 6, 2, 0, '5', 0, NULL, 'http://www.clarkscanada.com/products/ranges/mens-original/desert-boot/desert-boot-beeswax-leather', NULL),
(31, 6, 'RACHEL RACHEL ROY DEANDRAH DRESS FLAT', 'rachel-rachel-roy-deandrah-dress-flat', '765689', 'Gray and lime green loafers', 'Mcsweeney''s pug ethical blue bottle.  Chambray trust fund brunch, cliche VHS neutra gluten-free mustache.  Photo booth chambray jean shorts, 90''s ennui kogi occupy.  DIY 8-bit +1 trust fund selvage dreamcatcher, mumblecore umami typewriter helvetica hoodie mixtape brunch meggings.  Austin readymade flexitarian squid, fashion axe dreamcatcher literally salvia narwhal messenger bag farm-to-table fingerstache selvage.  Cardigan sustainable leggings seitan try-hard mumblecore fap.  Artisan bicycle rights stumptown bespoke keytar.', '91.00', NULL, '', NULL, 'loafers, lime green', 14, 1368038825, NULL, 1368590400, NULL, 1, 'product', 'p', NULL, NULL, 0, 3, 0, '3', 6, NULL, 'http://www.thebay.com/eng/shoes-loafersoxfords-_DEANDRAH_DRESS_FLAT-thebay/291526', NULL),
(32, 6, 'Rockport Men''s Margin Oxford', 'rockport-mens-margin-oxford', '345986', 'Brown and black oxfords', 'Williamsburg high life ugh, pork belly organic retro 90''s flexitarian.  Blog photo booth american apparel, authentic carles raw denim 8-bit letterpress viral gentrify.  Synth freegan jean shorts tonx, tousled kale chips selvage locavore vice.  Mumblecore synth YOLO pitchfork, ethical 8-bit occupy flannel ethnic mixtape chambray.  Messenger bag art party etsy fanny pack, craft beer tattooed wes anderson.  Marfa DIY literally, mcsweeney''s sriracha blue bottle kogi single-origin coffee farm-to-table mixtape 90''s.  Aesthetic fingerstache four loko, +1 food truck farm-to-table next level narwhal.', '100.00', NULL, '', NULL, 'rockports, oxfords, brown and black', 15, 1368043322, NULL, 1367467200, NULL, 1, 'product', 'p', NULL, NULL, 7, 4, 6, '1,5', 7, NULL, 'http://www.amazon.com/Rockport-Margin-Mens-Oxford/dp/B000EY5ORQ', NULL),
(33, 10, 'Supra Vaider Blue', 'supra-vaider-blue', '897524', 'All blue supra high tops', 'Williamsburg high life ugh, pork belly organic retro 90''s flexitarian.  Blog photo booth american apparel, authentic carles raw denim 8-bit letterpress viral gentrify.  Synth freegan jean shorts tonx, tousled kale chips selvage locavore vice.  Mumblecore synth YOLO pitchfork, ethical 8-bit occupy flannel ethnic mixtape chambray.  Messenger bag art party etsy fanny pack, craft beer tattooed wes anderson.  Marfa DIY literally, mcsweeney''s sriracha blue bottle kogi single-origin coffee farm-to-table mixtape 90''s.  Aesthetic fingerstache four loko, +1 food truck farm-to-table next level narwhal.', '80.00', NULL, '', NULL, 'all blue, supra, vaider', 16, 1368043893, NULL, 1367467200, NULL, 1, 'product', 'p', NULL, 'n', 13, 6, 10, '7,9', 0, NULL, 'http://www.supra2013.net/supra-vaider-high-tops-all-blue-p-912.html', NULL),
(34, 6, 'Nike Presto', 'nike-presto', '897340', 'Nike prestos from grade 7!', 'Artisan fanny pack high life vice, vinyl cliche actually fap gastropub plaid mlkshk post-ironic squid brunch.  Fashion axe tofu literally, put a bird on it locavore freegan street art kogi fanny pack fixie food truck american apparel salvia.  Locavore farm-to-table flannel ethical fanny pack bespoke.  Master cleanse hashtag banksy you probably haven''t heard of them, mumblecore tonx pickled meh portland dreamcatcher YOLO keffiyeh.  Gluten-free literally sartorial, fingerstache occupy raw denim artisan pickled cliche umami.  Keffiyeh DIY vinyl PBR neutra trust fund.  Artisan williamsburg selvage wayfarers, 8-bit mustache brooklyn blue bottle mlkshk aesthetic food truck.', '69.99', NULL, '', NULL, 'presto', 17, 1368044195, NULL, 1367553600, NULL, 1, 'product', 'p', NULL, NULL, 5, 1, 2, '1,2', 8, NULL, 'http://www.footlocker.eu/gb/en/Shoes/Nike-Presto-42.aspx/4206662604', NULL),
(35, 6, 'Adidas Dragon', 'adidas-dragon', '846832', 'Adidas dragons -- for dragons.', 'Salvia godard lomo bushwick.  DIY biodiesel cardigan fanny pack typewriter fixie, sartorial semiotics small batch tattooed cliche stumptown brunch literally.  Intelligentsia fashion axe YOLO keytar, chambray ugh blog typewriter post-ironic selfies artisan.  Keytar williamsburg gluten-free beard truffaut, 90''s neutra occupy sustainable helvetica gastropub viral echo park.  Wolf lo-fi swag DIY vice sartorial, artisan cosby sweater twee brunch hella.  Pop-up art party try-hard blue bottle viral, letterpress mustache aesthetic disrupt kogi tumblr raw denim occupy stumptown fashion axe.  Terry richardson biodiesel fixie authentic vinyl banh mi.', '65.00', NULL, '', NULL, 'dragon, runner', 18, 1368044704, NULL, 1368417600, NULL, 1, 'product', 'p', NULL, 'n', 8, 1, 4, '7', 13, NULL, 'http://www.adidas.com/us/product/mens-originals-dragon-shoes/EW391', NULL),
(36, 6, 'Nike Air Jordan XX8', 'nike-air-jordan-xx8', '094467', 'The new Air Jordan XX8''s!', 'Flannel biodiesel disrupt, YOLO chambray aesthetic hashtag ethnic.  Cosby sweater terry richardson YOLO DIY, pug artisan retro.  PBR cray helvetica blog, intelligentsia irony ethical godard mcsweeney''s portland bicycle rights.  Pitchfork bicycle rights single-origin coffee vice dreamcatcher, banh mi lomo pinterest fingerstache shoreditch four loko aesthetic.  Twee fanny pack american apparel flannel vice.  Hoodie brunch banh mi leggings, meggings polaroid fanny pack hashtag forage echo park mixtape meh.  American apparel stumptown try-hard swag, intelligentsia fixie forage high life echo park blue bottle banksy hella retro.', '200.00', NULL, '', NULL, 'wrapped, jordan', 19, 1368045025, NULL, 1368244800, NULL, 1, 'product', 'p', NULL, NULL, 1, 1, 2, '1,3', 0, NULL, 'http://www.footlocker.eu/gb/en/Shoes/Nike-Air-Jordan-Xx8-41.aspx/4101988104', NULL),
(37, 6, 'Men''s Earthkeepers Stormbuck Lite Chukka Boot', 'mens-earthkeepers-stormbuck-lite-chukka-boot', '462895', 'Cool low cut boots', 'Mumblecore typewriter tousled sartorial tattooed lo-fi PBR cray street art vice flannel.  Hashtag single-origin coffee intelligentsia, craft beer godard trust fund thundercats jean shorts marfa quinoa pitchfork.  Hella tumblr brunch banh mi messenger bag, disrupt irony bespoke before they sold out umami.  Shoreditch art party skateboard ugh, 90''s DIY bicycle rights messenger bag street art fixie.  Brooklyn next level wayfarers, williamsburg portland selvage meh selfies flannel seitan blue bottle fingerstache cray mlkshk.  DIY next level helvetica, photo booth farm-to-table kogi post-ironic wolf pinterest bespoke.  Brunch put a bird on it cliche fixie, pitchfork stumptown mixtape.', '110.00', NULL, '', NULL, 'low cut, boat shoes', 20, 1368046011, NULL, 1367985600, NULL, 1, 'product', 'p', NULL, NULL, 3, 2, 8, '4,6', 0, NULL, 'http://shop.timberland.com/product/index.jsp?c=1106674&productId=12885755&prodFindSrc=paramNav', NULL),
(38, 6, 'Allen Edmonds Men''s McAllister Wing Tip', 'allen-edmonds-mens-mcallister-wing-tip', '949573', 'For manly men only', 'Freegan art party authentic wayfarers disrupt, letterpress meh.  Gentrify readymade fingerstache gastropub retro typewriter blue bottle.  Selfies yr direct trade  intelligentsia chillwave.  Aesthetic hashtag readymade bespoke.  Hoodie ennui banksy synth authentic.  Williamsburg tumblr chambray readymade, master cleanse retro sriracha yr viral echo park seitan swag thundercats gentrify before they sold out.  Beard blog 90''s swag tonx, butcher forage meh 8-bit quinoa bespoke.', '345.00', NULL, '', NULL, 'leather, fine detailing', 21, 1368046375, NULL, 1367899200, NULL, 1, 'product', 'p', NULL, NULL, 9, 4, 0, '5', 7, NULL, 'http://www.amazon.com/Allen-Edmonds-Mens-McAllister-Walnut/dp/B001TDKXR2/ref=sr_1_5?s=shoes&ie=UTF8&qid=1368046234&sr=1-5', NULL),
(39, 6, 'Converse Chuck Taylor', 'converse-chuck-taylor', '839058', 'The classic chuck taylor', 'Godard helvetica semiotics lo-fi, +1 portland small batch biodiesel blog hashtag plaid mixtape.  Locavore cardigan banksy, food truck bespoke pug VHS hashtag narwhal whatever mlkshk blog terry richardson farm-to-table fingerstache.  Jean shorts forage deep v 8-bit street art.  Pickled bespoke fixie, wayfarers 90''s plaid try-hard ugh.  Typewriter gluten-free ugh fap, cliche mumblecore quinoa gentrify stumptown hashtag helvetica tofu.  Dreamcatcher banjo gluten-free, selfies 8-bit trust fund fap keffiyeh gentrify beard forage quinoa deep v you probably haven''t heard of them.  Portland tumblr yr shoreditch organic cosby sweater.', '55.00', NULL, '', NULL, 'classic, chuck taylor', 22, 1368047218, NULL, 1367380800, NULL, 1, 'product', 'p', NULL, NULL, 2, 1, 0, '1', 0, NULL, 'http://www.net-a-porter.com/product/311584?cm_mmc=LinkshareUS-_-Hy3bqNL2jtQ-_-Custom-_-LinkBuilder&siteID=Hy3bqNL2jtQ-WqYL0guMFbG9Fy1Lvs4xZw', NULL),
(40, 6, 'Nike Dunk SB', 'nike-dunk-sb', '9579755', 'Nike''s staple, the dunk', 'Pinterest blog you probably haven''t heard of them, banjo keytar hashtag aesthetic williamsburg fap meggings yr wolf trust fund occupy.  Hashtag odd future deep v ugh ennui.  Meh flexitarian sustainable, jean shorts cosby sweater wayfarers selfies whatever Austin farm-to-table art party terry richardson bushwick literally banksy.  Craft beer helvetica irony fanny pack, brooklyn ugh banjo pickled intelligentsia occupy.  Tumblr echo park williamsburg fap, squid cosby sweater viral vegan craft beer sustainable blue bottle dreamcatcher fashion axe YOLO.  Marfa 90''s echo park, chillwave sustainable PBR you probably haven''t heard of them pitchfork gluten-free.  Thundercats readymade cardigan, odd future cliche bespoke photo booth disrupt polaroid +1.', '250.00', NULL, 'Internal note on Nike Dunk SB', NULL, 'dunks', 23, 1368047607, NULL, 1368590400, NULL, 1, 'product', 'p', NULL, NULL, 5, 1, 2, '1', 0, NULL, 'http://www.flightclubny.com/p.php?fc=ny&c=sb&i=080120', NULL),
(41, 6, 'K-Swiss Clean Classic', 'kswiss-clean-classic', '385059', 'Classic K-Swisses', 'Bespoke VHS tumblr leggings bicycle rights hoodie.  8-bit chambray trust fund flexitarian beard PBR.  Chambray retro ethical meggings marfa post-ironic.  Marfa fingerstache organic, craft beer hella vice gastropub blog.  Raw denim you probably haven''t heard of them direct trade, skateboard bicycle rights scenester salvia flexitarian retro tofu.  Banjo vegan gastropub gentrify, pug fingerstache williamsburg viral bicycle rights street art carles tofu sustainable mcsweeney''s.  Tofu authentic kale chips, american apparel before they sold out stumptown sriracha blog twee plaid 3 wolf moon pop-up.', '70.00', NULL, '', NULL, 'classic, plain', 24, 1368048050, NULL, 1368763200, NULL, 1, 'product', 'p', NULL, NULL, 10, 1, 6, '1,2', 9, NULL, 'http://www.kswiss.com/shop/footwear/02874-035/Men/Clean_Classic/stingray_white/', NULL),
(42, 6, 'The Abington Work Ox Shoe', 'the-abington-work-ox-shoe', '305943', 'Cool shoes', 'Next level lo-fi mumblecore helvetica swag wayfarers, quinoa meh gluten-free ethnic.  Dreamcatcher cosby sweater ugh authentic intelligentsia etsy.  Synth raw denim keffiyeh Austin, letterpress brooklyn retro whatever aesthetic single-origin coffee ethnic cray mixtape fingerstache deep v.  Ethnic pug brunch tattooed raw denim small batch.  Twee whatever pitchfork artisan.  Flannel meh forage, Austin food truck +1 chillwave viral cardigan.  Stumptown bespoke kale chips narwhal.', '180.00', NULL, '', NULL, 'work, summer, boat', 25, 1368048801, NULL, 1367985600, NULL, 1, 'product', 'p', NULL, NULL, 3, 4, 8, '4,5', 10, NULL, 'http://www.karmaloop.com/product/The-Abington-Work-Ox-Shoe-in-Brown-Smooth/313664', NULL),
(43, 6, 'Adidas Opening Ceremony', 'adidas-opening-ceremony', '869203', 'This is an intense shoe', 'Church-key quinoa meh hoodie direct trade.  Actually literally cliche single-origin coffee.  Cray mumblecore pork belly thundercats, brunch food truck wes anderson typewriter etsy lo-fi.  Irony gastropub fashion axe food truck, selvage next level scenester echo park meh DIY godard.  Pug farm-to-table art party +1 tofu.  Messenger bag tumblr small batch bespoke.  Farm-to-table single-origin coffee post-ironic polaroid, etsy flexitarian 3 wolf moon selfies aesthetic helvetica tofu tumblr cardigan quinoa.', '235.00', NULL, '', NULL, 'opening ceremony', 26, 1368049349, NULL, 1369281600, NULL, 1, 'product', 'p', NULL, NULL, 8, 1, 4, '7', 10, NULL, 'http://www.karmaloop.com/product/The-Crazy-8-Tennis-Sneaker-in-Black-Multi/384218', NULL),
(44, 6, 'Android Homme Propulsion High 1.5', 'android-homme-propulsion-high-15', '294765', 'Speckled high-tops!', 'Before they sold out viral YOLO, portland dreamcatcher PBR banjo tumblr helvetica mcsweeney''s lo-fi whatever cred.  Cliche occupy try-hard, shoreditch yr ennui truffaut aesthetic.  3 wolf moon VHS vice, chambray small batch blue bottle literally +1 keytar blog.  DIY food truck direct trade  four loko etsy.  Vegan messenger bag salvia, retro four loko organic lomo iphone irony before they sold out blog.  Meggings portland wes anderson occupy high life.  Whatever Austin occupy thundercats meh.', '219.00', NULL, '', NULL, 'android homme, high-tops', 27, 1368050003, NULL, 1368590400, NULL, 1, 'product', 'p', NULL, NULL, 11, 1, 9, '1', 11, NULL, 'http://www.mypys.com/android-homme-propulsion-high-1-5-grey-fur', NULL),
(45, 6, 'ObyO JS Ostrich Slim - Jeremy Scott', 'obyo-js-ostrich-slim-jeremy-scott', '249634', 'Futury boots', 'Food truck plaid hashtag, twee marfa high life authentic keffiyeh american apparel craft beer lo-fi brooklyn whatever.  Neutra banksy vegan hella, helvetica godard 90''s tonx fanny pack master cleanse gastropub stumptown pork belly.  Mumblecore meh kale chips, letterpress bespoke before they sold out four loko master cleanse brunch readymade selvage single-origin coffee mcsweeney''s cardigan.  Fanny pack keffiyeh vegan, mixtape sustainable authentic bushwick.  Deep v wes anderson pop-up fap you probably haven''t heard of them.  High life etsy mumblecore narwhal, portland 3 wolf moon blog pop-up keffiyeh aesthetic flannel vegan wayfarers.  Small batch thundercats pitchfork wolf yr lo-fi craft beer, put a bird on it actually raw denim godard tumblr bushwick.', '239.99', NULL, '', NULL, 'boots, thick sole', 28, 1368050360, NULL, 1368417600, NULL, 1, 'product', 'p', NULL, NULL, 8, 2, 4, '1,5', 11, NULL, 'http://www.pickyourshoes.com/item.asp?itemname=38584', NULL),
(46, 6, 'Editorials Article', 'editorials-article', NULL, 'Before they sold out church-key tousled, viral hella freegan kogi direct trade  keytar craft beer skateboard shoreditch pickled cray.  Viral pork belly narwhal before they sold out, chambray plaid brooklyn small batch.  Pug +1 before they sold out, skateboard american apparel selvage cred umami tumblr irony actually.', 'Direct trade  actually deep v post-ironic single-origin coffee selfies.  Art party pork belly stumptown, +1 hella deep v keytar tousled high life neutra quinoa pickled lo-fi chambray you probably haven''t heard of them.  Narwhal gluten-free readymade fixie banjo farm-to-table.  Freegan polaroid brunch, marfa whatever williamsburg trust fund echo park craft beer yr mustache.  Pickled flannel etsy pork belly, lo-fi sartorial synth mcsweeney''s hashtag 3 wolf moon gastropub farm-to-table Austin quinoa.  Swag hoodie church-key, art party mixtape beard biodiesel pop-up.  Tumblr american apparel +1 blue bottle typewriter plaid meggings, vinyl ugh YOLO.', NULL, NULL, NULL, 1, 'editorial', 29, 1368213707, NULL, 1368158400, NULL, 1, 'article', 'p', 'e', NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(47, 6, 'shoesssss', 'shoesssss', 'sdhsh', 'rethrh', 'rethregher', '0.00', NULL, '', NULL, 'ertyertye', 30, 1368483164, NULL, 0, NULL, 1, 'product', 'p', NULL, NULL, 0, 0, 0, '0', 0, NULL, '', NULL),
(48, 6, 'funfunfun', 'funfunfun', NULL, '', '', NULL, NULL, NULL, 0, '', 30, 1368824401, NULL, 0, NULL, 1, 'photo', 'p', NULL, NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(49, 6, 'Test Photot', 'test-photot', NULL, 'fadsfadsf', 'gasfdasf', NULL, NULL, NULL, 1, 'dafdfds', 31, 1368827443, NULL, 1368676800, NULL, 1, 'photo', 'p', NULL, NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(50, 6, 'The Newest Article', 'the-newest-article', NULL, 'Newest article excerpt...', 'Newest article body', NULL, NULL, NULL, 1, 'newest, article', 32, 1369256425, NULL, 1369195200, NULL, 1, 'article', 'p', 'b', NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(51, 6, 'Official Article', 'official-article', NULL, 'This is the first official article of the site.', 'Keffiyeh shabby chic direct trade, put a bird on it Godard Echo Park Etsy typewriter McSweeney''s.  Intelligentsia butcher yr stumptown McSweeney''s.  Banksy trust fund four loko sriracha shoreditch.  Flexitarian 8-bit Odd Future cardigan Godard fap, Marfa pitchfork hella fixie.  Echo Park you probably haven''t heard of them food truck scenester iPhone.  Tattooed ugh dreamcatcher, skateboard farm-to-table fanny pack mlkshk Tonx asymmetrical scenester plaid messenger bag.  Semiotics kitsch forage meggings ethnic cornhole, readymade narwhal whatever viral authentic.', NULL, NULL, NULL, 6, 'OFFICIAL', 32, 1369340386, NULL, 1369281600, NULL, 1, 'article', 'p', 'b', NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(52, 6, 'Nike Lunar Pegasus 89', 'nike-lunar-pegasus-89', '3959204', 'The Nike Lunar Pegasus 89 comes in a navy, pink and orange colourway, and makes use of Nike''s most recent cushioning technology, the Lunarlon midsole.', 'Nike have repackaged one of their classic running silhouettes.\r\n\r\nThe Nike Lunar Pegasus 89 comes in a navy, pink and orange colourway, and makes use of Nike''s most recent cushioning technology, the Lunarlon midsole.\r\n\r\nNavy suede and mesh make up the upper part of the shoe alongside bright pink and orange accents, all finished off with neon pink contrast-stitching.\r\n\r\nThere is no release date for these at this moment in time, but you can ogle them in our gallery below and then head to the Nike website for more details.', '130.00', NULL, '', NULL, 'lunar, pegasus, nike', 33, 1369343804, NULL, 1369281600, NULL, 1, 'product', 'p', NULL, 'y', 5, 1, 2, '1,3,6,7', 10, NULL, '', NULL),
(53, 6, 'Adam''s Slider', 'adams-slider', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 34, 1369344161, NULL, 1369281600, NULL, 1, 'slider', 'p', NULL, NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(54, 6, 'Official Photo', 'official-photo', NULL, 'First official photo!', 'This is the newest photo on the site.', NULL, NULL, NULL, 3, '', 35, 1369345091, NULL, 1369281600, NULL, 1, 'photo', 'p', NULL, NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(55, 6, 'Official Video', 'official-video', NULL, 'Kai is the man.', 'sdfgbldfuhbgosdfbgoy bdfosy boduyfbg ob doyufbg ouyb sud fbgyofdgiuysdfbgh uiy ouyfdbg oudfg yg sdoufyg ldsug ly gldfg lysdg ludgfy u doy goyg oygouyg uyfu yfguoyguoyg uyg uygdfuysgfyg yusdfgyug ', NULL, NULL, NULL, 3, '', 36, 1369345389, NULL, 1369281600, NULL, 1, 'video', 'p', NULL, NULL, 0, 0, 0, '0', 0, NULL, NULL, 'http://www.youtube.com/watch?v=-Xa0NfCdLk4'),
(56, 6, 'Sad Cat Diaries', 'sad-cat-diaries', NULL, 'Exceerrrrrrrrrrrrrrrrptzzzz', 'BODYBBBODYBODYBODYBODYBODYBODYBODYBODYBODYBODYBODYBODYBODYBODYBODYBODYBODYBODYBODYBODYBODY', NULL, NULL, NULL, 1, '', 37, 1369410200, NULL, 1369972800, NULL, 1, 'video', 'p', NULL, NULL, 0, 0, 0, '0', 0, NULL, NULL, 'http://www.youtube.com/watch?v=PKffm2uI4dk'),
(57, 6, 'Sad Cat', 'sad-cat', NULL, 'dsfgsdfhgh', 'BODYBODYBDOYBODYBODYBODYBODYBODYBODYBODYBODYBO', NULL, NULL, NULL, 0, '', 38, 1369410385, NULL, 0, NULL, 1, 'photo', 'p', NULL, NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(58, 10, 'The Saddest Cat In The World', 'the-saddest-cat-in-the-world', NULL, 'dfgohisdf;hg;', ';ohusdfgohsdfhg''fdhg''doighjo'';dfjg;oeihrjg;oihg;/', NULL, NULL, NULL, 6, 'sad cat', 39, 1369410829, NULL, 1369368000, NULL, 1, 'article', 'p', 'b', NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(59, 12, 'Brooklyn', 'brooklyn', NULL, '', '', NULL, NULL, NULL, 1, '', 11, 1374551909, NULL, 1374552000, NULL, 1, 'photo', 'p', NULL, NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(60, 12, '', '', NULL, '', '', NULL, NULL, NULL, 0, '', 12, 1374552302, NULL, 1374552000, NULL, 1, 'video', 'p', NULL, NULL, 0, 0, 0, '0', 0, NULL, NULL, 'http://www.youtube.com/watch?v=uWFKGJb-4do'),
(61, 12, 'GIUSEPPE ZANOTTI NAVY VELVET AND GOLD SNEAKERS', 'giuseppe-zanotti-navy-velvet-and-gold-sneakers', '32266M050032', '', 'High top velvet sneakers in navy with black patent leather contrasts. Round toe. Black lace up closure. Embroidered logo patch at padded tongue. Gold tone metal detail at velcro tabbed eyerow. Extended heel collar in metallic gold with exposed gold tone zipper closure. Gold tone exposed zipper detail at eyerow. Textured rubber foxing in black. Tone on tone stitching. Leather/textile upper, rubber sole. Made in Italy\r\n Via SSENSE ', '810.00', NULL, '', NULL, 'Zanotti, Sneakers, Guiseppe ', 13, 1374552794, NULL, 1374552000, NULL, 1, 'product', 'p', NULL, 'y', 14, 11, 17, '1', 0, NULL, 'http://www.ssense.com/men/product/giuseppe_zanotti/navy_velvet_and_gold_sneakers/86505', NULL),
(62, 12, 'Brooklyn', 'brooklyn', NULL, '', 'dfdfdfdfdf', NULL, NULL, NULL, 1, '', 14, 1374636949, NULL, 1374552000, NULL, 1, 'article', 'p', 'b', NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(63, 12, 'mmmm', 'mmmm', NULL, '', ' mmm', NULL, NULL, NULL, 0, '', 15, 1374731425, NULL, 1374724800, NULL, 1, 'article', 'p', 'b', NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(64, 1, 'Home Slider', 'home-slider-super-old', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 16, 1376106135, NULL, 1376020800, NULL, 1, 'slider', 'p', NULL, NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(65, 12, 'Giuseppe Zanotti Leopard Print Moccasins', 'giuseppe-zanotti-leopard-print-moccasins', '', 'Standing out of the crowd made easy.', 'Standing out of the crowd made easy. That is what these pair of Giuseppe Zanotti Loafers are all about.  As long as you can afford these pair, these loafers make your life easy by sticking out during special events such as Fashion Week. With leopard print pony fur and talons made of gold, you are bound to grab somebody''s attention. Keep in mind when buying this pair, they are not meant for everybody. The most important reason it is not for everybody, you need to have pieces in your wardrobe that will fit well with these shoes. If most of your work consists of staying indoors, then this is another reason this is a good buy because of the leather soles. ', '0.00', NULL, '', NULL, 'Giuseppe Zanotti Loafers, Giuseppe Zanotti, Moccasin, Leopard print', 17, 1378512666, NULL, 1378440000, NULL, 1, 'product', 'p', NULL, 'y', 14, 3, 17, '0', 0, NULL, 'http://www.giuseppezanottidesign.com/gb/men/moccasins_cod44552366du.html?collection_id=23464', NULL),
(66, 12, 'Neumok Wingtip Oxfords', 'neumok-wingtip-oxfords', '', 'Tired of wearing bland dress shoes? These Neumok Wingtip Oxfords are the way to go. ', 'Tired of wearing bland dress shoes? These Neumok Wingtip Oxfords are the way to go. You may ask why? Well the reason being they are available in 6 different colours, so you have a variety to choose from, and you could mix and match it with a lot that will be available in your closet. \r\n\r\nThe most interesting part about buying these shoes, you have a variety of re-crafting packages available to you. Now, you may ask what does this mean? Well, once you get the pair of shoes, if you do not like how the leather has been polished or you need some sort of a bag in order to protect these shoes, all you have to do is send it back to them. With a variety of packages available at different prices, you can pick what suits your needs and they will complete your needs for you. What else can you really ask for? ', '0.00', NULL, '', NULL, 'Neumok Wingtip Oxfords, Allen Edmonds', 18, 1378958486, NULL, 0, NULL, 1, 'product', 'p', NULL, 'n', 9, 4, 33, '13', 0, NULL, 'http://www.allenedmonds.com/aeonline/producti_SF4025_1_40000000001_-1', NULL),
(67, 12, 'Neumok Wingtip Oxfords', 'neumok-wingtip-oxfords', NULL, 'Tired of wearing bland dress shoes? These Neumok Wingtip Oxfords are the way to go. ', 'Tired of wearing bland dress shoes? These Neumok Wingtip Oxfords are the way to go. You may ask why? Well the reason being they are available in 6 different colours, so you have a variety to choose from, and you could mix and match it with a lot that will be available in your closet. \r\n\r\nThe most interesting part about buying these shoes, you have a variety of re-crafting packages available to you. Now, you may ask what does this mean? Well, once you get the pair of shoes, if you do not like how the leather has been polished or you need some sort of a bag in order to protect these shoes, all you have to do is send it back to them. With a variety of packages available at different prices, you can pick what suits your needs and they will complete your needs for you. What else can you really ask for? ', NULL, NULL, NULL, 1, 'neumok, foundmissing, wingtip oxfords, dress shoes, mens database ', 19, 1378960930, NULL, 1378958400, NULL, 1, 'article', 'p', 'b', NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(68, 12, 'Giuseppe Zanotti Leopard Print Moccasins', 'giuseppe-zanotti-leopard-print-moccasins', NULL, 'Standing out of the crowd made easy.', 'Standing out of the crowd made easy. That is what these pair of Giuseppe Zanotti Loafers are all about.  As long as you can afford these pair, these loafers make your life easy by sticking out during special events such as Fashion Week. With leopard print pony fur and talons made of gold, you are bound to grab somebody''s attention. Keep in mind when buying this pair, they are not meant for everybody. The most important reason it is not for everybody, you need to have pieces in your wardrobe that will fit well with these shoes. If most of your work consists of staying indoors, then this is another reason this is a good buy because of the leather soles. ', NULL, NULL, NULL, 1, 'Giuseppe Zanotti Loafers, Giuseppe Zanotti, Moccasin, Leopard print, FoundMissing, high end fashion ', 20, 1378961605, NULL, 1378958400, NULL, 0, 'article', 'p', 'b', NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(69, 12, 'Desert Trek', 'desert-trek', NULL, '', 'If you are a fan of Clarks, this pair needs to be part of your closet. Even though they may seem plain, do not let that prohibit you from purchasing this. We all have our days we don''t want to wear something outrageous. Instead we would rather wear something that is just plain and simple. That is the main reason you can save this in your closet. ', NULL, NULL, NULL, 1, 'Desert Trek, Clarks, Clarks Originals', 21, 1378965439, NULL, 0, NULL, 0, 'article', 'p', 'b', NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(70, 12, 'Air Jordan 5 Retro â€œ3LAB5â€³', 'air-jordan-5-retro-3lab5', NULL, '', 'Starting off with the visuals for these Air Jordans, first question that would come to mind, who would let these pair of Air Jordans go through for production? Actually, a better question would be, who would actually wear these shoes? Everything about these shoes seem dreadful. Looking at the pattern alone, it comes off as really unattractive to the eye. Yet it seems like the brand thought someone out there would actually buy these. Besides some hypebeasts, it cannot be fathomed who else would be interested in these pairs? Any guesses?', NULL, NULL, NULL, 1, 'Air Jordan, Air Jordan 5 Retro, Air Jordan 5 Retro "3LAB5"', 22, 1379054060, NULL, 0, NULL, 0, 'article', 'p', 'b', NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(71, 12, 'Men''s Mad Mukluk Mid II Boot', 'mens-mad-mukluk-mid-ii-boot', NULL, '', 'Is it really cold outside? Is your main concern for the day staying warm and comfortable. These pair of shoes by Sorel look like they are really comfortable. With the leather allowing it to be waterproof, and the EVA midsole giving support and cushioning. This is the pair of shoes that will keep you warm and comfortable during a long and active day. In certain aspects it can be stylish as well, so you will not have to be concerned about being lame. If you''re not concerned that other people may think you''re lame for wearing these, that''s even better because you can wear these with confidence!', NULL, NULL, NULL, 1, 'Sorel, Men''s Mad Mukluk Mid II Boot', 23, 1379055149, NULL, 0, NULL, 0, 'article', 'p', 'b', NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(72, 12, 'test ', 'test-', '', '', '', '0.00', NULL, '', NULL, '', 24, 1379188551, NULL, 0, NULL, 1, 'product', 'p', NULL, 'n', 0, 0, 0, '7', 0, NULL, '', NULL),
(73, 12, 'Red Wing Moc Lug', 'red-wing-moc-lug', NULL, '', 'These boots unquestionably caught my attention while I was browsing for a pair of boots. Boots have been fashionable over a long period of time now. Picking the right boots to suit your style and flavour is very important since there are a variety of boots that you can find. \r\n\r\nThe Moc Toe Boots were first designed for hunters and workers. It has the ability to resist harsh conditions. If you are someone that works in harsh conditions, these boots are made for you. In case you are looking for something different, than what''s out there but comfortable at the same time. Once again, these boots are made for you. ', NULL, NULL, NULL, 1, 'Red Wings, Red Wings Moc Lug', 27, 1379437177, NULL, 0, NULL, 0, 'article', 'p', 'b', NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(74, 12, 'Balenciaga Multimaterial Perforated High Sneakers Prune', 'balenciaga-multimaterial-perforated-high-sneakers-prune', NULL, '', 'An important point to remember when buying any article of clothing, just because it''s brand named doesn''t mean it will be worth buying. This right here is a good example. Balenciaga is a brand with high prestige, but just like any other brand they may make mistakes with the products that they are releasing. With this shoe, first of all, there is too much going on. The shoe consists of a number of colours which makes it aggravating to look at. Other than that, it is also made of a variety of materials, which can make it annoying once again. Another problem with this shoe, there are a range of other brands that produce the same style of shoe. It could be a problem because you may find other brands that may make the exact kind of shoe. Therefore it would be in your best interest to stay away from this pair of Balenciagas. ', NULL, NULL, NULL, 1, 'Balenciaga, Balenciaga Multimaterial Perforated High Sneakers Prune', 28, 1379439698, NULL, 0, NULL, 0, 'article', 'p', 'b', NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(75, 12, 'New Balance 990 "Made In U.S.A" Burgundy ', 'new-balance-990-made-in-usa-burgundy-', NULL, '', 'Are you getting tired of seeing everybody wear Jordans? Don''t feel like wearing something that there is a possibility others own as well? Well New Balance is the brand to go to. Even though you may find that there are more people over the past year or two that own at least a pair or two of New Balances, there are a wide variety of styles and models available for your personal style. If that doesn''t get you excited, there are select styles available where you can customize the colours for your personal preference. So there is something for everybody. \r\n\r\nThis specific pair is intriguing because first of all you will not have to match it with anything. You can mix it with a number of colours and still look good in them. Other than that, keep in mind it is made of suede material so you have to be extra careful not to ruin them. ', NULL, NULL, NULL, 1, 'New Balance, New Balance 990, New Balance 990 Made In U.S.A Burgundy ', 29, 1379542037, NULL, 0, NULL, 0, 'article', 'p', 'b', NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(76, 12, 'Kris Van Assche Perforated Boot Sneakers', 'kris-van-assche-perforated-boot-sneakers', NULL, '', 'Distinct than the sort of boots we are used to seeing, these are a pair worth buying. Kris Van Assche have done a good job with these boots. Even though these shoes consist of three different colours, they are pleasing to the eye. Orange is a very insignificant part of the boot, yet it is the most eye-catching. These are the perfect kind of boots to be wearing in the winter because it is heavy duty, making it easier maneuver through the snow. ', NULL, NULL, NULL, 1, 'Kris Van Assche, Kris Van Assche Perforated Boot Sneakers', 30, 1379555955, NULL, 0, NULL, 0, 'article', 'p', 'b', NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(77, 12, 'Air Max ', 'air-max-', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 31, 1379681009, NULL, 1379649600, NULL, 1, 'slider', 'p', NULL, NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(78, 12, 'Ronnie Fieg''s Timberland', 'ronnie-fiegs-timberland', '', 'Release Date: December 14, 2013', 'Timberland has been well-known since 1979 for obtaining the ability to withstand harsh working conditions. Once Tupac was seen wearing Timberland in the movie Juice, demand for these shoes has sky-rocketed. Ever since, the demand hasn''t changed much and people still wear them, especially in the winter time. Ronnie Fieg''s collaboration has helped in decreasing the shoe by 4" and now matches classic 6â€³ workboot in height.', '0.00', NULL, '', NULL, 'Ronnie Fieg''s Timberland, Ronnie Fieg, Timberland, Plum', 32, 1379827613, NULL, 1386997200, NULL, 0, 'product', 'p', NULL, 'y', 3, 2, 8, '15', 0, NULL, '', NULL),
(79, 1, 'yppp', 'home-slider', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 33, 1379827891, NULL, 1379822400, NULL, 0, 'slider', 'p', NULL, NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(80, 12, 'Alexander McQueen Buckle Ankle Boots', 'alexander-mcqueen-buckle-ankle-boots', NULL, '', 'Today, September 23rd, Alexander McQueen has released the Autumn/Winter 2013 shoe collection. Out of the three pairs of shoes that they released, this was the most appealing. The reason being, leather straps are something that we are not accustomed to seeing on boots. So, it makes these shoes more appealing. Another reason this shoe is more appealing because of the red leather interior. Even though others will not be able to see the red leather interior. This is for the customer''s visual pleasure. If any of these two points about these shoes motivate you to buy them, do not hesitate to do so!  ', NULL, NULL, NULL, 1, 'Alexander McQueen, Buckle Ankle Boots, Alexander McQueen Ankle Boots', 34, 1379995908, NULL, 0, NULL, 0, 'article', 'p', 'b', NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(81, 12, 'Supra Chimera Black/Cheetah/Purple/Black', 'supra-chimera-blackcheetahpurpleblack', NULL, '', 'If you''re a fan of Dragon Ball Z, then looking at these shoes will remind you of "Cell" of Dragon Ball Z. What sticks out most about the shoe is the Cheetah print, and the high top aspect of the shoe. But let''s be serious, this shoe does not look attractive in anyway, especially because of the Cheetah print covering certain aspects of the shoe. ', NULL, NULL, NULL, 1, 'Supra, Supra Chimera', 35, 1380749551, NULL, 0, NULL, 0, 'article', 'p', 'b', NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(82, 12, 'Jean-Michel Basquiat x Reebok', '', NULL, '', 'From Supreme to Reebok, we have seen a number of brands use his material on their clothes, shoes etc. Looking at these shoes, it does not do his work any justice in anyway. It seems like they used his artwork only to influence customers to buy these shoes. The main reason for this, we all know that we rarely see people that are willing to buy Reeboks anymore. So, this was there team looking at other brands and noticing that using Jean-Michael Basquiat''s artwork worked for them. Why not, we utilize his artwork for our benefits and profits as well? This seems like a complete fail on their behalf. ', NULL, NULL, NULL, 1, '', 36, 1380752336, NULL, 0, NULL, 0, 'article', 'p', 'b', NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(83, 12, '', '', NULL, '', '', NULL, NULL, NULL, 0, '', 37, 1391982030, NULL, 0, NULL, 0, 'article', 'p', 'b', NULL, 0, 0, 0, '0', 0, NULL, NULL, NULL),
(84, 12, 'AIR TRAINER SC II MEGATRON', 'air-trainer-sc-ii-megatron', '', 'Release Date: November 26, 2013', 'These shoes are based on NFL player Calvin Johnson, Jr. who is a wide-receiver and an unguardable force. He is a well-known player because of the ability to tap into superhuman skills just as MEGATRON of The Transformers. The shoe features the DECEPTICON logo, circuitry board and glow-in-the-dark sole.\r\n\r\nImage courtesy of Capsule Toronto.', '0.00', NULL, '', NULL, 'NFL, Calvin Johnson Jr., MEGATRON, The Transformers, DECEPTICON, Air Trainer, AIR TRAINER SC II MEGATRON', 38, 1392169830, NULL, 1385442000, NULL, 0, 'product', 'p', NULL, 'y', 5, 11, 2, '1,8,9', 16, NULL, '', NULL),
(85, 12, 'Lunar Terra Arktos', 'lunar-terra-arktos', '', 'Release Date: December 11, 2013', 'The two key points regarding these shoes, the leather is waterproof and they have Nike Lunar cushioning that makes it a little bit more comfortable and smoother. Nike boots fans will definitely appreciate these.', '0.00', NULL, '', NULL, 'Lunar Terra Arktos, Nike, Nike Boots', 39, 1392492830, NULL, 1386738000, NULL, 0, 'product', 'p', NULL, 'y', 5, 2, 2, '13', 16, NULL, '', NULL),
(86, 12, 'SUPRA Skytop IV Red', 'supra-skytop-iv-red', '', 'Release Date: December 24, 2013', 'A skateshoe with ribbed padding on a tongue that is extra tall, as well as foam midsole and padded heel that is good for protection. All of this combined brings you a stylish pair of shoes.', '0.00', NULL, '', NULL, 'skateshoe, Supra, SUPRA Skytop IV Red', 40, 1392545598, NULL, 1387861200, NULL, 0, 'product', 'p', NULL, 'y', 4, 11, 10, '3', 0, NULL, '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `body` text NOT NULL,
  `entry_datetime` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `slug`, `body`, `entry_datetime`, `deleted`, `ordering`) VALUES
(1, 'About Us', 'about-us', 'As you find something, the ravenous cycle of fashion leaves us wanting more. Our collections grow but every piece inspires something new, that one thing missing to make your collection complete.\r\n\r\n\r\nProblem\r\n\r\nâ€¨â€¨Footwear in general is fragmented and displayed across the web in such a way that is is difficult for one to be able to search for a specific hard to find shoe, whether they would like to find information on the product or purchase it. Although, we are not an online store, it our duty to direct you to the information and reach of purchase. â€¨â€¨\r\n\r\nSolution\r\n\r\nâ€¨â€¨FoundMissing solves the search/hunting problem by working closely with selected brands on providing the newest detailed product information that is specifically catered to their preferences. FoundMissing exists to fill the void on allowing viewers to express their opinions on select shoes and provide an ease of access to purchasing the end product.\r\n\r\nCopyright Â© 2013 FoundMissing. All rights reserved. Do not use or reproduce without permission', 1376084166, 0, 0),
(2, 'Careers', 'careers', '\r\nGraphic Designer \r\nIf you are interested please submit your resume and 3-10 examples of work.\r\nBrainstorm and mock-up multiple versions of a proposed idea â€¢ Present ideas clearly and in an organized fashion\r\n\r\nResponsibilities \r\n\r\nWork on multiple projects\r\nDesign marketing collateral such as: brochures, pamphlets, catalogues, logos, eNewsletters\r\nDevelopment of visual corporate identity\r\n\r\nÂ \r\nWorking with Programs such as:\r\nPhotoshop\r\nIllustrator\r\nInDesign\r\nÂ \r\nQualifications:\r\nStrong Portfolio/ Keen knowledge in Photoshop, Illustrator, and InDesign CS3+\r\nStrong verbal and written communication skills\r\nOrganized and detail oriented\r\nTime management and ability to multi-task\r\nExcellent interpersonal skills (charismatic personality a must!) and a strong team player\r\nAmbitious and motivated to succeed\r\n\r\n\r\n\r\nIf you are interested please submit your resume and 3-10 examples of work to \r\ninfo@foundmissing.ca ', 1376084337, 0, 2),
(3, 'Privacy', 'privacy', 'The following Privacy Policy covers all the websites (â€œwebsitesâ€) owned, affiliated or operated by FoundMissing. (â€œFoundMissingâ€, â€œweâ€, â€œusâ€, â€œourâ€). It is our goal to bring you information tailored to your personal interests but, at the same time, protect your privacy. Please read the following to learn more about our privacy policy.\r\n\r\nBy visiting FoundMising websites, submitting information or using any of our products and services, you are accepting the practiced outlined in this Privacy Policy.\r\n\r\nThis Privacy Policy covers FoundMissingâ€™s treatment of personal information that FoundMissing gathers when you are on our websites and when you are using FoundMissing services. Also, this Privacy Policy covers FoundMissingâ€™s treatment of any personal information that FoundMissingâ€™s business partners share with us. This Privacy Policy does not cover information collected elsewhere, including without limitation offline and on sites linked to and from the website as well as the practices of third parties that FoundMissing does not control or own, or individuals that FoundMissing does not employ or manage.\r\n\r\nIn addition to reviewing this Privacy Policy, please read our User Agreement. Your use of our websites constitutes agreement to its terms and conditions as well.\r\n\r\nThis Privacy Policy may be modified from time to time so check back often. Continued access of our websites by you will constitute your acceptance of the changes or revisions to the Privacy Policy.\r\n\r\n1. Introduction\r\n\r\nThank you for visiting FoundMissing. This Privacy Policy is written to better explain how we collect and use your information. This Privacy Policy is a binding contract and you must agree to it to the same extent as our Terms and Conditions in order to use our services.\r\n\r\n2. Information You Provide to Us\r\n\r\nThe website generally collects personally identifying information with your specific knowledge and consent. This includes but is not limited to when you register for our forums, enter a sweepstakes and complete a survey. You can choose not to provide us with certain information, but you may not be able to take advantage of many of our special features such as posting on our forums. - Registration: In order to use select parts of our websites such as the database, you must complete a registration form. As part of this registration forum, we require your email address, name, city of origin, gender, shoe size, and date of birth. Optional information such as your referrer or time zone may also be requested.\r\n\r\n3. Personal Information We Collect When You Buy from Us\r\n\r\nIn order for you to place an order with us, you must provide us your name, e-mail address, mailing address, date of birth, Paypal information and telephone number. We will not disclose any information that you provide to us except in accordance with the provisions of this Privacy Policy.\r\n\r\n4. Automatic Information\r\n\r\nOur servers may automatically collect information about your computer when you visit our websites, including without limitation the website that referred you, your Internet Protocol (â€œIPâ€) and other usage and traffic information. FoundMissing uses this statistical information to analyze trends, gather demographic information and administer our websites so we may provide improved web experiences to our readers. The information is not in a manner that would identify you personally. We may provide statistical information, not personal information, to our partners about how our users, collectively use our websites. We share this information to our partners, so that they may further understand the usage of their areas and our websites, so that they too could provide you with a better web experience. There may also be occasion when we are legally required to provide access to our database in order to cooperate with police investigations or other legal proceedings. In those instances, the information is provided only for that purpose.\r\n\r\n5. Use of Information\r\n\r\nWe may use your information:\r\n\r\nTo enhance or improve user experience, the website, or service.\r\nTo process transactions and sell, transfer, and exchange your information to a third party for their marketing purposes with no additional consent.\r\nTo send e-mails about the site or respond to inquiries.\r\nTo send e-mails about the website and other products, news, or updates for FoundMissing.ca.\r\nTo send e-mails about third partiesâ€™ products or services.\r\n\r\n6. Cookies\r\n\r\nIn order to distinguish you from other usersâ€”especially other users on the same network as you â€” we need to use cookies to track your specific computer. Specifically, we use cookies to save your preferences, keep track of advertisements, and remember your shopping cart items. Additionally, our cookies may be accessible by third parties, or provided by third parties. For these reasons we require that you keep cookies enabled when using our website.\r\n\r\n7. Third Party Websites\r\n\r\nWe may link to third party websites from our own website. We have no control over, and are not responsible for, these third party websites or their use of your personal information. We recommend that you review their privacy policies and other agreements governing your use of their website.\r\n\r\n8. Third Party Access to Your Information\r\n\r\nWe protect your information from being accessed by third parties. However, since we may employ outside maintenance personnel or use a third party to host our material, there are third party entities which may have custody or access to your information. Because of this, it is necessary that you permit us to give access to your information to third parties to the same extent that you authorize us to do so. For greater certainty, every authorization which you grant to us in this Privacy Policy, you also grant to any third party that we may hire, contract, or otherwise retain the services of for the purpose of operating, maintaining, repairing, or otherwise improving or preserving our website or its underlying files or systems. You agree not to hold us liable for the actions of any of these third parties, even if we would normally be held vicariously liable for their actions, and that you must take legal action against them directly should they commit any tort or other actionable wrong against you. Additionally, we may store your information or share it with certain other third parties for reason that may or may not be related to maintenance, advertising, marketing, or other purposes related to our business. The following is a non-exhaustive list of other entities that we may store, share, or transfer your information with:\r\n\r\nGoogle Analytics\r\nParse.ly\r\n\r\n9. Marketing & Communication\r\n\r\nWhen registering for a FoundMissing database account, you will be given the option to opt-in to updates through the FoundMissing newsletter  ''new in'' and promotional emails. If you would prefer to opt-out from these e-mail, you can do so at any time by using the unsubscribe link at the bottom of the e-mail.\r\n\r\n10. Law Enforcement\r\n\r\nWe may disclose your information to a third party where we believe, in good faith, that it is desirable to do so for the purposes of a civil action, criminal investigation, or other legal matter. In the event that we receive a subpoena affecting your privacy, we may elect to notify you to give you an opportunity to file a motion to quash the subpoena, or we may attempt to quash it ourselves, but we are not obligated to do either.\r\n\r\n11. Security of Information\r\n\r\nWe use SSL certificates to help secure our website. However, we make no representations as to the security or privacy of your information. It is in our interest to keep our website secure, but we recommend that you use anti-virus software, firewalls, and other precautions to protect yourself from security threats.\r\n\r\n12. Amendments\r\n\r\nWe may amend this Privacy Policy from time to time, and the amended version will be posted on our website in place of the old version. We will also include the date that the currently displayed Privacy Policy took effect to help you determine whether there have been any changes since you last used our website. Like our Terms of Service, you must visit this page each time you come to our website and read and agree to it if the date it was last modified is more recent than the last time you agreed to the Privacy Policy. If you do not agree to the new Privacy Policy, you must immediately cease using our service.\r\n\r\n13. Contact\r\n\r\nAny inquiries about your rights under this Privacy Policy, or any other matters regarding your privacy, can be directed to info@foundmissing.ca', 1376084353, 0, 4),
(4, 'Legal Terms', 'legal-terms', '1. Introduction\r\n\r\nWelcome to FoundMissing.ca, a body corporate doing business in Ontario, Canada. This document constitutes a legally-binding agreement ("Agreement") governing the terms of providing you with our service. Throughout this document, the words "FoundMissing," "FoundMissing," "us," "we," and "our," refer to us, our website, FoundMissing.ca, or our service, FoundMissing, as is appropriate in the context of the use of the words. Likewise, the words "you" and "your" refer to you, the person who is being presented with this document for your agreement.\r\n\r\nBy registering for our website as a member, you are agreeing to receive regular newsletters and marketing related emails from FoundMissing. Should you wish to be removed from our email list, you can unsubscribe or terminate your account any time.\r\n\r\n2. Description of Service\r\n\r\nFoundMissing solves the search/hunting problem by working closely with selected brands on providing the newest detailed product information that is specifically catered to their preferences. FoundMissing exists to fill the void on allowing viewers to express their opinions on select shoes and provide an ease of access to purchasing the end product.\r\n\r\n3. Information Supplied\r\n\r\nWhen using our website and reviewing footwear, you will provide your name, e-mail address,city of origin, date of birth, shoe size, gender.\r\n\r\nIn addition to providing us with the above information about yourself, you must be eighteen years of age or older, or, if a higher minimum age of contractual capacity exists in your jurisdiction, you must be at least that age. Additionally, you represent to us that you are not otherwise considered to be under a disability to enter into a contract with us, whether it be because of an embargo in or against your jurisdiction or any other reason.\r\n\r\n4. Image Terms of Use\r\n\r\nEditors typically include images as part of their blog posts and the images that they are authorized to use is the following:\r\n\r\nImages that are licensed from vendors\r\nImages that are supplied to our editors or released into the public domain by public relations and marketing companies for press or other purposes\r\nImages that are supplied by readers, with the implied representation that the person submitting the image owns the copyright in the image and the right to give it to us for use on our site\r\nImages that are published on Photobucket or other public photos sites with licenses granted under Creative Commons, with attribution in accordance with the CC license granted in each case\r\nImages that are commissioned by FoundMissing\r\nImages that we believe to be covered by the Fair Use Doctrine, such as:\r\nThumbnail images of 150 x 150 pixels or less, cropped or reduced in size from the original source\r\nImages that are used to illustrate a newsworthy story, where the image itself tells a story\r\nImages used in a transformative manner, such as parody\r\nImages that are widely distributed that they have become part of the news\r\nIf FoundMissing receives any notice that an image is posted up inappropriately such as not keeping with the terms listed above, we reserve the right to remove that image.\r\n\r\nIf you think we have published an image or text that violates your copyright, we will address your concerns; however, if the image falls into one of the listed categories above and we believe that our use of the image is legitimate, we will not remove it from the site.\r\n\r\n5. Payment\r\n\r\nYou may pay for our services using Paypal. When providing credit card information, the credit card must be your own. If the credit card does not belong to you, you must have permission from the accountholder to use their card for your transactions.\r\n\r\nWe do not accept personal checks, money orders or direct bank transfers. Please note, items will not be shipped until the transaction has been cleared.\r\n\r\nAny use of fraudulent chargebacks simply to get free merchandise is not only strictly prohibited, but also criminal fraud, and will be dealt with by reporting such chargebacks to your local police department and a credit bureau, as well as referring the matter to a court of competent jurisdiction.\r\n\r\n6. Delivery\r\n\r\nOur ability to ship your order to you is only as good as the information that you provide us. Whether you are ordering products for yourself or others, you must ensure that you provide us with an accurate address. We cannot be held responsible for delivery failures if you donâ€™t give us the correct address or if it is an address that we can''t perform delivery to. If a shipment is returned as unclaimed, undeliverable, regardless of the reason, we shall be entitled to keep you liable for the shipping and handling fees that were associated with that shipment.\r\n\r\nDeliveries are available for those wishing to use a PO Box address. Please check that your rental agreement includes signed-for/registered items. Due to the signed-for nature of deliveries, we recommend that items be sent to a work or home address in order to avoid any delays in receiving your items.\r\n\r\nShipping will be done according to the costs and methods described on our website. Once we ship your item, we are no longer responsible for its condition or delivery to your chosen address. Any claims for damages on the item should be filed by you against the shipper.\r\n\r\n7. Order Confirmation\r\n\r\nAn e-mail confirmation will be sent once payment has been received. Once the order has been confirmed, the item(s) will be processed, packed and shipped out and as such, it will not be possible to cancel your order. In the event that you opt to cancel the order after delivery, you agree that the associated delivery charges will be deducted from your payment before making any refund by us.\r\n\r\n8. Shipping Confirmation\r\n\r\nOnce your order has been dispatched, a shipping confirmation e-mail will be sent. Please keep this e-mail as a reference as it will also include your order tracking number, carrier and instructions on how to track your package.\r\n\r\n9. Delivery\r\n\r\nPlease refer to our ''Delivery'' page.\r\n\r\n10. Returns & Exchanges\r\n\r\nPlease refer to our ''Returns & Exchanges'' page.\r\n\r\n11. Price Changes & Stock Availability\r\n\r\nPrices are subject to change without prior notice.\r\n\r\nWhile we endeavor to provide you with the most accurate and up-to-date information, occasionally ordered items may not be available as listed.\r\n\r\nIn the case that part of your order is unavailable or out of stock, the item(s) will be cancelled from the order with notification and where applicable, a refund shall be applied for the cost of the regarding item(s).\r\n\r\nFoundMissing ensures that all our product descriptions and prices are up-to-date and as accurate as possible. However, in few cases, pricing errors may occur with our listings. Should we discover a pricing error while processing your order and payment, we will immediately rectify the problem before cancelling and refunding you for your order. We will also make sure to inform you about the error with your order as soon as possible.\r\n\r\n12. Promotional & Store Credit Codes\r\n\r\nWhere a promotional code is required for an offer or discounted price, the code should be entered during the checkout process.\r\n\r\nCustomers should take care when entering codes, checking that the offer/credit has been applied. Should you find that the code is unapplicable or invalid, please refer to the specific conditions for the code as the promotion may have already expired. Should you have difficulty in applying store credit to your order, please contact us via e-mail at info@hypebeast.com, Monday to Friday, 10:00am to 6:00pm (Toronto time) for assistance.\r\n\r\n13. Discounts, Sales & Promotions\r\n\r\nFrom time to time, FoundMissing may offer discounts and/or promotions. Discount, sales and/or promotional offers and prices are only applicable when checking out whilst the offer is still valid. Time restrictions will be stated where applicable. FoundMissing reserves the right to amend, extend or terminate these offers at an earlier date without prior notice. For those who purchased over 24 hours before the sale, price adjustments will not be made. For those who purchased within 24 hours, we can refund the discount being offered at the time.\r\n\r\n14. Rules of Conduct\r\n\r\nWe host a forum as well as blogs that we may permit you to comment on. As a part of using our website, you must not:\r\n\r\nPost anything that violates any applicable law, foreign or domestic.\r\nPost anything defamatory, violent, pornographic, hateful, racist, or otherwise objectionable. We have the sole right to determine what is objectionable.\r\nHack, crack, phish, SQL inject, or otherwise threaten or actually harm the security of our systems or those of our users.\r\nInfringe on the intellectual property rights of anyone.\r\nDo anything that infringes on the rights of us or a third party.\r\nCreate more than one account, use another personâ€™s account, or allow another person to use your account.\r\nEngage in unsolicited marketing activities, whether commercial or non-commercial, on our forum or through its private messaging service, or on our blog comments section, or anywhere else on our website.\r\nAdvertise on our website without our permission.\r\nImpersonate another person or organization.\r\nPost any links to content which would, if posted on our own website, violate these Rules of Conduct.\r\nWe reserve the right to terminate our service to you for reasons other than those listed here. We may terminate your account without explanation.\r\n\r\n15. Communications Decency Act\r\n\r\nAs we are located in Toronto, we have certain defenses available to us for liability for defamation posted on our website by third parties, such as in our forums or on our blog.\r\n\r\nOur Rules of Conduct prohibit defamation from being published on our website. In order to complain about defamatory material on our website, please notify us with full details of where the defamation is located on our website (provide us with the URL), your name, and an explanation of how the material is defamatory. We will respond within a reasonable time and, if it appears that there are grounds to believe the material is defamatory, we will remove it. You may send your request to info@foundmissing.ca.\r\n\r\nSpecial Note to American Users\r\n\r\nPlease note that if you are thinking of suing us in the United States, Section 230 of the Communications Decency Act prohibits you from holding us liable for the comments of a third party. Additionally, the SPEECH Act prohibits you from enforcing in the United States any judgments obtained against us in a Hong Kong court for a third partyâ€™s defamatory statements, and if you attempted to sue us in a Hong Kong court for the actions of a third party our lawyers would make a motion in the U.S. courts where your deposition is to be taken to prevent such a deposition from occurring. Therefore, we stress the importance of following the proper Hong Kong notification procedure as described above.\r\n\r\n16. Contest Disclaimer\r\n\r\nThe maximum number of entries per person allowed during the contest period is as the contest has specified. Any attempt by a person to submit more than the stated maximum number of entries will be disqualified from participation in this contest. FoundMissing are not responsible for lost, late, incomplete, invalid, unintelligible, illegible, misdirected or postage-due entries, which will be disqualified. By participating, all entrants agree to abide by these official contest rules.\r\n\r\nIn consideration for your participation in the contest, the individual, group, organization, business, spectator, or other, does hereby release and forever discharge the FoundMissing, and its officers, board and employees, jointly and severally from any and all actions, cause of actions, claim and demands for, upon or by reason of any damage, loss or injury including death, which hereafter may be sustained by participating in the contest.\r\n\r\nThis release extends and applies to, and also covers and includes, all unknown, unforeseen, unanticipated and unsuspected injuries, damages, loss and liability and the consequences thereof including death, as well as those now disclosed and know to exist. The provision of any state, federal, local or territorial law or state providing substance that releases shall not extend to claims, demands, injuries, or damages which are know or unsuspected to exist at this time, to the person executing such release, are hereby expressly waived.\r\n\r\nIt is further understood and agreed that said participation in the contest not to be construed as an admission of any liability and acceptance of assumption of responsibility by FoundMissing, its officers, board and employees, jointly and severally, for all damages and expenses for which the FoundMissing , its officers, board and employees, become liable as a result of any alleged act of the parade participant.\r\n\r\nThe prize is non-transferable and cannot be exchanged for cash.\r\n\r\nFoundMissing reserves the right to offer alternative prize if any prize is not available or under special situation.\r\n\r\n17. Disclaimer\r\n\r\nWe do not actively screen blog comments and forum posts before they are uploaded to our website by third parties. As a part of your acceptance of this Agreement, you release us from any liability for the actions of third parties on our website, whether or not we would otherwise normally be held liable for such words under the laws of Ontario or any other jurisdiction.\r\n\r\n18. Copyright\r\n\r\nYou agree not to copy, distribute, display, disseminate, or otherwise reproduce any of the information on our website, or our website itself, without our prior written permission. This includes, but is not limited to, a prohibition on aggregating product information reviews and forum content made available on our website.\r\n\r\n19. Trademarks\r\n\r\n"FoundMissing" is a trademark used by us, FoundMissing, to uniquely identify our website, business, and service. You agree not to use this phrase anywhere without our prior written consent. Additionally, you agree not to use our trade dress, or copy the look and feel of our website or its design, without our prior written consent. You agree that this paragraph goes beyond the governing law on intellectual property law, and includes prohibitions on any competition that violates the provisions of this paragraph.\r\n\r\n20. Revocation of Consent\r\n\r\nWhere FoundMissing has given prior written consent for your use of our protected material in accordance with our above â€œCopyrightâ€ and â€œTrademarksâ€ provisions, we may revoke that consent at any time. If we so request, we may require that you immediately take action to remove from circulation, display, publication, or other dissemination, any of the marks, copyrighted content, or other materials that we previously consented for you to use.\r\n\r\n21. Notification of Changes\r\n\r\nWhenever FoundMissing changes its Terms of Use, changes will be posted to ensure users are aware of the new changes.\r\n\r\n22. Contact Information\r\n\r\nIf you have any questions, concerns or notices of violations to these terms and conditions, please contact the editor of FoundMissing by sending an email to info@foundmissing.ca\r\n\r\n\r\n', 1376084367, 0, 3),
(5, 'Contact Us', 'contact-us', 'For commissions, collaborations, advertising opportunities on the site, or just to get in touchâ€¦\r\nPlease email:\r\n \r\ndavid.bremang@foundmissing.ca', 1376084379, 0, 5),
(6, 'Branding', 'branding', '<link rel="stylesheet" href="/css/pages.css">\r\n<div id="wrapper">\r\n<h1>Logos and Branding</h1>\r\n<div class="panel">\r\n<h2>Our Logo</h2>\r\n<hr />\r\n<h3>Grab one, it''s free! Just don''t be a jabronie â€“ please don''t mess with it.<br />\r\nThese all come in a folder with EPS and PNG files of different sizes. <a href="images/twoople-logos.zip">Click here</a> to download everything at once.<br /><br />\r\n<div class="arrow-down"></div> This is the primary FoundMissing logo. Try to use this one whenever you can.</h3>\r\n    <div class="logowrapper" style="margin-top: 0px;">\r\n        <div id="logo-p">\r\n        <center><img src="/images/fm-primarywhitetrans-64.png" width="372" height="64" alt="foundmissing primary logo red"/></center>\r\n        <a href="images/twoople-primary.zip"><div class="download"></div></a>\r\n		</div>\r\n    </div>\r\n    \r\n    <div class="logowrapper" id="half" style="margin-right:2.5%;">\r\n      <div id="logo-s">\r\n        <center>\r\n          <img src="/images/fm-sectrans-128.png" width="178" height="128" alt="foundmissing secondary logo" />\r\n        </center>\r\n        <a href="images/twoople-secondary.zip"><div class="download"></div></a>\r\n        </div>\r\n        <div class="description" id="half">\r\n        <h3><div class="arrow-up"></div> This is our secondary logo. When things get too tight on space, use this guy.</h3>\r\n        </div>\r\n    </div>\r\n    \r\n  <div class="logowrapper" id="half">\r\n      <div id="logo-i">\r\n        <center>\r\n		  <img src="/images/fm-redtrans-128.png" width="128" height="128" alt="foundmissing icon" />\r\n        </center>\r\n        <a href="images/twoople-icon.zip"><div class="download"></div></a>\r\n        </div>\r\n        <div class="description" id="half">\r\n        <h3><div class="arrow-up"></div> Use these icons for UI stuff and instances with limited space.</h3>\r\n      </div>\r\n    </div>\r\n</div>\r\n\r\n    \r\n    \r\n    \r\n\r\n\r\n\r\n<div class="panel" style="padding-top: 50px; margin-top:40px;">\r\n<h2>Colour</h2>\r\n<hr />\r\n<h3>These are our primary and supporting colours.</h3>\r\n<div class="colour" id="fmred">\r\n    <div class="colourinner">\r\n    <h3>FM Infrared</h3>\r\n    <h4>HEX	#EC1C3A<br />\r\n        RGB	236, 28, 58<br />\r\n        CMYK	1, 99, 80, 0<br />\r\n        PMS	1788 C</h4>\r\n    </div>\r\n</div>\r\n<div class="colour" id="fmdgrey">\r\n	<div class="colourinner">\r\n    <h3>Dark Grey</h3>\r\n    <h4>HEX	#535858<br />\r\n    RGB	83, 88, 88<br />\r\n    CMYK	66, 54, 55, 29</h4>\r\n    </div>\r\n</div>\r\n<div class="colour" id="fmgrey">\r\n	<div class="colourinner">\r\n    <h3>Grey</h3>\r\n    <h4>HEX	#A0A6A6<br />\r\n    RGB	160, 166, 166<br />\r\n	CMYK	40, 29, 31, 0</h4>\r\n    </div>\r\n</div>\r\n<div class="colour" id="fmlgrey">\r\n	<div class="colourinner">\r\n    <h3>Light Grey</h3>\r\n    <h4>HEX	#F1F2F2<br />\r\n	RGB	241, 242, 242<br />\r\n	CMYK	4, 2, 3, 0</h4>\r\n    </div>\r\n</div>\r\n\r\n\r\n\r\n<div class="colour2" id="fmwhite">\r\n	<div class="colourinner">\r\n    </div>\r\n</div>\r\n<div class="colour2" id="fmcharles">\r\n    <div class="colourinner">\r\n    <h3>Charleston Green</h3>\r\n    <h4>HEX	#232B2B<br/>\r\n    	RGB	35, 43, 43<br/>\r\n    	CMYK	76, 63, 64, 66</h4>\r\n    </div>\r\n</div>\r\n<div class="colour2" id="fmpink">\r\n	<div class="colourinner">\r\n    <h3>Pink</h3>\r\n    <h4>HEX	#535858<br />\r\n    RGB	83, 88, 88<br />\r\n    CMYK	66, 54, 55, 29</h4>\r\n    </div>\r\n</div>\r\n<div class="colour2" id="fmgreen">\r\n	<div class="colourinner">\r\n    <h3>Green</h3>\r\n    <h4>HEX	#232B2B<br/>\r\n    	RGB	35, 43, 43<br/>\r\n    	CMYK	76, 63, 64, 66</h4>\r\n    </div>\r\n</div>\r\n</div>\r\n\r\n\r\n\r\n<div class="panel">\r\n<h2>Spacing</h2>\r\n<hr />\r\n<h3>Keep it spacy.<br />\r\nWhen placing the FM logo, try and keep around an icon''s worth of padding between it and adjacent elements.</h3>\r\n<center><img src="/images/fm-safearea.png" width="500" height="128" alt="foundmissing safe area" /></center>\r\n</div>\r\n\r\n\r\n\r\n\r\n\r\n<div class="panel">\r\n<h2>Typography</h2>\r\n<hr />\r\n<h3>We use Georgia for copy, Verdana for the real tiny stuff, and <a href="http://www.jlee.ca/rigby.html">Rigby</a> for titling.</h3>\r\n\r\n    \r\n    <div class="typespec" style="margin-top:40px;">\r\n    <h2 style="font-family: Verdana; margin-bottom:-5px;">Verdana</h2>\r\n    <h3 style="font-family: Verdana; letter-spacing:2.5px; font-size:16px; margin-bottom:-5px;">A B C D E F G H I J K L M N O P Q R S T U V W X Y Z</h3>\r\n    <h3 style="font-family: Verdana; letter-spacing:3.5px; font-size:16px; margin-bottom:-5px;">a b c d e f g h i j k l m n o p q r s t u v w x y z</h3>\r\n    <h3 style="font-family: Verdana; letter-spacing:3.7px; font-size:16px; margin-bottom:-5px;">0 1 2 3 4 5 6 7 8 9 + - = ! @ # $ % ^ & * ( : ? </h3>\r\n    </div>\r\n    \r\n    <div class="typespec">\r\n    <h2 style="font-family: Georgia; margin-bottom:-5px;">Georgia</h2>\r\n    <h3 style="font-family: Georgia; letter-spacing:3.5px; font-size:16px; margin-bottom:-5px;">A B C D E F G H I J K L M N O P Q R S T U V W X Y Z</h3>\r\n    <h3 style="font-family: Georgia; letter-spacing:5px; font-size:16px; margin-bottom:-5px;">a b c d e f g h i j k l m n o p q r s t u v w x y z</h3>\r\n    <h3 style="font-family: Georgia; letter-spacing:5.5px; font-size:16px; margin-bottom:-5px;">0 1 2 3 4 5 6 7 8 9 + - = ! @ # $ % ^ & * ( : ? </h3>\r\n    </div>\r\n    \r\n   	<div class="typespec">\r\n    <h2 style="font-family: rigby; margin-bottom:-5px;">Rigby</h2>\r\n    <h3 style="font-family: rigby; letter-spacing:4px; font-size:16px; margin-bottom:-5px;">A B C D E F G H I J K L M N O P Q R S T U V W X Y Z</h3>\r\n    <h3 style="font-family: rigby; letter-spacing:5.2px; font-size:16px; margin-bottom:-5px;">a b c d e f g h i j k l m n o p q r s t u v w x y z</h3>\r\n    <h3 style="font-family: rigby; letter-spacing:6px; font-size:16px; margin-bottom:-5px;">0 1 2 3 4 5 6 7 8 9 + - = ! @ # $ % ^ & * ( : ? </h3>\r\n    </div>\r\n</div>\r\n</div>', 1401664164, 0, 1),
(7, 'Partners', 'partners', 'partners', 1401664264, 0, 6),
(8, 'Advertising', 'advertising', 'adf', 1401664277, 0, 7);

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE IF NOT EXISTS `reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `excerpt` varchar(150) NOT NULL,
  `body` text NOT NULL,
  `shoe_id` int(11) NOT NULL,
  `entry_datetime` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `ordering` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `user_id`, `title`, `slug`, `excerpt`, `body`, `shoe_id`, `entry_datetime`, `deleted`, `ordering`) VALUES
(1, 15, 'Jordan XX8 SE', 'jordan-xx8-se', 'The Air Jordan XX8 SE Men''s Basketball Shoe is equipped with with Nike Zoom Air units for elite, low-profile cushioning during play. A lightweight', 'The Air Jordan XX8 SE Men''s Basketball Shoe is equipped with Nike Zoom Air units for elite, low-profile cushioning during play. A lightweight midsole with Flight Plate technology offers stability and support, while a multidirectional outsole delivers excellent traction.\n\nSince his game-winning shot that brought the National Championship to North Carolina, Michael Jordan has been at the forefront of basketball consciousness. He took the court in 1985 wearing the original Air Jordan I, setting a new standard for performance and style, simultaneously breaking league rules and his opponents'' will while capturing the imagination of fans worldwide. Ever evolving, ever at the pinnacle of imagination, the Air Jordan line is the ultimate in basketball footwear.', 1, 1402958468, 0, 0),
(2, 15, 'Lebrons 11 elite', 'lebrons-11-elite', 'he LeBron 11 Elite Men''s Basketball Shoe features Hyperposite construction, which blends the nearly seam-free, lightweight strength of Hyperfuse with ', 'he LeBron 11 Elite Men''s Basketball Shoe features Hyperposite construction, which blends the nearly seam-free, lightweight strength of Hyperfuse with the streamlined durability of Foamposite. Full-length Nike Zoom cushioning provides low-profile, responsive impact protection.', 6, 1402973876, 0, 1),
(3, 15, 'Nike Free Trainer 5.0 iD', 'nike-free-trainer-50-id', 'The Nike Free Trainer 5.0 iD delivers lightweight flexibility, superior support and a choices of outsoles designed to provide the best traction in the', 'The Nike Free Trainer 5.0 iD delivers lightweight flexibility, superior support and a choices of outsoles designed to provide the best traction in the gym or on the pavement. With NIKEiD, you can truly own your training by customizing the entire shoe with exclusive colors, graphics and materials.\r\n\r\nNike Frees are different than traditional athletic shoes. Theyâ€™re designed to let your feet move more freely and naturally, which means your muscles will be doing a bit more work than demanded of them in some other athletic shoes. Nike encourages you to get used to them gradually for a safer, more effective and more enjoyable experience. Please visit http://nike.com/nikefree for more information on how to get the best from your Nike Free shoes.', 7, 1403030893, 0, 2),
(4, 15, 'Gucci Diamante Leather High-Top Sneaker', 'gucci-diamante-leather-hightop-sneaker', 'n 1921 Florence, a craftsman named Guccio Gucci opened his store to sell leather goods, and a legend was born. Over the decades, people around the wor', 'n 1921 Florence, a craftsman named Guccio Gucci opened his store to sell leather goods, and a legend was born. Over the decades, people around the world have admired and appreciated works from the House of Gucci---whether it''s women''s ready-to-wear, men''s designs, children''s clothes, accessories, handbags, shoes, or fragrances. Today, designer Frida Giannini holds the reins of this venerable Italian line.', 8, 1403031259, 0, 3),
(5, 15, 'nike zoom kd vi', 'nike-zoom-kd-vi', 'The KD VI Men''s Basketball Shoe was built with a new, responsive Nike Zoom unit, a more supportive upper and a lower-cut silhouette for better flex th', ' <p><b>RESPONSIVE PERFORMANCE</b></p>\r\n<br>\r\n<p>The KD VI Men''s Basketball Shoe was built with a new, responsive Nike Zoom unit, a more supportive upper and a lower-cut silhouette for better flex through the ankle&mdash;resulting in KD''s lightest shoe to date.</p>\r\n<br>\r\n<p><b>Responsive Cushioning</b></p>\r\n<p>The KD VI has visible Max Air in the heel for maximum impact protection on hard landings,&#160;plus a Nike Zoom unit in the forefoot for lightweight, low-profile responsiveness. Together with a Phylon midsole, they provide exceptional cushioning for every position on the court.</p>\r\n<br>\r\n<p><b>Ultra-Light Support</b></p>\r\n<p>The shoe''s two-layer upper is ultra-thin and features Flywire technology&mdash;super-light, super-strong nylon cables that support the foot by encasing it in a secure hug. High-wrapped rubber at the outsole adds stability and support during quick cuts.</p>\r\n<br>\r\n<p><b>Lightweight Durability</b></p>\r\n<p>A multidirectional outsole pattern offers excellent traction and durability for total control, with layers of rubber stripped away in some areas to maintain the shoe''s light weight.</p>\r\n<br>\r\n<p><b>Enhanced Flex</b></p>\r\n<p>The collar of the KD VI is lower than that of its predecessor, allowing more flexibility through the ankle.</p>\r\n<br>\r\n<p><b>More Benefits</b></p>\r\n<ul>\r\n<li>Flywire technology on the tongue for an extra-secure fit</li>\r\n<li>Asymmetrical tongue that integrates with the upper for lockdown</li>\r\n</ul>', 9, 1403031786, 0, 4),
(6, 15, 'JORDAN MELO M10', 'jordan-melo-m10', 'Carmelo Anthony''s on-court skill was a key influence in the design of the Jordan Melo M10 Men''s Basketball Shoe, featuring revolutionary Flight Plate ', 'SUPERIOR RESPONSIVENESS TO ELEVATE YOUR GAME\r\nCarmelo Anthony''s on-court skill was a key influence in the design of the Jordan Melo M10 Men''s Basketball Shoe, featuring revolutionary Flight Plate technology for maximum responsiveness and signature details that celebrate Melo''s tenth year with Nike Basketball.\r\nMAXIMUM RESPONSIVENESS\r\nJordan''s signature Flight Plate technology harnesses the energy of every step for maximum responsiveness.\r\nSECURE LOCKDOWN\r\nDynamic Fit technology integrates with the laces for a locked-down fit that molds to the unique shape of your foot.\r\nSIGNATURE DETAILS\r\nThe heel counter features a chrome finish that incorporates Melo''s first signature logo, created for the first Jordan Melo shoe ten years ago.\r\nMORE BENEFITS\r\nMolded quarter panels made of full-grain leather for a premium look\r\nPerforated side overlays for breathability and support\r\nRubber outsole for durability and traction\r\nTPU heel counter for stability and support', 10, 1403032025, 0, 5);

-- --------------------------------------------------------

--
-- Table structure for table `stores`
--

CREATE TABLE IF NOT EXISTS `stores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `description` text,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `stores`
--

INSERT INTO `stores` (`id`, `title`, `description`, `ordering`, `deleted`) VALUES
(1, 'Chippewa', '', 0, 1),
(2, 'Sebago', '', 1, 1),
(3, 'Puma', '', 2, 1),
(4, 'Under Armour', '', 3, 1),
(5, 'CNCPTS', '', 4, 1),
(6, 'The Bay', '', 5, 1),
(7, 'Amazon', '', 6, 1),
(8, 'Foot Locker', '', 7, 1),
(9, 'K-Swiss', '', 8, 1),
(10, 'Karmaloop', '', 9, 1),
(11, 'Pick Your Shoes', '', 10, 1),
(12, 'The Pop Shop', '', 11, 1),
(13, 'The Shoe Company', '', 12, 1),
(14, 'Livestock ', '', 0, 0),
(15, 'Nrml', '', 1, 0),
(16, 'Capsule', '', 2, 0),
(17, 'Serpentine ', '', 3, 0),
(18, 'Adrift ', '', 4, 0),
(19, 'Get Outside', '', 5, 0),
(20, 'Off The Hook', '', 6, 0),
(21, 'Nomad', '', 7, 0),
(22, 'Haven ', '', 8, 0);

-- --------------------------------------------------------

--
-- Table structure for table `styles`
--

CREATE TABLE IF NOT EXISTS `styles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `description` text,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `styles`
--

INSERT INTO `styles` (`id`, `title`, `description`, `ordering`, `deleted`) VALUES
(1, 'Athletic', '', 1, 0),
(2, 'Boots', '', 2, 0),
(3, 'Loafers', '', 4, 0),
(4, 'Oxfords', '', 6, 0),
(5, 'Vegan', 'Shoes for vegans', 9, 0),
(6, 'High Top', '', 5, 1),
(7, 'Drivers ', '', 5, 0),
(8, 'Lace-Ups ', '', 8, 0),
(9, 'Dress Shoes ', '', 7, 0),
(10, 'Casual ', '', 3, 0),
(11, 'Sneakers', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE IF NOT EXISTS `subscribers` (
  `email` varchar(100) NOT NULL,
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscribers`
--

INSERT INTO `subscribers` (`email`) VALUES
('gully@theyoungastronauts.com'),
('tyler@hortm.com');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `admin` int(1) NOT NULL DEFAULT '0',
  `gender` varchar(10) DEFAULT NULL,
  `age` varchar(60) DEFAULT NULL,
  `shoe_size` varchar(25) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `country` varchar(60) DEFAULT NULL,
  `title` varchar(60) DEFAULT NULL,
  `bio` text,
  `twitter` varchar(50) DEFAULT NULL,
  `facebook` varchar(60) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `image` varchar(50) DEFAULT 'default_profile.jpg',
  `entry_datetime` int(11) DEFAULT '0',
  `update_datetime` int(11) NOT NULL DEFAULT '0',
  `deleted` int(1) NOT NULL DEFAULT '0',
  `karma` int(100) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `firstname`, `lastname`, `admin`, `gender`, `age`, `shoe_size`, `city`, `country`, `title`, `bio`, `twitter`, `facebook`, `website`, `image`, `entry_datetime`, `update_datetime`, `deleted`, `karma`) VALUES
(1, 'tylersavery', 'sdgdasfds', 'f5c38375cd3ffcbfe0957cb9ae6dce35', 'Tyler', 'Savery', 1, 'sgsdaf', NULL, 'sadgdsf', 'sdgsadf', NULL, NULL, NULL, 'asdfdsf', NULL, 'dsgdsaf', NULL, 0, 0, 0, 0),
(2, 'dfad', 'dfsg', '1530d0e446ff74e728158fd5fe89e592', 'dfadsf', 'dsf', 0, 'dsfasd', NULL, 'fgdsfg', 'fdgdf', NULL, NULL, NULL, 'fdsg', NULL, 'fdsg', NULL, 0, 0, 1, 0),
(3, 'adsfdasf', 'dsaf', '47df24e6aa353e1876e81388bfb24515', 'dasfadsf', 'das', 0, 'gfsdg', NULL, 'fdgf', 'fgs', NULL, NULL, NULL, 'fds', NULL, 'hdgfh', NULL, 0, 0, 1, 0),
(4, 'asfdsf', 'retw', '0745da98f74d2927db1f7151bbdbdce1', 'gsdaf', 'das', 0, 'dsf', NULL, 'gfd', 'hgfdtr', NULL, NULL, NULL, 'bvcbc', NULL, 'sdgf', NULL, 0, 0, 1, 0),
(5, 'adsfds', 'fdsg', 'eeab8d82f794c109fc842353426daee1', 'fgsdfg', 'fgds', 0, 'fdsg', NULL, 'fdsg', 'dfsg', NULL, NULL, NULL, 'dsfg', NULL, 'hdsfg', NULL, 0, 0, 1, 0),
(6, 'adam', '', '0282c80acb3ef9ef52aad7ff1bd9a4fb', 'adam', 'gulyas', 0, '', NULL, '', '', NULL, NULL, NULL, '', NULL, '', NULL, 0, 0, 1, 0),
(7, 'bobbyb', 'bobb@asdsa.com', 'f5c38375cd3ffcbfe0957cb9ae6dce35', 'bobby', 'b', 0, 'male', NULL, '12', 'Toronto', NULL, NULL, NULL, 'bobbyyhhh', NULL, 'hey.com', NULL, 0, 0, 1, 0),
(8, 'heyaaasd', 'dfa@sd.com', 'f5c38375cd3ffcbfe0957cb9ae6dce35', 'adsf', 'sdfsdaf', 0, 'male', NULL, '21', 'Toronto', NULL, NULL, NULL, 'somet', NULL, 'dasdsad.com', NULL, 0, 0, 1, 0),
(9, 'arny', NULL, 'e2c40727bbbefdda527baadf8d929f7a', 'Arnold', 'Palmer', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 0),
(10, 'john', NULL, '3e28cf872b83ec263d9de93852c433d4', 'John', 'Doe', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 0),
(11, 'adamg', 'gully@theyoungastronauts.com', '21744fb20bbed76d69100830a9d38107', 'Adam', 'Gulyas', 0, 'M', NULL, '11', 'Whitby', NULL, NULL, NULL, '@meerp', NULL, '', NULL, 0, 0, 0, 0),
(12, 'david', 'david', '00b72fbb0801e3dc426362e3ce8ff17c', 'David', 'Bremang', 1, 'Male', '52', '18', 'toronto', 'canada', NULL, NULL, 'http://www.twitter.com', '', 'http://www.foundmissing.ca', '5205b5755891b.jpg', 0, 1403217393, 0, 3),
(13, 'Taimoor', NULL, '561a9cdedbf74132b641cf3f2aefa5b6', 'Taimoor', '.', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(14, 'duke', NULL, '00b72fbb0801e3dc426362e3ce8ff17c', 'Duke', 'Sigulinski ', 0, NULL, NULL, NULL, NULL, 'Canada', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(15, 'mike', 'mike', '4d3911052cc5a9d1f929274de9fd6312', 'mike', 'nguyen', 1, 'male', '29', '10.0', 'mississauga', 'canada', 'web developer', 'You can have it your way, how do you want it\nYou gon'' back that thing up or should i push up on it \nTemperature rising, okay lets go to the next level\nDance floor jam packed, hot as a tea kettle\nI''ll break it down for you now, baby it''s simple\nIf you be a nympho, I''ll be a nympho\nIn the hotel or in the back of the rental\nOn the beach or in the park, it''s whatever you into\nGot the magic stick, I''m the love doctor\nHave your friends teasing you ''bout how sprung I gotcha\nWanna show me how you work it baby, no problem\nGet on top then get to bouncing round like a low rider\nI''m a seasons vet when it come to this shit\nAfter you work up a sweat you can play with the stick\nI''m trying to explain baby the best way I can\nI melt in your mouth girl, not in your hands (ha ha)', '', NULL, 'http://www.hoangnguyen.ca', '5183ec421f8c1.jpg', 0, 1403208097, 0, 2),
(16, 'mikenguyen', 'mikenguyen191@gmail.com', '4d3911052cc5a9d1f929274de9fd6312', 'Mike', 'Nguyen', 0, 'male', '28', '10', 'mississauga', 'Canada', 'The Professor', NULL, NULL, NULL, NULL, 'xv9m96vnyrz73v.jpg', 1402198539, 1402470339, 0, 4),
(17, 'batman007', 'batman@batman.com', '4d3911052cc5a9d1f929274de9fd6312', 'bruce', 'wayne', 0, 'male', '32', '10.5', 'Gotham', 'United States', 'The Dark Knight', 'There is a city in the north of the United States by the name of Gotham City, once a major industrial centre it was hit badly by the Great Depression and has never really recovered. Crime rates rose, corrupt officials and despair set in despite the best efforts of the original Green Lantern and Black Canary. The darkness had taken the city and it would take a Dark Knight to reclaim it.\n\n  Young Bruce Wayne is the only son of prominent Gotham City doctor Thomas Wayne and his wife Martha Wayne. Two events in his childhood forever scared the future of Bruce. The first was when he accidentally fell through a hole in the grounds of his ancestral home Wayne Manor and found himself in a cave system that ran under the house full of bats. For years afterwards Bruce would catch sight of one particularly large bat over and over again.\n\n  The second and most tragic event happened just after the family had left a showing of the classic film "The Mark of Zorro" at the cinema and was walking back to their home. They were confronted by a mugger who demanded that they handed over their valuables. Thomas Wayne put up a struggle and in the process he and his wife was shot dead. The mugger ran off leaving the shocked Bruce alone with the bodies of his dead parents. First on the scene was physician Leslie Thompkins who sought to look after Bruce.\n\n  Thomas''s brother Phil Wayne was named Bruce''s guardian and with the help of Thompkins and the family butler Alfred Pennyworth they raised him as best they could. For days after the funeral Bruce was unsettled, it came to a head one night when he ran to his parents grave and swore his vengeance on the criminal world. On that night Bruce Wayne died and the Batman was born.\n\n  Bruce put his utmost into his school work and upon graduation he left the US to travel the world learning from the brightest and the best in each field of speciality. He also travelled to remote schools on the Far East and studied for years under the masters of the various martial arts. Upon returning to the United States, he set out on a fact-finding mission into the depraved parts of Gotham''s seedier districts to see how the criminal element had evolved. While there he became involved in a street brawl where he was seriously injured. He managed to make his way back to Wayne Manor and slumped in his study slowly bleeding he considered the situation. He was injured because the local criminals would not fear him as plain common man on the street, yet his father always maintained that criminals were a superstitious and cowardly lot. He needed to make them fear but how?', 'https://twitter.com/TheBatman', 'https://www.facebook.com/batman', NULL, '2l2gvhlqx9u764.jpg', 1402198539, 1403225668, 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
