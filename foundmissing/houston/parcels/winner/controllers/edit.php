<?php
namespace Winner\Controllers;

class Edit extends \Manage\Controllers\Manage{

	private $winner;

	public $page_title;
	public $link_back;
	public $form;
	public $existing;
	public $new_winner = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		if(isset($_POST["submit"])){
			$this->submit_form();
		}

		if(isset($data["winner_id"])){
			$this->winner = \Winner\Models\Winner::find($data["winner_id"]);
			$this->page_title = "Editing Winner";
		} else {
			$this->winner = new \Winner\Models\Winner;
			$this->page_title = "New Winner";
			$this->new_winner = true;
		}

		$this->link_back = $this->winner->link_all();

		$form = new \Form\Models\Form("edit", "POST", "", "well");

		$item = new \Form\Models\FormItemHidden(array("id" => "id", "title" => "Id", "value" => $this->winner->id));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "user_id", "title" => "User Id", "value" => $this->winner->user_id));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "event", "title" => "Event", "value" => $this->winner->event));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "prize", "title" => "Prize", "value" => $this->winner->prize));
		$form->add_item($item);

		$item = new \Form\Models\FormItemSubmit(array("value" => "Save", "id" => "submit", "class" => "btn btn-primary", "cancel_url" => $this->winner->link_all()));
		$form->add_item($item);

		$this->form = $form->render();

	}

	public function controller(){
		$this->set_view("Winner\Views\Edit");
	}

	private function submit_form(){
		if($_POST["id"] > 0){
			$winner = \Winner\Models\Winner::find($_POST["id"]);
			$flash_msg = "Winner has been updated successfully.";
		} else {
			$winner = new \Winner\Models\Winner;
			$flash_msg = "Winner has been created successfully.";
			$winner->ordering = \Winner\Models\Winner::get_next_ordering_number();
		}

		$winner->user_id = $_POST["user_id"];
		$winner->event = $_POST["event"];
		$winner->prize = $_POST["prize"];

		$winner->save();
		$this->add_flash(array("message" => $flash_msg, "type" => "success", "Heading" => "Great!"));
		redirect_to($winner->link_all());
	}

}
