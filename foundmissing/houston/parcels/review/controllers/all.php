<?php
namespace Review\Controllers;

class All extends \Manage\Controllers\Manage{

	private $reviews;

	public $reviews_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Reviews";
		$this->link_new = \Review\Models\Review::link_new();

		$this->reviews = \Review\Models\Review::all(array("conditions" => "deleted = 0 AND publish_date > 1" , "order" => "ordering"));
		$archived = \Review\Models\Review::count(array("conditions" => "deleted = 1"));
		
		$unpublished = \Review\Models\Review::all(array("conditions" => "deleted = 0 AND publish_date = 0" , "order" => "ordering"));
		if($archived > 0){
			$this->archive_button = array("url" => \Review\Models\Review::link_archive(), "title" => "Archive");
		}

		foreach($this->reviews as $review){
			$this->has_data = true;
			$this->reviews_data[] = array(
				"id" => $review->id,
				"author" => $review->author,
				"title" => $review->title,
				"slug" => $review->slug,
				"excerpt" => $review->excerpt,
				"shoe_id" => $review->shoe_id,
				"entry_datetime" => time_to_friendly_date($review->entry_datetime),
				"link_edit" => $review->link_edit(),
				"link_delete" => $review->link_delete(),
			);
		}

		foreach($unpublished as $review){
			$this->has_data = true;
			$this->reviews_unpublished[] = array(
				"id" => $review->id,
				"author" => $review->author,
				"title" => $review->title,
				"slug" => $review->slug,
				"excerpt" => $review->excerpt,
				"shoe_id" => $review->shoe_id,
				"entry_datetime" => time_to_friendly_date($review->entry_datetime),
				"link_edit" => $review->link_edit(),
				"link_delete" => $review->link_delete(),
			);
		}
		
	}

	public function controller(){
		$this->set_view("Review\Views\All");
	}

}

