$(document).ready(function() {

	// Fixes search box position for mobile
	$("#mobile_search").css("margin-top", "65px");
	
	var user_email;
	var user_password;
	var user_confirm_pass;
	var user_username;
	var user_firstname;
	var user_lastname;
	
	var email;
	var password;
	var confirm_pass;
	var username;
	var firstname;
	var lastname;
	
	var url = "/ajax/validation";
	
	$("#register_form input[name=email]").focusout(function(){
		user_email = $("input[name=email]").val();
		var data = { type: "check_email", email: user_email };
		var status = $(this);
		
		var atpos = user_email.indexOf("@");
		var dotpos = user_email.lastIndexOf(".");
		
		if (atpos< 1 || dotpos<atpos+2 || dotpos+2>=user_email.length) {
			$(status).next().removeClass("check_disabled fa-check").addClass("check_active fa-times");
			if (!user_email) {
				$("#email_message").html("required").removeClass("check_disabled").addClass("message_active");
			} else {
				$("#email_message").html("invalid email").removeClass("check_disabled").addClass("message_active");
			}
			email = false;
		} else {
			$.post(url, data, function(response) {
				if (response == 1) {
					$(status).next().removeClass("check_disabled fa-check").addClass("check_active fa-times");
					$("#email_message").html("email exists").removeClass("check_disabled").addClass("message_active");
					email = false;
				} else if (response == 0) {
					$(status).next().removeClass("check_disabled fa-times").addClass("check_active fa-check");
					$("#email_message").removeClass("message_active").addClass("check_disabled");
					email = true;
				}
			});
		}
	});
	
	$("#register_form input[name=password]").focusout(function(){
		user_password = $("input[name=password]").val();
		var regularExpression = /^(?=.*\d)(?=.*[a-zA-Z]).{6,}$/;
		var valid = regularExpression.test(user_password);
		
		if (valid == true) {
			$(this).next().removeClass("check_disabled fa-times").addClass("check_active fa-check");
			$(this).next().next().removeClass("message_active").addClass("check_disabled");
		} else if (valid == false) {
			$(this).next().removeClass("check_disabled fa-check").addClass("check_active fa-times");
			$(this).next().next().html("password must be 6 characters long and include 1 number").removeClass("check_disabled").addClass("message_active");
		} else {
			$(this).next().removeClass("check_disabled fa-check").addClass("check_active fa-times");
			$(this).next().next().html("required").removeClass("check_disabled").addClass("message_active");
		}
		
		password = valid;
	});
	
	$("input[name=confirm_password]").focusout(function(){
		user_confirm_pass = $("input[name=confirm_password]").val();
		var valid;
		
		if (user_confirm_pass === user_password) {
			if (password) {
				$(this).next().removeClass("check_disabled fa-times").addClass("check_active fa-check");
				$(this).next().next().removeClass("message_active").addClass("check_disabled");
				valid = true;
			} else {
				$(this).next().removeClass("check_disabled fa-check").addClass("check_active fa-times");
				$(this).next().next().html("passwords do not match").removeClass("check_disabled").addClass("message_active");
				valid = false;
			}
		} else {
			$(this).next().removeClass("check_disabled fa-check").addClass("check_active fa-times");
			valid = false;
		}
	});
	
	$("input[name=username]").focusout(function(){
		user_username = $("input[name=username]").val();
		var data = { type: "check_username", username: user_username };
		var regex = /^[a-zA-Z0-9_]*$/;
		var status = $(this);
		
		var valid = regex.test(user_username);
		
		if (user_username != '') {
			if (valid) {
				$.post(url, data, function(response) {
					if (response == 1) {
						$(status).next().removeClass("check_disabled fa-check").addClass("check_active fa-times");
						$("#username_message").html("user name exists").removeClass("check_disabled").addClass("message_active");
						username = false;
					} else if (response == 0) {
						$(status).next().removeClass("check_disabled fa-times").addClass("check_active fa-check");
						$("#username_message").removeClass("message_active").addClass("check_disabled");
						username = true;
					}
				});
			} else {
				$(status).next().removeClass("check_disabled fa-check").addClass("check_active fa-times");
				$("#username_message").html("may only contain alphanumeric characters and underscores").removeClass("check_disabled").addClass("message_active");
				username = false;
			}
		} else {
			$(status).next().removeClass("check_disabled fa-check").addClass("check_active fa-times");
			$("#username_message").html("required").removeClass("check_disabled").addClass("message_active");
			username = false;
		}
	});
	
	$("input[name=firstname]").focusout(function() {
		user_firstname = $("input[name=firstname]").val();
		var regex = /^[a-zA-Z_]*$/;
		
		var valid = regex.test(user_firstname);
		
		if (valid) {
			if (user_firstname) {
				$(this).next().removeClass("check_disabled fa-times").addClass("check_active fa-check");
				$("#name_message").removeClass("message_active").addClass("check_disabled");
				firstname = true;
			} else {
				$(this).next().removeClass("check_disabled fa-check").addClass("check_active fa-times");
				$("#name_message").html("requried").removeClass("check_disabled").addClass("message_active");
				firstname = false;
			}
		} else {
			$(this).next().removeClass("check_disabled fa-check").addClass("check_active fa-times");
			$("#name_message").html("may only contain letters").removeClass("check_disabled").addClass("message_active");
			firstname = false;
		}
	});
	
	$("input[name=lastname]").focusout(function() {
		user_lastname = $("input[name=lastname]").val();
		var regex = /^[a-zA-Z_]*$/;
		
		var valid = regex.test(user_lastname);
		
		if (valid) {
			if (user_lastname) {
				$(this).next().removeClass("check_disabled fa-times").addClass("check_active fa-check");
					$("#name_message").removeClass("message_active").addClass("check_disabled");
					lastname = true;
			} else {
				$(this).next().removeClass("check_disabled fa-check").addClass("check_active fa-times");
				$("#name_message").html("requried").removeClass("check_disabled").addClass("message_active");
				lastname = false;
			}
		} else {
			$(this).next().removeClass("check_disabled fa-check").addClass("check_active fa-times");
			$("#name_message").html("may only contain letters").removeClass("check_disabled").addClass("message_active");
			lastname = false;
		}
	});
	
	$("#submit_form").click(function() {
		check_submission();
	});
	
	$('#login_form input').keydown(function(event){
        if ((event.which && event.which != 13) || (event.keyCode && event.keyCode != 13)) {
            return true;
        }

        if ((event.which && event.which == 13) || (event.keyCode && event.keyCode == 13)) {
			$('#login_submit').click();
            return false;
        } else {
            return true;
        }
    });
	
	$("#register_form input").keydown(function(event){
        if ((event.which && event.which == 13) || (event.keyCode && event.keyCode == 13)) {
			check_submission();
            return false;
        } else {
            return true;
        }
    });
	
	$("#register_form select").keydown(function(event){
        if ((event.which && event.which == 13) || (event.keyCode && event.keyCode == 13)) {
			check_submission();
            return false;
        } else {
            return true;
        }
    });
	
	function check_submission() {
		if (email == false || !user_email || password == false || !user_password || confirm_pass == false || !user_confirm_pass || username == false || !user_username || firstname == false || !user_firstname || lastname == false || !user_lastname) {
			$("#error_messages").html("please check required fields").removeClass("check_disabled").addClass("name_active");
		} else {
			$("#error_messages").removeClass("name_active").addClass("check_disabled");
			$("#register_form").submit();
		}
	}
});