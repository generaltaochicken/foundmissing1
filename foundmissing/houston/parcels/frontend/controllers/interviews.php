<?php

namespace Frontend\Controllers;

class Interviews extends \Frontend\Controllers\Frontend {
	public function __construct($uri, $data) {
		parent::__construct($uri, $data);
		$this->add_asset('css', 'interview.css', true);
		$this->title = "Interviews | FM";
	}
	
	public function controller() {
		$interviews = \Interview\Models\Interview::all(array('conditions' => 'deleted = 0 AND publish_date > 1'));
		$first = true;
		
		foreach ($interviews as $interview) {
			$comment_count = \Comment\Models\Comment::getCommentCount($interview->id, 2);
			
			if ($comment_count > 1) {
				$count = $comment_count . ' comments';
			} else {
				$count = $comment_count . ' comment';
			}
		
			$this->interview[] = array(
				'title' => $interview->title,
				'quote' => $interview->excerpt,
				'author_name' => $interview->author_name,
				'date' =>  time_to_friendly_date($interview->publish_date, 'Y.m.d'),
				'slug' => $interview->slug,
				'main_image' => ARTICLE_IMAGES . $interview->main_image,
				'first' => $first,
				'comment_count' => $count
			);
			
			$first = false;
		}
		$this->set_view('Frontend\Views\Interviews');
	}
}