<?php

namespace Frontend\Controllers;

class Founder extends \Frontend\Controllers\Frontend {
	
	private $data;
	private $articles						= array();
	
	public $article 							= array();
	public $article_previous 			= null;
	public $article_next 				= null;
	public $has_previous 				= false;
	public $has_next 					= false;
	public $has_comments			= false;
	public $comments 					= array();
	public $comments_count 		= 0;
	public $facebook_data			= array();
	public $su_views					= 0;
	public $gplus							= 0;
	public $tweets							= 0;
	
	public $has_user_voted;
	public $current_user_id;
	
	private $comment_id;
	private $fb_likes						= array();
	private $fb_shares					= array();
	private $query;
	
	public function __construct($uri, $data) {
        parent::__construct($uri, $data);
		
		$this->data = $data;
		$this->add_asset('css', 'article.css', true);
		$this->current_user_id = $this->_user->id;
		
		/* Get the article based on the slug name */
		if (isset($data['slug'])) {
			$article = \Founder\Models\Founder::find(array('conditions' => 'slug = "' . $data['slug'] . '"'));
			
			$this->article = array(
				'id' => $article->id,			
				'author_name' => $article->author_name,
				'title' => $article->title,
				'slug' => $article->slug,
				'excerpt' => $article->excerpt,
				'body' => nl2br($article->body),
				'date' => time_to_friendly_date($article->publish_date, 'Y.m.d'),
				'bg_image' => ARTICLE_IMAGES . $article->bg_image_header
			);
			
			// Set the title of the browser
			$title = ucwords($article->title) . ' | FM';
			$this->title = $title;
			
			if ($this->article['id'] == null ||  empty($this->article)) {
				redirect_to('/founders');
			}
			
		} else {
			redirect_to('/founders'); // Or redirect to a 404 page
		}
		
		$query = $this->domain_url . 'founders/' . $this->article['slug'];
		
		// Get the shares data
		$this->fb_likes = $this->get_facebook_likes($query);
		$this->fb_shares = $this->get_facebook_shares($query);
		$this->facebook_data = array(
			'likes' => $this->fb_likes[0]['like_count'],
			'shares' => $this->fb_shares['shares']
		);
		$this->su_views = $this->get_stumbleupon_views($query);
		$this->gplus = $this->get_google_plus($query);
		$this->tweets = $this->get_tweets($query);
	}
	
	public function controller() {
		// Get all articles into an array
		$all_articles = \Founder\Models\Founder::all(array('conditions' => 'deleted = 0 AND publish_date > 1', 'order' => 'publish_date DESC'));

		foreach($all_articles as $article) {
			$this->articles[] = array(
				'id' => $article->id,
				'title' => $article->title,
				'slug' => $article->slug
			);
		}
		
		$this->articles = array_reverse($this->articles);
		$this->get_previous_article();
		$this->get_next_article();
		//$this->get_article_comments();
		
		// Check to see if comments of articles are posted
		if (isset($_POST['submit_comment'])) {
			$this->submit_comment();
		}
		
		// Connect controller to the view
		$this->set_view('Frontend\Views\Founder');
	}
	
	public function get_next_article() {
		$articles = $this->articles;
		// Get the next article
		$next_value = null;
		
		foreach ($articles as $article) {
			if ($this->article['id'] == $article['id']) {
				$this->article_next= array(
					'id' => $next_value['id'],
					'title' => $next_value['title'],
					'slug' => $next_value['slug']
				);
				
				break;
			}
			
			$next_value = $article;
		}
		
		if ($this->article_next['id'] == null) {
			$this->has_next = false;
		} else {
			$this->has_next = true;
		}
	}
	
	public function get_previous_article() {
		// Get the previous article
		$articles					= array_reverse($this->articles);
		$previous_value 		= null;
		
		foreach ($articles as $article) {
			if ($this->article['id'] == $article['id']) {
				$this->article_previous = array(
					'id' => $previous_value['id'],
					'title' => $previous_value['title'],
					'slug' => $previous_value['slug']
				);
				
				break;
			}
			
			$previous_value = $article;
		}
		
		if ($this->article_previous['id'] != null) {
			$this->has_previous = true;
		} else {
			$this->has_previous = false;
		}
	}
	
	public function get_article_comments() {
		$comments = \Comment\Models\Comment::all(array('conditions' => 'deleted = 0 AND post_type = 1 AND parent_id = "' . $this->article['id'] . '" AND replied_to = 0'));
		$replies = \Comment\Models\Comment::all(array('conditions' => 'deleted = 0 AND post_type = 1 AND parent_id = "' . $this->article['id'] . '" AND replied_to > 0'));
		$karmas = \Comment_Karma\Models\Comment_Karma::all(array('conditions' => 'deleted = 0 AND parent_id = "' . $this->article['id'] . '"'));
		
		$user = new \User\Models\User;
		$reply_array = array();
		
		foreach ($replies as $reply) {
			$user_info = $user->get_user_info_by_id($reply->user_id);
			
			// Get karma count for the comment
			$karma_count = 0;
			
			foreach ($karmas as $karma) {
				if ($karma->comment_id == $reply->id) {
					$karma_count += $karma->type;
					
					if ($karma->user_id == $this->_user->id) {
						if ($karma->type == 1) {
							$up = true;
						} else if ($karma->type == -1) {
							$down = true;
						}
					}
				}
			}
			
			if ($reply->user_id == $this->_user->id) {
				$their_own = true;
			} else {
				$their_own = false;
			}
			
			$reply_array[] = array(
				'id' => $reply->id,
				'user_id' => $reply->user_id,
				'fullname' => $user_info['fullname'],
				'username' => $user_info['username'],
				'user_image' => '/uploads/images/' . $user_info['image'],
				'comment_body' => $reply->comment_body,
				'comment_karma' => $karma_count,
				'replied_to' => $reply->replied_to,
				'sub_replied_to' => $reply->sub_replied_to,
				'entry_datetime' =>  $this->time_elapsed_string($reply->entry_datetime),
				'update_datetime' => $reply->update_datetime,
				'user_voted_up_reply' => $up,
				'user_voted_down_reply' => $down,
				'users_reply' => $their_own
			);
			
			$up = false;
			$down = false;
		}
		
		foreach ($comments as $comment) {
			$user_info = $user->get_user_info_by_id($comment->user_id);
			
			// Get karma count for the comment
			$karma_count = 0;
			
			foreach ($karmas as $karma) {
				if ($karma->comment_id == $comment->id) {
					$karma_count += $karma->type;
					
					if ($karma->user_id == $this->_user->id) {
						if ($karma->type == 1) {
							$up = true;
						} else if ($karma->type == -1) {
							$down = true;
						}
					}
				}
			}
			
			if ($comment->user_id == $this->_user->id) {
				$is_own = true;
			} else {
				$is_own = false;
			}
			
			$this->comments[] = array(
				'id' => $comment->id,
				'user_id' => $comment->user_id,
				'fullname' => $user_info['fullname'],
				'username' => $user_info['username'],
				'user_image' => '/uploads/images/' . $user_info['image'],
				'comment_body' => $comment->comment_body,
				'comment_karma' => $karma_count,
				'replied_to' => $comment->replied_to,
				'sub_replied_to' => $comment->sub_replied_to,
				'entry_datetime' =>  $this->time_elapsed_string($comment->entry_datetime),
				'update_datetime' => $comment->update_datetime,
				'user_voted_up' => $up,
				'user_voted_down' => $down,
				'replies' => '',
				'users_comment' => $is_own
			);
			
			$up = false;
			$down = false;
			$is_own = false;
		}
		
		for ($i = 0; $i <= count($this->comments); $i++) {
			
			foreach ($reply_array as $reply) {
				if ($this->comments[$i]['id'] == $reply['replied_to']) {
					if ($reply['sub_replied_to']) {
						$parent_user 		= \Comment\Models\Comment::find($reply['sub_replied_to']);
						$sub_parent 		= \User\Models\User::find($parent_user->user_id);
					}
					
					$this->comments[$i]['replies'][] = array(
						'id' => $reply['id'],
						'user_id' => $reply['user_id'],
						'fullname' => $reply['fullname'],
						'username' => $reply['username'],
						'user_image' => $reply['user_image'],
						'comment_body' => $reply['comment_body'],
						'comment_karma' => $reply['comment_karma'],
						'replied_to' => $reply['replied_to'],
						'sub_replied_to' => $reply['sub_replied_to'],
						'entry_datetime' => $reply['entry_datetime'],
						'update_datetime' => $reply['update_datetime'],
						'user_voted_up_reply' => $reply['user_voted_up_reply'],
						'user_voted_down_reply' => $reply['user_voted_down_reply'],
						'users_reply' => $reply['users_reply'],
						'parent_user' => ucwords($this->comments[$i]['fullname']),
						'parent_username' => $this->comments[$i]['username'],
						'child_user' => ucwords($sub_parent->firstname . ' ' . $sub_parent->lastname),
						'child_username' => $sub_parent->username
					);
				}
			}
		}
		
		$this->comments_count = count($this->comments) + count($reply_array);
		
		if ($this->comments_count > 0) {
			$this->has_comments = true;
			if ($this->comments_count > 1) {
				$this->comments_count .= ' comments';
			} else {
				$this->comments_count .= ' comment';
			}
		}
	}
	
	public function submit_comment() {
		$comment = new \Comment\Models\Comment;
		$user = \User\Models\User::find($this->user_data['id']);
		
		$comment->parent_id = $this->article['id'];
		$comment->user_id = $this->user_data['id'];
		$comment->comment_body = $_POST['comment'];
		$comment->entry_datetime = time();
		$comment->post_type = 1;
		
		$user->karma += 1;
		$comment->save();
		$user->save();
		
		$this->update_activity_feed($comment->id);
		
		redirect_to('/articles/' . $this->data['slug']);
	}
	
	private function update_activity_feed($comment_id) {
		$activity = new \Activity\Models\Activity;
		$activity->user_id = $this->_user->id;
		$activity->activity_type = 'article_post';
		$activity->source_id = $this->article['id'];
		$activity->child_id = $comment_id;
		$activity->entry_datetime = time();
		
		$activity->save();
	}
}
?>