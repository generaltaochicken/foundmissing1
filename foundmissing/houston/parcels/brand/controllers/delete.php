<?php
namespace Brand\Controllers;

class Delete extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$brand = \Brand\Models\Brand::find($data["brand_id"]);

		if($brand){
			$brand->deleted = 1;
			$brand->save();
			$this->add_undo(array("message" => "Brand has been deleted!", "action" => $brand->link_restore()));

			redirect_to($brand->link_all());
		}
	}
}
