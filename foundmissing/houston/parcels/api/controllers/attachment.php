<?php
namespace Api\Controllers;

class Attachment extends \Api\Controllers\Api{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->response = $this->get_attachments();

		$this->send_response();

	}

	protected function get_attachments(){

		try {
			$item = \Attachment\Models\Attachment::find($this->data['attachment_id']);
		} 
		catch (\ActiveRecord\RecordNotFound $e){
			return false;
		}

		
		$array = array(
			"id" => $item->id,
			"type" => $item->type,
			"image_src" => $item->image_src(false),
			"image_src_absolute" => $item->image_src(true),
			"title" => $item->title,
			"caption" => $item->caption
		);
	
		return $array;
		
	}


	



}