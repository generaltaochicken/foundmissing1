<?php
namespace Gallery\Controllers;

class Edit extends \Manage\Controllers\Manage{

	private $gallery;

	public $page_title;
	public $link_back;
	public $form;
	public $existing;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		if(isset($_POST["submit"])){
			$this->submit_form();
		}

		if (isset($data["gallery_id"])){
			$this->gallery = \Gallery\Models\Gallery::find($data["gallery_id"]);
			$this->add_attachment = array('url' => '/manage/attachments/new?item_id=' . $data["gallery_id"] . '&type=album' . '&slug=' . $this->gallery->slug,
				'multiple' => '/manage/attachments/new?item_id=' . $data["gallery_id"] . '&type=album' . '&slug=' . $this->gallery->slug . '&upload=multiple');
				
			$this->page_title = "Editing Gallery";
		} else {
			$this->gallery = new \Gallery\Models\Gallery;
			$this->page_title = "New Gallery";
		}
		

		$this->link_back = $this->gallery->link_all();

		$form = new \Form\Models\Form("edit", "POST", "", "well");

		$item = new \Form\Models\FormItemHidden(array("id" => "id", "title" => "Id", "value" => $this->gallery->id));
		$form->add_item($item);
		
		$item = new \Form\Models\FormItemText(array("id" => "author_name", "title" => "Author Name", "value" => $this->gallery->author_name));
		$form->add_item($item);
		
		$item = new \Form\Models\FormItemText(array("id" => "title", "title" => "Title", "class" => "fillslug", "value" => $this->gallery->title));
		$form->add_item($item);
		
		$item = new \Form\Models\FormItemText(array("id" => "slug", "title" => "Slug", "value" => $this->gallery->slug));
		$form->add_item($item);
		
		$item = new \Form\Models\FormItemTextarea(array("id" => "description", "title" => "Description", "value" => $this->gallery->description));
		$form->add_item($item);
		
		$item = new \Form\Models\FormItemSelect(array("id" => "publish_date", "title" => "Publish", "options" => $this->get_publish_options()));
		$form->add_item($item);

		$item = new \Form\Models\FormItemSubmit(array("value" => "Save", "id" => "submit", "class" => "btn btn-primary", "cancel_url" => $this->gallery->link_all()));
		$form->add_item($item);

		$this->form = $form->render();
		
		if (isset($data["gallery_id"])) {
			$album_attachments = \Gallery_attachment\Models\Gallery_attachment::all(array('conditions' => 'gallery_id = ' . $data["gallery_id"]));
			
			foreach ($album_attachments as $aa) {
				$attachment = \Attachment\Models\Attachment::find($aa->attachment_id);
				$this->album_attachments[] = array(
					'image' => GALLERY_IMAGES . $this->gallery->slug . DS . $attachment->image_filename,
					'title' => $attachment->title,
					'caption' => $attachment->caption
				);
			}
		}

	}

	public function controller(){
		$this->set_view("Gallery\Views\Edit");
	}
	
	private function get_publish_options() {
		$published = array(
			array('value' => '1', 'title' => 'Yes', 'selected' => 0),
			array('value' => '0', 'title' => 'No', 'selected' => 1)
		);
		
		if  ($this->gallery->publish_date != 0 || $this->gallery->publish_date != null) {
			$published[0]['selected'] = 1;
			$published[1]['selected'] = 0;
		}
		
		return $published;
	}

	private function submit_form(){
		if($_POST["id"] > 0){
			$gallery = \Gallery\Models\Gallery::find($_POST["id"]);
			$flash_msg = "Gallery has been updated successfully.";
		} else {
			$gallery = new \Gallery\Models\Gallery;
			$gallery->entry_datetime = time();
			$flash_msg = "Gallery has been created successfully.";
			$gallery->ordering = \Gallery\Models\Gallery::get_next_ordering_number();
		}
		
		$gallery->title = $_POST["title"];
		$gallery->description = $_POST["description"];
		$gallery->author_name = $_POST["author_name"];
		$gallery->slug = $_POST["slug"];
		
		$gallery->publish_date = $_POST["publish_date"];
		if ($gallery->publish_date == 1) {
			if ($this->gallery->publish_date <= 1) {
				$gallery->publish_date = time();
			}
		}

		$gallery->save();
		$this->add_flash(array("message" => $flash_msg, "type" => "success", "Heading" => "Great!"));
		redirect_to($gallery->link_all());
	}

}
