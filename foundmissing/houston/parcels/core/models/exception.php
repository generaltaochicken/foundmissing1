<?php
/**
 * ---------------------------------------------------------------
 * Houston v1.1
 * ---------------------------------------------------------------
 * @author Tyler Savery | The Young Astronauts
 * @see http://www.houstonmvc.com
 * @license Open Source
 * ---------------------------------------------------------------
 * 
 * Exception Handler
 *
 */
namespace Core\Models;

/**
 * Main exception handler extends regular exceptions
 */
class Exception extends \Exception {

    /**
     * @param object The original exception sent by the PHP handler
     */
	public static function handleException($exception = '') {
		if (DEBUG) {
			// Split off the trace value if it exists
			if ($position = strpos($exception->getMessage(), 'Stack trace:') === false) {
				$message = $exception->getMessage();
			} else {
				$message = substr($exception->getMessage(), 0, $position);
			}
			$output = new \Mustache_Engine();
			echo $output->render(file_get_contents(CORE_ROOT.'views'.DS.'exception.html'), array(
				'exception' => &$exception,
				'message'   => $message,
				'trace'     => $exception->getTrace()
			));
		}
	}

}