<?php
namespace Footwear\Controllers;

class Edit extends \Manage\Controllers\Manage{

	private $footwear;

	public $page_title;
	public $link_back;
	public $form;
	public $existing;
	
	public $add_attachment;
	public $attachments = array();

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		if(isset($_POST["submit"])){
			$this->submit_form();
		}

		if(isset($data["footwear_id"])){
			$this->footwear = \Footwear\Models\Footwear::find($data["footwear_id"]);
			$this->add_attachment = array('url' => '/manage/attachments/new?item_id=' . $this->footwear->id . '&type=footwear',
				'multiple' => '/manage/attachments/new?item_id=' . $this->footwear->id . '&type=footwear&upload=multiple');
			$this->page_title = "Editing Footwear";
		} else {
			$this->footwear = new \Footwear\Models\Footwear;
			$this->page_title = "New Footwear";
		}

		$this->link_back = $this->footwear->link_all();

		$form = new \Form\Models\Form("edit", "POST", "", "well");

		$item = new \Form\Models\FormItemHidden(array("id" => "id", "title" => "Id", "value" => $this->footwear->id));
		$form->add_item($item);

		$item = new \Form\Models\FormItemSelect(array("id" => "brand_id", "title" => "Brand", "options" => $this->get_brand_options($this->footwear->brand_id)));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "model_name", "title" => "Name", "class" => "fillslug", "value" => $this->footwear->model_name));
		$form->add_item($item);
		
		$item = new \Form\Models\FormItemText(array("id" => "slug", "title" => "Slug", "value" => $this->footwear->slug));
		$form->add_item($item);

		$item = new \Form\Models\FormItemSelect(array("id" => "style_id", "title" => "Styles (Hold CTRL to select mutliple)", "multiple" => "true", "options" => $this->get_style_options($this->footwear->style_id)));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "tags", "title" => "Tags (separate with comma ex. hi-top, retro, jordans)", "value" => $this->footwear->tags));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "price", "title" => "Price (ex. 249.99)", "value" => $this->footwear->price));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "rating", "title" => "Rating (ex. 8.1)", "value" => $this->footwear->rating));
		$form->add_item($item);

		$item = new \Form\Models\FormItemSelect(array("id" => "color_id", "title" => "Colors (Hold CTRL to select mutliple)",  "multiple" => "true", "options" => $this->get_color_options($this->footwear->color_id)));
		$form->add_item($item);
		
		$item = new \Form\Models\FormItemText(array("id" => "release_date", "title" => "Release Date",  "class" => "datepicker", "value" => time_to_friendly_date($this->footwear->release_date)));
		$form->add_item($item);
		
		$item = new \Form\Models\FormItemTextarea(array("id" => "description", "title" => "Description", "value" => $this->footwear->description));
		$form->add_item($item);
		
		$item = new \Form\Models\FormItemText(array("id" => "external", "title" => "Link", "value" => $this->footwear->external));
		$form->add_item($item);

		$item = new \Form\Models\FormItemSubmit(array("value" => "Save", "id" => "submit", "class" => "btn btn-primary", "cancel_url" => $this->footwear->link_all()));
		$form->add_item($item);
		
		$item = new \Form\Models\FormItemSelect(array("id" => "publish_date", "title" => "Publish", "options" => $this->get_publish_options()));
		$form->add_item($item);

		$this->form = $form->render();
		
		if (isset($data["footwear_id"])) {
			$item_attachments = \Footwear_attachment\Models\Footwear_attachment::all(array('conditions' => 'footwear_id = ' . $data["footwear_id"]));
			
			foreach ($item_attachments as $aa) {
				$attachment = \Attachment\Models\Attachment::find($aa->attachment_id);
				$this->item_attachments[] = array(
					'image' => $attachment->image_filename,
					'title' => $attachment->title,
					'caption' => $attachment->caption
				);
			}
		}
	}

	public function controller(){
		$this->set_view("Footwear\Views\Edit");
	}

	private function submit_form(){
		if($_POST["id"] > 0){
			$footwear = \Footwear\Models\Footwear::find($_POST["id"]);
			$flash_msg = "Footwear has been updated successfully.";
		} else {
			$footwear = new \Footwear\Models\Footwear;
			$flash_msg = "Footwear has been created successfully.";
			$footwear->ordering = \Footwear\Models\Footwear::get_next_ordering_number();
		}

		$footwear->brand_id = $_POST["brand_id"];
		$footwear->model_name = $_POST["model_name"];
		$footwear->slug = $_POST["slug"];
		$footwear->tags = $_POST["tags"];
		$footwear->price = $_POST["price"];
		$footwear->rating = $_POST["rating"];
		$footwear->description = $_POST["description"];
		$footwear->external = $_POST["external"];
		$footwear->entry_datetime = time();
		$footwear->release_date = strtotime($_POST["release_date"]);
		
		if (is_array($_POST['style_id'])) {
			$footwear->style_id = implode(',', $_POST["style_id"]);
		}
		
		if (is_array($_POST['color_id'])) {
			$footwear->color_id = implode(',', $_POST["color_id"]);
		}

		$footwear->save();
		
		$this->add_flash(array("message" => $flash_msg, "type" => "success", "Heading" => "Great!"));
		//redirect_to($footwear->link_all());
		redirect_to($footwear->link_edit());
	}
	
	protected function get_brand_options($current){

		$brands = \Brand\Models\Brand::all(array('conditions' => 'deleted = 0', 'order' => 'title'));
		return $this->get_options($brands, $current);
	}
	
	protected function get_style_options($current){

		$styles = \Style\Models\Style::all(array('conditions' => 'deleted = 0', 'order' => 'ordering'));
		return $this->get_options($styles, $current, true);
	}
	
	protected function get_color_options($current){
		$color = \Color\Models\Color::all(array('conditions' => 'deleted = 0', 'order' => 'ordering'));
		return $this->get_options($color, $current, true);
	}
	
	private function get_publish_options() {
		$published = array(
			array('value' => '1', 'title' => 'Yes', 'selected' => 0),
			array('value' => '0', 'title' => 'No', 'selected' => 1)
		);
		
		if  ($this->footwear->publish != 0 || $this->footwear->publish != null) {
			$published[0]['selected'] = 1;
			$published[1]['selected'] = 0;
		}
		
		return $published;
	}

}
