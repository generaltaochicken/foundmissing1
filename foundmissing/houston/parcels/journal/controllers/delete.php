<?php
namespace Journal\Controllers;

class Delete extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$Journal = \Journal\Models\Journal::find($data["Journal_id"]);

		if($Journal){
			$Journal->deleted = 1;
			$Journal->save();
			$this->add_undo(array("message" => "Journal has been deleted!", "action" => $Journal->link_restore()));

			redirect_to($Journal->link_all());
		}
	}
}
