<?php
namespace Gallery\Controllers;

class All extends \Manage\Controllers\Manage{

	private $gallerys;

	public $gallerys_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Gallerys";
		$this->link_new = \Gallery\Models\Gallery::link_new();

		$this->gallerys = \Gallery\Models\Gallery::all(array("conditions" => "deleted = 0 AND publish_date > 1" , "order" => "ordering"));
		$unpublished = \Gallery\Models\Gallery::all(array("conditions" => "deleted = 0 AND publish_date = 0" , "order" => "ordering"));
		$archived = \Gallery\Models\Gallery::count(array("conditions" => "deleted = 1"));
		if($archived > 0){
			$this->archive_button = array("url" => \Gallery\Models\Gallery::link_archive(), "title" => "Archive");
		}

		foreach($this->gallerys as $gallery){
			$this->has_data = true;
			$this->gallerys_data[] = array(
				"id" => $gallery->id,
				"title" => $gallery->title,
				"description" => $gallery->description,
				"author_name" => $gallery->author_name,
				"entry_datetime" => time_to_friendly_date($gallery->entry_datetime),
				"link_edit" => $gallery->link_edit(),
				"link_delete" => $gallery->link_delete(),
				"ordering" => $gallery->ordering,
			);
		}
		
		foreach($unpublished as $gallery){
			$this->has_data = true;
			$this->gallyer_unpublished[] = array(
				"id" => $gallery->id,
				"title" => $gallery->title,
				"description" => $gallery->description,
				"author_name" => $gallery->author_name,
				"entry_datetime" => time_to_friendly_date($gallery->entry_datetime),
				"link_edit" => $gallery->link_edit(),
				"link_delete" => $gallery->link_delete(),
				"ordering" => $gallery->ordering,
			);
		}

	}

	public function controller(){
		$this->set_view("Gallery\Views\All");
	}

}

