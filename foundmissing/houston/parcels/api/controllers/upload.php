<?php
namespace Api\Controllers;

class Upload extends \Api\Controllers\Api{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->allow_post = true;

		$img = $_POST['image'];

		$img = str_replace('data:image/jpeg;base64,', '', $img);
	    $img = str_replace(' ', '+', $img);
	    $data = base64_decode($img);
	    $filename = uniqid() . '.jpg';
	    $file = IMAGE_UPLOAD_DIRECTORY . $filename;
	    $success = file_put_contents($file, $data);

	    print $success ? $filename : 'error';

	}

	public function controller(){

		return;

	}

	public function render_view(){
		return;
	}

}