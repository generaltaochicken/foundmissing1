<?php
namespace Frontpage\Controllers;

class Restore extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$frontpage = \Frontpage\Models\Frontpage::find($data["frontpage_id"]);

		if($frontpage){
			$frontpage->deleted = 0;
			$frontpage->save();
			$this->add_flash(array("message" => "Frontpage has been restored!"));

			redirect_to($frontpage->link_all());
		}
	}
}
