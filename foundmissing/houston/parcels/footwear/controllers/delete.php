<?php
namespace Footwear\Controllers;

class Delete extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$footwear = \Footwear\Models\Footwear::find($data["footwear_id"]);

		if($footwear){
			$footwear->deleted = 1;
			$footwear->save();
			$this->add_undo(array("message" => "Footwear has been deleted!", "action" => $footwear->link_restore()));

			redirect_to($footwear->link_all());
		}
	}
}
