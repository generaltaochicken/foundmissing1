<?php

namespace Form\Models;

class Form extends \ActiveRecord\Model {
    
    protected $form_id;
    protected $type;
    protected $action;
    protected $class;
    protected $enctype;
    protected $method;
    
    public $form_items = array();
    
    public function __construct($form_id = 'form', $method = 'POST', $action = '', $class = '', $enctype = 'multipart/form-data'){
        
        $this->form_id = $form_id;
        $this->method = $method;
        $this->action = $action;
        $this->class = $class;
        $this->enctype = $enctype;
        
    }
    
    public function get_head(){
        return '<form id="'. $this->form_id .'" method="'. $this->method .'" action="'. $this->action .'" class="'. $this->class .'" enctype="'. $this->enctype .'">';
    }
    
    public function get_foot(){
        return '</form>';
    }
    
    public function add_item($item){
        $this->form_items[] = $item;
    }
    
    public function render(){
        $output = $this->get_head();
                
        foreach($this->form_items as $item){
            $output .= $item->get_output();
        }
        
        $output .= $this->get_foot();
        
        return $output;
    }
    
    
    
}