<?php
namespace Designer\Controllers;

class Archive extends \Manage\Controllers\Manage{

	private $designers;

	public $designers_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Deleted Designers";

		$this->designers = \Designer\Models\Designer::all(array("conditions" => "deleted = 1"));
		$this->archive_button = array("url" => \Designer\Models\Designer::link_all(), "title" => "Back");
		foreach($this->designers as $designer){
			$this->has_data = true;
			$this->designers_data[] = array(
				"id" => $designer->id,
				"title" => $designer->title,
				"link_restore" => $designer->link_restore(),
			);
		}

	}

	public function controller(){
		$this->set_view("Designer\Views\All");
	}

}

