<?php
namespace Winner\Controllers;

class Archive extends \Manage\Controllers\Manage{

	private $winners;

	public $winners_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Deleted Winners";

		$this->winners = \Winner\Models\Winner::all(array("conditions" => "deleted = 1"));
		$this->archive_button = array("url" => \Winner\Models\Winner::link_all(), "title" => "Back");
		foreach($this->winners as $winner){
			$this->has_data = true;
			$this->winners_data[] = array(
				"id" => $winner->id,
				"user_id" => $winner->user_id,
				"entry_datetime" => $winner->entry_datetime,
				"event" => $winner->event,
				"prize" => $winner->prize,
				"link_restore" => $winner->link_restore(),
			);
		}

	}

	public function controller(){
		$this->set_view("Winner\Views\All");
	}

}

