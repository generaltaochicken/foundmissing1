<?php
namespace Api\Controllers;

class Item extends \Api\Controllers\Api{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->response = $this->get_item();

		$this->send_response();

	}

	protected function get_item(){

		try {
			$item = \Item\Models\Item::find($this->data['item_id']);
		} 
		catch (\ActiveRecord\RecordNotFound $e){
			return false;
		}

		$attachments = $item->attachments;
		$attachment_array = array();
		foreach($attachments as $attachment){

			$attachment_array[] = array(
				"id" => $attachment->id,
				"type" => $attachment->type,
				"image_src" => $attachment->image_src(false),
				"image_src_absolute" => $attachment->image_src(true),
				"title" => $attachment->title,
				"caption" => $attachment->caption
			);

		}
		
		$array = array(
			"id" => $item->id,
			"user_id" => $item->user_id,
			"category_id" => $item->category_id,
			"title" => $item->title,
			"excerpt" => $item->excerpt,
			"body" => $item->body,
			"publish_datetime" => $item->publish_datetime,
			"attachments" => $attachment_array
		);
	
		return $array;
		
	}




}