<?php

namespace Account\Controllers;

class Register extends \Account\Controllers\Account {

	public $form;
	public $error;
	public $temp_email;
	public $temp_password;
	
	private $_user_email;
	private $_user_fullname;

	public function __construct($uri, $data){
        parent::__construct($uri, $data);

		$this->add_asset('css', 'fm_login.css', true);
		$this->add_asset('js', 'validation.js', true);
		 
		if (isset($_SESSION['temp_email']) && isset($_SESSION['temp_password'])) {
			$this->temp_email = $_SESSION['temp_email'];
			$this->temp_password = $_SESSION['temp_password'];
		}

        if (isset($_POST['user_signup2'])){
            $this->submit_form();
        }
    }

    public function controller(){
    	$this->set_view('Account\Views\Register');
	}

	private function submit_form(){
		
		$this->user_code = $this->generateRandomString();
		
		$user = new \User\Models\User();
        $user->username = $_POST['username'];
        $user->firstname = $_POST['firstname'];
        $user->lastname = $_POST['lastname'];
        $user->email = $_POST['email'];
        $user->gender = $_POST['gender'];
        $user->shoe_size = $_POST['shoe_size'];
        $user->city = $_POST['city'];
        $user->country = $_POST['country'];
		$user->age = $_POST['age'];
		$user->title = $_POST['title'];
		$user->image = 'default_avatar.png';
		$user->entry_datetime = time();
		$user->update_datetime = time();
		$user->code = $this->user_code;


        if($_POST['password'] != ''){
            $user->password = md5(DB_SALT . $_POST['password']);
        }

        $user->save();
		$this->_user_email 			= $_POST['email'];
		$this->_user_fullname 		= ucwords($_POST['firstname'] . ' ' . $_POST['lastname']);
		//$this->SendConfirmationCode();
		
        $user->password = $_POST['password'];
        $login = $user->login();
		
		if($login){
		    redirect_to("/account/me");
		}

	}
	
	private function SendConfirmationCode() {
		require_once(PARCEL_ROOT . 'libraries/PHPMailer/PHPMailerAutoload.php');
		$mail = new \PHPMailer();
		$mail->isSMTP();                                      												// Set mailer to use SMTP
		$mail->Host = 'smtp.gmail.com';  															// Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               										// Enable SMTP authentication
		$mail->Username = 'info@foundmissing.ca';                 						// SMTP username
		$mail->Password = 'sneakers10';                    										// SMTP password
		$mail->SMTPSecure = 'tls';                            										// Enable encryption, 'ssl' also accepted

		$mail->From = 'info@foundmissing.ca';
		$mail->FromName = 'FoundMissing';
		$mail->addAddress($this->_user_email, $this->_user_fullname);  		// Add a recipient
		//$mail->addAddress('ellen@example.com');               							// Name is optional
		//$mail->addReplyTo('info@example.com', 'Information');
		//$mail->addCC('mike@foundmissing.com');
		//$mail->addBCC('bcc@example.com');

		$mail->WordWrap = 50;                                 										// Set word wrap to 50 characters
		//$mail->addAttachment('/var/tmp/file.tar.gz');         								// Add attachments
		//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    						// Optional name
		$mail->isHTML(true);                                  											// Set email format to HTML

		$mail->Subject 		= 'Welcome to FoundMissing';
		$mail->Body    		= '<img src="http://54.191.154.205/images/fm-primaryred-256.png" /><p>Hey ' . $this->_user_fullname . ',</p>
								<p>First and foremost we would like to thank you for attending FoundMissing\'s official launch. We’re glad to have you on board of this great journey. FoundMissing wouldn’t be here without the support system of the friends, family, co-workers, small businesses, and the city. Each and every one of you serve a purpose of making FoundMissing what it is today.</p>
								<p>FoundMissing is your go-to online hub, aimed to change the way you discover, share, and purchase popular men’s footwear brands. It doesn’t just end there. FoundMissing’s passionate and innovative team is focused on the craft to bring you the ultimate, visually appealing user experience.</p>
								<p>Here is your special code to enter our draw to win these special items from our sponsors. <b>' . $this->user_code . '</b></p>
								<img src="http://54.191.154.205/images/sponsors.png" />
								<h3>Want More?</h3>
								<p>The most unique #FMVIEW shot will win a pair of Air Jordan VI Retro ‘Spizike’.</p>
								<img src="http://54.191.154.205/images/main_homer.jpg" />
								<p>#FMVIEW is a point of view squared-frame shot of any shoe positioned to the corner of the frame.</p>
								<p>Contest Rules:<br />
									1) Must be a guest of the #FMOrigin event<br />
									2) Please ensure your Instagram privacy settings are public, so we can see your photo<br />
									3) Must register as a member of www.FoundMissing.ca in order to qualify for entry<br />
									4) This is about originality, so let’s see your best shot!</p>
									
								<img src="http://54.191.154.205/images/fmview_DEV01-3.jpg" />';
		
		
		//$mail->AltBody 		= 'Your raffle code ' . $this->user_code;

		if(!$mail->send()) {
			//echo 'Message could not be sent.';
			//echo 'Mailer Error: ' . $mail->ErrorInfo;
		} else {
			//echo 'Message has been sent';
		}
	}
	
	private function generateRandomString() {
		$length 				= 10;
		$characters 		= '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$randomString 	= '';
		
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, strlen($characters) - 1)];
		}
		
		return $randomString;
	}
}