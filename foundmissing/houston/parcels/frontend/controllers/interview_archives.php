<?php
namespace Frontend\Controllers;

class Interview_Archives extends \Frontend\Controllers\Frontend {
    
	private $data;
	
    public function __construct($uri, $data) {
		parent::__construct($uri, $data);
		$this->add_asset('css', 'interview.css', true);
		$interviews = \Interview\Models\Interview::all(array('conditions' => 'deleted = 0'));
		
		foreach ($interviews as $interview) {
			$this->interview[] = array(
				'title' => $interview->title,
				'slug' => $interview->slug,
				'image' => ARTICLE_IMAGES . $interview->main_image
			);
		}
	}
	
	public function controller() {
		$this->set_view('Frontend\Views\Interview_Archives');
	}
}