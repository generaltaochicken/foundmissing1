$(window).resize(function(){
	if ($(window).width() < 768) {
		var article_width = $('.article-container').width();
		$('.article').css("width", article_width);
	} else {
		var article_width = $('.article-container').width() * 0.25;
		$('.article').css("width", article_width);
	}
	
	var width = $("#footwear_sidemenu").width();
	$("#footwear_category_options").css("left", width);
	$("#footwear_brands_options").css("left", width);
	
	var textbox_width = $("#fw_brands_userinput").outerWidth(true);
	$("#user_input_brand").css("width", textbox_width);
});

$(document).ready(function() {
	var toggle_reset;
	var url = "/ajax/getoptions";
	
	$("#footwear").toggle(function() {
		var height = $("#footwear_tool").height() + 5;
		$(".footwear-page-fix").animate({marginTop: '+=' + height}, "fast");
		$("#footwear_tool").slideDown("fast");
		toggle_reset = false;
	}, function() {
		var height = $("#footwear_tool").height() + 5;
		$(".footwear-page-fix").animate({marginTop: '-=' + height}, "fast");
		
		$("#footwear_tool").slideUp("fast");
		
		if ($("#footwear_category_options").is(":visible")) {
			$("#fw_select_cat_button").click();
			toggle_reset = true;
		}
		
		if ($("#footwear_brands_options").is(":visible")) {
			$("#brands_select_button").click();
			toggle_reset = true;
		}
		
		if ($("#footwear_sidemenu").is(":visible")) {
			$(".fw_side_menu_button").click();
		}
	});
	
	$(".fw_side_menu_button").toggle(function() {
		$("#footwear_sidemenu").slideDown("fast");
	}, function() {
		$("#footwear_sidemenu").slideUp("fast");
		
		if ($("#footwear_category_options").is(":visible") && toggle_reset == false) {
			$("#fw_select_cat_button").click();
		};
		
		if ($("#footwear_brands_options").is(":visible") && toggle_reset == false) {
			$("#brands_select_button").click();
		}
	});
	
	$("#fw_brands_userinput").on("input", function() {
		var brand_name = $("#fw_brands_userinput").val();
		var data = { type: 'get_brands_autocomplete', brand_name: brand_name };
		
		$.post(url, data, function(response) {
			$("#user_input_brand").html(response);
		});
		
		var width = $("#fw_brands_userinput").outerWidth(true);
		$("#user_input_brand").css("width", width);
		$("#user_input_brand").show();
	});
	
	$("span.user_brand_id").live("click", function() {
		var brand_values = $("#footwear_brands").val();
		var brand_name = $(this).text();
		
		$("#user_input_brand").stop();
		$("#fw_brands_userinput").val(brand_name);
		$("#fw_brands_userinput").focus();
		
		if ($("#footwear_brands").val() != "") {
			$("#footwear_brands").val(brand_values + ", " + brand_name);
		} else {
			$("#footwear_brands").val(brand_values + brand_name);
		}
		
		$(".brand_id:contains(" + brand_name + ")").addClass("selected_option");
		$(".footwear_tags").append("<span data-brandname='" + brand_name + "' class='brand_name_tag'>" + brand_name + "<span class='delete'>x</span></span>");
		even_footwear_bar();
	});
	
	$("#fw_brands_userinput").blur(function() {
		$("#user_input_brand").delay(200).hide(0, function () {
			$(this).empty();
		});
	});
	
	$("#fw_brands_userinput").focusin(function() {
		$("#user_input_brand").show();
	});
	
	
	var got_brands = false;
	$("#brands_select_button").toggle(function() {
		var data = { type: 'get_brands' };
		
		if (!got_brands) {
			$.post(url, data, function(response) {
				$("#footwear_brands_options").html(response);
				got_brands = true;
			});
		}
		
		var width = $("#footwear_sidemenu").width();
		$("#footwear_brands_options").css("left", width);
		$("#footwear_brands_options").slideDown("fast");
		
	}, function() {
		$("#footwear_brands_options").slideUp("fast");
	});
	
	var got_categories = false;
	$("#fw_select_cat_button").toggle(function() {
		$(this).css('-webkit-transform','rotate(90deg)'); 
		$(this).css('-moz-transform','rotate(90deg)');
		$(this).css('transform','rotate(90deg)');
		
		var data = { type: 'get_categories' };
		
		if (!got_categories) {
			$.post(url, data, function(data) {
				$("#footwear_category_options").html(data);
				got_categories = true;
			});
		}
		
		var width = $("#footwear_sidemenu").width();
		$("#footwear_category_options").css("left", width);
		$("#footwear_category_options").slideDown("fast");
		
	}, function() {
		$(this).css('-webkit-transform','rotate(0deg)'); 
		$(this).css('-moz-transform','rotate(0deg)');
		$(this).css('transform','rotate(0deg)');
		$("#footwear_category_options").slideUp("fast");
	});
	
	$(".brand_id").live("click", function() {
		var elem = $(this);
		var brand_id = elem.data("id");
		var brand_name = elem.text();
		var brand_values = $("#footwear_brands").val();
		
		if (elem.hasClass("selected_option")) {
			elem.removeClass("selected_option");
			
			if (brand_values.indexOf(brand_name) > -1) {
				if (brand_values.indexOf(brand_name + ',') > -1) {
					brand_values = brand_values.replace(brand_name + ', ', "");
				} else if (brand_values.indexOf(', ' + brand_name) > -1) {
					brand_values = brand_values.replace(', ' + brand_name, "");
				} else {
					brand_values = brand_values.replace(brand_name, "");
				}
				
				$("#footwear_brands").val(brand_values);
			}
			
			$(".brand_name_tag:contains(" + brand_name + ")").remove();
			even_footwear_bar();
		} else {
			elem.addClass("selected_option");
			
			if ($("#footwear_brands").val() != "") {
				$("#footwear_brands").val(brand_values + ", " + brand_name);
			} else {
				$("#footwear_brands").val(brand_values + brand_name);
			}
			
			$(".footwear_tags").append("<span data-brandname='" + brand_name + "' class='brand_name_tag'>" + brand_name + "<span class='delete'>x</span></span>");
			
			
			even_footwear_bar();
		}
		
		if (auto_refine) {
			auto_refined();
		}
	});
	
	$(".styles_id").live("click", function() {
		var elem = $(this);
		var brand_id = elem.data("id");
		var brand_name = elem.text();
		var brand_values = $("#footwear_select_category").val();
		
		if (elem.hasClass("selected_option")) {
			elem.removeClass("selected_option");
			
			if (brand_values.indexOf(brand_name) > -1) {
				if (brand_values.indexOf(brand_name + ',') > -1) {
					brand_values = brand_values.replace(brand_name + ', ', "");
				} else if (brand_values.indexOf(', ' + brand_name) > -1) {
					brand_values = brand_values.replace(', ' + brand_name, "");
				} else {
					brand_values = brand_values.replace(brand_name, "");
				}
				
				$("#footwear_select_category").val(brand_values);
			}
			
			$(".brand_name_tag:contains(" + brand_name + ")").remove();
			even_footwear_bar();
		} else {
			elem.addClass("selected_option");
			
			if ($("#footwear_select_category").val() != "") {
				$("#footwear_select_category").val(brand_values + ", " + brand_name);
			} else {
				$("#footwear_select_category").val(brand_values + brand_name);
			}
			
			$(".footwear_tags").append("<span data-categoryname='" + brand_name + "' class='brand_name_tag'>" + brand_name + "<span class='delete'>x</span></span>");
			
			
			even_footwear_bar();
		}
		
		if (auto_refine) {
			auto_refined();
		}
	});
	
	$(".delete").live("click", function() {
		if ($(this).parent().data("brandname")) {
			var brand_name = $(this).parent().data("brandname")
			var brand_values = $("#footwear_brands").val();
			$(".brand_id:contains(" + brand_name + ")").removeClass("selected_option");
			$(this).parent().remove();
		} else if ($(this).parent().data("categoryname")) {
			var brand_name = $(this).parent().data("categoryname")
			var brand_values = $("#footwear_select_category").val();
			$(".styles_id:contains(" + brand_name + ")").removeClass("selected_option");
			$(this).parent().remove();
		} else if ($(this).parent().data("colorid")) {
			var brand_name = $(this).parent().data("colorid");
			var brand_values = $("#footwear_colors").val();
			$(".color_box[data-colorid='" + brand_name + "']").removeClass("selected_color");
			$(this).parent().remove();
		};
		
		if (brand_values.indexOf(brand_name) > -1) {
			if (brand_values.indexOf(brand_name + ',') > -1) {
				brand_values = brand_values.replace(brand_name + ', ', "");
			} else if (brand_values.indexOf(', ' + brand_name) > -1) {
				brand_values = brand_values.replace(', ' + brand_name, "");
			} else {
				brand_values = brand_values.replace(brand_name, "");
			}
			
			if ($(this).parent().data("brandname")) {
				$("#footwear_brands").val(brand_values);
			} else if ($(this).parent().data("categoryname")) {
				$("#footwear_select_category").val(brand_values);
			} else if ($(this).parent().data("colorid")) {
				$("#footwear_colors").val(brand_values);
			}
		}
		
		even_footwear_bar();
		
		if (auto_refine) {
			auto_refined();
		}
	});
	
	$(".color_box").live("click", function() {
		var color = $(this).attr("title");
		var id = $(this).data("colorid");
		var brand_values = $("#footwear_colors").val();
		
		if ($(this).hasClass("selected_color")) {
			$(this).removeClass("selected_color");
			
			$('.brand_name_tag[data-colorid="' + id + '"]').remove();
			
			if (brand_values.indexOf(id) > -1) {
				if (brand_values.indexOf(id + ',') > -1) {
					brand_values = brand_values.replace(id + ', ', "");
				} else if (brand_values.indexOf(', ' + id) > -1) {
					brand_values = brand_values.replace(', ' + id, "");
				} else {
					brand_values = brand_values.replace(id, "");
				}
				
				$("#footwear_colors").val(brand_values);
			}
		} else {
			$(this).addClass("selected_color");
			
			if ($("#footwear_colors").val() != "") {
				$("#footwear_colors").val(brand_values + ", " + id);
			} else {
				$("#footwear_colors").val(brand_values + id);
			}
			
			$(".footwear_tags").append("<span class='brand_name_tag' data-colorid='" + id + "'>" + color + "<span class='delete'>x</span></span>");
		}
		
		even_footwear_bar();
		
		if (auto_refine) {
			auto_refined();
		}
	});
	
	$("#start_over").live("click", function() {
		$(".brand_id").removeClass("selected_option");
		$(".styles_id").removeClass("selected_option");
		$(".color_box").removeClass("selected_color");
		$(".brand_name_tag").remove();
		$("#footwear_brands").val('');
		$("#fw_brands_userinput").val('');
		$("#footwear_select_category").val('');
		$("#footwear_colors").val('');
		
		even_footwear_bar();
	});
	
	$(".submit_refine").live("click", function() {
		var brands = $("#footwear_brands").val();
		var categories = $("#footwear_select_category").val();
		var colors = $("#footwear_colors").val();
		var price =  JSON.stringify({ min: $("#min_price").val(), max: $("#max_price").val() });
		
		console.log(price);
		var data = { type: 'refine_search', brands: brands, categories: categories, colors: colors, price: price };
		var height = $("#footwear_tool").height() + 60;
		
		$.post(url, data, function(response) {
			$(".fw_side_menu_button").click();
			$("#main_container").hide().html(response).fadeIn("slow");
			
			$(".footwear-page-fix").css("margin-top", height);
		});
	});
	
	var auto_refine = false;
	$(".auto_refine").toggle(function() {
		$(".auto_refine").css("color", "#FFFFFF");
		auto_refine = true;
	}, function() {
		$(".auto_refine").css("color", "#A0A6A6");
		auto_refine = false;
	});
	
	// FAVORITE A SHOE
	$(".footwear_bookmark").live("click", function() {
		var elem = $(this);
		var id = elem.data("shoeid");
		
		if (elem.hasClass("footwear_bookmarked")) {
			var type = 'delete_favorites';
			var data = { id: id, type: type };
			
			$.post(url, data, function(response) {
				
				var res = jQuery.parseJSON(response);
				if (res.status) {
				
					elem.removeClass("footwear_bookmarked");
				} else {
				
				}
			});
		} else {
			var type = 'add_favorites';
			var data = { id: id, type: type };
			
			$.post (url, data, function(response) {
				var res = jQuery.parseJSON(response);
				if (res.status) {
					elem.addClass("footwear_bookmarked");
				} else {
				
				}
			});
		}
	});
	
	$(".remove_fav").live("click", function() {
		var elem = $(this);
		var id = elem.parent().parent().data("shoeid");
		var type = 'delete_favorites';
		var data = { id: id, type: type };
		
		$.post(url, data, function(response) {
			var res = jQuery.parseJSON(response);
				if (res.status) {
					elem.parent().parent().css("width", "1000px").animate({marginLeft: "1000px"}, "fast", function() {
						elem.parent().parent().remove();
					});					
				}
		});
	});
	
	// ADJUST WIDTH OF ARTICLE COLUMN
	if ($(window).width() < 768) {
		var article_width = $('.article-container').outerWidth();
		$('.article').css("width", article_width);
	} else {
		var article_width = $('.article-container').width() * 0.25;
		$('.article').css("width", article_width);
	}

	// HEADER SEARCH BAR
	$("#s").focus(function () {
		$(".glyphicon-search").css("z-index", "9999");
		$("#s").css("border", "1px solid rgba(200, 200, 200, 1)");
	});
	
	$("#s").blur(function () {
		$("#s").css("border", "none");
	});
	
	// FRONTPAGE
	var clicked = false;
	$('.second_image').click(function() {
		if (!clicked) {
			$('<div class="bg-overlay-pink"></div>').appendTo($('.second_image'));
			clicked = true;
		} else {
			$('.bg-overlay-pink').remove();
			clicked = false;
		}
	});
	
	// SIDEBAR
	var left = $('#side-menu').offset().left;
	
	//when the trigger is clicked we check to see if the popout is currently hidden
	//based on the hidden we choose the correct animation
	$('#image_menu').click( function() {
		/*if ($('#side-menu').hasClass('deactive')) {
			$('#side-menu').removeClass('deactive');
			showPopout();
		}
		else {
			$('#side-menu').addClass('deactive');
			hidePopout();
		}*/
		window.location.href = "/account";
	});

	function showPopout() {
		$("#side-menu").css({left:left})  // Set the left to its calculated position
             .animate({"left":"430"}, "slow");
	}

	function hidePopout() {
		$('#side-menu').animate({
			left: left
		}, 'slow', function () {
		});
	}
	
	// UPLOAD PROFILE PICTURE
	$(".user-profile-pic").live('click', function() {
		$("#image_filename").trigger("click");
	});
	
	$('#image_filename').change(function() {
		$('#edit').submit();
	});
	
	
	
	// REVIEW
	$(".review_box").live("hover", function() {
		var brand = $(this).attr("brand");
		var model = $(this).attr("model");
		var release_date = $(this).attr("release_date");
		var tags = $(this).attr("tags");
		var excerpt = $(this).attr("excerpt");
		var author_name = $(this).attr("author_name");
		var rating = $(this).attr("rating");
		var slug = $(this).attr("slug");
		var comment_count = $(this).attr("comment_count");
		
		var categories = jQuery.parseJSON($(this).attr("categories"));
		var styles = categories.join(", ");
		
		
		$("#brand").text(brand);
		$("#model").text(model);
		$("#release_date").text(release_date);
		$("#tags").text(tags);
		$("#excerpt").text(excerpt + '...');
		$("#author_name").text('by ' + author_name);
		$("#rating").text(rating + '/10');
		$("#categories").text(styles);
		$("#review_comment_count").text(comment_count);
		$(".slug").attr('href', '/reviews/' + slug);
	});
	
	// NOTIFICATIONS
	$(".notifications").click(function() {
		$(this).addClass("disable");
		var notify_ids = $(this).data("notifyid");
		var notification_url = "/ajax/notification";
		var data = { checked: 1, ids: notify_ids };
		
        jQuery.post(notification_url, data, function(response) {
		});
	});
	
	var total_clicked = 0;
	
	// LOAD MORE ACTIVITIES
	$(".load_activities").live("click", function() {
		total_clicked++;
		var offset = total_clicked * 5;
		var elem = $(this);
		var isProfile = elem.data("profile");
		var profile_user_id = elem.data("userprofileid");
		var activity_url = "/ajax/activity";
		var data = { position: offset, isProfile: isProfile, profile_user: profile_user_id };
		
		$(".loading_animation").css("display", "inline");
		
		jQuery.post(activity_url, data, function(data) {
			var output = $.parseJSON(data);
			//elem.before(output.activities);
			
			$(output.activities).hide().insertBefore(elem).slideDown({
				duration: "slow", 
				easing: "swing"
			});
			
			if (output.more_activities == false) {
				$(".load_activities").css("display", "none");
			}
			
			$(".loading_animation").css("display", "none");
		});
	});
	
	// UPDATE PROFILE
	$(".save-profile-button").live("click", function() {
		var website = $('input[name="edit_website"]').val();
		var facebook = $('input[name="edit_facebook"]').val();
		var twitter = $('input[name="edit_twitter"]').val();
		var bio = $('textarea[name="edit_bio"]').val();
		var name = $('input[name="edit_name"]').val();
		var age = $('input[name="edit_age"]').val();
		var city = $('input[name="edit_city"]').val();
		var country = $('input[name="edit_country"]').val();
		var title = $('input[name="edit_title"]').val();
		
		var editprofile_url = "/ajax/editprofile";
		var data = { type: 'edit', website: website, facebook: facebook, twitter: twitter, bio: bio, name: name, age: age, city: city, country: country, title: title };
		
		$(".save_animation").css("display", "inline");
			
		jQuery.post(editprofile_url, data, function(response) {
			location.reload();
		});
	});
	
	// Change Password
	$(".change_password_button").live("click", function() {
		var old_password = $('input[name="current_password"]').val();
		var confirm_password = $('input[name="confirm_password"]').val();
		var new_password = $('input[name="new_password"]').val();
		
		var editprofile_url = "/ajax/editprofile";
		var data = { type: 'password', old: old_password, new_pass: new_password };
		
		if (old_password && confirm_password && new_password) {
			$(".password_message").css("display", "none");
			$(".save_animation").css("display", "inline");
			
			jQuery.post(editprofile_url, data, function(response) {
				if (response == 0) {
					$(".save_animation").css("display", "none");
					$(".password_message").html("incorrect password");
					$(".password_message").css("display", "inline");
				} else {
					$(".save_animation").css("display", "none");
					$(".password_message").html("success");
					$(".password_message").css("display", "inline");
					$('input[name="current_password"]').val('');
					$('input[name="confirm_password"]').val('');
					$('input[name="new_password"]').val('');
				}
			});
		} else {
			$(".password_message").html("all fields required");
			$(".password_message").css("display", "inline");
		}
	});
	
	$(".navbar-toggle").live("click", function() {
		if ($("#top_bar").hasClass("transparent_bg")) {
			$("#top_bar").removeClass("transparent_bg");
			$("#top_bar").addClass("solid_bg");
		} else if ($("#top_bar").hasClass("solid_bg")) {
			$(".solid_bg").slideUp(function() {
				$("#top_bar").removeClass("solid_bg");
				$("#top_bar").addClass("transparent_bg");
				$("#top_bar").slideDown();
			});
		}
	});
	
	// SHARE ON FACEBOOK
	window.fbAsyncInit = function() {
		FB.init({
			appId      	: '713735605354107',
			xfbml      	: true,
			version    : 'v2.0'
        });
	};

	(function(d, debug){var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];if   (d.getElementById(id)) {return;}js = d.createElement('script'); js.id = id; js.async = true;js.src = "//connect.facebook.net/en_US/all" + (debug ? "/debug" : "") + ".js";ref.parentNode.insertBefore(js, ref);}(document, /*debug*/ false));
	function postToFeed(title, desc, url, image){
		var obj = {method: 'feed',link: url, picture: image ,name: title,description: desc};
		function callback(response){}
		FB.ui(obj, callback);
	}
	
	// SOCIAL MEDIA SHARES
	$(".social_media_share").click(function() {
		var elem = $("#article_data");
		var url = elem.data("url");
		var page = elem.data("slug");
		var link = url + page;
		var title = elem.data('title');
		var image = url + elem.data('image');
		var desc = elem.data('desc');
		
		switch ($(this).data("id")) {
			case "fb_share":
				postToFeed(title, desc, link, image);
				/*var query = "http://www.facebook.com/sharer.php?s=100&p[url]=" + encodeURIComponent(link)
							+ "&p[images][0]=" + image + "&p[title]=" + title + "&p[summary]=" + desc;*/
				break;
			case "tblr_share":
				var query = "http://www.tumblr.com/share/link?url=" + encodeURIComponent(link) 
							+ "&name=" + encodeURIComponent(title) + "&description=" 
							+ encodeURIComponent(desc);
				var width = 515;
				break;
			case "stblupon_share":
				var query = 'http://stumbleupon.com/submit?url=' + encodeURIComponent(link) 
							+ '&amp;title=' + encodeURIComponent(title);
				var width = 800;
				break;
			case "google_share":
				var query = "https://plus.google.com/share?url=" + encodeURIComponent(link);
				var width = 490;
				break;
			case "twitter_share":
				var query = "http://twitter.com/share?url=" + encodeURIComponent(link);
				break;
			default:
				break;
		}
		
		window.open(query, "_blank", "scrollbars=1, resizable=1, height=600, width=" + width);
	});
});


function even_footwear_bar() {
	var height = $("#footwear_search").outerHeight(true);
	$("#footwear_search_options").css("min-height", height);
	$("#footwear_side").css("min-height", height);
	
	
	var toolbar_height = $("#footwear_tool").height() + 60;
	$(".footwear-page-fix").css("margin-top", toolbar_height);
}

function auto_refined() {
	var url = "/ajax/getoptions";
	var brands = $("#footwear_brands").val();
	var categories = $("#footwear_select_category").val();
	var colors = $("#footwear_colors").val();
	
	var data = { type: 'refine_search', brands: brands, categories: categories, colors: colors };
	var height = $("#footwear_tool").height() + 60;
	
	$.post(url, data, function(response) {
		$("#main_container").hide().html(response).fadeIn("slow");
		
		$(".footwear-page-fix").css("margin-top", height);
	});
}