$(document).ready(function() {
	
	// Move to comment
	if (window.location.hash) {
		var hash = window.location.hash;
		$('.articles-screen').animate({ 
			scrollTop: $(hash).offset().top - 50
		}, 2500);
	}
	
	// COMMENTS
	$(".expand-reply").live("click", function() {
		$(this).parent().parent().parent().next(".comment-reply-box-row").css("display", "block");
	});
	
	$(".cancel-button").live("click", function() {
		$(this).parent().parent().css("display", "none");
	});
	
	$(".reply-submit").live("click", function() {
		var article_id = $('#article_id').val(); 
		var user_id = $(this).next().val();
		var comment_body = $(this).parent().prev().children().val();
		var comment_id = $(this).attr('id');
		var close = $(this).parent().parent();
		var post_type = $(this).data("posttype");
		var url = "/ajax/article";
		var data = { article_id: article_id, comment_body: comment_body, comment_id: comment_id, user_id: user_id, post_type: post_type, mode: 'reply' };
		
		if (!$.trim(comment_body)) {
			alert("cannot be empty");
		} else {
			jQuery.post(url, data, function(data) {
				// Display new reply
				var comment_info = $.parseJSON(data);
				var points = $(".user_points").text();
				$(".user_points").text("+" + comment_info.user_points + " points");
				
				$('#comment_' + comment_id).after('<div class="row" style="padding: 0 15px 15px 15px;"><div class="col-md-offset-1 col-md-11" style="padding-right: 0;"><div class="comments"><div class="comment-user"><div class="row"><div class="col-md-2"><div class="comment-user-image"  style="background-image: url(' + comment_info.user_image + '); -ms-background-position-x: center; -ms-background-position-y: center; background-position: center center; background-size: cover;"></div></div><div class="col-md-1 comment-karma"><div class="row"><div class="karma-up karma_novote"><i class="fa fa-plus"></i></div></div><div class="row"><div class="karma-count">' + comment_info.comment_karma + '</div></div><div class="row"><div class="karma-down karma_novote"><i class="fa fa-minus"></i></div></div></div><div class="col-md-9 comment-body"><div class="row"><div class="col-md-12 comment-owner"><a href="/profile/' + comment_info.username + '">' + comment_info.user_fullname + '</a><span>&nbsp;/ ' + comment_info.entry_datetime + '</span></div></div><div class="row"><div class="col-md-12 comment-content"><p>' + comment_info.comment_body + '</p></div></div></div></div></div></div></div>');
				
				$(close).css("display", "none");
			});
		}
	});
	
	$('.sub-reply-submit').live('click', function() {
		var article_id 				= $('#article_id').val();
		var user_id				= $(this).data("userid"); // Author's ID
		var fullname				= $(this).data("fullname"); // Author's full name
		var parent_comment	= $(this).data("parent");
		var comment_id			= $(this).data("reply");
		var post_type 			= $(this).data("posttype");
		var comment_body 	= $(this).parent().prev().children().val();
		var close 					= $(this).parent().parent();
		var url 							= "/ajax/article";
		
		var data = { article_id: article_id, comment_body: comment_body, comment_id: comment_id, user_id: user_id, parent_comment: parent_comment, fullname: fullname, post_type: post_type, mode: 'reply' };
		if (!$.trim(comment_body)) {
			alert("cannot be empty");
		} else {
			jQuery.post(url, data, function(data) {
				var comment_info = $.parseJSON(data);
				var points = $(".user_points").text
				$(".user_points").text("+" + comment_info.user_points + " points");
				
				$('#the_text_box').before('<div class="row" style="padding: 0 15px 15px 15px;"><div class="col-md-offset-1 col-md-11" style="padding-right: 0;"><div class="comments"><div class="comment-user"><div class="row"><div class="col-md-2"><div class="comment-user-image"  style="background-image: url(' + comment_info.user_image + '); -ms-background-position-x: center; -ms-background-position-y: center; background-position: center center; background-size: cover;"></div></div><div class="col-md-1 comment-karma"><div class="row"><div class="karma-up karma_novote"><i class="fa fa-plus"></i></div></div><div class="row"><div class="karma-count">' + comment_info.comment_karma + '</div></div><div class="row"><div class="karma-down karma_novote"><i class="fa fa-minus"></i></div></div></div><div class="col-md-9 comment-body"><div class="row"><div class="col-md-12 comment-owner"><a href="/profile/' + comment_info.username + '">' + comment_info.user_fullname + '</a><span>&nbsp;/ ' + comment_info.entry_datetime + '</span>&nbsp;&nbsp;<span class="replied_to"><i class="fa fa-reply"></i>&nbsp;&nbsp;&nbsp;&nbsp;to ' + comment_info.recipients_name + '</span></div></div><div class="row"><div class="col-md-12 comment-content"><p>' + comment_info.comment_body + '</p></div></div></div></div></div></div></div>');
				
				$(close).css("display", "none");
				
				console.log(data);
			});
		}
	});
	
	// Karma
	$(".karma-up").live('click', function() {
		var article_id = $('#article_id').val();
		var comment_id = $(this).attr("comment_id");
		var user_id = $(this).next().val();
		var post_type = $(this).data("posttype");
		
		var url = "/ajax/article";
		var data = { article_id: article_id, comment_id: comment_id, user_id: user_id, post_type: post_type, type: 1, mode: 'karma' };
        
        jQuery.post(url, data, function(response) {
			$("#karma_id_" + comment_id).text(response);
			
			if ($("#down_comment_" + comment_id).hasClass("karma-voted-down")) {
				$("#down_comment_" + comment_id).removeClass("karma-voted-down");
				$("#down_comment_" + comment_id).removeClass("karma_novote");
				$("#down_comment_" + comment_id).addClass("karma-down");
			}
			
			$("#up_comment_" + comment_id).removeClass("karma-up");
			$("#up_comment_" + comment_id).addClass("karma_novote");
			$("#up_comment_" + comment_id).addClass("karma-voted-up");
        });
		
	});
	
	$(".karma-down").live('click', function() {
		var article_id = $('#article_id').val();
		var comment_id = $(this).attr("comment_id");
		var user_id = $(this).next().val();
		var post_type = $(this).data("posttype");
		
		var url = "/ajax/article";
		var data = { article_id: article_id, comment_id: comment_id, user_id: user_id, post_type: post_type, type: -1, mode: 'karma' };
		
        jQuery.post(url, data, function(response) {
			$("#karma_id_" + comment_id).text(response);
			
			if ($("#up_comment_" + comment_id).hasClass("karma-voted-up")) {
				$("#up_comment_" + comment_id).removeClass("karma-voted-up");
				$("#up_comment_" + comment_id).removeClass("karma_novote");
				$("#up_comment_" + comment_id).addClass("karma-up");
			}
			
			$("#down_comment_" + comment_id).removeClass("karma-down");
			$("#down_comment_" + comment_id).addClass("karma_novote");
			$("#down_comment_" + comment_id).addClass("karma-voted-down");
        });
	});
});