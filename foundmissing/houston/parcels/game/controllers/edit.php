<?php
namespace Game\Controllers;

class Edit extends \Manage\Controllers\Manage{

	private $game;

	public $page_title;
	public $link_back;
	public $form;
	public $existing;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		if(isset($_POST["submit"])){
			$this->submit_form();
		}

		if(isset($data["game_id"])){
			$this->game = \Game\Models\Game::find($data["game_id"]);
			$this->page_title = "Editing Game";
		} else {
			$this->game = new \Game\Models\Game;
			$this->page_title = "New Game";
		}

		$this->link_back = $this->game->link_all();

		$form = new \Form\Models\Form("edit", "POST", "", "well");

		$item = new \Form\Models\FormItemHidden(array("id" => "id", "title" => "Id", "value" => $this->game->id));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "title", "title" => "Title", "value" => $this->game->title));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "system", "title" => "System", "value" => $this->game->system));
		$form->add_item($item);

		$item = new \Form\Models\FormItemSubmit(array("value" => "Save", "id" => "submit", "class" => "btn btn-primary", "cancel_url" => $this->game->link_all()));
		$form->add_item($item);

		$this->form = $form->render();

	}

	public function controller(){
		$this->set_view("Game\Views\Edit");
	}

	private function submit_form(){
		if($_POST["id"] > 0){
			$game = \Game\Models\Game::find($_POST["id"]);
			$flash_msg = "Game has been updated successfully.";
		} else {
			$game = new \Game\Models\Game;
			$flash_msg = "Game has been created successfully.";
			$game->ordering = \Game\Models\Game::get_next_ordering_number();
		}

		$game->title = $_POST["title"];
		$game->system = $_POST["system"];

		$game->save();
		$this->add_flash(array("message" => $flash_msg, "type" => "success", "Heading" => "Great!"));
		redirect_to($game->link_all());
	}

}
