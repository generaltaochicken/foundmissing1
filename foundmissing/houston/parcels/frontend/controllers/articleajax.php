<?php

namespace Frontend\Controllers;

class ArticleAjax extends \Frontend\Controllers\Frontend {

    private $article_id;
	private $user_id;
	private $user_comment_id;
	private $user;
	private $comment_body;
	private $parent_comment;
	private $fullname;
	
    public function __construct($uri, $data){
        parent::__construct($uri, $data);

        $this->data = $data;
		$this->user = $this->_user->id;
		
		if (isset($_POST['mode'])) {
			$this->article_id 				= $_POST['article_id'];
			$this->user_id 				= $_POST['user_id'];
			$this->comment_id 		= $_POST['comment_id'];
			$this->type 						= $_POST['type'];
			$this->comment_body 	= $_POST['comment_body'];
			$this->parent_comment = $_POST['parent_comment'];
			$this->fullname 				= $_POST['fullname'];
			$this->post_type				= $_POST['post_type'];
			
			if ($this->post_type == null) {
				$this->post_type = 1; // Articles
			}
			
			if ($_POST['mode'] == 'karma') {
				$this->update_comment_karma();
				$this->get_karma_count();
			} else if ($_POST['mode'] == 'reply') {
				$this->submit_reply();
			}
		}
    }

    public function controller(){
	
    }
	
	public function update_comment_karma() {
		$op = \User\Models\User::find($this->user_id);
		
		if ($this->has_user_voted()) {
			$comments_karma = \Comment_Karma\Models\Comment_Karma::find($this->user_comment_id);
			
			if ($comments_karma->type == -1) {
				$comments_karma->type = 1;
				
				if ($this->user_id != $this->user) {
					$op->karma += 1;
					$op->save();
				}
				
				$comments_karma->save();
			} else if ($comments_karma->type == 1) {
				$comments_karma->type = -1;
				
				if ($this->user_id != $this->user) {
					$op->karma -= 1;
					$op->save();
				}
				
				$comments_karma->save();
			}
		
		} else {
			$comments_karma = new \Comment_Karma\Models\Comment_Karma();
			$comments_karma->type = $this->type;
			$comments_karma->parent_id = $this->article_id;
			$comments_karma->user_id = $this->_user->id;
			$comments_karma->comment_id = $this->comment_id;
			$comments_karma->post_type = $this->post_type;
			$comments_karma->entry_datetime = time();
			
			$comments_karma->save();
			
			if ($this->user_id != $this->user) {
				$op->karma += 1;
				$op->save();
			}
		}
		
	}
	
	public function get_karma_count() {
		$comments_karma = \Comment_Karma\Models\Comment_Karma::all(array('conditions' => 'deleted = 0 AND parent_id = "' . $this->article_id . '" AND post_type = ' . $this->post_type . ' AND comment_id = "' . $this->comment_id . '" AND post_type = ' . $this->post_type));

		$karma_count = 0;
		foreach ($comments_karma as $karma) {
			$karma_count += $karma->type;
		}
			
		echo $karma_count;
	}
	
	public function has_user_voted() {
		$comments_karma = \Comment_Karma\Models\Comment_Karma::all(array('conditions' => 'user_id = "' . $this->user . '" AND parent_id = "' . $this->article_id . '" AND post_type = ' . $this->post_type . ' AND comment_id = "' . $this->comment_id . '"'));
		
		if (count($comments_karma) > 0) {
			foreach ($comments_karma as $karma) {
				$this->user_comment_id = $karma->id;
			}
			return true;
		} else {
			return false;
		}
	}
	
	public function submit_reply() {
		$comment = new \Comment\Models\Comment;
		$user = \User\Models\User::find($this->user);
		
		$comment->parent_id = $this->article_id;
		$comment->user_id = $this->user;
		$comment->comment_body = $this->comment_body;
		$comment->post_type = $this->post_type;
		$comment->entry_datetime = time();
		
		if (isset($_POST['parent_comment'])) {
			$comment->replied_to = $this->parent_comment;
			$comment->sub_replied_to = $this->comment_id;
		} else {
			$comment->replied_to = $this->comment_id;
			$comment->sub_replied_to = '0';
		}
		
		$user->karma += 1;

		$user->save();
		$comment->save();
		
		$this->new_comment = $comment->id;
		$this->update_activity_feed_reply();
		$this->send_notification();
		
		$return_array = array(
			"comment_body" => $comment->comment_body,
			"comment_karma" => 0,
			"entry_datetime" => $this->time_elapsed_string($comment->entry_datetime),
			"user_fullname" => $this->_user->fullname(),
			"user_image" => "/uploads/images/" . $this->_user->image,
			"user_points" => $user->karma,
			"username" => $this->_user->username,
			"recipients_name" => ucwords($this->fullname)
		);
		
		echo json_encode($return_array);
	}
	
	protected function send_notification() {
		$notification = new \Notification\Models\Notification;
		
		$notification->recipient_id = $this->user_id;
		$notification->sender_id = $this->_user->id;
		$notification->activity_type = "comment_reply";
		$notification->object_url = '#comment_' . $this->new_comment;
		$notification->entry_datetime = time();
		
		$notification->save();
	}
	
	protected function update_activity_feed_reply() {
		$activity = new \Activity\Models\Activity;
		
		$activity->user_id = $this->_user->id;
		
		switch ($this->post_type) {
			case 2:
				$activity->activity_type = 'interview_comment';
				break;
			case 3:
				$activity->activity_type = 'review_comment';
				break;
			default:
				$activity->activity_type = 'article_comment';
				break;
		}
		
		$activity->source_id = $this->article_id;
		$activity->parent_id = $this->user_id;
		$activity->child_id = $this->new_comment;
		$activity->entry_datetime = time();
		
		$activity->save();
	}

}