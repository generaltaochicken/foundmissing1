<?php
namespace Gallery\Controllers;

class Archive extends \Manage\Controllers\Manage{

	private $gallerys;

	public $gallerys_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Deleted Gallerys";

		$this->gallerys = \Gallery\Models\Gallery::all(array("conditions" => "deleted = 1"));
		$this->archive_button = array("url" => \Gallery\Models\Gallery::link_all(), "title" => "Back");
		foreach($this->gallerys as $gallery){
			$this->has_data = true;
			$this->gallerys_data[] = array(
				"id" => $gallery->id,
				"description" => $gallery->description,
				"entry_datetime" => $gallery->entry_datetime,
				"link_restore" => $gallery->link_restore(),
			);
		}

	}

	public function controller(){
		$this->set_view("Gallery\Views\All");
	}

}

