<?php

namespace Frontend\Controllers;

class NotificationAjax extends \Frontend\Controllers\Frontend {
	
    public function __construct($uri, $data){
        parent::__construct($uri, $data);

        $this->data = $data;
		$this->user_id = $this->_user->id;
		
		if (isset($_POST['checked'])) {
			$this->read_notifications();
		}
    }

    public function controller(){
	
    }
	
	protected function read_notifications() {
		$array = $_POST['ids'];
		foreach ($array as $arr) {
			$notifications = \Notification\Models\Notification::find($arr);
			$notifications->is_unread = 0;
			$notifications->save();
		}
	}

}