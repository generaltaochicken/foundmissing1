<?php
namespace Footwear_attachment\Controllers;

class Edit extends \Manage\Controllers\Manage{

	private $footwear_attachment;

	public $page_title;
	public $link_back;
	public $form;
	public $existing;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		if(isset($_POST["submit"])){
			$this->submit_form();
		}

		if(isset($data["footwear_attachment_id"])){
			$this->footwear_attachment = \Footwear_attachment\Models\Footwear_attachment::find($data["footwear_attachment_id"]);
			$this->page_title = "Editing Footwear_attachment";
		} else {
			$this->footwear_attachment = new \Footwear_attachment\Models\Footwear_attachment;
			$this->page_title = "New Footwear_attachment";
		}

		$this->link_back = $this->footwear_attachment->link_all();

		$form = new \Form\Models\Form("edit", "POST", "", "well");

		$item = new \Form\Models\FormItemHidden(array("id" => "id", "title" => "Id", "value" => $this->footwear_attachment->id));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "footwear_id", "title" => "Footwear Id", "value" => $this->footwear_attachment->footwear_id));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "attachment_id", "title" => "Attachment Id", "value" => $this->footwear_attachment->attachment_id));
		$form->add_item($item);

		$item = new \Form\Models\FormItemSubmit(array("value" => "Save", "id" => "submit", "class" => "btn btn-primary", "cancel_url" => $this->footwear_attachment->link_all()));
		$form->add_item($item);

		$this->form = $form->render();

	}

	public function controller(){
		$this->set_view("Footwear_attachment\Views\Edit");
	}

	private function submit_form(){
		if($_POST["id"] > 0){
			$footwear_attachment = \Footwear_attachment\Models\Footwear_attachment::find($_POST["id"]);
			$flash_msg = "Footwear_attachment has been updated successfully.";
		} else {
			$footwear_attachment = new \Footwear_attachment\Models\Footwear_attachment;
			$flash_msg = "Footwear_attachment has been created successfully.";
			$footwear_attachment->ordering = \Footwear_attachment\Models\Footwear_attachment::get_next_ordering_number();
		}

		$footwear_attachment->footwear_id = $_POST["footwear_id"];
		$footwear_attachment->attachment_id = $_POST["attachment_id"];

		$footwear_attachment->save();
		$this->add_flash(array("message" => $flash_msg, "type" => "success", "Heading" => "Great!"));
		redirect_to($footwear_attachment->link_all());
	}

}
