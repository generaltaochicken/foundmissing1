<?php
namespace Category\Models;

class Category extends \Core\Models\Base_Model{

	protected static $manage_prefix = "/manage/categories/";

	public function link_edit(){
		return self::$manage_prefix . "edit/" . $this->id;
	}

	public function link_delete(){
		return self::$manage_prefix . "delete/" . $this->id;
	}

	public function link_restore(){
		return self::$manage_prefix . "restore/" . $this->id;
	}

	public function link_new(){
		return self::$manage_prefix . "new";
	}

	public function link_all(){
		return self::$manage_prefix;
	}

	public function link_archive(){
		return self::$manage_prefix . "archive";
	}

	public function get_frontend_data(){
		return array(
			"id" => $this->id,
			"title" => $this->title,
			"entry_datetime" => $this->entry_datetime,
		);
	}
	
	public function get_category_name($id) {
		$category = \Category\Models\Category::find($id);
		
		return $category->title;
	}
	
	public function get_category_id_by_name($name) {
		$category = \Category\Models\Category::first(array('select' => 'id', 'conditions' => 'deleted = 0 AND title = "' . $name . '"'));
		
		return $category->id;
	}
}