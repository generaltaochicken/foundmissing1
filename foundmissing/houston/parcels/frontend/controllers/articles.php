<?php
namespace Frontend\Controllers;

class Articles extends \Frontend\Controllers\Frontend {
    
	private $data;
	public $articles = array();
	
    public function __construct($uri, $data) {
        parent::__construct($uri, $data);
		
		$this->data = $data;
		$this->add_asset('css', 'interview.css', true);
		$this->title = "Articles | FM";
		  
    }

    public function controller(){
		$articles = \Article\Models\Article::all(array('conditions' => 'deleted = 0 AND publish_date > 1', 'order' => 'publish_date DESC'));
		$first = true;
		require_once(PARCEL_ROOT.'libraries/Mobile-Detect-2.8.3/Mobile_Detect.php');
		$detect = new \Mobile_Detect;
		
        foreach($articles as $article){
			if (strlen($article->excerpt) > 50) {
				if($detect->isAndroidOS() || $detect->isMobile()){
					$article->excerpt = substr($article->excerpt, 0, 50) . '....';
				} else {
					$article->excerpt = substr($article->excerpt, 0, 215) . '....';
				}
			}
			
			$comment_count = \Comment\Models\Comment::getCommentCount($article->id, 2);
			
			if ($comment_count > 1) {
				$count = $comment_count . ' comments';
			} else {
				$count = $comment_count . ' comment';
			}

            $this->articles[] = array(
                'id' => $article->id,
                'author_name' => $article->author_name,
                'title' => $article->title,
                'slug' => $article->slug,
                'excerpt' => $article->excerpt,
                'body' => $article->body,
                'date' => time_to_friendly_date($article->publish_date, 'Y.m.d'),
                'main_image' => ARTICLE_IMAGES . $article->bg_image_header,
				'comment_count' => $count,
				'first' => $first
            );
			
			$first = false;
        }
		
		$this->set_view('Frontend\Views\Articles');
	}
}