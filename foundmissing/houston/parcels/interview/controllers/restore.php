<?php
namespace Interview\Controllers;

class Restore extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$interview = \Interview\Models\Interview::find($data["interview_id"]);

		if($interview){
			$interview->deleted = 0;
			$interview->save();
			$this->add_flash(array("message" => "Interview has been restored!"));

			redirect_to($interview->link_all());
		}
	}
}
