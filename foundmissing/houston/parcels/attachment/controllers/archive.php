<?php
namespace Attachment\Controllers;

class Archive extends \Manage\Controllers\Manage{

	private $attachments;

	public $attachments_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Deleted Attachments";

		$this->attachments = \Attachment\Models\Attachment::all(array("conditions" => "deleted = 1"));
		$this->archive_button = array("url" => \Attachment\Models\Attachment::link_all(), "title" => "Back");
		foreach($this->attachments as $attachment){
			$this->has_data = true;
			$this->attachments_data[] = array(
				"id" => $attachment->id,
				"type" => $attachment->type,
				"image_filename" => $attachment->image_filename,
				"extension" => $attachment->extension,
				"entry_datetime" => $attachment->entry_datetime,
				"title" => $attachment->title,
				"caption" => $attachment->caption,
				"link_restore" => $attachment->link_restore(),
			);
		}

	}

	public function controller(){
		$this->set_view("Attachment\Views\All");
	}

}

