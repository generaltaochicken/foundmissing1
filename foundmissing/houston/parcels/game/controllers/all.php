<?php
namespace Game\Controllers;

class All extends \Manage\Controllers\Manage{

	private $games;

	public $games_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Games";
		$this->link_new = \Game\Models\Game::link_new();

		$this->games = \Game\Models\Game::all(array("conditions" => "deleted = 0" , "order" => "ordering"));
		$archived = \Game\Models\Game::count(array("conditions" => "deleted = 1"));
		if($archived > 0){
			$this->archive_button = array("url" => \Game\Models\Game::link_archive(), "title" => "Archive");
		}

		foreach($this->games as $game){
			$this->has_data = true;
			$this->games_data[] = array(
				"id" => $game->id,
				"title" => $game->title,
				"system" => $game->system,
				"entry_datetime" => $game->entry_datetime,
				"link_edit" => $game->link_edit(),
				"link_delete" => $game->link_delete(),
				"ordering" => $game->ordering,
			);
		}

	}

	public function controller(){
		$this->set_view("Game\Views\All");
	}

}

