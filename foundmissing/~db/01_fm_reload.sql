-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 07, 2014 at 05:08 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `foundmissingapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE IF NOT EXISTS `activities` (
  `id` int(14) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `activity_type` varchar(30) DEFAULT NULL,
  `source_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `child_id` int(11) DEFAULT NULL,
  `entry_datetime` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


--
-- Table structure for table `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entry_datetime` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '1',
  `author_name` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `excerpt` varchar(240) NOT NULL,
  `body` text NOT NULL,
  `bg_image_header` varchar(255) NOT NULL,
  `publish_date` int(9) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `articles`
--

-- --------------------------------------------------------

--
-- Table structure for table `attachments`
--

CREATE TABLE IF NOT EXISTS `attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL,
  `image_filename` varchar(255) NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `entry_datetime` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `caption` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `attachments`
--

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE IF NOT EXISTS `brands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `description` text,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `title`, `description`, `ordering`, `deleted`) VALUES
(1, 'Air Jordan', '', 0, 0),
(2, 'Converse', 'Converse is an American lifestyle company with a production output that primarily consists of shoes, lifestyle fashion, and athletic apparel. The company has existed since the early 20th century and has been a subsidiary of Nike, Inc. since 2003.', 1, 0),
(3, 'Timberland', 'Timberland LLC is an American manufacturer and retailer of outdoors wear with a focus on footwear. Timberland footwear is marketed towards people intending outdoor and casual use.', 2, 0),
(4, 'Supra', 'KR3W is an apparel company based in Fountain Valley, California, United States, that specializes in skateboarding-related products. The brand was cofounded by Angel Cabada in 2002.', 3, 0),
(5, 'Nike', 'Nike, Inc. is an American multinational corporation that is engaged in the design, development, manufacturing and worldwide marketing and selling of footwear, apparel, equipment, accessories and services.', 4, 0),
(6, 'Clarks', '', 5, 0),
(7, 'Rockport', '', 6, 0),
(8, 'Adidas', '', 7, 0),
(9, 'Allen Edmonds', '', 8, 0),
(10, 'K-Swiss', '', 9, 0),
(11, 'Android Homme', '', 10, 0),
(12, 'Kriss Kross', '', 11, 1),
(13, 'New Balance', '', 12, 0),
(14, 'Giuseppe Zanotti', '', 13, 0),
(15, 'Gucci', '', 14, 0);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `entry_datetime` int(11) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `ordering`, `entry_datetime`, `deleted`) VALUES
(1, 'Sneaker', 0, 0, 0),
(2, 'Boot', 1, 0, 0),
(3, 'Dress', 2, 0, 0),
(4, 'Design', 3, 0, 1),
(5, 'Miscellaneous', 4, NULL, 1),
(6, 'Random', 5, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

CREATE TABLE IF NOT EXISTS `colors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `hex` varchar(10) DEFAULT NULL,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `colors`
--

INSERT INTO `colors` (`id`, `title`, `hex`, `ordering`, `deleted`) VALUES
(1, 'Black', '#000000', 0, 0),
(2, 'White', '#ffffff', 2, 0),
(3, 'Red', '#FF0000', 3, 0),
(4, 'Beige', '#F7FCC5', 4, 0),
(5, 'Brown', '#8C6246', 5, 0),
(6, 'Silver', '#E0E0E0', 6, 0),
(7, 'Blue', '#0000FF', 1, 0),
(8, 'Purple ', '#FF00FF', 8, 0),
(9, 'Orange', '#FF9100', 9, 0),
(10, 'Infrared ', '#EB1C37', 10, 0),
(11, 'Lime Green ', '#30F000', 11, 0),
(12, 'Royal Blue ', '#4169E1', 12, 0),
(13, 'Navy Blue ', '#000080', 13, 0),
(14, 'Grey ', '#404040', 7, 0),
(15, 'Plum', '#ED1F9A', 14, 0);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(6) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comment_body` varchar(255) NOT NULL,
  `replied_to` int(6) NOT NULL DEFAULT '0',
  `sub_replied_to` int(6) DEFAULT NULL,
  `entry_datetime` int(11) NOT NULL,
  `update_datetime` int(11) NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `post_type` int(1) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `post_type` (`post_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `comments`
--

-- --------------------------------------------------------

--
-- Table structure for table `comment_karmas`
--

CREATE TABLE IF NOT EXISTS `comment_karmas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_type` int(5) NOT NULL,
  `type` int(1) NOT NULL DEFAULT '0',
  `parent_id` int(8) NOT NULL,
  `comment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `entry_datetime` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `ordering` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=64 ;

--
-- Dumping data for table `comment_karmas`
--

-- --------------------------------------------------------

--
-- Table structure for table `contents`
--

CREATE TABLE IF NOT EXISTS `contents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `k` varchar(255) DEFAULT NULL,
  `v` text,
  `entry_datetime` int(11) NOT NULL DEFAULT '0',
  `update_datetime` int(11) NOT NULL DEFAULT '0',
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `id_2` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `contents`
--

INSERT INTO `contents` (`id`, `k`, `v`, `entry_datetime`, `update_datetime`, `deleted`) VALUES
(1, 'title', 'PLAYFIGHT IS A VFX STUDIO', 0, 0, 1),
(2, 'subtitle', 'WE PRODUCE TOP TIER VISUAL EFFECTS, ANIMATION AND PRE-VISUALIZATION FOR <span>FEATURE FILMS</span>, <span>TELEVISION</span> AND <span>ADVERTISING</span>. OUR OFFICES ARE LOCATED IN DOWNTOWN TORONTO', 0, 0, 1),
(3, 'facebook', 'https://www.facebook.com/pages/FoundMissing/158144410869120', 0, 0, 0),
(4, 'twitter', 'https://twitter.com/_foundmissing', 0, 0, 0),
(5, 'instagram', 'http://instagram.com/_foundmissing', 0, 0, 0),
(6, 'rss', 'http://rss.com/foundmissing', 0, 0, 0),
(7, 'contact_subtitle', 'We are so fun.', 0, 0, 1),
(8, 'address1', '489 Queen Street East', 0, 0, 1),
(9, 'address2', 'Toronto, On M5A 1V1', 0, 0, 1),
(10, 'phone', '416-809-6594', 0, 0, 1),
(11, 'email', 'hello@playfight.com', 0, 0, 1),
(12, 'job_email', 'jobs@playfight.com', 0, 0, 1),
(13, 'mission_statement', 'So the word is out and the Jordan III white/cement will make another visit come 2011. From pictures that have surfaced one has to be disappointed at the use of what looks like a glossy cheap leather. Outside of that and the fact that there is no Nike Air (wasnâ€™t on the 2003 release either) the shoe looks OK. Take a look at the side pictures below before you make your decision, and at the very least check out the different releases side by side. Enjoy.', 0, 0, 0),
(14, 'youtube', 'http://www.youtube.com/user/FOUNDMISSINGtv', 0, 0, 0),
(15, 'tumblr', 'http://thefoundmissing.tumblr.com/', 0, 0, 0),
(16, 'pinterest', 'http://pinterest.com', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `designers`
--

CREATE TABLE IF NOT EXISTS `designers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `description` text,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=54 ;

--
-- Dumping data for table `designers`
--

INSERT INTO `designers` (`id`, `title`, `description`, `ordering`, `deleted`) VALUES
(1, 'Cole Haan', '', 2, 0),
(2, 'Nike', '', 1, 0),
(3, 'Converse', '', 3, 0),
(4, 'Adidas', '', 4, 0),
(5, 'New Balance', '', 5, 0),
(6, 'Red Wing', '', 6, 0),
(7, 'Clarks Original', '', 8, 0),
(8, 'Timberland', '', 13, 0),
(9, 'Android Homme', '', 14, 0),
(10, 'Supra II', '', 11, 0),
(11, 'Krisvanassche', '', 16, 0),
(12, 'Balenciaga ', '', 15, 0),
(13, 'Givenchy ', '', 21, 0),
(14, 'Dsquared2 ', '', 18, 0),
(15, 'Ann Demeulemeester ', '', 17, 0),
(16, 'Balmain ', '', 20, 0),
(17, 'Giuseppe Zanotti', '', 12, 0),
(18, 'Raf Simons ', '', 22, 0),
(19, 'Lanvin', '', 23, 0),
(20, 'Saint Laurent ', '', 19, 0),
(21, 'Maison Martin Margiela ', '', 24, 0),
(22, 'Pierre Hardy ', '', 25, 0),
(23, 'MCQ Alexander Mcqueen ', '', 26, 0),
(24, 'Neil Barret ', '', 27, 0),
(25, 'Thom Browne ', '', 28, 0),
(26, 'Paul Smith ', '', 29, 0),
(27, 'Tiger of Sweden ', '', 30, 0),
(28, 'Dr. Martens ', '', 32, 0),
(29, 'Y-3 ', '', 31, 0),
(30, 'Tod''s', '', 33, 0),
(31, 'Asics', '', 34, 0),
(32, 'John Varvatos ', '', 35, 0),
(33, 'Ellen Admonds ', '', 36, 0),
(34, 'Brunello Cucinelli ', '', 37, 0),
(35, 'Oliberte ', '', 39, 0),
(36, 'Toms ', '', 38, 0),
(37, 'Reebok', '', 7, 0),
(38, 'Asics', '', 40, 0),
(39, 'Sorel ', '', 41, 0),
(40, 'Sebago ', '', 42, 0),
(41, 'Sperry Topsider', '', 43, 0),
(42, 'Feit ', '', 44, 0),
(43, 'Danner ', '', 45, 0),
(44, 'Del Toro', '', 46, 0),
(45, 'Chippewa ', '', 47, 0),
(46, 'ETQ ', '', 49, 0),
(47, 'Puma ', '', 10, 0),
(48, 'Jordan ', '', 0, 0),
(49, 'Vans ', '', 9, 0),
(50, 'Christian Louboutin', '', 48, 0),
(51, 'Gucci ', '', 50, 0),
(52, 'Diemme ', '', 51, 0),
(53, 'Yuketen ', '', 52, 0);

-- --------------------------------------------------------

--
-- Table structure for table `favorites`
--

CREATE TABLE IF NOT EXISTS `favorites` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `entry_datetime` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`item_id`),
  KEY `user_id_2` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `favorites`
--

-- --------------------------------------------------------

--
-- Table structure for table `footwears`
--

CREATE TABLE IF NOT EXISTS `footwears` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `model_name` varchar(255) NOT NULL,
  `style_id` varchar(150) DEFAULT NULL,
  `tags` varchar(255) DEFAULT NULL,
  `price` varchar(15) DEFAULT NULL,
  `rating` varchar(5) DEFAULT NULL,
  `color_id` varchar(150) DEFAULT NULL,
  `release_date` int(11) DEFAULT NULL,
  `entry_datetime` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `footwears`
--

-- --------------------------------------------------------

--
-- Table structure for table `footwear_attachments`
--

CREATE TABLE IF NOT EXISTS `footwear_attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `footwear_id` int(11) NOT NULL,
  `attachment_id` int(11) NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `entry_datetime` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `footwear_attachments`
--
-- --------------------------------------------------------

--
-- Table structure for table `frontpages`
--

CREATE TABLE IF NOT EXISTS `frontpages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slider` int(1) NOT NULL DEFAULT '0',
  `position` int(1) NOT NULL DEFAULT '0',
  `type` varchar(50) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `entry_datetime` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `frontpages`
--

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE IF NOT EXISTS `galleries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author_name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `slug` varchar(100) NOT NULL,
  `entry_datetime` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '1',
  `publish_date` int(9) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`),
  UNIQUE KEY `title_2` (`title`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `galleries`
--


-- --------------------------------------------------------

--
-- Table structure for table `gallery_attachments`
--

CREATE TABLE IF NOT EXISTS `gallery_attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gallery_id` int(11) NOT NULL,
  `attachment_id` int(11) NOT NULL,
  `entry_datetime` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `gallery_attachments`
--

-- --------------------------------------------------------

--
-- Table structure for table `interviews`
--

CREATE TABLE IF NOT EXISTS `interviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entry_datetime` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `author_name` varchar(55) NOT NULL,
  `title` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `excerpt` text NOT NULL,
  `body` text NOT NULL,
  `main_image` varchar(255) NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `publish_date` int(9) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `interviews`
--


-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recipient_id` int(11) NOT NULL,
  `sender_id` int(11) DEFAULT NULL,
  `activity_type` varchar(25) NOT NULL,
  `object_url` varchar(255) NOT NULL,
  `entry_datetime` int(11) NOT NULL,
  `is_unread` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `recipient_id` (`recipient_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `notifications`
--
-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `body` text NOT NULL,
  `entry_datetime` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `slug`, `body`, `entry_datetime`, `deleted`, `ordering`) VALUES
(1, 'About Us', 'about-us', 'FoundMissing\r\n\r\nAs you find something, the ravenous cycle of fashion leaves us wanting more. Our collections grow but every piece inspires something new, that one thing missing to make your collection complete.\r\n\r\n\r\nProblem\r\n\r\nâ€¨â€¨Footwear in general is fragmented and displayed across the web in such a way that is is difficult for one to be able to search for a specific hard to find shoe, whether they would like to find information on the product or purchase it. Although, we are not an online store, it our duty to direct you to the information and reach of purchase. â€¨â€¨\r\n\r\nSolution\r\n\r\nâ€¨â€¨FoundMissing solves the search/hunting problem by working closely with selected brands on providing the newest detailed product information that is specifically catered to their preferences. FoundMissing exists to fill the void on allowing viewers to express their opinions on select shoes and provide an ease of access to purchasing the end product.\r\n\r\nCopyright Â© 2014 FoundMissing. All rights reserved. ', 1376084166, 0, 0),
(2, 'Careers', 'careers', 'You feel you have the talent to work with FoundMissing ? If so please give us a shout at \r\n\r\njobs@foundmissing.ca\r\n\r\n', 1376084337, 0, 2),
(3, 'Privacy', 'privacy', 'The following Privacy Policy covers all the websites (â€œwebsitesâ€) owned, affiliated or operated by FoundMissing. (â€œFoundMissingâ€, â€œweâ€, â€œusâ€, â€œourâ€). It is our goal to bring you information tailored to your personal interests but, at the same time, protect your privacy. Please read the following to learn more about our privacy policy.\r\n\r\nBy visiting FoundMising websites, submitting information or using any of our products and services, you are accepting the practiced outlined in this Privacy Policy.\r\n\r\nThis Privacy Policy covers FoundMissingâ€™s treatment of personal information that FoundMissing gathers when you are on our websites and when you are using FoundMissing services. Also, this Privacy Policy covers FoundMissingâ€™s treatment of any personal information that FoundMissingâ€™s business partners share with us. This Privacy Policy does not cover information collected elsewhere, including without limitation offline and on sites linked to and from the website as well as the practices of third parties that FoundMissing does not control or own, or individuals that FoundMissing does not employ or manage.\r\n\r\nIn addition to reviewing this Privacy Policy, please read our User Agreement. Your use of our websites constitutes agreement to its terms and conditions as well.\r\n\r\nThis Privacy Policy may be modified from time to time so check back often. Continued access of our websites by you will constitute your acceptance of the changes or revisions to the Privacy Policy.\r\n\r\n1. Introduction\r\n\r\nThank you for visiting FoundMissing. This Privacy Policy is written to better explain how we collect and use your information. This Privacy Policy is a binding contract and you must agree to it to the same extent as our Terms and Conditions in order to use our services.\r\n\r\n2. Information You Provide to Us\r\n\r\nThe website generally collects personally identifying information with your specific knowledge and consent. This includes but is not limited to when you register for our forums, enter a sweepstakes and complete a survey. You can choose not to provide us with certain information, but you may not be able to take advantage of many of our special features such as posting on our forums. - Registration: In order to use select parts of our websites such as the database, you must complete a registration form. As part of this registration forum, we require your email address, name, city of origin, gender, shoe size, and date of birth. Optional information such as your referrer or time zone may also be requested.\r\n\r\n3. Personal Information We Collect When You Buy from Us\r\n\r\nIn order for you to place an order with us, you must provide us your name, e-mail address, mailing address, date of birth, Paypal information and telephone number. We will not disclose any information that you provide to us except in accordance with the provisions of this Privacy Policy.\r\n\r\n4. Automatic Information\r\n\r\nOur servers may automatically collect information about your computer when you visit our websites, including without limitation the website that referred you, your Internet Protocol (â€œIPâ€) and other usage and traffic information. FoundMissing uses this statistical information to analyze trends, gather demographic information and administer our websites so we may provide improved web experiences to our readers. The information is not in a manner that would identify you personally. We may provide statistical information, not personal information, to our partners about how our users, collectively use our websites. We share this information to our partners, so that they may further understand the usage of their areas and our websites, so that they too could provide you with a better web experience. There may also be occasion when we are legally required to provide access to our database in order to cooperate with police investigations or other legal proceedings. In those instances, the information is provided only for that purpose.\r\n\r\n5. Use of Information\r\n\r\nWe may use your information:\r\n\r\nTo enhance or improve user experience, the website, or service.\r\nTo process transactions and sell, transfer, and exchange your information to a third party for their marketing purposes with no additional consent.\r\nTo send e-mails about the site or respond to inquiries.\r\nTo send e-mails about the website and other products, news, or updates for FoundMissing.ca.\r\nTo send e-mails about third partiesâ€™ products or services.\r\n\r\n6. Cookies\r\n\r\nIn order to distinguish you from other usersâ€”especially other users on the same network as you â€” we need to use cookies to track your specific computer. Specifically, we use cookies to save your preferences, keep track of advertisements, and remember your shopping cart items. Additionally, our cookies may be accessible by third parties, or provided by third parties. For these reasons we require that you keep cookies enabled when using our website.\r\n\r\n7. Third Party Websites\r\n\r\nWe may link to third party websites from our own website. We have no control over, and are not responsible for, these third party websites or their use of your personal information. We recommend that you review their privacy policies and other agreements governing your use of their website.\r\n\r\n8. Third Party Access to Your Information\r\n\r\nWe protect your information from being accessed by third parties. However, since we may employ outside maintenance personnel or use a third party to host our material, there are third party entities which may have custody or access to your information. Because of this, it is necessary that you permit us to give access to your information to third parties to the same extent that you authorize us to do so. For greater certainty, every authorization which you grant to us in this Privacy Policy, you also grant to any third party that we may hire, contract, or otherwise retain the services of for the purpose of operating, maintaining, repairing, or otherwise improving or preserving our website or its underlying files or systems. You agree not to hold us liable for the actions of any of these third parties, even if we would normally be held vicariously liable for their actions, and that you must take legal action against them directly should they commit any tort or other actionable wrong against you. Additionally, we may store your information or share it with certain other third parties for reason that may or may not be related to maintenance, advertising, marketing, or other purposes related to our business. The following is a non-exhaustive list of other entities that we may store, share, or transfer your information with:\r\n\r\nGoogle Analytics\r\nParse.ly\r\n\r\n9. Marketing & Communication\r\n\r\nWhen registering for a FoundMissing database account, you will be given the option to opt-in to updates through the FoundMissing newsletter  ''new in'' and promotional emails. If you would prefer to opt-out from these e-mail, you can do so at any time by using the unsubscribe link at the bottom of the e-mail.\r\n\r\n10. Law Enforcement\r\n\r\nWe may disclose your information to a third party where we believe, in good faith, that it is desirable to do so for the purposes of a civil action, criminal investigation, or other legal matter. In the event that we receive a subpoena affecting your privacy, we may elect to notify you to give you an opportunity to file a motion to quash the subpoena, or we may attempt to quash it ourselves, but we are not obligated to do either.\r\n\r\n11. Security of Information\r\n\r\nWe use SSL certificates to help secure our website. However, we make no representations as to the security or privacy of your information. It is in our interest to keep our website secure, but we recommend that you use anti-virus software, firewalls, and other precautions to protect yourself from security threats.\r\n\r\n12. Amendments\r\n\r\nWe may amend this Privacy Policy from time to time, and the amended version will be posted on our website in place of the old version. We will also include the date that the currently displayed Privacy Policy took effect to help you determine whether there have been any changes since you last used our website. Like our Terms of Service, you must visit this page each time you come to our website and read and agree to it if the date it was last modified is more recent than the last time you agreed to the Privacy Policy. If you do not agree to the new Privacy Policy, you must immediately cease using our service.\r\n\r\n13. Contact\r\n\r\nAny inquiries about your rights under this Privacy Policy, or any other matters regarding your privacy, can be directed to info@foundmissing.ca', 1376084353, 0, 4),
(4, 'Legal Terms', 'legal-terms', '1. Introduction\r\n\r\nWelcome to FoundMissing.ca, a body corporate doing business in Ontario, Canada. This document constitutes a legally-binding agreement ("Agreement") governing the terms of providing you with our service. Throughout this document, the words "FoundMissing," "FoundMissing," "us," "we," and "our," refer to us, our website, FoundMissing.ca, or our service, FoundMissing, as is appropriate in the context of the use of the words. Likewise, the words "you" and "your" refer to you, the person who is being presented with this document for your agreement.\r\n\r\nBy registering for our website as a member, you are agreeing to receive regular newsletters and marketing related emails from FoundMissing. Should you wish to be removed from our email list, you can unsubscribe or terminate your account any time.\r\n\r\n2. Description of Service\r\n\r\nFoundMissing solves the search/hunting problem by working closely with selected brands on providing the newest detailed product information that is specifically catered to their preferences. FoundMissing exists to fill the void on allowing viewers to express their opinions on select shoes and provide an ease of access to purchasing the end product.\r\n\r\n3. Information Supplied\r\n\r\nWhen using our website and reviewing footwear, you will provide your name, e-mail address,city of origin, date of birth, shoe size, gender.\r\n\r\nIn addition to providing us with the above information about yourself, you must be eighteen years of age or older, or, if a higher minimum age of contractual capacity exists in your jurisdiction, you must be at least that age. Additionally, you represent to us that you are not otherwise considered to be under a disability to enter into a contract with us, whether it be because of an embargo in or against your jurisdiction or any other reason.\r\n\r\n4. Image Terms of Use\r\n\r\nEditors typically include images as part of their blog posts and the images that they are authorized to use is the following:\r\n\r\nImages that are licensed from vendors\r\nImages that are supplied to our editors or released into the public domain by public relations and marketing companies for press or other purposes\r\nImages that are supplied by readers, with the implied representation that the person submitting the image owns the copyright in the image and the right to give it to us for use on our site\r\nImages that are published on Photobucket or other public photos sites with licenses granted under Creative Commons, with attribution in accordance with the CC license granted in each case\r\nImages that are commissioned by FoundMissing\r\nImages that we believe to be covered by the Fair Use Doctrine, such as:\r\nThumbnail images of 150 x 150 pixels or less, cropped or reduced in size from the original source\r\nImages that are used to illustrate a newsworthy story, where the image itself tells a story\r\nImages used in a transformative manner, such as parody\r\nImages that are widely distributed that they have become part of the news\r\nIf FoundMissing receives any notice that an image is posted up inappropriately such as not keeping with the terms listed above, we reserve the right to remove that image.\r\n\r\nIf you think we have published an image or text that violates your copyright, we will address your concerns; however, if the image falls into one of the listed categories above and we believe that our use of the image is legitimate, we will not remove it from the site.\r\n\r\n5. Payment\r\n\r\nYou may pay for our services using Paypal. When providing credit card information, the credit card must be your own. If the credit card does not belong to you, you must have permission from the accountholder to use their card for your transactions.\r\n\r\nWe do not accept personal checks, money orders or direct bank transfers. Please note, items will not be shipped until the transaction has been cleared.\r\n\r\nAny use of fraudulent chargebacks simply to get free merchandise is not only strictly prohibited, but also criminal fraud, and will be dealt with by reporting such chargebacks to your local police department and a credit bureau, as well as referring the matter to a court of competent jurisdiction.\r\n\r\n6. Price Changes & Stock Availability\r\n\r\nPrices are subject to change without prior notice.\r\n\r\nWhile we endeavor to provide you with the most accurate and up-to-date information, occasionally ordered items may not be available as listed.\r\n\r\nIn the case that part of your order is unavailable or out of stock, the item(s) will be cancelled from the order with notification and where applicable, a refund shall be applied for the cost of the regarding item(s).\r\n\r\nFoundMissing ensures that all our product descriptions and prices are up-to-date and as accurate as possible. However, in few cases, pricing errors may occur with our listings. Should we discover a pricing error while processing your order and payment, we will immediately rectify the problem before cancelling and refunding you for your order. We will also make sure to inform you about the error with your order as soon as possible.\r\n\r\n7. Promotional & Store Credit Codes\r\n\r\nWhere a promotional code is required for an offer or discounted price, the code should be entered during the checkout process.\r\n\r\nCustomers should take care when entering codes, checking that the offer/credit has been applied. Should you find that the code is unapplicable or invalid, please refer to the specific conditions for the code as the promotion may have already expired. Should you have difficulty in applying store credit to your order, please contact us via e-mail at info@hypebeast.com, Monday to Friday, 10:00am to 6:00pm (Toronto time) for assistance.\r\n\r\n8. Discounts, Sales & Promotions\r\n\r\nFrom time to time, FoundMissing may offer discounts and/or promotions. Discount, sales and/or promotional offers and prices are only applicable when checking out whilst the offer is still valid. Time restrictions will be stated where applicable. FoundMissing reserves the right to amend, extend or terminate these offers at an earlier date without prior notice. For those who purchased over 24 hours before the sale, price adjustments will not be made. For those who purchased within 24 hours, we can refund the discount being offered at the time.\r\n\r\n9. Rules of Conduct\r\n\r\nWe host a review section as well as blogs that we may permit you to comment on. As a part of using our website, you must not:\r\n\r\nPost anything that violates any applicable law, foreign or domestic.\r\nPost anything defamatory, violent, pornographic, hateful, racist, or otherwise objectionable. We have the sole right to determine what is objectionable.\r\nHack, crack, phish, SQL inject, or otherwise threaten or actually harm the security of our systems or those of our users.\r\nInfringe on the intellectual property rights of anyone.\r\nDo anything that infringes on the rights of us or a third party.\r\nCreate more than one account, use another personâ€™s account, or allow another person to use your account.\r\nEngage in unsolicited marketing activities, whether commercial or non-commercial, on our forum or through its private messaging service, or on our blog comments section, or anywhere else on our website.\r\nAdvertise on our website without our permission.\r\nImpersonate another person or organization.\r\nPost any links to content which would, if posted on our own website, violate these Rules of Conduct.\r\nWe reserve the right to terminate our service to you for reasons other than those listed here. We may terminate your account without explanation.\r\n\r\n10. Communications Decency Act\r\n\r\nAs we are located in Toronto, we have certain defenses available to us for liability for defamation posted on our website by third parties, such as in our forums or on our blog.\r\n\r\nOur Rules of Conduct prohibit defamation from being published on our website. In order to complain about defamatory material on our website, please notify us with full details of where the defamation is located on our website (provide us with the URL), your name, and an explanation of how the material is defamatory. We will respond within a reasonable time and, if it appears that there are grounds to believe the material is defamatory, we will remove it. You may send your request to info@foundmissing.ca.\r\n\r\n\r\n11. Contest Disclaimer\r\n\r\nThe maximum number of entries per person allowed during the contest period is as the contest has specified. Any attempt by a person to submit more than the stated maximum number of entries will be disqualified from participation in this contest. FoundMissing are not responsible for lost, late, incomplete, invalid, unintelligible, illegible, misdirected or postage-due entries, which will be disqualified. By participating, all entrants agree to abide by these official contest rules.\r\n\r\nIn consideration for your participation in the contest, the individual, group, organization, business, spectator, or other, does hereby release and forever discharge the FoundMissing, and its officers, board and employees, jointly and severally from any and all actions, cause of actions, claim and demands for, upon or by reason of any damage, loss or injury including death, which hereafter may be sustained by participating in the contest.\r\n\r\nThis release extends and applies to, and also covers and includes, all unknown, unforeseen, unanticipated and unsuspected injuries, damages, loss and liability and the consequences thereof including death, as well as those now disclosed and know to exist. The provision of any state, federal, local or territorial law or state providing substance that releases shall not extend to claims, demands, injuries, or damages which are know or unsuspected to exist at this time, to the person executing such release, are hereby expressly waived.\r\n\r\nIt is further understood and agreed that said participation in the contest not to be construed as an admission of any liability and acceptance of assumption of responsibility by FoundMissing, its officers, board and employees, jointly and severally, for all damages and expenses for which the FoundMissing , its officers, board and employees, become liable as a result of any alleged act of the parade participant.\r\n\r\nThe prize is non-transferable and cannot be exchanged for cash.\r\n\r\nFoundMissing reserves the right to offer alternative prize if any prize is not available or under special situation.\r\n\r\n12. Disclaimer\r\n\r\nWe do not actively screen blog comments and forum posts before they are uploaded to our website by third parties. As a part of your acceptance of this Agreement, you release us from any liability for the actions of third parties on our website, whether or not we would otherwise normally be held liable for such words under the laws of Ontario or any other jurisdiction.\r\n\r\n13. Copyright\r\n\r\nYou agree not to copy, distribute, display, disseminate, or otherwise reproduce any of the information on our website, or our website itself, without our prior written permission. This includes, but is not limited to, a prohibition on aggregating product information reviews and forum content made available on our website.\r\n\r\n14. Trademarks\r\n\r\n"FoundMissing" is a trademark used by us, FoundMissing, to uniquely identify our website, business, and service. You agree not to use this phrase anywhere without our prior written consent. Additionally, you agree not to use our trade dress, or copy the look and feel of our website or its design, without our prior written consent. You agree that this paragraph goes beyond the governing law on intellectual property law, and includes prohibitions on any competition that violates the provisions of this paragraph.\r\n\r\n15. Revocation of Consent\r\n\r\nWhere FoundMissing has given prior written consent for your use of our protected material in accordance with our above â€œCopyrightâ€ and â€œTrademarksâ€ provisions, we may revoke that consent at any time. If we so request, we may require that you immediately take action to remove from circulation, display, publication, or other dissemination, any of the marks, copyrighted content, or other materials that we previously consented for you to use.\r\n\r\n16. Notification of Changes\r\n\r\nWhenever FoundMissing changes its Terms of Use, changes will be posted to ensure users are aware of the new changes.\r\n\r\n17. Contact Information\r\n\r\nIf you have any questions, concerns or notices of violations to these terms and conditions, please contact the editor of FoundMissing by sending an email to info@foundmissing.ca\r\n', 1376084367, 0, 3),
(5, 'Contact Us', 'contact-us', 'For general inquiries collaborations, advertising opportunities on the site.\r\nPlease email:\r\n \r\ninfo@foundmissing.ca', 1376084379, 0, 5),
(6, 'Branding', 'branding', '<link rel="stylesheet" href="/css/pages.css">\r\n<div id="wrapper">\r\n<h1>Logos and Branding</h1>\r\n<div class="panel">\r\n<h2>Our Logo</h2>\r\n<hr />\r\n<h3>Grab one, it''s free! Just don''t be a jabronie â€“ please don''t mess with it.<br />\r\nThese all come in a folder with EPS and PNG files of different sizes. <a href="images/twoople-logos.zip">Click here</a> to download everything at once.<br /><br />\r\n<div class="arrow-down"></div> This is the primary FoundMissing logo. Try to use this one whenever you can.</h3>\r\n    <div class="logowrapper" style="margin-top: 0px;">\r\n        <div id="logo-p">\r\n        <center><img src="/images/fm-primarywhitetrans-64.png" width="372" height="64" alt="foundmissing primary logo red"/></center>\r\n        <a href="images/twoople-primary.zip"><div class="download"></div></a>\r\n		</div>\r\n    </div>\r\n    \r\n    <div class="logowrapper" id="half" style="margin-right:2.5%;">\r\n      <div id="logo-s">\r\n        <center>\r\n          <img src="/images/fm-sectrans-128.png" width="178" height="128" alt="foundmissing secondary logo" />\r\n        </center>\r\n        <a href="images/twoople-secondary.zip"><div class="download"></div></a>\r\n        </div>\r\n        <div class="description" id="half">\r\n        <h3><div class="arrow-up"></div> This is our secondary logo. When things get too tight on space, use this guy.</h3>\r\n        </div>\r\n    </div>\r\n    \r\n  <div class="logowrapper" id="half">\r\n      <div id="logo-i">\r\n        <center>\r\n		  <img src="/images/fm-redtrans-128.png" width="128" height="128" alt="foundmissing icon" />\r\n        </center>\r\n        <a href="images/twoople-icon.zip"><div class="download"></div></a>\r\n        </div>\r\n        <div class="description" id="half">\r\n        <h3><div class="arrow-up"></div> Use these icons for UI stuff and instances with limited space.</h3>\r\n      </div>\r\n    </div>\r\n</div>\r\n\r\n    \r\n    \r\n    \r\n\r\n\r\n\r\n<div class="panel" style="padding-top: 50px; margin-top:40px;">\r\n<h2>Colour</h2>\r\n<hr />\r\n<h3>These are our primary and supporting colours.</h3>\r\n<div class="colour" id="fmred">\r\n    <div class="colourinner">\r\n    <h3>FM Infrared</h3>\r\n    <h4>HEX	#EC1C3A<br />\r\n        RGB	236, 28, 58<br />\r\n        CMYK	1, 99, 80, 0<br />\r\n        PMS	1788 C</h4>\r\n    </div>\r\n</div>\r\n<div class="colour" id="fmdgrey">\r\n	<div class="colourinner">\r\n    <h3>Dark Grey</h3>\r\n    <h4>HEX	#535858<br />\r\n    RGB	83, 88, 88<br />\r\n    CMYK	66, 54, 55, 29</h4>\r\n    </div>\r\n</div>\r\n<div class="colour" id="fmgrey">\r\n	<div class="colourinner">\r\n    <h3>Grey</h3>\r\n    <h4>HEX	#A0A6A6<br />\r\n    RGB	160, 166, 166<br />\r\n	CMYK	40, 29, 31, 0</h4>\r\n    </div>\r\n</div>\r\n<div class="colour" id="fmlgrey">\r\n	<div class="colourinner">\r\n    <h3>Light Grey</h3>\r\n    <h4>HEX	#F1F2F2<br />\r\n	RGB	241, 242, 242<br />\r\n	CMYK	4, 2, 3, 0</h4>\r\n    </div>\r\n</div>\r\n\r\n\r\n\r\n<div class="colour2" id="fmwhite">\r\n	<div class="colourinner">\r\n    </div>\r\n</div>\r\n<div class="colour2" id="fmcharles">\r\n    <div class="colourinner">\r\n    <h3>Charleston Green</h3>\r\n    <h4>HEX	#232B2B<br/>\r\n    	RGB	35, 43, 43<br/>\r\n    	CMYK	76, 63, 64, 66</h4>\r\n    </div>\r\n</div>\r\n<div class="colour2" id="fmpink">\r\n	<div class="colourinner">\r\n    <h3>Pink</h3>\r\n    <h4>HEX	#535858<br />\r\n    RGB	83, 88, 88<br />\r\n    CMYK	66, 54, 55, 29</h4>\r\n    </div>\r\n</div>\r\n<div class="colour2" id="fmgreen">\r\n	<div class="colourinner">\r\n    <h3>Green</h3>\r\n    <h4>HEX	#232B2B<br/>\r\n    	RGB	35, 43, 43<br/>\r\n    	CMYK	76, 63, 64, 66</h4>\r\n    </div>\r\n</div>\r\n</div>\r\n\r\n\r\n\r\n<div class="panel">\r\n<h2>Spacing</h2>\r\n<hr />\r\n<h3>Keep it spacy.<br />\r\nWhen placing the FM logo, try and keep around an icon''s worth of padding between it and adjacent elements.</h3>\r\n<center><img src="/images/fm-safearea.png" width="500" height="128" alt="foundmissing safe area" /></center>\r\n</div>\r\n\r\n\r\n\r\n\r\n\r\n<div class="panel">\r\n<h2>Typography</h2>\r\n<hr />\r\n<h3>We use Georgia for copy, Verdana for the real tiny stuff, and <a href="http://www.jlee.ca/rigby.html">Rigby</a> for titling.</h3>\r\n\r\n    \r\n    <div class="typespec" style="margin-top:40px;">\r\n    <h2 style="font-family: Verdana; margin-bottom:-5px;">Verdana</h2>\r\n    <h3 style="font-family: Verdana; letter-spacing:2.5px; font-size:16px; margin-bottom:-5px;">A B C D E F G H I J K L M N O P Q R S T U V W X Y Z</h3>\r\n    <h3 style="font-family: Verdana; letter-spacing:3.5px; font-size:16px; margin-bottom:-5px;">a b c d e f g h i j k l m n o p q r s t u v w x y z</h3>\r\n    <h3 style="font-family: Verdana; letter-spacing:3.7px; font-size:16px; margin-bottom:-5px;">0 1 2 3 4 5 6 7 8 9 + - = ! @ # $ % ^ & * ( : ? </h3>\r\n    </div>\r\n    \r\n    <div class="typespec">\r\n    <h2 style="font-family: Georgia; margin-bottom:-5px;">Georgia</h2>\r\n    <h3 style="font-family: Georgia; letter-spacing:3.5px; font-size:16px; margin-bottom:-5px;">A B C D E F G H I J K L M N O P Q R S T U V W X Y Z</h3>\r\n    <h3 style="font-family: Georgia; letter-spacing:5px; font-size:16px; margin-bottom:-5px;">a b c d e f g h i j k l m n o p q r s t u v w x y z</h3>\r\n    <h3 style="font-family: Georgia; letter-spacing:5.5px; font-size:16px; margin-bottom:-5px;">0 1 2 3 4 5 6 7 8 9 + - = ! @ # $ % ^ & * ( : ? </h3>\r\n    </div>\r\n    \r\n   	<div class="typespec">\r\n    <h2 style="font-family: rigby; margin-bottom:-5px;">Rigby</h2>\r\n    <h3 style="font-family: rigby; letter-spacing:4px; font-size:16px; margin-bottom:-5px;">A B C D E F G H I J K L M N O P Q R S T U V W X Y Z</h3>\r\n    <h3 style="font-family: rigby; letter-spacing:5.2px; font-size:16px; margin-bottom:-5px;">a b c d e f g h i j k l m n o p q r s t u v w x y z</h3>\r\n    <h3 style="font-family: rigby; letter-spacing:6px; font-size:16px; margin-bottom:-5px;">0 1 2 3 4 5 6 7 8 9 + - = ! @ # $ % ^ & * ( : ? </h3>\r\n    </div>\r\n</div>\r\n</div>', 1401664164, 0, 1),
(7, 'Partners', 'partners', '<a href="http://tinypic.com?ref=ivznlj" target="_blank"><img src="http://i62.tinypic.com/ivznlj.jpg" border="0" alt="Image and video hosting by TinyPic"></a>', 1401664264, 0, 6),
(8, 'Advertising', 'advertising', 'adf', 1401664277, 1, 7);

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE IF NOT EXISTS `reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `excerpt` varchar(150) NOT NULL,
  `body` text NOT NULL,
  `shoe_id` int(11) NOT NULL,
  `cover_image` varchar(100) NOT NULL,
  `entry_datetime` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `ordering` int(1) NOT NULL DEFAULT '1',
  `publish_date` int(9) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


-- --------------------------------------------------------

--
-- Table structure for table `stores`
--

CREATE TABLE IF NOT EXISTS `stores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `description` text,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `stores`
--

INSERT INTO `stores` (`id`, `title`, `description`, `ordering`, `deleted`) VALUES
(1, 'Chippewa', '', 0, 1),
(2, 'Sebago', '', 1, 1),
(3, 'Puma', '', 2, 1),
(4, 'Under Armour', '', 3, 1),
(5, 'CNCPTS', '', 4, 1),
(6, 'The Bay', '', 5, 1),
(7, 'Amazon', '', 6, 1),
(8, 'Foot Locker', '', 7, 1),
(9, 'K-Swiss', '', 8, 1),
(10, 'Karmaloop', '', 9, 1),
(11, 'Pick Your Shoes', '', 10, 1),
(12, 'The Pop Shop', '', 11, 1),
(13, 'The Shoe Company', '', 12, 1),
(14, 'Livestock ', '', 0, 0),
(15, 'Nrml', '', 1, 0),
(16, 'Capsule', '', 2, 0),
(17, 'Serpentine ', '', 3, 0),
(18, 'Adrift ', '', 4, 0),
(19, 'Get Outside', '', 5, 0),
(20, 'Off The Hook', '', 6, 0),
(21, 'Nomad', '', 7, 0),
(22, 'Haven ', '', 8, 0);

-- --------------------------------------------------------

--
-- Table structure for table `styles`
--

CREATE TABLE IF NOT EXISTS `styles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `category_id` int(1) NOT NULL,
  `description` text,
  `ordering` int(11) NOT NULL DEFAULT '1',
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `styles`
--

INSERT INTO `styles` (`id`, `title`, `category_id`, `description`, `ordering`, `deleted`) VALUES
(1, 'Basketball', 1, '', 1, 0),
(2, 'Cross-Trainer', 1, '', 2, 0),
(3, 'Skate', 1, '', 4, 0),
(4, 'Casual', 1, '', 6, 0),
(5, 'Runner', 1, 'Shoes for vegans', 9, 0),
(6, 'Chukka', 2, '', 5, 0),
(7, 'Hiker ', 2, '', 5, 0),
(8, 'Winter ', 2, '', 8, 0),
(9, 'Above Ankle', 2, '', 7, 0),
(10, 'Dress Boots', 2, '', 3, 0),
(11, 'Oxford', 3, '', 0, 0),
(12, 'Monk', 3, NULL, 1, 0),
(13, 'Derby', 3, NULL, 1, 0),
(14, 'Loafer', 3, NULL, 1, 0),
(15, 'Other', 3, NULL, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE IF NOT EXISTS `subscribers` (
  `email` varchar(100) NOT NULL,
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscribers`
--
-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `admin` int(1) NOT NULL DEFAULT '0',
  `gender` varchar(10) DEFAULT NULL,
  `age` varchar(60) DEFAULT NULL,
  `shoe_size` varchar(25) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `country` varchar(60) DEFAULT NULL,
  `title` varchar(60) DEFAULT NULL,
  `bio` text,
  `twitter` varchar(50) DEFAULT NULL,
  `facebook` varchar(60) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `image` varchar(50) DEFAULT 'default_profile.jpg',
  `entry_datetime` int(11) DEFAULT '0',
  `update_datetime` int(11) NOT NULL DEFAULT '0',
  `deleted` int(1) NOT NULL DEFAULT '0',
  `karma` int(100) NOT NULL DEFAULT '0',
  `code` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `users`
--


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
