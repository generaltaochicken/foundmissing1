<?php
/**
 * ---------------------------------------------------------------
 * Houston v1.1
 * ---------------------------------------------------------------
 * @author Tyler Savery | The Young Astronauts
 * @see http://www.houstonmvc.com
 * @license Open Source
 * ---------------------------------------------------------------
 * 
 * Controller for api requests
 *
 */

namespace Core\Controllers;

class Api_Controller {
    
    protected $uri;
    protected $data;
    protected $output = 'json';

    protected $response_code;

    protected $json_response;
    protected $_user;
    
    public function __construct($uri, $data) {
        $this->uri          = $uri;
        $this->data         = $data;

        if (isset($uri['type'])) {
            $this->output = 'json';
        } else {
            $this->output = 'json';
        }
    }
    
    public function render_view() {
        switch ($this->output) {
        	case 'html':
        		echo $this->data;
                break;
        	case 'json':
        		
                header('Content-type: application/json');
                header("HTTP/1.0 " . $this->response_code);
                $this->json_response['code'] = $this->response_code;
        		echo json_encode($this->json_response);
                die();
                break;
            case 'jsonp':
                header('Content-type: application/json');
                if (isset($_GET['callback'])) {
                    $callback = $_GET['callback'];
                } else {
                    $callback = 'jsonpCallback';
                }
                echo $callback.'('.json_encode($this).')';
                break;
        	case 'xml':
        	    header('Content-type: application/xml');
			    $xml = new SimpleXMLElement("<?xml version=\"1.0\"?><ajax></ajax>"); 
			    $f = create_function('$f, $c, $a',' 
			            foreach ($a as $k=>$v) {
			                if(is_array($v)) { 
			                    $ch=$c->addChild($k); 
			                    $f($f,$ch,$v); 
			                } else { 
			                    $c->addChild($k,$v); 
			                } 
			            }'); 
			    $f($f, $xml, $this);
			    echo $xml->asXML();
                break;
        }
    }


    public function request($param){

        return $_REQUEST[$param];

    }

    public function controller(){
        return;
    }

    public function get_redirect(){
        if(isset($_GET['redirect'])){
            return $_GET['redirect'];
        }

        return "/manage/messages";
    }

    public function add_flash($flash){
        $_SESSION['flashes'][] = $flash;
    }

}