<?php

namespace Form\Models;

class FormItemTextarea extends FormItem {
    
    public function __construct($attributes){
        $this->type = 'textarea';
        $this->template = 'textarea';
        parent::__construct($attributes);
    }
    
    
    
}