<?php

namespace Core\Models;

class Base_Model extends \ActiveRecord\Model{

	public static function get_next_ordering_number(){
		$recent_item = self::find(array(
			'conditions' => array('deleted=0'),
			'order' => 'ordering desc'
			)
		);

		if($recent_item){
			return $recent_item->ordering + 1;
		}

		return 0;

	}

}