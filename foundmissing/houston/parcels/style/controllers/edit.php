<?php
namespace Style\Controllers;

class Edit extends \Manage\Controllers\Manage{

	private $style;

	public $page_title;
	public $link_back;
	public $form;
	public $existing;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		if(isset($_POST["submit"])){
			$this->submit_form();
		}

		if(isset($data["style_id"])){
			$this->style = \Style\Models\Style::find($data["style_id"]);
			$this->page_title = "Editing Style";
		} else {
			$this->style = new \Style\Models\Style;
			$this->page_title = "New Style";
		}

		$this->link_back = $this->style->link_all();

		$form = new \Form\Models\Form("edit", "POST", "", "well");

		$item = new \Form\Models\FormItemHidden(array("id" => "id", "title" => "Id", "value" => $this->style->id));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "title", "title" => "Title", "value" => $this->style->title));
		$form->add_item($item);
		
		$item = new \Form\Models\FormItemSelect(array("id" => "category_id", "title" => "Category", "options" => $this->get_category_options($this->style->category_id)));
		$form->add_item($item);

		$item = new \Form\Models\FormItemTextarea(array("id" => "description", "title" => "Description", "value" => $this->style->description));
		$form->add_item($item);

		$item = new \Form\Models\FormItemSubmit(array("value" => "Save", "id" => "submit", "class" => "btn btn-primary", "cancel_url" => $this->style->link_all()));
		$form->add_item($item);

		$this->form = $form->render();

	}

	public function controller(){
		$this->set_view("Style\Views\Edit");
	}

	private function submit_form(){
		if($_POST["id"] > 0){
			$style = \Style\Models\Style::find($_POST["id"]);
			$flash_msg = "Style has been updated successfully.";
		} else {
			$style = new \Style\Models\Style;
			$flash_msg = "Style has been created successfully.";
			$style->ordering = \Style\Models\Style::get_next_ordering_number();
		}

		$style->title = $_POST["title"];
		$style->category_id = $_POST["category_id"];
		$style->description = $_POST["description"];

		$style->save();
		$this->add_flash(array("message" => $flash_msg, "type" => "success", "Heading" => "Great!"));
		redirect_to($style->link_all());
	}
	
	protected function get_category_options($current){
		$categories = \Category\Models\Category::all(array('conditions' => 'deleted = 0', 'order' => 'ordering'));
		return $this->get_options($categories, $current);
	}

}
