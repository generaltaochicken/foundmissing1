<?php
namespace Account\Controllers;

class Logout extends \Account\Controllers\Account {

    public function __construct($uri, $data) {
        $user = new \User\Models\User();
        unset($_SESSION[$user->login_session_name]);

        $this->add_flash(array('message' => 'Logged out successfully', 'type' => 'success'));

        redirect_to('/account/login');
    }
}