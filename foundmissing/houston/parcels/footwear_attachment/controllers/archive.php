<?php
namespace Footwear_attachment\Controllers;

class Archive extends \Manage\Controllers\Manage{

	private $footwear_attachments;

	public $footwear_attachments_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Deleted Footwear_attachments";

		$this->footwear_attachments = \Footwear_attachment\Models\Footwear_attachment::all(array("conditions" => "deleted = 1"));
		$this->archive_button = array("url" => \Footwear_attachment\Models\Footwear_attachment::link_all(), "title" => "Back");
		foreach($this->footwear_attachments as $footwear_attachment){
			$this->has_data = true;
			$this->footwear_attachments_data[] = array(
				"id" => $footwear_attachment->id,
				"footwear_id" => $footwear_attachment->footwear_id,
				"attachment_id" => $footwear_attachment->attachment_id,
				"entry_datetime" => $footwear_attachment->entry_datetime,
				"link_restore" => $footwear_attachment->link_restore(),
			);
		}

	}

	public function controller(){
		$this->set_view("Footwear_attachment\Views\All");
	}

}

