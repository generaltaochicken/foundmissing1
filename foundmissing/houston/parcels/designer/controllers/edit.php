<?php
namespace Designer\Controllers;

class Edit extends \Manage\Controllers\Manage{

	private $designer;

	public $page_title;
	public $link_back;
	public $form;
	public $existing;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		if(isset($_POST["submit"])){
			$this->submit_form();
		}

		if(isset($data["designer_id"])){
			$this->designer = \Designer\Models\Designer::find($data["designer_id"]);
			$this->page_title = "Editing Designer";
		} else {
			$this->designer = new \Designer\Models\Designer;
			$this->page_title = "New Designer";
		}

		$this->link_back = $this->designer->link_all();

		$form = new \Form\Models\Form("edit", "POST", "", "well");

		$item = new \Form\Models\FormItemHidden(array("id" => "id", "title" => "Id", "value" => $this->designer->id));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "title", "title" => "Title", "value" => $this->designer->title));
		$form->add_item($item);

		$item = new \Form\Models\FormItemTextarea(array("id" => "description", "title" => "Description", "value" => $this->designer->description));
		$form->add_item($item);

		$item = new \Form\Models\FormItemSubmit(array("value" => "Save", "id" => "submit", "class" => "btn btn-primary", "cancel_url" => $this->designer->link_all()));
		$form->add_item($item);

		$this->form = $form->render();

	}

	public function controller(){
		$this->set_view("Designer\Views\Edit");
	}

	private function submit_form(){
		if($_POST["id"] > 0){
			$designer = \Designer\Models\Designer::find($_POST["id"]);
			$flash_msg = "Designer has been updated successfully.";
		} else {
			$designer = new \Designer\Models\Designer;
			$flash_msg = "Designer has been created successfully.";
			$designer->ordering = \Designer\Models\Designer::get_next_ordering_number();
		}

		$designer->title = $_POST["title"];
		$designer->description = $_POST["description"];

		$designer->save();
		$this->add_flash(array("message" => $flash_msg, "type" => "success", "Heading" => "Great!"));
		redirect_to($designer->link_all());
	}

}
