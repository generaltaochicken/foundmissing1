<?php
namespace Style\Controllers;

class All extends \Manage\Controllers\Manage{

	private $styles;

	public $styles_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Styles";
		$this->link_new = \Style\Models\Style::link_new();

		$this->styles = \Style\Models\Style::all(array("conditions" => "deleted = 0" , "order" => "ordering"));
		$archived = \Style\Models\Style::count(array("conditions" => "deleted = 1"));
		if($archived > 0){
			$this->archive_button = array("url" => \Style\Models\Style::link_archive(), "title" => "Archive");
		}

		foreach($this->styles as $style){
			$this->has_data = true;
			$this->styles_data[] = array(
				"id" => $style->id,
				"title" => $style->title,
				"category" => \Category\Models\Category::get_category_name($style->category_id),
				"link_edit" => $style->link_edit(),
				"link_delete" => $style->link_delete(),
				"ordering" => $style->ordering,
			);
		}

	}

	public function controller(){
		$this->set_view("Style\Views\All");
	}

}

