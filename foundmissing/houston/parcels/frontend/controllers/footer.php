<?php

namespace Frontend\Controllers;

class Footer extends \Frontend\Controllers\Frontend {

    public $links_data = array();
    public $social_links = array();

    public function __construct($uri, $data){
        parent::__construct($uri, $data);
    }


    public function controller () {
        $this->set_view('Frontend\Views\Footer');
    }

}