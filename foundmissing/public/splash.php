<html>
<head>
<title>FOUNDMISSING</title>

<link href="//vjs.zencdn.net/4.6/video-js.css" rel="stylesheet">
<link rel="shortcut icon" href="/icon.png">
<script src="//vjs.zencdn.net/4.6/video.js"></script>
<meta name="keywords" content="FoundMissing, Nike, Footwear, Men's Lifestyle, Shoes">
<meta name="description" content="As you find something, the ravenous cycle of fashion leaves us wanting more. Our collections grow but every piece inspires something new, that one thing missing to make your collection complete.">
<style>
@font-face {
font-family: 'rigby';
	src:url('/fonts/rigby_v8-webfont.eot');
	src:url('/fonts/rigby_v8-webfont.eot?#iefix') format('embedded-opentype'),
		url('/fonts/rigby_v8-webfont.woff') format('woff'),
		url('/fonts/rigby_v8-webfont.ttf') format('truetype'),
		url('/fonts/rigby_v8-webfont-webfont.svg') format('svg');
	font-weight: normal;
	font-style: normal;
}

body {
	background-color: #000;
}
	
#coming-soon{
	position: fixed;
	top: 50%;
	left: 50%;
	text-align: center;
	font-family: Helvetica, Arial, sans-serif;
	font-size: 12px;
	margin-left: -512px;
	margin-top: -312px;
}

.no_results {
	font-family: rigby;
	text-transform: uppercase;
	font-size: 72px;
	width: 100%;
	color: #535858;
	text-align: center;
	top: 40%;
	position: absolute;
}

.small {
	font-size: 48px;
}

/*img {
	width: 500px;
	height: 500px;
	position: absolute;
	left: 50%;
	top: 50%;
	margin-left: -250px;
	margin-top: -250px;
}*/

img {
	height: 35px;
	width: 35px;
	margin-top: 5px;
	position: absolute;
	right: 0;
	margin-right: 170px;
	top: -13px;
	cursor: pointer;
}

.article_caption_small {
	margin-right: 15px;
	margin-top: 15px;
	
	z-index: 9999;
}

.article_caption_small form {
	position: relative;
	z-index: 9999;
}

.article_caption_small form button {
	color: #FFFFFF;
	font-family: rigby;
	font-size: 18px;
	letter-spacing: 2px;
	float: right;
	color: #FFFFFF;
	text-transform: uppercase;
	background: none;
	border: none;
	cursor: pointer;
	z-index: 9999;
	
}

#video_container {
	position: fixed;
	width: 100%;
	top: 50%;
	margin-top: -325px;
	z-index: 1;
}

</style>
</head>
<body>
	<!--<div id="coming-soon">
		<video id="example_video_1" class="video-js vjs-default-skin vjs-big-play-centered"
		  controls preload="auto" width="1024" height="624"
		  poster="/uploads/videos/featureimg-DEV01-1.png"
		  data-setup='{"example_option":true}'>
			<source src="/uploads/videos/fm_preview.mp4" type='video/mp4' />
			<p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
		</video>
		
	</div>
	
	<!--<img src="/images/fm_origin.png" />-->
	<div class="article_caption_small text-center"><form method="POST" action="/"><button name="submit" type="submit">Continue to &nbsp;&nbsp;&nbsp;&nbsp; FoundMissing</button><img src="/images/fm-redtrans-128.png" /></form></div>
	<div id="video_container" class="row"><div class="col-md-12"><iframe src="//player.vimeo.com/video/112126945" width="100%" height="650px" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div></div>
	
	<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-53625002-1', 'auto');
	ga('send', 'pageview');

	</script>

</body>
</html>
