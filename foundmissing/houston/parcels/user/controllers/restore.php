<?php
namespace User\Controllers;

class Restore extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$user = \User\Models\User::find($data["user_id"]);

		if($user){
			$user->deleted = 0;
			$user->save();
			$this->add_flash(array("message" => "User has been restored!"));

			redirect_to($user->link_all());
		}
	}
}
