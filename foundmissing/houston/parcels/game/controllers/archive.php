<?php
namespace Game\Controllers;

class Archive extends \Manage\Controllers\Manage{

	private $games;

	public $games_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Deleted Games";

		$this->games = \Game\Models\Game::all(array("conditions" => "deleted = 1"));
		$this->archive_button = array("url" => \Game\Models\Game::link_all(), "title" => "Back");
		foreach($this->games as $game){
			$this->has_data = true;
			$this->games_data[] = array(
				"id" => $game->id,
				"title" => $game->title,
				"system" => $game->system,
				"entry_datetime" => $game->entry_datetime,
				"link_restore" => $game->link_restore(),
			);
		}

	}

	public function controller(){
		$this->set_view("Game\Views\All");
	}

}

