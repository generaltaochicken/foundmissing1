<?php
namespace Footwear\Controllers;

class Archive extends \Manage\Controllers\Manage{

	private $footwears;

	public $footwears_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Deleted Footwears";

		$this->footwears = \Footwear\Models\Footwear::all(array("conditions" => "deleted = 1"));
		$this->archive_button = array("url" => \Footwear\Models\Footwear::link_all(), "title" => "Back");
		foreach($this->footwears as $footwear){
			$this->has_data = true;
			$this->footwears_data[] = array(
				"id" => $footwear->id,
				"brand_id" => $footwear->brand_id,
				"model_name" => $footwear->model_name,
				"style_id" => $footwear->style_id,
				"tags" => $footwear->tags,
				"price" => $footwear->price,
				"rating" => $footwear->rating,
				"color_id" => $footwear->color_id,
				"entry_datetime" => $footwear->entry_datetime,
				"link_restore" => $footwear->link_restore(),
			);
		}

	}

	public function controller(){
		$this->set_view("Footwear\Views\All");
	}

}

