<?php

namespace Frontend\Controllers;

class Listing extends \Frontend\Controllers\Frontend {

	public $has_articles = false;
	public $has_reviews = false;
	public $has_interviews = false;
	public $has_results = false;
	public $result_count;
	
	public function __construct($uri, $data){
        parent::__construct($uri, $data);
		
		$this->title = "Search | FM";
    }

    public function controller(){
	
		if (isset($_GET['search'])) {
			$this->query = $_GET["search"];
			$conditions = 'deleted = 0 AND title LIKE "%' . $this->query . '%"';
		} else {
			$conditions = 'deleted = 0';
		}
		
		$articles = \Article\Models\Article::all(array('conditions' => $conditions));
		if ($articles != null) {
			$this->has_articles = true;
			$this->has_results = true;
			$this->result_count = count($articles);
		}
		
		foreach ($articles as $article) {
			$this->article[] = array(
				'id' => $article->id,
				'title' => $article->title,
				'link' => '/articles/' . $article->slug,
				'image' => $article->bg_image_header,
				'excerpt' => $article->excerpt
			);
		}
		
		$join = 'LEFT JOIN footwears f ON(reviews.shoe_id = f.id)';

		$reviews = \Review\Models\Review::all(array('select' => 'reviews.*', 'joins' => $join, 'conditions' => 'reviews.deleted = 0 AND title LIKE "%' . $this->query . '%" OR tags LIKE "%' . $this->query . '%"'));
		
		if ($reviews != null) {
			$this->has_reviews = true;
			$this->has_results = true;
			$this->result_count += count($reviews);
		}
		
		foreach ($reviews as $review) {
			$this->review[] = array(
				'id' => $review->id,
				'title' => $review->title,
				'link' => '/reviews/' . $review->slug,
				'image' => $review->get_main_image(),
				'excerpt' => $review->excerpt
			);
		}
		
		$this->set_view('Frontend\Views\Listing');
	}
}