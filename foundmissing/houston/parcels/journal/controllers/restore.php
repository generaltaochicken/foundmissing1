<?php
namespace Journal\Controllers;

class Restore extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$Journal = \Journal\Models\Journal::find($data["Journal_id"]);

		if($Journal){
			$Journal->deleted = 0;
			$Journal->save();
			$this->add_flash(array("message" => "Journal has been restored!"));

			redirect_to($Journal->link_all());
		}
	}
}
