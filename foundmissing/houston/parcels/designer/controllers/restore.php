<?php
namespace Designer\Controllers;

class Restore extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$designer = \Designer\Models\Designer::find($data["designer_id"]);

		if($designer){
			$designer->deleted = 0;
			$designer->save();
			$this->add_flash(array("message" => "Designer has been restored!"));

			redirect_to($designer->link_all());
		}
	}
}
