<?php

namespace Mobile\Controllers;

class Mobile extends \Core\Controllers\Base_Controller {

	public function __construct($uri, $data){
        parent::__construct($uri, $data);
		$this->title = "F O U N D M I S S I N G";
		//$this->dev = DEV;
		
		$this->add_asset('css', 'reset.css', 'core');
		$this->add_asset('css', 'bootstrap.min.css', true);
		$this->add_asset('css', 'font-awesome.min.css', true);
		$this->add_asset('css', 'bootstrap-theme.min.css', true);
		$this->add_asset('css', 'mobile.css', true);
		$this->add_asset('css', 'font-awesome.min.css', true);
		
		//$this->add_asset('js', 'jquery.min.js', 'core');
		$this->add_asset('js', 'jquery-1.11.1.js', true);
		//$this->add_asset('js', 'bootstrap.js', true);
		$this->add_asset('js', 'transit.js', true);
		//$this->add_asset('js', 'waitforimages.js', true);
		//$this->add_asset('js', 'touchswipe.js', true);
		//$this->add_asset('js', 'jquery.lazyload.js', true);
		$this->add_asset('js', 'mobile.js', true);
	}

}

?>