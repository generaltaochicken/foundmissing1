<?php

namespace Frontend\Controllers;

class Page extends \Frontend\Controllers\Frontend {

    private $data;

	public function __construct($uri, $data){
        parent::__construct($uri, $data);

        $this->data = $data;
		$this->add_asset('css', 'pages.css', true);

    }

    public function controller(){

        $page = \Page\Models\Page::find(array('conditions' => 'slug = "' . $this->data['slug'] . '"'));
    
        $this->content = array(
            'title' => $page->title,
            'body' => nl2br($page->body)
        );
		
		if ($this->content['title'] == null) {
			redirect_to('/');
		} else {
			$this->set_view('Frontend\Views\Page');
		}
       
	}

}