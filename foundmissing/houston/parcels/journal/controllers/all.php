<?php
namespace Journal\Controllers;

class All extends \Manage\Controllers\Manage{

	private $journals;

	public $journals_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Journals";
		$this->link_new = \Journal\Models\Journal::link_new();

		$this->journals = \Journal\Models\Journal::all(array("conditions" => "deleted = 0" , "order" => "ordering"));
		$archived = \Journal\Models\Journal::count(array("conditions" => "deleted = 1"));
		if($archived > 0){
			$this->archive_button = array("url" => \Journal\Models\Journal::link_archive(), "title" => "Archive");
		}

		foreach($this->journals as $Journal){
			$this->has_data = true;
			$this->journals_data[] = array(
				"id" => $Journal->id,
				"entry_datetime" => $Journal->entry_datetime,
				"title" => $Journal->title,
				"author" => $Journal->author,
				"body" => $Journal->body,
				"excerpt" => $Journal->excerpt,
				"main_image" => $Journal->main_image,
				"publish_date" => $Journal->publish_date,
				"slug" => $Journal->slug,
				"link_edit" => $Journal->link_edit(),
				"link_delete" => $Journal->link_delete(),
			);
		}

	}

	public function controller(){
		$this->set_view("Journal\Views\All");
	}

}

