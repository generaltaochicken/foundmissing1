<?php
namespace Store\Controllers;

class All extends \Manage\Controllers\Manage{

	private $stores;

	public $stores_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Stores";
		$this->link_new = \Store\Models\Store::link_new();

		$this->stores = \Store\Models\Store::all(array("conditions" => "deleted = 0" , "order" => "ordering"));
		$archived = \Store\Models\Store::count(array("conditions" => "deleted = 1"));
		if($archived > 0){
			$this->archive_button = array("url" => \Store\Models\Store::link_archive(), "title" => "Archive");
		}

		foreach($this->stores as $store){
			$this->has_data = true;
			$this->stores_data[] = array(
				"id" => $store->id,
				"title" => $store->title,
				"description" => $store->description,
				"link_edit" => $store->link_edit(),
				"link_delete" => $store->link_delete(),
				"ordering" => $store->ordering,
			);
		}

	}

	public function controller(){
		$this->set_view("Store\Views\All");
	}

}

