<?php
namespace Store\Controllers;

class Restore extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$store = \Store\Models\Store::find($data["store_id"]);

		if($store){
			$store->deleted = 0;
			$store->save();
			$this->add_flash(array("message" => "Store has been restored!"));

			redirect_to($store->link_all());
		}
	}
}
