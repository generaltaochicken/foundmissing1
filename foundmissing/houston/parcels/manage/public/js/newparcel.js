var App = {};

App.fields = [];
App.fields.i = 0;

App.Init = function() {

	App.Cache();
	App.BindListeners();


};

App.Cache = function() {
	App.dom = {};

	App.dom.formContainer = $(".form-container");

	App.dom.addCustom = $("a.add.add-custom");
	App.dom.addPk = $("a.add.add-pk");
	App.dom.addEntry_datetime = $("a.add.add-entry_datetime");
	App.dom.addUpdate_datetime = $("a.add.add-update_datetime");
	App.dom.addOrdering = $("a.add.add-ordering");
	App.dom.addDeleted = $("a.add.add-deleted");

	App.dom.infoName = $("#info-name");
	App.dom.infoPlural = $("#info-plural");
	App.dom.infoAuthor = $("#info-author");
	App.dom.infoDescription = $("#info-description");

	App.dom.submit = $("#submit");


};

App.BindListeners = function(){


	App.dom.addCustom.on('click', function(e){
		e.preventDefault();
		App.FieldTemplate('custom');

	});

	App.dom.addPk.on('click', function(e){
		e.preventDefault();
		App.FieldTemplate('pk');

	});

	App.dom.addEntry_datetime.on('click', function(e){
		e.preventDefault();
		App.FieldTemplate('entry_datetime');

	});

	App.dom.addUpdate_datetime.on('click', function(e){
		e.preventDefault();
		App.FieldTemplate('update_datetime');

	});

	App.dom.addOrdering.on('click', function(e){
		e.preventDefault();
		App.FieldTemplate('ordering');

	});


	App.dom.addDeleted.on('click', function(e){
		e.preventDefault();
		App.FieldTemplate('deleted');

	});

	App.dom.submit.on('click', function(){

		App.CreateParcel();

	});



};


App.FieldTemplate = function(type){

	App.fields.i++;

	$.ajax({
	    url : '/manage/ajax/field/' + App.fields.i,
	    type:"GET",
	    data: '',
	    success: function(data) {

	        App.dom.formContainer.append(data);
	        
	        var container = $("#field-" + App.fields.i);

	        container.find('.btn-danger').on('click', function(e){
	        	e.preventDefault();
	        	container.remove();
	        });

	        if(type == 'custom'){

			}

			if(type == 'pk'){
				container.find('.type').val('pk');
				container.find('.name').val('id');
				container.find('.size').val('11');
				container.find('.default').val('');
				container.find('.table').prop('checked', true);
				container.find('.edit').prop('checked', false);

				container.find('.ai').val('true');

				container.find('input, select').attr('disabled', true);
			}

			if(type == 'entry_datetime'){
				container.find('.type').val('date');
				container.find('.name').val('entry_datetime');
				container.find('.size').val('11');
				container.find('.default').val('');
				container.find('.table').prop('checked', true);
				container.find('.edit').prop('checked', false);
				container.find('.ai').val('false');
				container.find('input, select').attr('disabled', true);
			}

			if(type == 'update_datetime'){
				container.find('.type').val('date');
				container.find('.name').val('update_datetime');
				container.find('.size').val('11');
				container.find('.default').val('');
				container.find('.table').prop('checked', true);
				container.find('.edit').prop('checked', false);
				container.find('.ai').val('false');
				container.find('input, select').attr('disabled', true);
			}

			if(type == 'ordering'){
				container.find('.type').val('int');
				container.find('.name').val('ordering');
				container.find('.size').val('11');
				container.find('.default').val('1');
				container.find('.table').prop('checked', false);
				container.find('.edit').prop('checked', false);
				container.find('.ai').val('false');
				container.find('input, select').attr('disabled', true);
			}

			if(type == 'deleted'){
				container.find('.type').val('int');
				container.find('.name').val('deleted');
				container.find('.size').val('1');
				container.find('.default').val('0');
				container.find('.table').prop('checked', false);
				container.find('.edit').prop('checked', false);
				container.find('.ai').val('false');
				container.find('input, select').attr('disabled', true);
			}
	       
	    }
	});


}

App.Validate = function(){
	return true;
}

App.CreateParcel = function(){
	if(confirm("Are you sure?")){

		if(App.Validate()){

			var obj = {};

			obj.meta = {};
			obj.meta.name = App.dom.infoName.val();
			obj.meta.plural = App.dom.infoPlural.val();
			obj.meta.author = App.dom.infoAuthor.val();
			obj.meta.description = App.dom.infoDescription.val();
			obj.meta.version = "1.0.0";
			obj.meta.created = "";
			obj.meta.icon = "star";
			
			obj.meta.dependencies = {};
			obj.meta.dependencies.core = "";

			var name = App.dom.infoName.val();
			var plural = App.dom.infoPlural.val();
			var modelName = Util.ucwords(name);

			obj.routes = {};
			obj.routes['manage/' + plural] 								= modelName + '\\Controllers\\All';
			obj.routes['manage/' + plural + '/all'] 					= modelName + '\\Controllers\\All';
			obj.routes['manage/' + plural + '/new'] 					= modelName + '\\Controllers\\Edit';
			obj.routes['manage/' + plural + '/edit/:' + name + '_id'] 	= modelName + '\\Controllers\\Edit';
			obj.routes['manage/' + plural + '/delete/:' + name + '_id'] = modelName + '\\Controllers\\Delete';
			obj.routes['manage/' + plural + '/restore/:' + name + '_id']= modelName + '\\Controllers\\Restore';
			obj.routes['manage/' + plural + '/archive'] 				= modelName + '\\Controllers\\Archive';
			obj.routes['manage/' + plural + '/reorder/:this_id/:swap_id']= modelName + '\\Controllers\\Reorder';


			obj.models = {};

			var modelname = App.dom.infoName.val();

			obj.models[modelname] = {};
			obj.models[modelname].fields = {};

			var containers = $(".field");

			for(i=0;i<containers.length;i++){
				var fieldname = containers.eq(i).find('.name').val();

				var type = containers.eq(i).find('.type').val();
				var widget = containers.eq(i).find('.type').val();
				var size = containers.eq(i).find('.size').val();
				var def = containers.eq(i).find('.default').val();
				var pres = Util.presentation_words(fieldname);
				var table = (containers.eq(i).find('.table').is(":checked")) ? true : false;
				var edit = (containers.eq(i).find('.edit').is(":checked")) ? true : false

				obj.models[modelname].fields[fieldname] = {};

				if(type == 'pk'){
					type = 'int';
					obj.models[modelname].fields[fieldname].pk = true;
					obj.models[modelname].fields[fieldname].ai = true;
				}

				if(type == 'int' && def == ''){
					def = 0;
				}

				if(type == 'date'){
					type = 'int';
				}

				if(type != 'text'){
					obj.models[modelname].fields[fieldname].size = size;
				}

				if(type == 'file'){
					widget = 'file';
					type = 'varchar';
				}

				if(def){
					obj.models[modelname].fields[fieldname].default = def;
				}

				obj.models[modelname].fields[fieldname].type = type;
				obj.models[modelname].fields[fieldname].presentation = pres;
				obj.models[modelname].fields[fieldname].table_view = table;
				obj.models[modelname].fields[fieldname].edit_view = edit;
				obj.models[modelname].fields[fieldname].widget = widget;

			}

			App.SaveObjectAsJson(obj);

		} else {
			alert("A problem has occurred!")
		}

	}
}


App.SaveObjectAsJson = function(obj){

	var jsonString = JSON.stringify(obj);
	var data = {name: obj.meta.name, plural: App.dom.infoPlural.val(),  json: jsonString};

	 $.ajax({
	    url : '/manage/ajax/save',
	    type:"POST",
	    data: data,
	    success: function(redirect) {
	    	window.location = redirect;
	    }
	 });


}

// Interactions

//Classes

// On Load
$(document).ready(function(){

	App.Init();

});

/* UTILITY FUNCTIONS */

var Util = {};

Util.presentation_words = function(str){

	str = str.replace('_', ' ');
	return Util.ucwords(str);

}

Util.ucwords = function(str) {
    return (str + '').replace(/^([a-z])|\s+([a-z])/g, function ($1) {
        return $1.toUpperCase();
    });
}