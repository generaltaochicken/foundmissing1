<?php
namespace Game\Controllers;

class Restore extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$game = \Game\Models\Game::find($data["game_id"]);

		if($game){
			$game->deleted = 0;
			$game->save();
			$this->add_flash(array("message" => "Game has been restored!"));

			redirect_to($game->link_all());
		}
	}
}
