<?php
namespace Store\Controllers;

class Archive extends \Manage\Controllers\Manage{

	private $stores;

	public $stores_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Deleted Stores";

		$this->stores = \Store\Models\Store::all(array("conditions" => "deleted = 1"));
		$this->archive_button = array("url" => \Store\Models\Store::link_all(), "title" => "Back");
		foreach($this->stores as $store){
			$this->has_data = true;
			$this->stores_data[] = array(
				"id" => $store->id,
				"title" => $store->title,
				"description" => $store->description,
				"link_restore" => $store->link_restore(),
			);
		}

	}

	public function controller(){
		$this->set_view("Store\Views\All");
	}

}

