<?php
namespace Api\Controllers;

class Categorys extends \Api\Controllers\Api{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->response = $this->get_categorys();
		$this->send_response();

	}

	protected function get_categorys(){

		$conditions = array('deleted' => 0);

		if($this->request('get', 'parent_id')){
			$conditions['parent_id'] = $this->request('get', 'parent_id');
		}

		$args = array('conditions' => $conditions);

		$items = \Category\Models\Category::all($args);
		$array = array();
		foreach($items as $item){
			$array[] = array(
				"id" => $item->id,
				"parent_id" => $item->parent_id,
				"title" => $item->title,
				"slug" => $item->slug,
				"attachment_id" => $item->attachment_id
			);
		}

		return $array;
		
	}

}