<?php
namespace Founder\Controllers;

class Delete extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$article = \Founder\Models\Founder::find($data["article_id"]);

		if($article){
			$article->deleted = 1;
			$article->save();
			$this->add_undo(array("message" => "Founder has been deleted!", "action" => $article->link_restore()));

			redirect_to($article->link_all());
		}
	}
}
