<?php

namespace Manage\Controllers;

class AdminAjax extends \Manage\Controllers\Manage {
    public function __construct($uri, $data){
        parent::__construct($uri, $data);
		
		if (isset($_POST['type'])) {
			if ($_POST['type'] == 'brand_filter') {
				$this->get_shoes_by_brand($_POST['brand']);
				echo $this->render_footwear();
			}
		}
    }

    public function controller(){
    	
    }

	protected function get_shoes_by_brand($id) {
		if ($id != 0) {
			$footwears = \Footwear\Models\Footwear::all(array('conditions' => 'deleted = 0 AND brand_id = ' . $id));
		} else {
			$footwears = \Footwear\Models\Footwear::all(array('conditions' => 'deleted = 0'));
		}
		
		foreach($footwears as $footwear){
			$brand = \Brand\Models\Brand::find($footwear->brand_id);
			
			$colors = '';
			$exploded = explode(',', $footwear->color_id);
			$count = 1;
			
			foreach ($exploded as $color) {
				if ($count == $exploded) {
					$colors .= \Color\Models\Color::get_color_name($color);
				} else {
					$colors .= \Color\Models\Color::get_color_name($color) . ', ';
				}
			}
			
			$this->has_data = true;
			$this->footwears_data[] = array(
				"id" => $footwear->id,
				"brand_id" => $brand->title,
				"model_name" => $footwear->model_name,
				"style_id" => $footwear->style_id,
				"tags" => $footwear->tags,
				"price" => $footwear->price,
				"rating" => $footwear->rating,
				"color_id" => $colors,
				"entry_datetime" => $footwear->entry_datetime,
				"link_edit" => $footwear->link_edit(),
				"link_delete" => $footwear->link_delete(),
				"ordering" => $footwear->ordering,
			);
		}
	}

	protected function render_footwear() {
		foreach ($this->footwears_data as $data) {
			$output .= '<tr rel="' . $data['id'] . '">
							<td>' . $data['id'] . '</td>
							<td>' . $data['brand_id'] . '</td>
							<td>' . $data['model_name'] . '</td>
							<td>' . $data['style_id'] . '</td>
							<td>' . $data['tags'] . '</td>
							<td>' . $data['price'] . '</td>
							<td>' . $data['rating'] . '</td>
							<td>' . $data['color_id'] . '</td>
							<td>
								<a style="background-color:#FFFFFF; color: #000" href="/manage/footwear/edit/' . $data['id'] . '" class="btn">Edit</a>
								<a style="background-color:#EB1C39; color: #FFFFFF;" href="/manage/footwear/delete/' . $data['id'] . '" class="btn">Delete</a>
							</td>
						</tr>
							';
		}
		
		return $output;
	}
}