<?php
namespace Comment_karma\Controllers;

class Edit extends \Manage\Controllers\Manage{

	private $comment_karma;

	public $page_title;
	public $link_back;
	public $form;
	public $existing;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		if(isset($_POST["submit"])){
			$this->submit_form();
		}

		if(isset($data["comment_karma_id"])){
			$this->comment_karma = \Comment_karma\Models\Comment_karma::find($data["comment_karma_id"]);
			$this->page_title = "Editing Comment_karma";
		} else {
			$this->comment_karma = new \Comment_karma\Models\Comment_karma;
			$this->page_title = "New Comment_karma";
		}

		$this->link_back = $this->comment_karma->link_all();

		$form = new \Form\Models\Form("edit", "POST", "", "well");

		$item = new \Form\Models\FormItemHidden(array("id" => "id", "title" => "Id", "value" => $this->comment_karma->id));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "type", "title" => "Type", "value" => $this->comment_karma->type));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "comment_id", "title" => "Comment Id", "value" => $this->comment_karma->comment_id));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "user_id", "title" => "User Id", "value" => $this->comment_karma->user_id));
		$form->add_item($item);

		$item = new \Form\Models\FormItemSubmit(array("value" => "Save", "id" => "submit", "class" => "btn btn-primary", "cancel_url" => $this->comment_karma->link_all()));
		$form->add_item($item);

		$this->form = $form->render();

	}

	public function controller(){
		$this->set_view("Comment_karma\Views\Edit");
	}

	private function submit_form(){
		if($_POST["id"] > 0){
			$comment_karma = \Comment_karma\Models\Comment_karma::find($_POST["id"]);
			$flash_msg = "Comment_karma has been updated successfully.";
		} else {
			$comment_karma = new \Comment_karma\Models\Comment_karma;
			$flash_msg = "Comment_karma has been created successfully.";
			$comment_karma->ordering = \Comment_karma\Models\Comment_karma::get_next_ordering_number();
		}

		$comment_karma->type = $_POST["type"];
		$comment_karma->comment_id = $_POST["comment_id"];
		$comment_karma->user_id = $_POST["user_id"];

		$comment_karma->save();
		$this->add_flash(array("message" => $flash_msg, "type" => "success", "Heading" => "Great!"));
		redirect_to($comment_karma->link_all());
	}

}
