<?php
namespace Gallery_attachment\Controllers;

class Restore extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$gallery_attachment = \Gallery_attachment\Models\Gallery_attachment::find($data["gallery_attachment_id"]);

		if($gallery_attachment){
			$gallery_attachment->deleted = 0;
			$gallery_attachment->save();
			$this->add_flash(array("message" => "Gallery_attachment has been restored!"));

			redirect_to($gallery_attachment->link_all());
		}
	}
}
