<?php
namespace Footwear\Controllers;

class All extends \Manage\Controllers\Manage{

	private $footwears;

	public $footwears_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Footwear";
		$this->link_new = \Footwear\Models\Footwear::link_new();

		$this->footwears = \Footwear\Models\Footwear::all(array("conditions" => "deleted = 0" , "order" => "ordering"));
		$archived = \Footwear\Models\Footwear::count(array("conditions" => "deleted = 1"));
		if($archived > 0){
			$this->archive_button = array("url" => \Footwear\Models\Footwear::link_archive(), "title" => "Archive");
		}
		
		$this->all_brands = \Brand\Models\Brand::all(array("conditions" => "deleted = 0"));
		
		foreach($this->footwears as $footwear){
			$brand = \Brand\Models\Brand::find($footwear->brand_id);
			
			$colors = '';
			$exploded = explode(',', $footwear->color_id);
			$count = 1;
			
			foreach ($exploded as $color) {
				if ($count == count($exploded)) {
					$colors .= \Color\Models\Color::get_color_name($color);
				} else if (($count + 1) == count($exploded)) {
					$colors .= \Color\Models\Color::get_color_name($color) . ' & ';
				} else {
					$colors .= \Color\Models\Color::get_color_name($color) . ', ';
				}
				
				$count++;
			}
			
			$this->has_data = true;
			$this->footwears_data[] = array(
				"id" => $footwear->id,
				"brand_id" => $brand->title,
				"model_name" => $footwear->model_name,
				"style_id" => $footwear->style_id,
				"tags" => $footwear->tags,
				"price" => $footwear->price,
				"rating" => $footwear->rating,
				"color_id" => $colors,
				"entry_datetime" => $footwear->entry_datetime,
				"link_edit" => $footwear->link_edit(),
				"link_delete" => $footwear->link_delete(),
				"ordering" => $footwear->ordering,
			);
		}
	}

	public function controller(){
		$this->set_view("Footwear\Views\All");
	}

}

