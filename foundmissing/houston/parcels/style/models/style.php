<?php
namespace Style\Models;

class Style extends \Core\Models\Base_Model{

	protected static $manage_prefix = "/manage/styles/";

	public function link_edit(){
		return self::$manage_prefix . "edit/" . $this->id;
	}

	public function link_delete(){
		return self::$manage_prefix . "delete/" . $this->id;
	}

	public function link_restore(){
		return self::$manage_prefix . "restore/" . $this->id;
	}

	public function link_new(){
		return self::$manage_prefix . "new";
	}

	public function link_all(){
		return self::$manage_prefix;
	}

	public function link_archive(){
		return self::$manage_prefix . "archive";
	}

	public function get_frontend_data(){
		return array(
			"id" => $this->id,
			"title" => $this->title,
			"category" => $this->category_id
		);
	}
	
	public function get_style_id_by_name($name) {
		$style = \Style\Models\Style::first(array('select' => 'id', 'conditions' => 'deleted = 0 AND title = "' . $name . '"'));
		
		return $style->id;
	}
	
	public function get_style_name_by_id($id) {
		$style = \Style\Models\Style::first(array('select' => 'title', 'conditions' => 'deleted = 0 AND id = "' . $id . '"'));
		
		return $style->title;
	}
}