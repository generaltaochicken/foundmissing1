<?php
namespace Style\Controllers;

class Delete extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$style = \Style\Models\Style::find($data["style_id"]);

		if($style){
			$style->deleted = 1;
			$style->save();
			$this->add_undo(array("message" => "Style has been deleted!", "action" => $style->link_restore()));

			redirect_to($style->link_all());
		}
	}
}
