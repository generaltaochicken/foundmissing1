<?php
/**
 * ---------------------------------------------------------------
 * Houston v1.1
 * ---------------------------------------------------------------
 * @author Tyler Savery | The Young Astronauts
 * @see http://www.houstonmvc.com
 * @license Open Source
 * ---------------------------------------------------------------
 * 
 */
namespace Core;

class Construct {

    static $db_conn  = false;
    static $database = null;
    static $parcels  = '';

    /**
     * Initiates the main definitions and environment settings.
     */
    public static function init() {
        
        session_start();

        date_default_timezone_set('America/New_York');
        
        define('ENVIRONMENT',  $_SERVER['SERVER_NAME']);

        define('HOUSTON_ROOT', DOCUMENT_ROOT.'houston'.DS);
        define('CONFIG_ROOT',   DOCUMENT_ROOT.'config'.DS);
        define('PUBLIC_ROOT',  DOCUMENT_ROOT.'public'.DS);

        define('LIBRARY_ROOT', HOUSTON_ROOT.'libraries'.DS);
        
        define('PARCEL_ROOT',  HOUSTON_ROOT.'parcels'.DS);
        define('CORE_ROOT',    PARCEL_ROOT.'core'.DS);
		
        define('UPLOAD_DIRECTORY', PUBLIC_ROOT . 'uploads'. DS);
        define('IMAGE_UPLOAD_DIRECTORY', UPLOAD_DIRECTORY . 'images'. DS);
        define('VIDEO_UPLOAD_DIRECTORY', UPLOAD_DIRECTORY . 'images'. DS);

        
        define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

        $config_file = json_decode(file_get_contents(CONFIG_ROOT.ENVIRONMENT.'.config.json'), true);
        $app_file = json_decode(file_get_contents(CONFIG_ROOT.'app.json'), true);
        
        static::$parcels = join(',', $app_file['parcels']);
        static::$database = $config_file['database'];
        
        foreach ($app_file['settings'] as $constant_name => $value) {
            if(!defined($constant_name)){
                define($constant_name, $value);
            }
        }
        
        foreach ($config_file['settings'] as $constant_name => $value) {
            if(!defined($constant_name)){
                define($constant_name, $value);
            }
        }
        
        foreach ($config_file['database'] as $constant_name => $value) {
            if(!defined($constant_name)){
                define($constant_name, $value);
            } 
        }
        
        foreach ($app_file['libraries'] as $value) {
            require_once(LIBRARY_ROOT.$value);
        }
        
        // Set debug mode details
        if (! DEBUG) {
            error_reporting(0);
            ini_set('display_errors', 'Off');
        }


        
        require_once(LIBRARY_ROOT.'PHPActiveRecord1.0/ActiveRecord.php');
        
        $conns = array(
            'main' => 'mysql://' . DB_USER . ':' . DB_PASS . '@' . DB_HOST . '/' . DB_NAME
        );
        
       \ActiveRecord\Config::initialize(function($c) use ($conns) {
                $c->set_connections($conns);
                $c->set_default_connection('main');                
        });  
        
    }

    /**
     * Gets the list of parcels
     */
    public static function get_parcels() {
        return explode(',', static::$parcels);
    }

    /**
     * Initializes the core database for the entire framework.
     */
    public static function init_database() {
        if (! static::$db_conn) {
            try {
                static::$db_conn = new \PDO(
                    DB_TYPE.
                        ':host='.DB_HOST.
                        ';dbname='.DB_NAME,
                    DB_USER,
                    DB_PASS,
                    array(
                        \PDO::ATTR_PERSISTENT => true, 
                        \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION
                    )
                );
            } catch (\PDOException $e) {
                throw new \Exception($e);
                return false;
            }
        }
        return true;
    }
}

/**
 * ---------------------------------------------------------------
 * @author Tyler Savery | The Young Astronauts
 * @see http://www.houstonmvc.com
 * @license Open Source
 * ---------------------------------------------------------------
 * 
 * Autoloads many of the dependent classes within Houston.
 *
 * @parcel Loader
 */
class Loader {
    
    /**
     * Initiates the autoload register function.
     */
    static function init() {
        set_include_path(get_include_path().PATH_SEPARATOR.dirname(dirname(__FILE__)));
        spl_autoload_register('Core\Loader::loadClass', true);
    }
    
    /**
     * Actually performs the autoloader.
     *
     * @param string $class_name Defines the name of the class that is to be required.
     * @return boolean If method success, will return true.
     */
    static function loadClass($class_name) {
        
        $class_array = explode('\\', strtolower($class_name));
             
        switch(sizeof($class_array)) {
            // App instance of a parcel
            case 4:
                list($app, $parcel, $type, $name) = $class_array;
                $class_file = APP_ROOT.'parcels'.DS.$parcel.DS.$type.DS.$name.'.php';
                break;
            // Standard Houston parcel references
            case 3:
                list($parcel, $type, $name) = $class_array;
                $class_file = PARCEL_ROOT.$parcel.DS.$type.DS.$name.'.php';
                break;
        }
        
        if (! empty($class_file) && is_file($class_file)) {
            require_once($class_file);
            if (! class_exists($class_name)) {
                throw new \Exception('The class <b class="highlight">'.$class_name.'</b> ('.$class_file.') was not defined correctly.');
            }
        } else {
            throw new \Exception('The class <b class="highlight">'.$class_name.'</b> ('.$class_file.') does not exist.');
        }
        return true;
        
    }
    
}

/**
 * ---------------------------------------------------------------
 * @author Tyler Savery | The Young Astronauts
 * @see http://www.houstonmvc.com
 * @license Open Source
 * ---------------------------------------------------------------
 * 
 * A script that handles resolving the paths of Houston.
 *
 * @parcel Router
 */

class Router {

    static public $request_uri;
    static public $routes;
    static public $controller;
    static public $params;
    static public $route_found = false;
    static public $target;
    static public $url;
    
    /**
     * Resolves the server URI request.
     */
    static public function resolve() {
        
        $request = $_SERVER['REQUEST_URI'];
        $pos = strpos($request, '?');
        if ($pos) {
            $request = substr($request, 0, $pos);
        }
        static::$request_uri = $request;
        static::$routes = array();
        
        foreach (explode(',', \Core\Construct::$parcels) as $parcel) {
            if (file_exists(CONFIG_ROOT.'app.json')) {
                if(static::parse_app(CONFIG_ROOT.'app.json')){
                    break;
                }
                
            }
            /*
            if (file_exists(APP_ROOT.'parcels'.DS.$parcel.DS.$parcel.'.json')) {
                if (static::parse_parcel($parcel, APP_ROOT.'parcels'.DS.$parcel.DS.$parcel.'.json')) {
                    break;
                }
            }
            */
            if (file_exists(PARCEL_ROOT.$parcel.DS.$parcel.'.json')) {
                if (static::parse_parcel($parcel, PARCEL_ROOT.$parcel.DS.$parcel.'.json')) {
                    break;
                }
            }
        }

        // Instantiate the controller and call render
        $controller = new static::$controller(static::$request_uri, static::$params);
        $controller->controller();
        $controller->render_view();
    }

    /**
     * Actually performs the autoloader.
     *
     * @param string Parcel name to search
     * @return boolean JSON file to read
     */
    static public function parse_parcel($parcel, $json_file) {
        $json = json_decode(file_get_contents($json_file));
        
        
        // Insert route to detect asset
        $json->routes->{'/:type/'.$parcel.'/:asset'} = '\Core\Controllers\Asset_Controller';
        
        foreach ($json->routes as $key => $value) {
            $routes[$key] = $value;
            if (static::map_routes($routes)) {
                return true;
            }
        }
    }
    
    static public function parse_app($app_routes){
        $json = json_decode(file_get_contents($app_routes));
        
        foreach ($json->routes as $key => $value) {
            $routes[$key] = $value;
            if (static::map_routes($routes)) {
                return true;
            }
        }
        
    }

    /**
     * Maps the routes to their exact locations.
     */
    static public function map_routes($routes) {
        foreach ($routes as $route => $controller) {
            static::check_route($route, static::$request_uri, $controller);
            if (static::$route_found) {
                static::$route_found = true;
                $params = static::$params;
                static::$controller = static::$target;

                unset($params['controller']);
                static::$params = array_merge($params, $_GET);
                return true;
            }
        }
        if (empty(static::$controller)) {
            static::$controller ='Core\\Controllers\\Static_404_Controller';
        }
    }

    /**
     * Checks the individual route.
     *
     * @param string URL route to compare against
     * @param string Request URI values
     * @param string Target of the controller to send the request to
     */
    static function check_route($url, $request_uri, $target) {

        static::$url = $url;
        if (substr($url, 0, 1) != '/') {
            $url = '/'.$url;
        }
        static::$params = array();
        static::$target = $target;
        $p_names = array();
        $p_values = array();

        preg_match_all('@:([\w]+)@', $url, $p_names, PREG_PATTERN_ORDER);
        $p_names = $p_names[0];

        $url_regex = preg_replace_callback(
            '@:[\w]+@',
            array(@self, 'regex_url'),
            $url
        );
        $url_regex .= '/?';

        if (preg_match('@^'.$url_regex.'$@', $request_uri, $p_values)) {
            array_shift($p_values);
            foreach($p_names as $index => $value) {
                static::$params[substr($value,1)] = urldecode($p_values[$index]);
            }
            static::$route_found = true;
        }
        unset($p_names); unset($p_values);
    }

    /**
     * The regex parser callback, used by check_route()
     *
     * @param string Strips any dynamic matche keys
     */
    function regex_url($matches) {
        $key = str_replace(':', '', $matches[0]);
        return '([a-zA-Z.0-9_\+\-%]+)';
    }

}