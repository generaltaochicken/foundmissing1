<?php
namespace Winner\Models;

class Winner extends \Core\Models\Base_Model{

	protected static $manage_prefix = "/manage/winners/";

	public function link_edit(){
		return self::$manage_prefix . "edit/" . $this->id;
	}

	public function link_delete(){
		return self::$manage_prefix . "delete/" . $this->id;
	}

	public function link_restore(){
		return self::$manage_prefix . "restore/" . $this->id;
	}

	public function link_new(){
		return self::$manage_prefix . "new";
	}

	public function link_all(){
		return self::$manage_prefix;
	}

	public function link_archive(){
		return self::$manage_prefix . "archive";
	}

	public function get_frontend_data(){
		return array(
			"id" => $this->id,
			"user_id" => $this->user_id,
			"entry_datetime" => $this->entry_datetime,
			"event" => $this->event,
			"prize" => $this->prize,
		);
	}
}