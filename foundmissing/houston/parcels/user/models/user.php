<?php
namespace User\Models;

class User extends \ActiveRecord\Model{

	protected static $manage_prefix = "/manage/users/";

	public $_errors = array();
	public $login_session_name = 'logged_in';
	public $password_regex = "^.*(?=.{8,})(?=.*[a-zA-Z])[a-zA-Z0-9]+$^";
	public $password_rule = "Must contain at least one (1) number, and must be at least eight (8) characters.";

	public $default_image_url;

	public function data_with_key($key){
		$data = json_decode($this->data);
		return $data->$key;
	}

	public function set_data_with_key($key, $value){
		$data = json_decode($this->data);
		$data->$key = $value;
		$data = json_encode($data);
		$this->data = $data;
		return $data;
	}

	public function link_edit(){
		return self::$manage_prefix . "edit/" . $this->id;
	}

	public function link_delete(){
		return self::$manage_prefix . "delete/" . $this->id;
	}

	public function link_restore(){
		return self::$manage_prefix . "restore/" . $this->id;
	}

	public function link_new(){
		return self::$manage_prefix . "new";
	}

	public function link_all(){
		return self::$manage_prefix;
	}

	public function link_archive(){
		return self::$manage_prefix . "archive";
	}


	//core parts of user
	
	public function get_logged_in_user(){
		
		$user = $this->find($_SESSION[$this->login_session_name]);
		return $user;
	}
	
	public function login(){
		$this->password = $this->hash_password($this->password);
		
		$options = array('conditions' => array("email = '" . $this->email . "' AND password = '" . $this->password . "'"));
		$user = \User\Models\User::find($options);
		        
		if(sizeof($user) < 1) {
			
			unset($_SESSION[$this->login_session_name]);
			$this->_errors[] = "Incorrect E-Mail/Password. Try Again.";
			return false;
		} else {
			//set the session and the user class id.
			$_SESSION[$this->login_session_name] = $user->id;
			
			return $user;
		}
	}
	
	public function logout(){
		unset($_SESSION[$this->login_session_name]);
	}
	
	public function update_logintime() {
		$this->update_datetime = time();
		$this->save();
		
		return;
	}
	
	protected function validate_registration(){
		
		// Check if user already exists
		if($this->user_exists()){
			$this->_errors[] = 'User Already Exists';
			return false;
		}
		//Validate Email
		/*
		if(!$this->valid_email()){
			$this->_errors[] = 'Invalid Email';
			return false;
		}
		*/
		
		//Validate Password
		if(!$this->valid_password()){
			$this->_errors[] = 'Invalid Password. ' . $this->password_rule;
			return false;
		}
		//Validate Name
		if(!$this->valid_name()){
			$this->_errors[] = 'Invalid First/Last name';
			return false;
		}
		
		return true;
		
	}
	
	public function register(){
		if($this->validate_registration()){
			$this->password = $this->hash_password($this->password);
            $this->entry_datetime = time();
			$this->save();
			
			$_SESSION[$this->login_session_name] = $this->id;
                return true;
			}
            
		return false;
		
	}
	
	protected function user_exists(){
		$options = array('conditions' => array("username = '" . $this->username . "'"));
		if(sizeof(\User\Models\User::find($options)) > 0){
			return true;
		}
		return false;
	}
	
	protected function valid_email(){
		if(!filter_var($this->email, FILTER_VALIDATE_EMAIL)){
			return false;
		}
		return true;
	}
	
	protected function valid_password(){
		if(!filter_var($this->password, FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => $this->password_regex)))){
			return false;
		}
		return true;		
	}
	
	protected function valid_name(){
		if(strlen($this->firstname) < 1 || strlen($this->lastname) < 1){
			return false;
		}
		return true;
	}
	
	protected function hash_password($password){
		$new_password = md5(DB_SALT . $password);
		return $new_password;
	}


	public function fullname(){
		return ucwords($this->firstname . " " . $this->lastname);
	}
	
	public function get_image_reply() {
		return $this->image;
	}
	
	public function get_uusername() {
		return $this->username;
	}

	public function shorthand_name(){
		return ucwords($this->firstname . " " . $this->lastname[0] . ".");
	}
	
	public function get_user_info_by_id($id) {
		$user = $this->find($id);
		$userinfo = array('fullname' => $user->firstname . ' ' . $user->lastname, 'username' => $user->username, 'image' => $user->image);
		
		return $userinfo;
	}
	
	public function get_user_fullname_by_id($id) {
		$user = \User\Models\User::find($id);
		return $user->firstname . ' ' . $user->lastname;
	}
	
	public function get_user_email_by_id($id) {
		$user = \User\Models\User::find($id);
		return $user->email;
	}
	
	public function get_user_code_by_id($id) {
		$user = \User\Models\User::find($id);
		return $user->code;
	}


}