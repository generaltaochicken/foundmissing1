<?php

namespace Account\Controllers;

class Login extends \Account\Controllers\Account {

	public $form;
	public $error;

	public function __construct($uri, $data){
        parent::__construct($uri, $data);
		$this->add_asset('css', 'fm_login.css', true);
		$this->add_asset('js', 'validation.js', true);

        if(isset($_POST['user_login']) || isset($_POST['api'])){
            $this->submit_form();
        } else if (isset($_POST['user_signup'])) {
			$_SESSION['temp_email'] = $_POST['email'];
			$_SESSION['temp_password'] = $_POST['password'];
			redirect_to("/account/register");
		}
    }

    public function controller(){
    	$this->set_view('Account\Views\Login');
	}

	private function submit_form(){
		// CLEAN DATA
		require_once(PARCEL_ROOT.'libraries/HTMLPurifier/HTMLPurifier.auto.php');
		$config 				= \HTMLPurifier_Config::createDefault();
		$purifier 				= new \HTMLPurifier($config);
		
		$user = new \User\Models\User();
		$user->email = $purifier->purify($_POST['email']);
		$user->password = $purifier->purify($_POST['password']);

		$login = $user->login();

		if($login){
			$login->update_logintime();
		    $this->add_flash(array('message' => 'Welcome Back ' . $login->firstname . "!", 'type' => 'success'));
		    redirect_to("/account/me");
		} else {
		    $this->error = 'Incorrect Login Details';
		}

	}

}