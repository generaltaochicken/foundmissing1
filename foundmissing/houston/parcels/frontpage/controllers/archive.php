<?php
namespace Frontpage\Controllers;

class Archive extends \Manage\Controllers\Manage{

	private $frontpages;

	public $frontpages_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Deleted Frontpages";

		$this->frontpages = \Frontpage\Models\Frontpage::all(array("conditions" => "deleted = 1"));
		$this->archive_button = array("url" => \Frontpage\Models\Frontpage::link_all(), "title" => "Back");
		foreach($this->frontpages as $frontpage){
			$this->has_data = true;
			$this->frontpages_data[] = array(
				"id" => $frontpage->id,
				"slider" => $frontpage->slider,
				"position" => $frontpage->position,
				"type" => $frontpage->type,
				"parent_id" => $frontpage->parent_id,
				"entry_datetime" => $frontpage->entry_datetime,
				"link_restore" => $frontpage->link_restore(),
			);
		}

	}

	public function controller(){
		$this->set_view("Frontpage\Views\All");
	}

}

