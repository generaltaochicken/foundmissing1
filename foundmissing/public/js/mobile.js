// Mobile.js

jQuery(document).ready(function() {
	var url = "/ajaxmobile/";
	var homeWidth = $("body").outerWidth(true);
	
	jQuery("#wrapper_home .section").click(function() {
		var elem = $(this);
		var task = elem.data("target");
		var data = { task: task };
		var icon;
		
		if (task == "Articles") {
			icon = "/images/icons/gray/48px/48_article.png";
		} else if (task == "Interviews") {
			icon = "/images/icons/gray/48px/48_interview.png";
		} else if (task == "Reviews") {
			icon = "/images/icons/gray/48px/48_review.png";
		} else if (task == "Gallery") {
			icon = "/images/icons/gray/48px/48_gallery.png";
		} else {
			icon = '';
		}
		
		elem.children(".section_background").children(".bg_overlay").css("background-color", "#EB1C39");
		
		$('body').append('<header id="header"><div class="header"><div id="close"><i class="fa fa-chevron-left darkgrey"></i></div><img class="icon_head" src="' + icon + '" /></div></header>');
		
		$("#main").css("width", homeWidth);
		
		$("#main").transition({ x: -homeWidth, queue: false }, function() {
			$("#wrapper_home").hide();
		});
		
		displayLoader("#main");
		
		$.post(url, data, function(response) {
			$(".loading_fm").fadeOut(function() {
				$("body").scrollTop("0");
				$("#main").css({
					"position" : "absolute",
					"margin-top" : "50px"
				});
				
				$("#main").html(response);
				$("#header").transition({ x: -homeWidth, queue: false });
				$(".header").show();
				$("#main .loading_fm").remove();
			});
			
			elem.children(".section_background").children(".bg_overlay").css("background-color", "#000");
		});
	});
	
	var up = false;
	var mywindow = $(window);
	var mypos = mywindow.scrollTop();
	var newscroll;
	mywindow.scroll(function () {
		newscroll = mywindow.scrollTop();
		
		if (newscroll > mypos && !up) {
			$("#header").stop().transition({y: -50});
			$("#header2").stop().transition({y: -50});
			up = true;
		} else if (newscroll < mypos && up) {
			$("#header").stop().transition({y: 0});
			$("#header2").stop().transition({y: 0});
			up = false;
		}
		
		mypos = newscroll;
	});
	
	
	/*$("body").on("click", ".image", function() {
		$("#header2").stop().transition({y: -50});
		$("#main_article").css("margin-top", "0");
		$(".full_image").css("display", "block");
		
		var image = $(this).children("img");
		var heightOfImage = image.height();
		var image_link = image.attr("src");
		
		$(".image_container").append("<img src='" + image_link + "' />");
	});*/
});

function displayLoader(screen) {
	$(screen).append("<div class='loading_fm'><img src='/images/fm-redtrans-128.png' /></div>");
	
	var i;
	var count = 0;
	var deg = 180;
	
	for (i = 0; i < 500; i++) {
		if (count <= 3) {
			$(".loading_fm img").transition({ rotate: deg + "deg" });
			deg += 90;
			count++;
		} else {
			$(".loading_fm img").transition({ rotate: deg + "deg" });
			count = 0;
			deg = 180;
		}
	}
}