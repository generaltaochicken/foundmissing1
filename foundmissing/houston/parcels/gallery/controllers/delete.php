<?php
namespace Gallery\Controllers;

class Delete extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$gallery = \Gallery\Models\Gallery::find($data["gallery_id"]);

		if($gallery){
			$gallery->deleted = 1;
			$gallery->save();
			$this->add_undo(array("message" => "Gallery has been deleted!", "action" => $gallery->link_restore()));

			redirect_to($gallery->link_all());
		}
	}
}
