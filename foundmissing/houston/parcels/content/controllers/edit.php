<?php
namespace Content\Controllers;

class Edit extends \Manage\Controllers\Manage{

	private $content;

	public $page_title;
	public $link_back;
	public $form;
	public $existing;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		if(isset($_POST["submit"])){
			$this->submit_form();
		}

		if(isset($data["content_id"])){
			$this->content = \Content\Models\Content::find($data["content_id"]);
			$this->page_title = "Editing Content";
		} else {
			$this->content = new \Content\Models\Content;
			$this->page_title = "New Content";
		}

		$this->link_back = $this->content->link_all();

		$form = new \Form\Models\Form("edit", "POST", "", "well");

		$item = new \Form\Models\FormItemHidden(array("id" => "id", "title" => "#", "value" => $this->content->id));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "k", "title" => "Key", "value" => $this->content->k));
		$form->add_item($item);

		$item = new \Form\Models\FormItemTextarea(array("id" => "v", "title" => "Value", "value" => $this->content->v));
		$form->add_item($item);

		$item = new \Form\Models\FormItemSubmit(array("value" => "Save", "id" => "submit", "class" => "btn btn-primary", "cancel_url" => $this->content->link_all()));
		$form->add_item($item);

		$this->form = $form->render();

	}

	public function controller(){
		$this->set_view("Content\Views\Edit");
	}

	private function submit_form(){
		if($_POST["id"] > 0){
			$content = \Content\Models\Content::find($_POST["id"]);
			$flash_msg = "Content has been updated successfully.";
		} else {
			$content = new \Content\Models\Content;
			$flash_msg = "Content has been created successfully.";
		}

		$content->k = $_POST["k"];
		$content->v = $_POST["v"];

		$content->save();
		$this->add_flash(array("message" => $flash_msg, "type" => "success", "Heading" => "Great!"));
		redirect_to($content->link_all());
	}

}
