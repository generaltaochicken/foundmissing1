<?php

namespace Manage\Controllers;

class SaveParcel extends \Core\Controllers\Base_Controller{

	public function __construct($uri, $data){
        parent::__construct($uri, $data);

        $name = $_POST['name'];
        $plural = $_POST['plural'];
        $json_string = $_POST['json'];

        $directory = PARCEL_ROOT . $name . DS;

        if(!is_dir($directory)){
        	mkdir($directory);
        }

        $file = $directory . $name . '.json';

        $fh = fopen($file, 'w');
        fwrite($fh, indent_json($json_string));
        fclose($fh);

        //add as installed app
        $config_file = CONFIG_ROOT.'app.json';
        $config_array = json_decode(file_get_contents($config_file), true);
        $config_array['parcels'][] = $name;

        $config_json = json_encode($config_array);

        $fh = fopen($config_file, 'w');
        fwrite($fh, indent_json($config_json));
        fclose($fh);

        die('/manage/generate/' . $name . '/' . $plural);

    }


    public function controller(){

    	return;

    }

}
