<?php
namespace User\Controllers;

class Edit extends \Manage\Controllers\Manage{

	private $user;

	public $page_title;
	public $link_back;
	public $form;
	public $existing;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		if(isset($_POST["submit"])){
			$this->submit_form();
		}

		if(isset($data["user_id"])){
			$this->user = \User\Models\User::find($data["user_id"]);
			$this->page_title = "Editing User";
		} else {
			$this->user = new \User\Models\User;
			$this->page_title = "New User";
		}

		$this->link_back = $this->user->link_all();

		$form = new \Form\Models\Form("edit", "POST", "", "well");

		$item = new \Form\Models\FormItemHidden(array("id" => "id", "title" => "#", "value" => $this->user->id));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "username", "title" => "Username", "value" => $this->user->username));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "firstname", "title" => "First Name", "value" => $this->user->firstname));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "lastname", "title" => "Last Name", "value" => $this->user->lastname));
		$form->add_item($item);

		$item = new \Form\Models\FormItemTextarea(array("id" => "data", "title" => "Data", "value" => $this->user->data));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "admin", "title" => "Admin?", "value" => $this->user->admin));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "password", "title" => "Change Password", "value" => ""));
		$form->add_item($item);

		$item = new \Form\Models\FormItemSubmit(array("value" => "Save", "id" => "submit", "class" => "btn btn-primary", "cancel_url" => $this->user->link_all()));
		$form->add_item($item);

		$this->form = $form->render();

	}

	public function controller(){
		$this->set_view("User\Views\Edit");
	}

	private function submit_form(){
		if($_POST["id"] > 0){
			$user = \User\Models\User::find($_POST["id"]);
			$flash_msg = "User has been updated successfully.";
		} else {
			$user = new \User\Models\User;
			$flash_msg = "User has been created successfully.";
		}

		$user->username = $_POST["username"];
		$user->firstname = $_POST["firstname"];
		$user->lastname = $_POST["lastname"];
		$user->data = $_POST["data"];
		$user->admin = $_POST["admin"];

		if($_POST['password'] != ''){
            $user->password = md5(DB_SALT . $_POST['password']);
        }

		$user->save();
		$this->add_flash(array("message" => $flash_msg, "type" => "success", "Heading" => "Great!"));
		redirect_to($user->link_all());
	}

}
