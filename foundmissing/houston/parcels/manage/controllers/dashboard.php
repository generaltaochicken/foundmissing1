<?php

namespace Manage\Controllers;

class Dashboard extends \Manage\Controllers\Manage {

    public function __construct($uri, $data){
        parent::__construct($uri, $data);
    }

    public function controller(){
    	$this->set_view("Manage\Views\Dashboard");
    }


}