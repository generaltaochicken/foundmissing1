<?php
namespace Api\Controllers;

class Items extends \Api\Controllers\Api{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->response = $this->get_items();
		$this->send_response();

	}

	protected function get_items(){

		//ordering
		if($this->request('get', 'order_by')){
			$order = $this->request('get', 'order_by');

			if($this->request('get', 'order_direction')){
				$order .= ' ' . $this->request('get', 'order_direction');
			}

		} else {
			$order = '';
		}

		//conditions
		$conditions = array('deleted' => 0);

		if($this->request('get', 'user_id')){
			$conditions['user_id'] = (int)$this->request('get', 'user_id');
		}

		if($this->request('get', 'category_id')){
			$conditions['category_id'] = (int)$this->request('get', 'category_id');
		}

		if($this->request('get', 'type')){
			$conditions['type'] = $this->request('get', 'type');
		}

		if($this->request('get', 'style')){
			$conditions['style_id'] = (int)$this->request('get', 'style');
		}

		if($this->request('get', 'designer')){
			$conditions['designer_id'] = (int)$this->request('get', 'designer');
		}

		if($this->request('get', 'color')){
			$conditions['colors'] = $this->request('get', 'color');
		}

		if($this->request('get', 'store')){
			$conditions['store_id'] = (int)$this->request('get', 'store');
		}

		if($this->request('get', 'price_min')){
			$price_min = (int)$this->request('get', 'price_min');
		}

		if($this->request('get', 'price_max')){
			$price_max = (int)$this->request('get', 'price_max');
		}

		if($this->request('get', 'new_release')){
			$conditions['new_release'] = $this->request('get', 'new_release');
		}


		$args = array('conditions' => $conditions, 'order' => $order);

		$items = \Item\Models\Item::all($args);
		$array = array();
		foreach($items as $item){

			if(isset($price_min) && $item->price < $price_min) {
				continue;
			}

			if(isset($price_max) && $item->price > $price_max) {
				continue;
			}

			$array[] = array(
				"id" => $item->id,
				"user_id" => $item->user_id,
				"category_id" => $item->category_id,
				"title" => $item->title,
				"type" => $item->type,
				"excerpt" => $item->excerpt,
				"body" => $item->body,
				"slug" => $item->slug,
				"skew" => $item->skew,
				"price" => $item->price,
				"rating" => $item->rating,
				"tags" => $item->tags,
				"brand_id" => $item->brand_id,
				"style_id" => $item->style_id,
				"designer_id" => $item->designer_id,
				"colors" => $item->colors,
				"year" => $item->year,
				"purchase_url" => $item->purchase_url,
				"video_url" => $item->video_url,
				"entry_datetime" => $item->entry_datetime,
				"update_datetime" => $item->update_datetime,
				"publish_datetime" => $item->publish_datetime,
				"unpublish_datetime" => $item->unpublish_datetime,
				"thumb_url" => $item->get_thumbnail_url()
			);
		}

		return $array;

	}




}