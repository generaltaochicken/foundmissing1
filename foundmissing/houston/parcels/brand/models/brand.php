<?php
namespace Brand\Models;

class Brand extends \Core\Models\Base_Model{

	protected static $manage_prefix = "/manage/brands/";

	public function link_edit(){
		return self::$manage_prefix . "edit/" . $this->id;
	}

	public function link_delete(){
		return self::$manage_prefix . "delete/" . $this->id;
	}

	public function link_restore(){
		return self::$manage_prefix . "restore/" . $this->id;
	}

	public function link_new(){
		return self::$manage_prefix . "new";
	}

	public function link_all(){
		return self::$manage_prefix;
	}

	public function link_archive(){

		return self::$manage_prefix . "archive";
	}

	public function get_frontend_data(){
		return array(
			"id" => $this->id,
			"title" => $this->title,
			"description" => $this->description,
		);
	}
	
	public function get_brand_id_by_name($name) {
		$brand = \Brand\Models\Brand::first(array('select' => 'id', 'conditions' => 'deleted = 0 AND title = "' . $name . '"'));
		
		return $brand->id;
	}
	
	public function get_brand_name_by_id($id) {
		$brand = \Brand\Models\Brand::first(array('select' => 'title', 'conditions' => 'deleted = 0 AND id = "' . $id . '"'));
		
		return $brand->title;
	}
	
	public function get_brand_description($id) {
		$brand = \Brand\Models\Brand::first(array('select' => 'description', 'conditions' => 'deleted = 0 AND id = "' . $id . '"'));
		
		return $brand->description;
	}
}