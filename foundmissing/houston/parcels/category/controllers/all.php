<?php
namespace Category\Controllers;

class All extends \Manage\Controllers\Manage{

	private $categories;

	public $categories_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Categories";
		$this->link_new = \Category\Models\Category::link_new();

		$this->categories = \Category\Models\Category::all(array("conditions" => "deleted = 0" , "order" => "ordering"));
		$archived = \Category\Models\Category::count(array("conditions" => "deleted = 1"));
		if($archived > 0){
			$this->archive_button = array("url" => \Category\Models\Category::link_archive(), "title" => "Archive");
		}

		foreach($this->categories as $Category){
			$this->has_data = true;
			$this->categories_data[] = array(
				"id" => $Category->id,
				"title" => $Category->title,
				"entry_datetime" => $Category->entry_datetime,
				"link_edit" => $Category->link_edit(),
				"link_delete" => $Category->link_delete(),
				"ordering" => $Category->ordering,
			);
		}

	}

	public function controller(){
		$this->set_view("Category\Views\All");
	}

}

