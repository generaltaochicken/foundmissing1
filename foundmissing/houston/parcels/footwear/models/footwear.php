<?php
namespace Footwear\Models;

class Footwear extends \Core\Models\Base_Model{

	protected static $manage_prefix = "/manage/footwear/";

	public function link_edit(){
		return self::$manage_prefix . "edit/" . $this->id;
	}

	public function link_delete(){
		return self::$manage_prefix . "delete/" . $this->id;
	}

	public function link_restore(){
		return self::$manage_prefix . "restore/" . $this->id;
	}

	public function link_new(){
		return self::$manage_prefix . "new";
	}

	public function link_all(){
		return self::$manage_prefix;
	}

	public function link_archive(){
		return self::$manage_prefix . "archive";
	}

	public function get_frontend_data(){
		return array(
			"id" => $this->id,
			"brand_id" => $this->brand_id,
			"model_name" => $this->model_name,
			"style_id" => $this->style_id,
			"tags" => $this->tags,
			"price" => $this->price,
			"release_date" => $this->release_date,
			"rating" => $this->rating,
			"color_id" => $this->color_id,
			"entry_datetime" => $this->entry_datetime,
			"description" => $this->description,
			"external" => $this->external
		);
	}
}