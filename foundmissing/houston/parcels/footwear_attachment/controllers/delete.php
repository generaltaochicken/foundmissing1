<?php
namespace Footwear_attachment\Controllers;

class Delete extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$footwear_attachment = \Footwear_attachment\Models\Footwear_attachment::find($data["footwear_attachment_id"]);

		if($footwear_attachment){
			$footwear_attachment->deleted = 1;
			$footwear_attachment->save();
			$this->add_undo(array("message" => "Footwear_attachment has been deleted!", "action" => $footwear_attachment->link_restore()));

			redirect_to($footwear_attachment->link_all());
		}
	}
}
