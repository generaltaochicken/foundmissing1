<?php
namespace Comment\Controllers;

class Delete extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$comment = \Comment\Models\Comment::find($data["comment_id"]);

		if($comment){
			$comment->deleted = 1;
			$comment->save();
			$this->add_undo(array("message" => "Comment has been deleted!", "action" => $comment->link_restore()));

			redirect_to($comment->link_all());
		}
	}
}
