<?php

namespace Frontend\Controllers;

class Reviews extends \Frontend\Controllers\Frontend {

	public $posts = array();
    public $categories = array();
    public $reviews = array();
	public $initial_review = array();
	
	protected $attachments = array();

	public function __construct($uri, $data){
        parent::__construct($uri, $data);
		$this->add_asset('css', 'review.css', true);
		
		$this->title = "Reviews | FM";
		
        if(isset($data['slug'])){
            $category = \Category\Models\Category::find(array('conditions' => 'slug = "' . $data['slug'] . '"'));
            if(!$category){
                redirect_to('/blog');
            }
            $this->category_id = $category->id;
        }

    }

    public function controller(){
		$reviews = \Review\Models\Review::all(array('conditions' => 'deleted = 0 AND publish_date > 1', 'order' => 'publish_date DESC'));
		
		foreach ($reviews as $review) {
			$comment_count = \Comment\Models\Comment::getCommentCount($review->id, 3);
			if ($comment_count > 1) {
				$count = $comment_count . ' comments';
			} else {
				$count = $comment_count . ' comment';
			}
			
			$footwear = \Footwear\Models\Footwear::find($review->shoe_id);
			$brand = \Brand\Models\Brand::find($footwear->brand_id);
			//$user = \User\Models\User::find($review->user_id);
			
			$style_id = explode(",", $footwear->style_id);
			$categories = array();
			
			foreach ($style_id as $styles) {
				$style = \Style\Models\Style::find($styles);
				$categories[] = $style->title;
			}
			
			$categories = json_encode($categories);

			$this->reviews[] = array(
				'title' => $review->title,
				'slug' => $review->slug,
				'excerpt' => $review->excerpt,
				'body' => $review->body,
				'main_image' => REVIEW_IMAGES . $review->cover_image,
				'brand' => $brand->title,
				'name' => $footwear->model_name,
				'tags' => $footwear->tags,
				'release_date' => time_to_friendly_date($footwear->release_date, 'Y.m.d'),
				'author_name' => $review->author,
				'styles' => $categories,
				'rating' => $footwear->rating,
				'count' => $count
			);
		}
		
		$styles = json_decode($this->reviews[0]['styles']);
		
		if ($styles) {
			$this->initial_review = array(
				'slug' => $this->reviews[0]['slug'],
				'excerpt' => $this->reviews[0]['excerpt'],
				'brand' => $this->reviews[0]['brand'],
				'name' => $this->reviews[0]['name'],
				'tags' => $this->reviews[0]['tags'],
				'release_date' => $this->reviews[0]['release_date'],
				'author_name' => 'by ' . $this->reviews[0]['author_name'],
				'styles' => implode(", ", $styles),
				'rating' => $this->reviews[0]['rating'],
				'count' => $this->reviews[0]['count']
			);
		}

		$this->set_view('Frontend\Views\Reviews');
	}

}