<?php
namespace Comment\Controllers;

class Restore extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$comment = \Comment\Models\Comment::find($data["comment_id"]);

		if($comment){
			$comment->deleted = 0;
			$comment->save();
			$this->add_flash(array("message" => "Comment has been restored!"));

			redirect_to($comment->link_all());
		}
	}
}
