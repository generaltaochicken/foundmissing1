<?php
namespace Category\Controllers;

class Edit extends \Manage\Controllers\Manage{

	private $Category;

	public $page_title;
	public $link_back;
	public $form;
	public $existing;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		if(isset($_POST["submit"])){
			$this->submit_form();
		}

		if(isset($data["Category_id"])){
			$this->Category = \Category\Models\Category::find($data["Category_id"]);
			$this->page_title = "Editing Category";
		} else {
			$this->Category = new \Category\Models\Category;
			$this->page_title = "New Category";
		}

		$this->link_back = $this->Category->link_all();

		$form = new \Form\Models\Form("edit", "POST", "", "well");

		$item = new \Form\Models\FormItemHidden(array("id" => "id", "title" => "Id", "value" => $this->Category->id));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "title", "title" => "Title", "value" => $this->Category->title));
		$form->add_item($item);

		$item = new \Form\Models\FormItemSubmit(array("value" => "Save", "id" => "submit", "class" => "btn btn-primary", "cancel_url" => $this->Category->link_all()));
		$form->add_item($item);

		$this->form = $form->render();

	}

	public function controller(){
		$this->set_view("Category\Views\Edit");
	}

	private function submit_form(){
		if($_POST["id"] > 0){
			$Category = \Category\Models\Category::find($_POST["id"]);
			$flash_msg = "Category has been updated successfully.";
		} else {
			$Category = new \Category\Models\Category;
			$flash_msg = "Category has been created successfully.";
			$Category->ordering = \Category\Models\Category::get_next_ordering_number();
		}

		$Category->title = $_POST["title"];

		$Category->save();
		$this->add_flash(array("message" => $flash_msg, "type" => "success", "Heading" => "Great!"));
		redirect_to($Category->link_all());
	}

}
