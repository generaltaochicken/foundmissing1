<?php
namespace Page\Controllers;

class Archive extends \Manage\Controllers\Manage{

	private $pages;

	public $pages_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Deleted Pages";

		$this->pages = \Page\Models\Page::all(array("conditions" => "deleted = 1"));
		$this->archive_button = array("url" => \Page\Models\Page::link_all(), "title" => "Back");
		foreach($this->pages as $page){
			$this->has_data = true;
			$this->pages_data[] = array(
				"id" => $page->id,
				"title" => $page->title,
				"slug" => $page->slug,
				"body" => $page->body,
				"entry_datetime" => $page->entry_datetime,
				"link_restore" => $page->link_restore(),
			);
		}

	}

	public function controller(){
		$this->set_view("Page\Views\All");
	}

}

