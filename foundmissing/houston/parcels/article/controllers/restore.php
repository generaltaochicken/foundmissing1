<?php
namespace Article\Controllers;

class Restore extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$article = \Article\Models\Article::find($data["article_id"]);

		if($article){
			$article->deleted = 0;
			$article->save();
			$this->add_flash(array("message" => "Article has been restored!"));

			redirect_to($article->link_all());
		}
	}
}
